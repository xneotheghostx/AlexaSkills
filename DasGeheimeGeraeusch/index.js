﻿var Alexa = require('alexa-sdk');
var dynamo = require('./dynamoDB');
var constants = require('./constants');
var moment = require('./moment');
var https = require('https');

var sounds = require('./sounds.json');

exports.handler = function (event, context, callback) {

    var alexa = Alexa.handler(event, context);
    alexa.appId = constants.appId;
    alexa.registerHandlers(handlers);
    alexa.execute();
};

var handlers = {

    'NewSession': function () {

        console.log("Session init");

        var self = this;

        dynamo.getCurrentSound(self.event.session, function (data) {

            console.log("CurrentSound: " + data.message);

            if (data.item) {
                self.attributes['currentSound'] = data.item['tries'];
                self.attributes['csUnix'] = data.item['unix'];

                dynamo.getUserState(self.event.session, function (data) {

                    console.log("User: " + data.message);

                    if (data.item) {
                        Object.assign(self.event.session.attributes, data.item);

                        if (self.attributes["userName"] === "InitalName") {
                            self.attributes['setName'] = true;
                            self.attributes['verifyName'] = false;
                            self.emit('NoUserName');
                        } else {
                            var eightDateTime = moment(moment().format("DD.MM.YYYY 00:00"), "DD.MM.YYYY HH:mm").add(7, "hours");
                            var currentDateTime = moment();
                            var parsedDateTime = moment(self.attributes['unixLast'], "X");
                            if (currentDateTime.diff(eightDateTime, 'seconds', true) >= 0 && parsedDateTime.diff(eightDateTime, 'seconds', true) <= 0) {
                                
                                if (self.attributes['unix'] > 0) {
                                    var speech = "Du hast das letzte Geräusch erraten, um zu erfahren, ob du zu den fünf schnellsten gehört hast, aktiviere die tägliche Zusammenfassung. ";
                                } else {
                                    var speech = "Jemand hat das letzte geheime Geräusch erraten. Hier kommt nochmal das Geräusch: <audio src='" + sounds[self.attributes['currentSound'] - 1]["sound"] + "' /> Die Lösung war, " + sounds[self.attributes['currentSound'] - 1]["answers"][0] + ". ";
                                }
                                self.attributes["tries"] = 0;
                                self.attributes["unix"] = 0;

                                self.emit('WelcomeBackIntent', speech);    
                            }
                            self.emit('WelcomeBackIntent');
                        }
                    } else {
                        self.emit('ResetIntent');
                    }
                });

            } else {
                self.emit('ResetIntent');
            }
        });
    },
    'LaunchRequest': function () {

        this.emit('NewSession');
    },
    'ResetIntent': function () {

        this.attributes["userType"] = "User";
        this.attributes["userName"] = "InitalName";
        this.attributes["unix"] = 0;
        this.attributes["unixLast"] = 0;
        this.attributes["tries"] = 0;

        this.attributes['setName'] = true;
        this.attributes['verifyName'] = false;
        this.emit('WelcomeIntent');
    },

    'WelcomeIntent': function () { // Erster Start

        var speech = "<say-as interpret-as='interjection'>Willkommen</say-as> beim Spiel das geheime Geräusch. "
            + "Ich freue mich das du mit mir Spielen möchtest. Ich spiele dir ein Geräusch vor und du hast dann drei versuche, das Geräusch zu erraten. "
            + "Wenn du es heute nicht schaffen solltest, kein Problem. <break time='150ms'/> Morgen bekommst du erneut drei versuche, um das Geräusch zu erraten. "
            + "Für Hilfe zum Spiel, sage Hilfe <break time='150ms'/> und wenn du einen Tipp brauchst, sage Tipp. "
            + "<break time='150ms' /> Und jetzt wünsche ich dir <say-as interpret-as='interjection'>viel glück</say-as> beim Spielen."
            + "<break time='100ms' /> Wenn du in meiner Highscore-Liste auftauchen möchtest, nenne mir einen Namen. ";
        var reprompt = "Wenn du nicht namentlich in der Liste auftauchen möchtest, "
            + "sage Anonym oder nenne jetzt einen Namen. ";
        this.emit(':ask', speech, reprompt);
    },
    'WelcomeBackIntent': function (speech) { // Rückkehrer
        if (typeof speech === 'undefined')
            var speech = "";

        if (this.attributes['tries'] < 3) {
            var speech = "Hallo " + this.attributes.userName + ", willkommen zurück zu meinem Spiel das geheime Geräusch. "
                + "Ich freue mich, das du wieder mit mir Spielen möchtest. "
                + "Für die Hilfe, sage Hilfe. Mit Tipp, bekommst du einen Tipp zum lösen. " + speech + " Und jetzt wünsche ich dir wieder viel Spaß beim raten. ";
            if (this.attributes['currentSound'] < sounds.length)
                this.emit('SoundIntent', speech);
            else
                this.emit('NoSoundIntent', speech);
        } else {
            var speech = "Hallo " + this.attributes.userName + ", willkommen zurück zu meinem Spiel das geheime Geräusch. "
            + "Ich freue mich, das du wieder mit mir Spielen möchtest. "
            + "Du hast deine 3 Versuche für heute aufgebraucht. Du kannst dir das Geräusch trotzdem anhören und dir schonmal ein paar Gedanken machen. ";
            var reprompt = "Sage Geräusch, um das Geräusch zu hören oder Stop um zu beenden. ";
            this.emit(':ask', speech + reprompt, reprompt);
        }
    },
    'NoUserName': function () { // Start aber noch kein Name gesetzt
        var speech = "Willkommen zurück zu meinem Spiel, das geheime Geräusch. "
            + "Du hast dir noch keinen Namen gegeben. Wenn du in meiner Highscore-Liste auftauchen möchtest, nenne mir einen Namen. ";
        var reprompt = "Wenn du nicht namentlich in der Liste auftauchen möchtest, "
            + "sage Anonym oder nenne jetzt einen Namen. ";
        this.emit(':ask', speech, reprompt);
    },


    'VerifyUserName': function () {
        
        if (this.attributes['setName'] && this.attributes['setName'] === true && this.attributes['verifyName'] === false) {

            if (blacklist.indexOf(this.event.request.intent.slots.firstName.value.toLowerCase()) == -1) {
                this.attributes['setName'] = false;
                this.attributes['verifyName'] = true;

                this.attributes.userName = this.event.request.intent.slots.firstName.value;

                var speech = "Dein Name lautet nun " + this.attributes.userName + " .<break time='100ms' /> Ist das richtig oder falsch? ";
                var reprompt = "Dein Name lautet nun " + this.attributes.userName + " .<break time='100ms' /> Ist das richtig oder falsch? ";
            }
            else {
                var speech = "Der genannte Name ist nicht erlaubt, bitte nenne einen anderen.";
                var reprompt = "Nenne mir bitte einen Namen.";
            }
            this.emit(':ask', speech, reprompt);
        }
        if (this.event.request.intent.slots.firstName) {
            this.emit('SoundGuessIntent', this.event.request.intent.slots.firstName.value);
        } else {
            this.emit('Error');
        }
        
    },
    'SetUserName': function () {
        if (this.attributes['setName'] === false) {
            var speech = "Danke " + this.attributes.userName + ", ";
            this.emit('SoundIntent', speech);
        }
        this.emit('Error');
    },
    'AbortUserName': function () {
        this.attributes['setName'] = true;
        this.attributes['verifyName'] = false;

        var speech = "OK, nenne mir einen Namen. ";
        var reprompt = "Nenne mir einen Namen. ";
        this.emit(':ask', speech, reprompt);
    },
    'AnonymIntent': function () {
        if (this.attributes['setName'] === true) {
            this.attributes.userName = "Anonym";

            var speech = "OK, ich respektiere deinen Wunsch. ";
            this.emit('SoundIntent', speech);
        }
        this.emit('Error');
    },

    'AMAZON.YesIntent': function () {
        if (this.attributes['verifyName'] === true) {
            this.attributes['verifyName'] = false;
            this.emit('SetUserName');
        }
        this.emit('Error');
    },
    'AMAZON.NoIntent': function () {
        if (this.attributes['verifyName'] === true) {
            this.attributes['verifyName'] = false;
            this.emit('AbortUserName');
        }
        this.emit('Error');
    },

    'SoundIntent': function (speech) { // Abspielen des eigentlichen Sounds
        if (typeof speech === 'undefined')
            var speech = "";
        speech += "Gleich spiele ich dir das Geräusch vor. Wenn du es nicht sofort verstanden hast oder es noch einmal hören möchtest, sage nochmal oder wiederholen.<break time='100ms'/> ";
        this.emit('RepeatSoundIntent', speech);
    },
    'NoSoundIntent': function (speech) {
        if (typeof speech === 'undefined')
            var speech = "";
        speech += "Heute gibt es leider kein Geräusch. Aber keine Sorge. Wir sorgen in Kürze für Nachschub. Versuche es einfach morgen nochmal. ";
        this.emit(':tell', speech);
    },

    'RepeatSoundIntent': function (speech) {
        if (typeof speech === 'undefined')
            var speech = "";

        speech += "Achtung. Hier kommt das Geräusch. <break time='150ms' /> <audio src='" + sounds[this.attributes['currentSound']]["sound"] + "' /> ";
        if (this.attributes['tries'] < 3) {
            speech += "Und? Hast du eine Idee? Dann nenne mir jetzt deine Lösung <break time='100ms' /> oder sage wiederholen. ";
            var reprompt = "Nenne mir jetzt deine Lösung <break time='100ms' /> oder sage wiederholen.";
        } else {
            speech += 'Du hast deine Versuche für heute aufgebraucht. Versuche es morgen nochmal. Sage Stop, um das Spiel zu beenden. ';
            var reprompt = 'Du möchtest weiterraten? Morgen hast du wieder 3 neue Versuche. Sage Stop, um das Spiel zu beenden. ';
        }
        this.emit(':ask', speech, reprompt);
    },

    'HintIntent': function () {
        var index = getHint(this);
        if (index <= -1) {
            var speech = "Es gibt noch keinen Tipp. <break time='100ms' /> Versuche es am besten morgen nochmal. Sage Geräusch, um das Geräusch zu hören oder Stop um zu beenden.";
        } else {
            var speech = "Es gibt" + (index + 1 == 1 ? " einen Tipp. " : (index + 1) + " <phoneme alphabet='ipa' ph='tɪps'>Tipps</phoneme>. ");
            for (var i = 0; i < index + 1; i++) {
                speech += sounds[this.attributes['currentSound']]["hints"][i] + " ";
            }
            if (this.attributes['tries'] < 3)
                speech += "Hast du jetzt eine Idee? Dann nenne die Lösung oder sage Geräusch, um das Geräusch zu hören. "
            else
                speech += "Sage Geräusch, um das Geräusch zu hören oder Stop um zu beenden. "
        }
        var reprompt = "Sage Geräusch, um das Geräusch zu hören oder Stop um zu beenden.. ";
        this.emit(':ask', speech, reprompt);
    },

    'SoundGuessIntent': function (slot) { // Nachdem der Spieler seinen Tipp abgegeben hat

        if (this.attributes['tries'] < 3 && this.attributes["unix"] == 0 && this.attributes["userName"] !== "InitalName") {
            if (this.event.request.intent.slots.soundGuess)
                var guess = this.event.request.intent.slots.soundGuess.value;

            if (slot)
                var guess = slot;

            if (!guess)
                this.emit('Unhandled');

            console.log("Guess: " + guess);

            var correctGuess = false;
            for (var possibleGuess in sounds[this.attributes['currentSound']]["answers"]) {
                if (sounds[this.attributes['currentSound']]["answers"][possibleGuess] === guess) {
                    correctGuess = true;
                }
            }

            if (correctGuess) {
                /*https.get('https://nordicskills.de/Gerausch/controller.php?password=nordicPass&action=SoundGuessed', (res) => {
                    console.log('statusCode:', res.statusCode);
                    console.log('headers:', res.headers);

                    res.on('data', (chunk) => { });
                    res.on('end', () => {
                        
                    });
                });*/
                console.log("Gelöst von " + this.attributes["userName"]);
                this.attributes["unix"] = parseInt(moment().format("X"));

                var speech = '<say-as interpret-as="interjection">juhu</say-as>, die Lösung war ' + sounds[this.attributes['currentSound']]["answers"][0] + '. '
                    + "Warte bis morgen früh um 8 Uhr, um ein neues Geräusch zu erraten. Sage Stop, um das Spiel zu beenden. ";
                this.emit(':ask', speech, speech);
            } else {

                //var speech = guess + '<say-as interpret-as="interjection">oh oh</say-as>, das war leider nicht richtig. ';
                //this.emit(':ask', speech, speech);
                this.emit('WrongGuessIntent');
            }
        } else if (this.attributes["userName"] === "InitalName") {

            var speech = "Du kannst noch keinen Tipp abgeben, da du dir noch keinen Namen gegeben hast. Nenne mir einfach deinen Namen oder sage Anonym, wenn du deinen Namen nicht preisgeben möchtest. ";
            var reprompt = "Nenne mir jetzt deinen Namen oder sage Anonym. ";
            this.emit(':ask', speech + reprompt, speech);
        } else if (this.attributes["unix"] > 0) {

            var speech = "Du hast bereits das Geräusch erraten. Morgen früh <break time='10ms'/> kannst du über die tägliche Zusammenfassung erfahren, ob du zu den fünf schnellsten gehörst. "
                + "Du kannst trotzdem Geräusch sagen, um das Geräusch nochmal zu hören, oder Stop um den Skill zu beenden. ";
            var reprompt = "Sage Geräusch, um das Geräusch nochmal zu hören oder Stop, um den Skill zu beenden. ";
            this.emit(':ask', speech, speech);
        } else {
            
            var speech = 'Gebe anderen Mitspielern auch eine Chance. Für heute hast du deine drei versuche aufgebraucht. Morgen früh um 8 Uhr bekommst du wieder drei neue. ';
            var reprompt = "Sage Stop, um das Spiel zu beenden. ";
            this.emit(':ask', speech + reprompt, speech);
        }
    },
    'NoGuessIntent': function () {

        var speech = "Wenn du keine Idee hast, versuch es doch einfach mit einem Tipp. Sage Tipp, für einen <phoneme alphabet='ipa' ph='tɪp'>Tipp</phoneme>. Oder Stop, um das Spiel zu beenden. ";
        this.emit(':ask', speech, speech);
    },

    'WrongGuessIntent': function () { // Falsche Antwort
        this.attributes['tries'] = this.attributes['tries'] + 1;
        var speech = 'Das war leider nicht richtig. ';
        if (this.attributes['tries'] >= 3)
            speech += 'Du hast alle deine Versuche für heute aufgebraucht. Versuch es einfach morgen früh um 8 Uhr nochmal. Sage Stop, um das Spiel zu beenden. ';
        else
            speech += 'Du hast noch ' + ((3 - this.attributes['tries'] ) == 1 ? "einen Versuch. " : (3 - this.attributes['tries']) + " Versuche. Sage Geräusch, um das Geräusch nochmal abzuspielen.");
        this.emit(':ask', speech, speech);
    },

    'AMAZON.HelpIntent': function () {

        var speech = "Beim Spiel das geheime Geräusch, spiele ich dir ein Geräusch vor <break time='100ms' /> und du hast drei versuche es zu erraten. <break time='200ms' /> "
            + "Solltest du es an einem Tag mit drei versuchen nicht erraten <break time='100ms' /> und es wurde bis zum Folge Tag von keinem Anderen mitspieler erraten, bekommst du morgens um 8 Uhr "
            + "erneut drei versuche es zu lösen. Wurde das geheime Geräusch allerdings gefunden und gelöst, wird am nächsten Tag, ein neues geheimes Geräusch zum erraten bereitgestellt. "
            + "<break time='200ms' /> Du kannst dir jedes geräusch mehrmals anhören, bzw. wiederholen lassen. Solltest du ein Geräusch einmal nicht richtig verstehen, kann es hilfreich sein, "
            + "dein Alexa Gerät etwas lauter zu stellen. <break time='100ms' /> Hast du das Gefühl, das Alexa dich nicht richtig versteht, vermeide die Hintergrundgeräusche. "
            + "Wurde das geheime Geräusch gefunden und gelöst, werden die schnellsten fünf Spieler, der Reihe nach aufgeführt. "
            + "Möchtest du in deiner Täglichen zusammenfassung, über das geheime Geräusch informiert werden? <break time='200ms' /> Kein Problem. Es gibt einen Skill der das für dich macht. "
            + "<break time='150ms' /> Möchtest du deinen Namen ändern? <break time='100ms' /> Dann sage einfach. Name ändern. "
            + "<break time='150ms'/> Möchtest du das Geräusch jetzt hören? Dann sage, Geräusch. Andernfalls sage Stop zum beenden. ";
        var reprompt = "Möchtest du das Geräusch jetzt hören? Dann sage, Geräusch. Andernfalls sage Stop zum beenden. ";
        this.emit(':ask', speech, reprompt);
    },
    'AMAZON.CancelIntent': function () {
        this.emit('SessionEndedRequest');
    },
    'AMAZON.StopIntent': function () {
        this.emit('SessionEndedRequest');
    },
    'SessionEndedRequest': function () {

        var self = this;
        self.attributes['unixLast'] = parseInt(moment().format('X'));
        dynamo.putUserState(self.event.session, function (data) {
            console.log("PutUserState: " + data.message);

            var speech = "Vielen Dank, das du mit mir gespielt hast. <say-as interpret-as='interjection'>bis bald.</say-as>. ";
            self.emit(':tell', speech);
        });
    },
    'Unhandled': function () {
        this.emit('WrongGuessIntent');
    },
    'Error': function () {
        var speech = "Entschuldigung, ich habe dich leider nicht verstanden. Bitte wiederhole deinen Befehl";
        this.emit(':ask', speech, speech);
    }
};

function getHint(self) {
    var index = -1;

    var currentDateTime = moment();
    var parsedDateTime = moment(self.attributes['csUnix'], "X");
    var diff = currentDateTime.diff(parsedDateTime, 'days', true);

    if (diff >= 2 && diff < 3) {
        index = 0;
    } else if (diff >= 3 && diff < 4) {
        index = 1;
    } else if (diff >= 4) {
        index = 2;
    }
    return index;
}

var blacklist =
[
    "geheime geräusch",
    "hitler",
    "nazi",
    "neonazi",
    "hure",
    "porno",
    "pornostar",
    "kinderporno",
    "kindersex",
    "kinderfick",
    "kinderficker",
    "kinderschänder",
    "kindergewalt",
    "vergewaltigung",
    "vergewaltiger",
    "start",
    "stop",
    "bitch",
    "whore",
    "fuck",
    "porn",
    "rapist"
]