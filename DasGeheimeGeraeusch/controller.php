﻿<?php
require 'aws.phar';
use Aws\DynamoDb\DynamoDbClient;

$client = DynamoDbClient::factory(array(
    'region' => 'eu-west-1',
	'version' => 'latest',
	'credentials' => [
        'key'    => 'AKIAJT2ERA44XLKHXTTA',
        'secret' => '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',
    ],
));

if($_GET["password"] == "nordicPass") {
	
	switch($_GET["action"]) {
		case "Update":
			
			$sounds = json_decode(file_get_contents("sounds.json"), true);

			$result = $client->getItem(array(
				'ConsistentRead' => true,
				'TableName' => 'DasGeheimeGerauschDB',
				'Key'       => array(
					'userId'   => array('S' => 'CurrentSound')
				)
			));

			$result2 = $client->query(array(
				"TableName" => 'DasGeheimeGerauschDB',
				"IndexName" => "userType-unix-index",
				"KeyConditionExpression" => "#n = :n",
				//ConsistentRead: false,
				"ExpressionAttributeNames" => [
					"#n" => "userType"
				],
				"ExpressionAttributeValues" => [
					":n" => array('S' => "User")
				],
				"ScanIndexForward" => true
			));

			$response1['uid'] = "1";
			$response1['updateDate'] = date("Y-m-d", strtotime(' +2 day'))."T01:00:00.0Z";
			$response1['titleText'] = "Das geheime Geräusch";
			$response1['mainText'] = "Das geheime Geraeusch wurde noch nicht erraten. Ich spiele dir jetzt das Geraeusch vor.";
			$response1['redirectionUrl'] = "https://www.nordicskills.de/";

			$response2['uid'] = "2";
			$response2['updateDate'] = date("Y-m-d", strtotime(' +2 day'))."T02:00:00.0Z";
			$response2['titleText'] = "Das geheime Geräusch";
			$response2['mainText'] = "";
			$response2['streamUrl'] = substr($sounds[$result['Item']['tries']['N']]['sound'], 0, $sounds[$result['Item']['tries']['N']]['sound'].length - 4)."Normalized".substr($sounds[$result['Item']['tries']['N']]['sound'], $sounds[$result['Item']['tries']['N']]['sound'].length - 4);
			$response2['redirectionUrl'] = "https://www.nordicskills.de/";

			$response3['uid'] = "3";
			$response3['updateDate'] = date("Y-m-d", strtotime(' +2 day'))."T03:00:00.0Z";
			$response3['titleText'] = "Das geheime Geräusch";
			$response3['mainText'] = "Wenn du eine Idee hast und mitspielen moechtest, dann sage, Alexa, starte das geheime Geraeusch. Viel Spass beim mitraten.";
			$response3['redirectionUrl'] = "https://www.nordicskills.de/";

			$found = false;

			foreach ($result2['Items'] as $i) {
				if($i['unix']['N'] > $result['Item']['unix']['N']) {
					$found = true;
				}
			}

			if(count($result2['Items']) > 0 && $found == true) {
				
				$nextSound = $result['Item']['tries']['N'] + 1;
					
				$response['uid'] = "0";
				$response['updateDate'] = date("Y-m-d", strtotime(' +2 day'))."T00:00:00.0Z";
				$response['titleText'] = "Das geheime Geräusch";
				$response['redirectionUrl'] = "https://www.nordicskills.de/";
				$temp = "";
				$tempCount = 0;
				foreach ($result2['Items'] as $i) {
					if($i['unix']['N'] > $result['Item']['unix']['N'] && $i['unix']['N'] > 0 && $tempCount < 5) {
						$date = new DateTime();
						$date->setTimestamp($i['unix']['N']);
						$date->modify('+ 1 hour');
						$temp .= ($i['userName']['S'] == "InitalName" ? "Anonym" : $i['userName']['S']).". Am ".$date->format("d.m.Y")." um ".$date->format("H:i").". "; 
						$tempCount = $tempCount + 1;
					}
				}
				if($tempCount == 1) {
					$response['mainText'] = "Das geheime Geraeusch wurde erraten. Der Gewinner ist. ".$temp;
				} else {
					$response['mainText'] = "Das geheime Geraeusch wurde erraten. Die ".$tempCount." schnellsten sind der Reihe nach: ".$temp;
				}

				$result = $client->putItem(array(
					'TableName' => 'DasGeheimeGerauschDB',
					'Item' => array(
						'userId'  => array('S' => 'CurrentSound'),
						'userName'  => array('S' => 'CurrentSound'),
						'userType'  => array('S' => 'CurrentSound'),
						'unix'    => array('N' => (string)time()),
						'tries'    => array('N' => (string)$nextSound)
					)
				));

				if($nextSound > count($sounds)) {

					$response['mainText'] .= "Heute gibt es leider kein Geräusch. Aber keine Sorge. Wir sorgen in Kürze für Nachschub. Versuche es einfach morgen nochmal. ";
					
					$fp = fopen('flash.json', 'w');
					fwrite($fp, json_encode(array($response)));
					fclose($fp);
					
					echo 'Update und kein neuer Sound freigegeben.';
				} else {
					$response['mainText'] .= "Das naechste geheime Geraeusch wurde noch nicht erraten. Ich spiele dir jetzt das Geraeusch vor.";
					$response['redirectionUrl'] = "https://www.nordicskills.de/";

					$response2['uid'] = "2";
					$response2['updateDate'] = date("Y-m-d", strtotime(' +2 day'))."T02:00:00.0Z";
					$response2['titleText'] = "Das geheime Geräusch";
					$response2['mainText'] = "";
					$response2['streamUrl'] = substr($sounds[$nextSound]['sound'], 0, $sounds[$nextSound]['sound'].length - 4)."Normalized".substr($sounds[$nextSound]['sound'], $sounds[$nextSound]['sound'].length - 4);
					$response2['redirectionUrl'] = "https://www.nordicskills.de/";
				
					$fp = fopen('flash.json', 'w');
					fwrite($fp, json_encode(array($response, $response2, $response3)));
					fclose($fp);

					echo 'Update und neuen Sound '.$nextSound.' freigegeben.';
				}
			} else {
				if(($result['Item']['tries']['N'] + 1) > count($sounds)) {

					$response7['uid'] = "7";
					$response7['updateDate'] = date("Y-m-d", strtotime(' +2 day'))."T01:00:00.0Z";
					$response7['titleText'] = "Das geheime Geräusch";
					$response7['mainText'] = "Heute gibt es leider kein Geräusch. Aber keine Sorge. Wir sorgen in Kürze für Nachschub. Versuche es einfach morgen nochmal. ";
					$response7['redirectionUrl'] = "https://www.nordicskills.de/";

					$fp = fopen('flash.json', 'w');
					fwrite($fp, json_encode(array($response7)));
					fclose($fp);
				} else {
					$fp = fopen('flash.json', 'w');
					fwrite($fp, json_encode(array($response1, $response2, $response3)));
					fclose($fp);
				}
				echo 'Update durchgeführt.';
			}
			break;
		default:
			echo 'Keine action angegeben.';
	}
} else {
	echo 'Passwort falsch oder nicht angegeben.';
}
?>