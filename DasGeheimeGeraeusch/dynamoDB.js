'use strict'

var fs = require('fs');
var AWS = require("aws-sdk")
var skill = require('./index').skill
var config = require('./config.json')

var LOCAL_CONTEXT = true

try {
  fs.accessSync( process.env[(process.platform === 'win32') ? 'USERPROFILE' : 'HOME'] + '/.aws')
} catch (e) {
  LOCAL_CONTEXT = false
}

// if this is being tested locally, load appropriate creds
if ( LOCAL_CONTEXT && config.awsProfileName ) {
  AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: config.awsProfileName })
}

var docClient = new AWS.DynamoDB.DocumentClient({ region: config.dynamoRegion || 'us-east-1' })


function getUserState ( session, cb ) {
  if ( ! config.dynamoTableName ) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

  var params = {
    "TableName": config.dynamoTableName,
    "Key": {
        "userId": session.user.userId
    },
    "AttributesToGet": [
        "userName",
        "unix",
        "unixLast",
        "tries"
    ],
  }

  docClient.get( params, handler( cb ) )
}
function putUserState(session, cb) {
    if (!config.dynamoTableName) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

    var params = {
        "TableName": config.dynamoTableName,
        "Item": {
            "userId": session.user.userId,
            "userName": session.attributes.userName,
            "userType": "User",
            "unix": session.attributes.unix,
            "unixLast": session.attributes.unixLast,
            "tries": session.attributes.tries
        }
    }

    docClient.put(params, handler(cb))
}

function getFastestUsers(session, cb) {
    if (!config.dynamoTableName) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

    var params = {
        TableName: config.dynamoTableName,
        IndexName: "userType-unix-index",
        KeyConditionExpression: "#n = :n",
        ConsistentRead: false,
        ExpressionAttributeNames: {
            "#n": "userType"
        },
        ExpressionAttributeValues: {
            ":n": "User"
        },
        ScanIndexForward: true,
        Limit: 5,
        Select: "ALL_ATTRIBUTES"
    }

    docClient.query(params, handler(cb))
}

function getCurrentSound(session, cb) {
    if (!config.dynamoTableName) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

    var params = {
        "TableName": config.dynamoTableName,
        "Key": {
            "userId": "CurrentSound"
        },
        "AttributesToGet": [
            "tries",
            "unix"
        ],
    }

    docClient.get(params, handler(cb))
}

function handler ( cb ) {
  return function ( err, data ) {
    if (err) {
      cb({
        message: "ERROR: Dynamo DB - " + JSON.stringify( err, null, 2 ),
        item: null
      })
    } else {
        if (typeof data.Items === "undefined") {
            cb({
                message: "SUCCESS: Dynamo DB",
                item: data.Item
            })
        } else {
            cb({
                message: "SUCCESS: Dynamo DB",
                item: data.Items
            })
        }
    }
  }
}

module.exports = {
  putUserState: putUserState,
  getUserState: getUserState,
  getFastestUsers: getFastestUsers,
  getCurrentSound: getCurrentSound
}
