/*
 * Copyright 2018 Amazon.com, Inc. and its affiliates. All Rights Reserved.
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * http://aws.amazon.com/asl/
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
'use strict';

const questions = require('./questions');
const settings = require('./settings');
const GAME_TITLE = 'Querbeet';

const messages = {
  de: {
    translation: {
      'QUESTIONS': questions.questions,
      'GENERAL_HELP': {
        outputSpeech: 'Dies ist ein Quiz-Spiel für Einzelspieler ohne Echo Buttons oder für Mehrspieler mit Buttons. Ich stelle dir oder euch, nach dem Start 25 Fragen. Nach jeder 5. Frage ' +
                      'gebe ich ein Zwischenergebnis und nach der letzten Frage das Endergebnis bekannt. Wenn du oder ihr eine Frage nicht Beantworten könnt oder sie Probleme beim beantworten macht, ' +
                      'sage weiter oder überspringen. Um zu starten, sage einfach Start oder Anfangen. Was möchtest du als Nächstes tun? ',
        reprompt: "Wenn du anfangen möchtest, sage Start oder Anfang",
        displayTitle: GAME_TITLE + ' - Hilfe',
        displayText: 'Dies ist ein Quiz-Spiel für Einzelspieler ohne Echo Buttons oder für Mehrspieler mit Buttons. Ich stelle dir oder euch, nach dem Start 25 Fragen. Nach jeder 5. Frage  ' +
          'gebe ich ein Zwischenergebnis und nach der letzten Frage das Endergebnis bekannt. Um zu starten, sage einfach Start oder Anfangen. Was möchtest du als Nächstes tun? '
      },
      'UNHANDLED_REQUEST': {
        outputSpeech: "Entschuldigung, das habe ich nicht verstanden. Bitte wiederhole es noch einmal!",
        reprompt: "Sag es bitte nochmal. Du kannst um Hilfe bitten, wenn Du nicht sicher bist, was du sagen kannst."
      },
      'GOOD_BYE': {
        outputSpeech: "Auf Wiederhören. Bis zum nächsten mal!",
        reprompt: ''
      },

      //
      //--------------------  Start Game Related Prompts -------------------------------------------
      //
      'START_GAME': {
        outputSpeech: settings.AUDIO.START_SOUND_AUDIO + "Willkommen bei " + GAME_TITLE + ". Dem Quizspiel für Besserwisser oder solche, die es werden wollen. Es können bis zu " +
          settings.GAME.MAX_PLAYERS + " Spieler mitspielen. Für Einzelspieler ohne Buttons, für mehr Spieler mit Buttons. " +
          "Wieviele Spieler wollen mitspielen?",
        reprompt: "Wieviele Spieler?",
        displayTitle: GAME_TITLE + " - Willkommen",
        displayText: "<font size='5'>Willkommen bei " + GAME_TITLE + ". Dem Quizspiel für Besserwisser oder solche, die es werden wollen. Es können bis zu " +
          settings.GAME.MAX_PLAYERS + " Spieler mitspielen.</font> "
      },
      'RESUME_GAME': {
        outputSpeech: 'Ok, wir werden dort weitermachen, wo du aufgehört hast. ' +
          'Wieviele Spieler wollen mitspielen?',
        reprompt: 'Was hattest du gesagt, wieviele Spieler?',
        displayTitle: GAME_TITLE + " - Willkommen",
        displayText: "<font size='5'>Willkommen zurück!</font>"
      },
      'DONT_RESUME_GAME': {
        outputSpeech: "OK, <say-as interpret-as='interjection'>wie du meinst</say-as>, dann lass uns ein neues Spiel starten. Wie viele Spieler wollen mitspielen?",
        reprompt: 'Was meinst du, wieviele Spieler?',
        displayTitle: GAME_TITLE + " - Willkommen",
        displayText: "<font size='5'>Ok. Lass uns ein neues Spiel starten.</font>"
      },
      'ASK_TO_RESUME': {
        outputSpeech: settings.AUDIO.ROLL_CALL_COMPLETE + "Klasse das du wieder da bist. Du hast ein altes Spiel mit {{player_count}}. " +
          "Möchtest du es weiterspielen?",
        reprompt: 'Möchtest du das alte Spiel weiterspielen?',
        displayTitle: GAME_TITLE + " - Willkommen",
		displayText: "<font size='5'>Du hast ein altes Spiel mit {{player_count}}.</font>"
      },
      'PLAYERCOUNT_INVALID': {
        outputSpeech: 'Bitte nenne eine Zahl zwischen eins und ' + settings.GAME.MAX_PLAYERS,
        reprompt: 'Und? wieviele wollen mitspielen? Nenne mir eine Zahl zwischen eins und ' + settings.GAME.MAX_PLAYERS
      },
      'SINGLE_PLAYER_GAME_READY': {
        outputSpeech: settings.AUDIO.OLD_ROLL_CALL_COMPLETE + ["Fantastisch! Bist du bereit das Spiel zu beginnen?"],
        reprompt: "Wenn du soweit bist, sage ja. Wollen wir mit dem Spiel beginnen?",
        displayTitle: GAME_TITLE + " - Willkommen",
        displayText: "<font size='5'>Bist du bereit das Spiel zu beginnen?</font>"
      },

      //
      //--------------------  Roll Call Related Prompts -------------------------------------------
      //
      'ROLL_CALL_HELP': {
        outputSpeech: GAME_TITLE + ' ist ein Quiz-Spiel für Echo Buttons. ' +
          'Jeder Mitspieler muß sich im Spiel mit seinem Button anmelden. ' +
          'Zum Anmelden drückt jeder Spieler einmal seinen Button. Möchtest du damit fortfahren? ',
        reprompt: "Entschuldigung, das habe ich nicht verstanden. Was wolltest du sagen? ",
        displayTitle: GAME_TITLE + ' - Hilfe',
        displayText: "Jeder Mitspieler, muß sich im Spiel mit seinem Echo Button anmelden. " +
          "Möchtest du damit fortfahren? "
      },
      'ROLL_CALL_CONTINUE': {
        outputSpeech: "Ok. Alle Mitspieler müssen jetzt ihren Button drücken, " +
          "damit ich weiß welche Button's benutzt werden. ",
        displayTitle: GAME_TITLE + " - Wilkommen",
        displayText: "Zum fortfahren des Spiels müssen alle Spieler ihren Button einmal drücken! "
      },
      'ROLL_CALL_TIME_OUT': {
        outputSpeech: "<say-as interpret-as='interjection'>oh oh</say-as>, " +
          "Die Zeit ist um. Und es sieht so aus, dass nicht alle ihren Button gedrückt haben. " +
          "Wollt ihr weiter machen? ",
        reprompt: "Wollen wir jetzt fortfahren? "
      },
      'ROLL_CALL_RESUME_GAME': {
        outputSpeech: "Um weiter zu spielen müssen alle Mitspieler ihren Button drücken! ",
        displayTitle: GAME_TITLE + " - Willkommen ",
        displayText: "<font size='5'>Um weiter zu spieln müssen alle Mitspieler ihren Button drücken! </font>"
      },
      'ROLL_CALL_COMPLETE': {
        outputSpeech: ["Klasse! Wir können anfangen. Spieler, seit ihr soweit? ",
        "Genial. Alle Spieler haben sich registriert. Seit ihr bereit das Spiel zu starten? "],
        reprompt: "Wollen wir das Spiel jetzt starten? ",
        displayTitle: GAME_TITLE + " - Willkommen",
        displayText: "Seit ihr bereit das Spiel zu starten?"
      },
      'ROLL_CALL_HELLO_PLAYER': {
		outputSpeech: "Hallo, Spieler <say-as interpret-as='cardinal'>{{player_number}}</say-as>."
      },
      'ROLL_CALL_NEXT_PLAYER_PROMPT': {
		outputSpeech: "Ok, Spieler <say-as interpret-as='cardinal'>{{player_number}}</say-as> du bist drann, drücke deinen Button. "
      },

      //
      //--------------------  Game Play Related Prompts -------------------------------------------
      //
      'GAME_CANCELLED': {
        outputSpeech: "Danke für's spielen. Bis zum nächsten mal!" +
          "Ich habe den Spielstand gespeichert. So kannst du später, sofern du willst von dort weiterspielen. ",
        reprompt: '',
        displayText: "Bis zum nächsten mal! ",
        displayTitle: "Danke für's mitspielen. "
      },
      'GAME_FINISHED_INTRO': {
        outputSpeech: settings.AUDIO.ENDE_PUNKTE_SOUND + "<say-as interpret-as='interjection'>huch</say-as> Das Spiel ist schon zuende. Dann lasst uns den Endstand anhören. "
      },
      'SINGLE_PLAYER_GAME_FINISHED_INTRO': {
        outputSpeech: settings.AUDIO.ENDE_PUNKTE_SOUND + "<say-as interpret-as='interjection'>Schade</say-as> das Spiel ist schon zuende. Dann lass uns hören, wieviele Punkte du hast."
      },
      'GAME_FINISHED': {
        outputSpeech: "Dann sage ich danke für`s spielen und ich wünsche viel glück beim nächstenmal, bis bald. " + settings.AUDIO.ENDE_KLATSCHEN ,
        reprompt: '',
        displayText: "Bis zum nächsten mal!",
        displayTitle: "Danke für`s spielen."
      },
      'PLAY_GAME_FIRST_QUESTION': {
        outputSpeech: "Ok! Last uns das Spiel beginnen! "
      },
      'PLAY_GAME_SKIP_QUESTION': {
        outputSpeech: "In Ordnung. Lass uns eine andere Frage versuchen. "
      },
      'PLAY_GAME_SKIP_LAST_QUESTION': {
        outputSpeech: "<say-as interpret-as='interjection'>oh nein</say-as>. Das war die letzte Frage. "
      },
      'PLAY_GAME_MID_GAME': {
        outputSpeech: "Ok! Lass uns weiter machen. " +
		  "Wir sind bei Frage {{current_question}}! "
      },
      'ANSWER_TIME_OUT_DURING_PLAY': {
        outputSpeech: "Ich habe keinen Button gehört. Möchtest du weiterspielen? ",
        reprompt: "Möchtest du jetzt weiterspielen? "
      },
      'BUZZ_IN_DURING_PLAY': {
		    outputSpeech: "Ok, Spieler <say-as interpret-as='cardinal'>{{player_number}}</say-as> wie lautet die Antwort? ",
        reprompt: "Spieler <say-as interpret-as='cardinal'>{{player_number}}</say-as> bis du da?  "
      },
      'CORRECT_ANSWER_DURING_PLAY': {
		    outputSpeech: "Richtig! Gut gemacht spieler <say-as interpret-as='cardinal'>{{player_number}}</say-as>.<break time='1s'/>. "
      },
      'INCORRECT_ANSWER_DURING_PLAY': {
		    outputSpeech: "Tut mir leid. Spieler <say-as interpret-as='cardinal'>{{player_number}}</say-as> die Antwort ist falsch. "
      },
      'INCORRECT_ANSWER_TOO_MANY_TIMES': {
		    outputSpeech: "Spieler <say-as interpret-as='cardinal'>{{player_number}}</say-as> diese Antwort ist leider auch falsch. " +
		  "Die richtige Antwort ist: {{current_answer}}. <break time='250ms'/> Gut. Dann lass es uns mit einer anderen Frage versuchen. "
      },
      'SINGLE_PLAYER_CORRECT_ANSWER_DURING_PLAY': {
        outputSpeech: "Richtig! Gut gemacht. "
      },
      'SINGLE_PLAYER_INCORRECT_ANSWER_DURING_PLAY': {
        outputSpeech: "Tut mir leid, die Antwort ist nicht richtig. " +
		  "Die richtige Antwort ist: {{current_answer}}. <break time='250ms'/> Gut. Dann bekommst du eine andere Frage. "
      },
      'MISUNDERSTOOD_ANSWER': {
        outputSpeech: "Entschuldigung, Das habe ich nicht verstanden. Bitte wiederhole deine Antwort! ",
        reprompt: "Meine Ohren sind jetzt sauber. Bitte wiederhole deine Antwort! "
      },
      'ANSWER_WITHOUT_BUTTONS': {
        outputSpeech: "<say-as interpret-as='interjection'>na und?</say-as> " +
          "<break time='1s'/>Drücke deinen Button um die Frage zu beantworten! "
      },
      'ANSWER_BEFORE_QUESTION': {
        outputSpeech: "Ich habe die Frage doch noch garnicht gestellt! Warte doch bis ich soweit bin. " +
          "Wenn du die Antwort dann kennst, drücke den Button! Bist du so weit? ",
        reprompt: "Wollen wir weiterspielen? "
      },
      'ASK_QUESTION_DISPLAY': {
        displayTitle: GAME_TITLE + " - Frage: {{question_number}}"
      },
      'ANSWER_QUESTION_CORRECT_DISPLAY': {
        displayTitle: GAME_TITLE + " - Spieler {{player_number}}",
        displayText: ["Gut gemacht! Stimmt. ",
        "Genial! Das ist richtig. ",
        "Richtig! Das war klasse. "]
      },
      'ANSWER_QUESTION_INCORRECT_DISPLAY': {
        displayTitle: GAME_TITLE + " - Spieler {{player_number}}",
        displayText: ["Upps! Das ist nicht richtig. ",
        "Oh nein! Das ist leider Falsch. ",
        "Nö nö, das war es nicht! "]
      },
      'SINGLE_PLAYER_ANSWER_QUESTION_CORRECT_DISPLAY': {
        displayTitle: GAME_TITLE,
        displayText: ["Gut gemacht! Stimmt. ",
        "Genial! Das ist richtig. ",
        "Richtig! Das war klasse. "]
      },
      'SINGLE_PLAYER_ANSWER_QUESTION_INCORRECT_DISPLAY': {
        displayTitle: GAME_TITLE,
        displayText: ["Upps! Das war nicht richtig. ",
        "Oh nein! Das ist leider Falsch. ",
        "Fast richtig, beim nächsten mal klappt es! "]
      },
      'ASK_FIRST_QUESTION_NEW_GAME_DISPLAY': {
        displayTitle: GAME_TITLE + " - Neues Spiel",
        displayText: "Mach dich bereit. Gleich geht es los! "
      },
      'ASK_FIRST_QUESTION_RESUME_DISPLAY': {
        displayTitle: GAME_TITLE + " - Spiel fortsetzen",
        displayText: "Mach dich bereit. Gleich spielen wir weiter! "
      },
      'GAME_PLAY_HELP': {
        outputSpeech: 'Spielhilfe für Querbeet. ' +
        'Während des Spiels werde ich jeweils eine Frage stellen. ' +
        'Wenn du die Antwort kennst, drückst du deinen Button und bekommst ' +
        'die Möglichkeit zu antworten. Für jede richtige Antwort ' +
        'bekommst du einen Punkt. Möchtest du jetzt weiterspielen? ',
        reprompt: "Upps! Das habe ich nicht verstanden. Bitte wiederhole deine Antwort! ",
        displayTitle: GAME_TITLE + " - Hilfe",
        displayText: 'Während des Spiels werde ich jeweils eine Frage stellen. ' +
        'Wenn du die Antwort kennst, drückst du deinen Button und bekommst ' +
        'die Möglichkeit zu antworten. Für jede richtige Antwort ' +
        'bekommst du einen Punkt. '
      },

      //
      //--------------------  Round Summary Related Prompts -------------------------------------
      //
      'GAME_ROUND_SUMMARY_INTRO': {
        outputSpeech: "Ok. Hier kommt ein zwischenergebniss, nach der <say-as interpret-as='ordinal'>{{round}}</say-as> Runde. "
      },
      'GAME_ROUND_SUMMARY_OUTRO': {
        outputSpeech: "Lass uns weitermachen! "
      },

      //
      //--------------------  Scoring Related Prompts -------------------------------------------
      //
      'SCORING_TIED_NO_ANSWERS': {
        outputSpeech: "Es steht unentschieden! Mit keinen richtigen Antworten. Das geht doch besser. Oder? "
      },
      'SCORING_TIED_ONE_ANSWER': {
        outputSpeech: "Es steht unentschieden! Mit einer richtigen Antwort. Was für ein Spiel! "
      },
      'SCORING_TIED_MULTIPLE_ANSWERS': {
		  outputSpeech: "Es steht unentschieden! Mit {{answer_count}} richtigen Antworten von " + settings.GAME.QUESTIONS_PER_GAME + "Fragen. Was für ein Spiel! "
      },
      'SCORING_SINGLE_PLAYER_NO_ANSWERS': {
        outputSpeech: "Du hast noch keine Fragen richtig beantwortet"
      },
      'SCORING_SINGLE_PLAYER_ONE_ANSWER': {
        outputSpeech: "Du hast eine Frage richtig beantwortet"
      },
      'SCORING_SINGLE_PLAYER_MULTIPLE_ANSWERS': {
        outputSpeech: "Du hast {{answer_count}} richtige Antworten von " + settings.GAME.QUESTIONS_PER_GAME + "Fragen. "
      },
      'SCORING_MULTI_PLAYERS': {
        outputSpeech: "Auf Platz <say-as interpret-as='number'>{{place}}</say-as> " +
          " {{score_details}}"
      }
    }
  },
};

module.exports = messages;