'use strict';
/*
 * Copyright 2018 Amazon.com, Inc. and its affiliates. All Rights Reserved.
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * http://aws.amazon.com/asl/
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */


/**
 * Questions library
 * 
 * Use this file to create your own set of questions.
 * 
 * Object properties:
 *      index:          The index of the question in this list
 *      question:       The question you want Alexa to ask
 *      answers:        The list of available answers
 *      correct_answer: The correct answer to the question
 * 
 * When adding or updating questions and answers, you must take the list of all values 
 * in each of the 'answers' arrays for all questions and add them to a custom slot 
 * in your skill called 'answers'.
 * 
 * The 'answers' custom slot is be mapped to a couple of intents in the interaction model.
 * One intent, named 'AnswerOnlyIntent', contains only the slot, by itself, in order 
 * to maximize the accuracy of the model.
 * 
 * For example: 
 *      AnswerOnlyIntent {answers}
 * 
 * The other intent, 'AnswerQuestionIntent', provides more complex speech patterns
 * to match other utternaces users may include with their answers.
 * 
 * For example:
 *      AnswerQuestionIntent is it {answers}
 *      AnswerQuestionIntent it is {answers}
 *      AnswerQuestionIntent the answer is {answers}
 *      AnswerQuestionIntent I think the answer is {answers}
 * 
 * See model file at models/de-DE.json for a complete example.
 */
module.exports = Object.freeze({ 
    questions: [
        {
            index: 1,
            question: 'Wie heißen die Übungskurse an der Hochschule?',
            answers: ['Riorosum', 'Audi Max', 'Inskrition', 'Seminar'],
            correct_answer: 'Seminar'
        },
        {
            index: 2,
            question: 'Was ist ein Xylophon?',
            answers: ['Schmetterling', 'Musikinstrument', 'Saft und Obstpresse'],
            correct_answer: 'Musikinstrument'
        },
        {
            index: 3,
            question: 'Wie lautet der Name des Weltraumtouristen aus den USA, der im Oktober 2005 fast 10 Tage im Weltraum verbrachte?',
            answers: ['Steven Trott', 'Gregory Olsen', 'Michael Taylor', 'Dennis Tito'],
            correct_answer: 'Gregory Olsen'
        },
        {
            index: 4,
            question: 'Am 65. Hochzeitstag feiert man die?',
            answers: ['Gnadenhochzeit', 'Eiserne Hochzeit', 'Diamantene Hochzeit', 'Steinerne Hochzeit'],
            correct_answer: 'Eiserne Hochzeit'
        },
        {
            index: 5,
            question: 'Der dänische Theologe und Philosoph Kierkegaard hieß mit Vornamen?',
            answers: ['Sven', 'Sören', 'Nils', 'Holger'],
            correct_answer: 'Sören'
        },
        {
            index: 6,
            question: 'Was heißt im Volksmund. Schwindelkraut?',
            answers: ['Lügenzahn', 'Koriander', 'Cardamine', 'Kardamom'],
            correct_answer: 'Koriander'
        },
        {
            index: 7,
            question: 'Wie hieß das Flugzeug von Charles Lindbergh?',
            answers: ['Santa Maria', 'Spirit of Saint Louis', 'Lisa Marie', 'Yellow Submarine'],
            correct_answer: 'Spirit of Saint Louis'
        },
        {
            index: 8,
            question: 'Wer war Alexander Girardi?',
            answers: ['Volksschauspieler', 'Ferdinand Lucina', 'Apotheker', 'Heinrich Harrer'],
            correct_answer: 'Volksschauspieler'
        },
        {
            index: 9,
            question: 'Achtung und gut zuhören. <break time="200ms"/><audio src="https://nordicskills.de/audio/Querbeet/Frage/fur_elise.mp3" />Dieses Klavierstück von Ludwig van Beethoven heisst?',
            answers: ['Für Elise', 'Für Gabi', 'Für Laura', 'Für Marie'],
            correct_answer: 'Für Elise'
        },
		{
            index: 10,
            question: 'Männlein, Männlein, Timpe Te, Buttje, Buttje in der See, Meine Frau, die Ilsebill, Will nicht so, wie ich wohl will. Woher stammt dieser Text?',
            answers: ['Thomas Neumann', 'Helmut Kohl', 'Vom Angler und seinem Bruder', 'Von dem Fischer und seiner Frau'],
            correct_answer: 'Von dem Fischer und seiner Frau'
        },
		{
            index: 11,
            question: 'Zu welchem Zweck werden Deiche errichtet?',
            answers: ['Hochwasserschutz', 'Fundament für Hochhäuser', 'Touristenattraktion', 'Stromgewinnung'],
            correct_answer: 'Hochwasserschutz'
        },
		{
            index: 12,
            question: 'Was bedeutet terrone?',
            answers: ['Stierkämpfer', 'Kartoffelfresser', 'Erdfresser', 'Erdkröte'],
            correct_answer: 'Erdfresser'
        },
		{
            index: 13,
            question: 'Was bedeutet Indolenz?',
            answers: ['Gleichgültigkeit', 'Unverschämtheit', 'Nachgibigkeit', 'Zahlungsunfähigkeit'],
            correct_answer: 'Gleichgültigkeit'
        },
		{
            index: 14,
            question: 'Wie heißt das männliche Pendant der Krankenschwester?',
            answers: ['Krankenbruder', 'Krankenpfleger'],
            correct_answer: 'Krankenpfleger'
        },
		{
            index: 15,
            question: 'Welche Farbe hat der mittlere, obere olympische Ring?',
            answers: ['Gelb', 'Schwarz', 'Grün', 'Rot'],
            correct_answer: 'Schwarz'
        },
		{
            index: 16,
            question: 'Der Kopf mit 2 Gesichtern ist benannt nach?',
            answers: ['Jason', 'Jambus', 'Janus', 'Jargon'],
            correct_answer: 'Janus'
        },
		{
            index: 17,
            question: 'Eine Meinungsübereinstimmung nennt man?',
            answers: ['Konspekt', 'Konsequenz', 'Konsistenz', 'Konsens'],
            correct_answer: 'Konsens'
        },
		{
            index: 18,
            question: 'Zerstör mir meine Kreise nicht. sagte?',
            answers: ['Arhistoteles', 'Goethe', 'Blaise Pascal', 'Archimedes'],
            correct_answer: 'Archimedes'
        },
		{
            index: 19,
            question: 'Achtung und gut zuhören. <break time="200ms"/><audio src="https://nordicskills.de/audio/Querbeet/Frage/Klaviersonate_elf.mp3" />Welche Klaviersonate von Mozart war das?',
            answers: ['Nummer zehn', 'Nummer elf', 'Nummer neun', 'Nummer fünfzehn'],
            correct_answer: 'Nummer elf'
        },
		{
            index: 20,
            question: 'Vor einem großen Walde wohnte ein armer Holzhacker mit seiner Frau und seinen zwei Kindern. Wer ist hier gemeint?',
            answers: ['Bremer Stadtmusikanten', 'Hänsel und Gretel', 'Schneeweißchen und Rosenrot', 'Der süße Brei'],
            correct_answer: 'Hänsel und Gretel'
        },
		{
            index: 21,
            question: 'Maria Magdalena von Losch nannte sich?',
            answers: ['Zarah Leander', 'Lale Andersen', 'Marlene Dietrich', 'Maria Magdalena'],
            correct_answer: 'Marlene Dietrich'
        },
		{
            index: 22,
            question: 'Wodurch wurde das schottische Dorf Gretna Green berühmt?',
            answers: ['Friedensnobelpreis', 'Trauung Minderjärige', 'Richard Löwenherz', 'Tretroller aus Leicht Metal'],
            correct_answer: 'Trauung Minderjärige'
        },
		{
            index: 23,
            question: 'Was ist eine Topografie?',
            answers: ['Segelregatta', 'Fruchtpresse', 'Applaus im stehen', 'Landschaftsbeschreibung'],
            correct_answer: 'Landschaftsbeschreibung'
        },
		{
            index: 24,
            question: 'Welches Comic-Duo besteht aus einem altklugen Jungen und einem Spielzeugtiger?',
            answers: ['Klein Calvin', 'Calvin und Klein', 'Calvin und Hobbs', 'Calvin und Tiggs'],
            correct_answer: 'Calvin und Hobbs'
        },
		{
            index: 25,
            question: 'Wie nennt man einen verstellbaren Schraubenschlüssel?',
            answers: ['Engländer', 'Amerikaner', 'Holländer', 'Schweden'],
            correct_answer: 'Engländer'
        },
		{
            index: 26,
            question: 'Wie heissen die beiden heute noch erhaltenen Felsentempel, die Pharao Ramses II. am westlichen Nilufer bauen ließ?',
            answers: ['Abu Simbel', 'Elephantine', 'Kom Ombo', 'Medinet Habu'],
            correct_answer: 'Abu Simbel'
        },
		{
            index: 27,
            question: 'Wer schrieb den historischen Roman Sinuhe der Ägypter?',
            answers: ['Mika Waltari', 'William Saroyan', 'Thomas Mann', 'Marquerite Duras'],
            correct_answer: 'Mika Waltari'
        },
		{
            index: 28,
            question: 'Wer spielte den untoten Priester Imhotep im Film die Mumie von 1932?',
            answers: ['Bela Lugosi', 'Jonny Weismüller', 'Christopher Lee', 'Boris Karloff'],
            correct_answer: 'Boris Karloff'
        },
		{
            index: 29,
            question: 'Achtung und gut zuhören. <break time="200ms"/><audio src="https://nordicskills.de/audio/Querbeet/Frage/Klaviersonate_elf.mp3" />die 5. Sinfonie von Mozart ist auch bekannt als?',
            answers: ['Trauersinfonie', 'Totesinfonie', 'Erfolgssinfonie', 'Schicksalssinfonie'],
            correct_answer: 'Schicksalssinfonie'
        },
		{
            index: 30,
            question: 'Es war einmal ein kleines Mädchen, dem war Vater und Mutter gestorben, und es war so arm, dass es kein Kämmerchen mehr hatte. Wer ist hier gemeint?',
            answers: ['Hänsel und Gretel', 'Dornröschen', 'Sterntaler', 'Rotkäppchen'],
            correct_answer: 'Sterntaler'
        },
		{
            index: 31,
            question: 'Die altägyptische Hochkultur befasste sich mit verschiedenen Wissensgebieten. Welche der folgenden Disziplinen gab es in dieser Form noch nicht?',
            answers: ['Mathematik', 'Astronomie', 'Medizin', 'Philosophie'],
            correct_answer: 'Philosophie'
        },
		{
            index: 32,
            question: 'Was bedeutet Hieroglyphen sinngemäss?',
            answers: ['Heilige Schriftzeichen', 'Heilige Bilder', 'Heilige Worte', 'Heiliger Atem'],
            correct_answer: 'Heilige Schriftzeichen'
        },
		{
            index: 33,
            question: 'Das Leben der Königin Kleopatra wurde 1963 verfilmt. Wer spielte die Hauptrolle?',
            answers: ['Romy Schneider', 'Sophia Loren', 'Liz Taylor', 'Julie Christie'],
            correct_answer: 'Liz Taylor'
        },
		{
            index: 34,
            question: 'Durch das wiederkehrende Hochwasser war der Nil das Symbol der Fruchtbarkeit. Mit welchem Symbol wurde er in Verbindung gebracht?',
            answers: ['Papyruspflanze', 'Dattelpalme', 'Olivenbaum', 'Bambusrohr'],
            correct_answer: 'Papyruspflanze'
        },
		{
            index: 35,
            question: 'Was sind Hieroglyphen?',
            answers: ['Piktogramme', 'Sinnzeichen', 'Symbole', 'Ikonen'],
            correct_answer: 'Sinnzeichen'
        },
		{
            index: 36,
            question: 'In welchem Jahr flog erstmals eine Frau ins All?',
            answers: ['Neunzehnhundertdreiundvierzig', 'Neunzehnhundertdreiundsechzig', 'Neunzehnhundertdreiundachtzig', 'Neunzehnhundertdreiundfünfzig'],
            correct_answer: 'Neunzehnhundertdreiundsechzig'
        },
		{
            index: 37,
            question: 'Wandelstern ist das deutsche Wort für?',
            answers: ['Komet', 'Quasar', 'Planet', 'Trabant'],
            correct_answer: 'Planet'
        },
		{
            index: 38,
            question: 'Welche europäische Trägerrakete wurde 1979 erstmals gestartet?',
            answers: ['Luna', 'Viking', 'Ariane', 'Apollo'],
            correct_answer: 'Ariane'
        },
		{
            index: 39,
            question: 'Achtung und gut zuhören. <break time="200ms"/><audio src="https://nordicskills.de/audio/Querbeet/Frage/Hummelflug.mp3" />dieses Musikstück aus der gleichnamigen Oper von Rimski-Korsakow heist?',
            answers: ['Vogelflug', 'Wespenflug', 'Hummelflug', 'Bienenflug'],
            correct_answer: 'Hummelflug'
        },
		{
            index: 40,
            question: 'Es war einmal eine kleine süße Dirne, die hatte jedermann lieb, der sie nur ansah, am allerliebsten aber ihre Großmutter. Wer ist hier gemeint?',
            answers: ['Rotkäppchen', 'Schneewittchen', 'Dornröschen', 'Aschenputtel'],
            correct_answer: 'Rotkäppchen'
        },
		{
            index: 41,
            question: 'Welcher Planet ist nach dem römischen Meeresgott benannt?',
            answers: ['Saturn', 'Neptun', 'Jupiter', 'Mars'],
            correct_answer: 'Neptun'
        },
		{
            index: 42,
            question: 'Welcher Planet hat die meisten Monde?',
            answers: ['Saturn', 'Jupiter', 'Uranus', 'Mars'],
            correct_answer: 'Saturn'
        },
		{
            index: 43,
            question: 'Wie heiß ist es im Zentrum der Sonne?',
            answers: ['Fünfzehn Millionen Grad', 'Fünfundvierzig Millionen Grad', 'Dreizig Millionen Grad', 'Sechzig Millionen Grad'],
            correct_answer: 'Fünfzehn Millionen Grad'
        },
		{
            index: 44,
            question: 'Wie heisst der grösste Planet unseres Sonnensystems?',
            answers: ['Jupiter', 'Venus', 'Merkur', 'Mars'],
            correct_answer: 'Jupiter'
        },
		{
            index: 45,
            question: 'Welcher Himmelskörper leuchtet selbst?',
            answers: ['Mond', 'Wandelstern', 'Planet', 'Fixstern'],
            correct_answer: 'Fixstern'
        },
		{
            index: 46,
            question: 'Welcher dieser Körper hat die größte Anziehungskraft (Fallbeschleunigung)?',
            answers: ['Erde', 'Mond', 'Mars', 'Jupiter'],
            correct_answer: 'Jupiter'
        },
		{
            index: 47,
            question: 'Aus welcher Sprache stammt, Astronomie?',
            answers: ['Französisch', 'Lateinisch', 'Arabisch', 'Griechisch'],
            correct_answer: 'Griechisch'
        },
		{
            index: 48,
            question: 'Wie lange ist das Sonnenlicht bis zu uns unterwegs?',
            answers: ['Drei Minuten', 'Acht Minuten', 'Achtzehn Minuten', 'Dreiundzanzig Minuten'],
            correct_answer: 'achtzehn minuten'
        },
		{
            index: 49,
            question: 'Achtung und gut zuhören. <break time="200ms"/><audio src="https://nordicskills.de/audio/Querbeet/Frage/Donauwalzer.mp3" />Der Walzer wurde von Johann Strauss Junior komponiert?',
            answers: ['Reinwalzer', 'Donauwalzer', 'Elbewalzer', 'Opernwalzer'],
            correct_answer: 'Donauwalzer'
        },
		{
            index: 50,
            question: 'Wer reitet so spät durch Nacht und Wind? Es ist der Vater mit seinem Kind. <break time="200ms"/>Aus welchen Gedicht stammen diese Zeilen?',
            answers: ['Die Nibelungen', 'Die Brüder', 'Königskinder', 'Der Erlkönig'],
            correct_answer: 'Der Erlkönig'
        },
		{
            index: 51,
            question: 'Wie nennt man einen kurzfristigen, heftigen Strahlungsausbruch von Sternen?',
            answers: ['Flavour', 'Flare', 'Flur', 'Flake'],
            correct_answer: 'Flare'
        },
		{
            index: 52,
            question: 'Wie heißt das erste unbemannte rückholbare Weltraumlabor der ESA?',
            answers: ['Heureca', 'Eureca', 'Skylab', 'Ariane'],
            correct_answer: 'Eureca'
        },
		{
            index: 53,
            question: 'Zu welchem Sternbild gehört der Atair?',
            answers: ['Fische', 'Krebs', 'Adler', 'Löwe'],
            correct_answer: 'Adler'
        },
		{
            index: 54,
            question: 'Welcher Planet ist der Sonne am nächsten?',
            answers: ['Venus', 'Mars', 'Merkur', 'Erde'],
            correct_answer: 'Merkur'
        },
		{
            index: 55,
            question: 'Wie heißt das Gegenteil des Vollmondes?',
            answers: ['Leermond', 'Halbmond', 'Neumond', 'Mondsichel'],
            correct_answer: 'Neumond'
        },
		{
            index: 56,
            question: 'Wie hieß eine neoplatonische Philosophin, Astronomin und Mathematikerin, die 370 n. Chr. in Alexandria geboren wurde?',
            answers: ['Sappho', 'Hypatia', 'Alexandra', 'Kleopatra'],
            correct_answer: 'Hypatia'
        },
		{
            index: 57,
            question: 'Welcher Kalender beruht nicht auf dem reinen Sonnenjahr?',
            answers: ['Ägyptische', 'Gregorianische', 'Moslemische', 'Türkische'],
            correct_answer: 'Moslemische'
        },
		{
            index: 58,
            question: 'Was ist in der Marsatmosphäre gar nicht vorhanden?',
            answers: ['Sauerstoff', 'Schwefel', 'Kohlenstoffdioxid', 'Stickstoff'],
            correct_answer: 'Schwefel'
        },
		{
            index: 59,
            question: 'Achtung und gut zuhören. <break time="200ms"/><audio src="https://nordicskills.de/audio/Querbeet/Frage/kleine_nachmusik.mp3" />Eine kleine Nachtmusik, ist von?',
            answers: ['Strauss', 'Vivaldi', 'Beethoven', 'Mozart'],
            correct_answer: 'Mozart'
        },
		{
            index: 60,
            question: 'Meines Lebens schönster Traum Hängt an diesem Apfelbaum! <break time="200ms"/>Aus welcher Geschichte stammen diese Zeilen?',
            answers: ['Max und Moritz', 'Sterntaler', 'König Drosselbart', 'Des Kaisers neue Kleider'],
            correct_answer: 'Max und Moritz'
        },
		{
            index: 61,
            question: 'Wer entwickelte im 16. Jahrhundert die Lehre vom heliozentrischen Planetensystem?',
            answers: ['Galilei', 'Kepler', 'Kues', 'Kopernikus'],
            correct_answer: 'Kopernikus'
        },
		{
            index: 62,
            question: 'Sein oder nicht sein, sagte?',
            answers: ['Lohengrin', 'Hamlet', 'Cäsar', 'Schiller'],
            correct_answer: 'Hamlet'
        },
		{
            index: 63,
            question: 'Wer schrieb das moderne Gedicht von Ottos Mops?',
            answers: ['Georg Thomalla', 'Ernst Jandl', 'Friedrich Schiller', 'Friedrich Torberg'],
            correct_answer: 'Ernst Jandl'
        },
		{
            index: 64,
            question: 'In welcher Sprache wurde das neue Testament verfasst?',
            answers: ['Hebräisch', 'Lateinisch', 'Griechisch', 'Althochdeutsch'],
            correct_answer: 'Griechisch'
        },
		{
            index: 65,
            question: 'Wer schrieb, Die Verschwörung des Catilina?',
            answers: ['Lukan', 'Cäsar', 'Cicero', 'Sallust'],
            correct_answer: 'Sallust'
        },
		{
            index: 66,
            question: 'Wie heißt der kleine Eisbär im gleichnamigen Kinderbuch?',
            answers: ['Björn', 'Nils', 'Peer', 'Lars'],
            correct_answer: 'lars'
        },
		{
            index: 67,
            question: 'Unter welchem Namen ist Netty Radvanyi bekannter?',
            answers: ['Marilyn Monroe', 'Christa Wolf', 'Gabriela Mistral', 'Anna Seghers'],
            correct_answer: 'Anna Seghers'
        },
		{
            index: 68,
            question: 'Wie heißt der römische Gott des Handels?',
            answers: ['Hermes', 'Poseidon', 'Merkur', 'Zeus'],
            correct_answer: 'Merkur'
        },
		{
            index: 69,
            question: 'Achtung und gut zuhören. <break time="200ms"/><audio src="https://nordicskills.de/audio/Querbeet/Frage/Herbst.mp3" />Dieses Stück aus, Die Vier Jahreszeiten von Vivaldi heist?',
            answers: ['Herbst', 'Frühling', 'Winter', 'Sommer'],
            correct_answer: 'Herbst'
        },
		{
            index: 70,
            question: 'Wer kämpft, kann verlieren. Wer nicht kämpft, hat schon verloren. <break time="200ms"/>Wer hat das gesagt?',
            answers: ['Fritz Grünbaum', 'Friedrich Nietzsche', 'Albert Einstein', 'Bertold Brecht'],
            correct_answer: 'Bertold Brecht'
        },
		{
            index: 71,
            question: 'Welche jüdische Schülerin verfasste ein berühmtes Tagebuch über den Nationalsozialismus?',
            answers: ['Hannah Arendt', 'Sophie Scholl', 'Anne Frank', 'Bertha von Suttner'],
            correct_answer: 'Anne Frank'
        },
		{
            index: 72,
            question: 'Wie viel Strom wird gespart, wenn man Wäsche statt mit 60°C nur mit 30°C wäscht?',
            answers: ['Fünfzig Prozent', 'Dreizig Prozent', 'Siebzig Prozent', 'Zehn Prozent'],
            correct_answer: 'Fünfzig Prozent'
        },
		{
            index: 73,
            question: 'Wer schrieb Das weite Land?',
            answers: ['Karl Kraus', 'Arthur Schnitzler', 'Franz Kafka'],
            correct_answer: 'Arthur Schnitzler'
        },
		{
            index: 74,
            question: 'Wie hieß Goethes einziges Kind?',
            answers: ['Stella', 'Ernst', 'Elisabeth', 'August'],
            correct_answer: 'August'
        },
		{
            index: 75,
            question: 'Wie heißt die Rheinnixe der deutschen Sage?',
            answers: ['Nessie', 'Medusa', 'Loreley', 'Arielle'],
            correct_answer: 'Loreley'
        },
		{
            index: 76,
            question: 'Wer schrieb, Tod eines Handlungsreisenden?',
            answers: ['John Steinbach', 'William Faulkner', 'Arthur Miller'],
            correct_answer: 'Arthur Miller'
        },
		{
            index: 77,
            question: 'Wie heißt Mutter Courage in Bert Brechts Roman?',
            answers: ['Anna Fierling', 'Petra Fünfling', 'Klara Drillinck', 'Maria Zwylling'],
            correct_answer: 'Anna Fierling'
        },
		{
            index: 78,
            question: 'Was ist das deutsche Wort für Komödie?',
            answers: ['Liebespiel', 'Trauerspiel', 'Lustspiel', 'Lustwandeln'],
            correct_answer: 'Lustspiel'
        },
		{
            index: 79,
            question: 'Wer ist Autor der Peanuts?',
            answers: ['Jim Davids', 'Charles m Schulz', 'Harald Foster', 'Rolf Kauka'],
            correct_answer: 'Charles m Schulz'
        },
		{
            index: 80,
            question: 'Eine Frage raubt mir den Verstand: Bin Ich verrückt oder sind es alle anderen hier?<break time="200ms"/>Von wem stammt das Zitat?',
            answers: ['Michail Gorbatschow', 'Hans Christian Andersen', 'Albert Einstein', 'Bertold Brecht'],
            correct_answer: 'Albert Einstein'
        },
		{
            index: 81,
            question: 'Wer erhielt 1960 den Georg-Büchner-Preis?',
            answers: ['Erich Kästner', 'Paul Celan', 'Ingeborg Bachmann', 'Peter Abraham'],
            correct_answer: 'Paul Celan'
        },
		{
            index: 82,
            question: 'Wer erhielt 1999 den Nobelpreis für Literatur?',
            answers: ['Heinz Konsalik', 'Günter Grass', 'Luis Trenker', 'Hansi Hinterseer'],
            correct_answer: 'Günter Grass'
        },
		{
            index: 83,
            question: 'Wer schrieb den Roman Steppenwolf?',
            answers: ['Raimund Harmstorf', 'Hermann Hesse', 'Franz Stifter', 'Gernot Friedle'],
            correct_answer: 'Hermann Hesse'
        },
		{
            index: 84,
            question: 'Welcher König verwandelte alles in Gold?',
            answers: ['Paktolos', 'Äneas', 'Midas', 'Ascanius'],
            correct_answer: 'Midas'
        },
		{
            index: 85,
            question: 'Wie lautet die Internetkennung für Spanien?',
            answers: ['sp', 'ep', 'es', 'sn'],
            correct_answer: 'es'
        },
		{
            index: 86,
            question: 'Wie nennt man einen Verweis auf einer Website zu einer anderen?',
            answers: ['Superlink', 'Hyperlink', 'Microlink'],
            correct_answer: 'Hyperlink'
        },
		{
            index: 87,
            question: 'In welchem Jahre entwickelte Timothy Berners-Lee das World Wide Web?',
            answers: ['Neunzehnhundertdreiundachtzig', 'Neunzehnhundertfünfundachtzig', 'Neunzehnhundertneunundachtzig', 'Neunzehnhundertzweiundneunzig'],
            correct_answer: 'Neunzehnhundertneunundachtzig'
        },
		{
            index: 88,
            question: 'Wann wurde die erste E-Mail versendet?',
            answers: ['Neunzehnhundertsiebzig', 'Neunzehnhunderteinundziebzig', 'Neunzehnhundertzeiundziebzig', 'Neunzehnhundertdreiundziebzig'],
            correct_answer: 'Neunzehnhunderteinundziebzig'
        },
		{
            index: 89,
            question: 'Wer erkannte bereits um 1700 die Vorteile des Binärsystems?',
            answers: ['Newton', 'Celsius', 'Leibniz', 'Gauß'],
            correct_answer: 'Leibniz'
        },
		{
            index: 90,
            question: 'Ein Tag an dem Du nicht gelacht hast, ist ein verlorener Tag. <break time="200ms"/>Dieses Zitat bewegte die Welt. Von wem stammt es?',
            answers: ['Mike Krüger', 'Richard Nixon', 'Charlie Chaplin', 'Willy Brand'],
            correct_answer: 'Charlie Chaplin'
        },
		{
            index: 91,
            question: 'Wieviel Kilobyte sind ein Megabyte?',
            answers: ['Zweitausendachtundvierzig', 'Eintausendvierundzwanzig', 'Sechshundertzwölf', 'Fünftausend'],
            correct_answer: 'Eintausendvierundzwanzig'
        },
		{
            index: 92,
            question: 'Wie nennt man einen virtuellen Raum?',
            answers: ['Internet', 'World Wide Web', 'Cyberspace', 'Raumzeit'],
            correct_answer: 'Cyberspace'
        },
		{
            index: 93,
            question: 'An welchem Fluss liegt Augsburg?',
            answers: ['Inn', 'Lech', 'Isar', 'Pech'],
            correct_answer: 'Lech'
        },
		{
            index: 94,
            question: 'Wer trat 1948 in die FDP ein und aus Ärger über Jürgen W. Möllemann 2002 aus?',
            answers: ['Mildred Scheel', 'Claudia Roth', 'Otto Graf Lambsdorff', 'Hildegard Hamm Brücher'],
            correct_answer: 'Hildegard Hamm Brücher'
        },
		{
            index: 95,
            question: 'Wer gewann im Jahre 2004 in Athen die olympische Bronzemedaille im 200 Meter Rückenschwimmen?',
            answers: ['Franziska van Almsik', 'Sandra Völker', 'Antje Buschschulte'],
            correct_answer: 'Antje Buschschulte'
        },
		{
            index: 96,
            question: 'Wie hieß der zweite deutsche Bundespräsident?',
            answers: ['Theodor Heuss', 'Walter Scheel', 'Gustav Heinemann', 'Heinrich Lübke'],
            correct_answer: 'Heinrich Lübke'
        },
		{
            index: 97,
            question: 'An welchem Fluss liegt Stade in Niedersachsen?',
            answers: ['Klinge', 'Schwinge', 'Schlei', 'Brei'],
            correct_answer: 'Schwinge'
        },
		{
            index: 98,
            question: 'Der Musiker und Sänger Rio Reiser starb im Jahre?',
            answers: ['Neunzehnhundertfünfundneunzig', 'Neunzehnhundertsechsundneunzig', 'Neunzehnhundertachtundneunzig', 'Zweitausend'],
            correct_answer: 'Neunzehnhundertsechsundneunzig'
        },
		{
            index: 99,
            question: 'Rudolf Scharping war Ministerpräsident von?',
            answers: ['Bayern', 'Hessen', 'Rheinland Pfalz', 'Nordrein Westfalen'],
            correct_answer: 'Rheinland Pfalz'
        },
		{
            index: 100,
            question: 'Der Adler ist gelandet. <break time="200ms"/>Dieses Satz bewegte die Welt. Von wem stammt dieser Satz?',
            answers: ['Peter Lustig', 'Neil Armstrong', 'Karl Heinz Rummenigge', 'Ronald Reagan'],
            correct_answer: 'Neil Armstrong'
        },
		{
            index: 101,
            question: 'In welchen Städten fanden die Halbfinalspiele der Fußball WM 2006 statt?',
            answers: ['Berlin und Münschen', 'Köln und Berlin', 'Dresden und Leipzig', 'München und Dortmund'],
            correct_answer: 'München und Dortmund'
        },
		{
            index: 102,
            question: 'Der große deutsche Dichter Schiller starb in?',
            answers: ['Stuttgard', 'Frankfurt am Main', 'Frankfurtt an der Oder', 'Weimar'],
            correct_answer: 'Weimar'
        },
		{
            index: 103,
            question: 'In welchem Bundesland liegt der höchste Berg Deutschlands, die Zugspitze?',
            answers: ['Hessen', 'Bayern', 'Thüringen', 'Baden Württemberg'],
            correct_answer: 'Bayern'
        },
		{
            index: 104,
            question: 'Die fünfstelligen PLZ in Deutschland gibt es seit dem Jahre?',
            answers: ['Neunzehnhunderdreiundachzig', 'Neunzehnhundertdreiundsiebzig', 'Neunzehnhundertdreiundneunzig', 'Zweitausendfünf'],
            correct_answer: 'Neunzehnhundertdreiundneunzug'
        },
		{
            index: 105,
            question: 'Wie viele Jahre gehörte Oskar Lafontaine der SPD an?',
            answers: ['Dreißig', 'Fünfunddreißig', 'Neununddreißig', 'Dreiundvierzig'],
            correct_answer: 'Neununddreißig'
        },
		{
            index: 106,
            question: 'Wann wurde Deutschland erstmals Weltmeister im Frauenfußball?',
            answers: ['Neunzehnhunderteinundneunzig', 'Neunzehnhundertvierundneunzig', 'Neunzehnhundertneunundneunzig', 'Zweitausenddrei'],
            correct_answer: 'Zweitausenddrei'
        },
		{
            index: 107,
            question: 'Welcher deutsche Boxer gewann 1988 bei den Olympischen Sommerspielen in Seoul Gold im Mittelgewicht?',
            answers: ['Rene Weller', 'Wladimir Klitschko', 'Henry Maske', 'Axel Schulz'],
            correct_answer: 'Henry Maske'
        },
		{
            index: 108,
            question: 'Woher stammt Rosenkohl ursprünglich?',
            answers: ['Belgien', 'Italien', 'Mexiko', 'Türkei'],
            correct_answer: 'Belgien'
        },
		{
            index: 109,
            question: 'Der wievielte Eurovision Song Contest fand 2005 statt?',
            answers: ['Zwanzigste', 'Fünfundzwanzigste', 'Fünfzigste', 'Fünfundziebzigste'],
            correct_answer: 'Fünfzigste'
        },
		{
            index: 110,
            question: 'Niemand hat die Absicht, eine Mauer zu errichten. <break time="200ms"/>Von wem ist dieses berühmte Satz?',
            answers: ['Helmut Kohl', 'Walter Ulbricht', 'Erich Honecker', 'Erich Mielke'],
            correct_answer: 'Walter Ulbricht'
        },
		{
            index: 111,
            question: 'Wann kam die Mandoline nach Deutschland?',
            answers: ['Fünfzehntes Jahrhundert', 'Siebzehntes Jahrhundert', 'Achtzehntes Jahrhundert', 'Neunzehntes Jahrhundert'],
            correct_answer: 'Achtzehntes Jahrhundert'
        },
		{
            index: 112,
            question: 'Strangers In The Night, war ein Riesenerfolg von?',
            answers: ['Bing Crosby', 'Frank Sinatra', 'Johny Cash', 'Kurt Cobain'],
            correct_answer: 'Frank Sinatra'
        },
		{
            index: 113,
            question: 'As Time Goes By, ist ein Song aus dem Film?',
            answers: ['Der Blaue Engel', 'Vom Winde verweht', 'Citizen Kane', 'Casablanca'],
            correct_answer: 'Casablanca'
        },
		{
            index: 114,
            question: 'Mambo Number Five war 1999 ein Welterfolg für Lou?',
            answers: ['Wega', 'Saga', 'Bega', 'Seega'],
            correct_answer: 'Bega'
        },
		{
            index: 115,
            question: 'Wer heiratete neunmal?',
            answers: ['Elizabeth Taylor', 'Greta Garbo', 'Lana Turner', 'Zsa Zsa Gabor'],
            correct_answer: 'Zsa Zsa Gabor'
        },
		{
            index: 116,
            question: 'Wie heißt ein bekannter Schlagertitel von Wolfgang Lippert?',
            answers: ['Helga kommt', 'Erna kommt', 'Petra kommt', 'Lola rennt'],
            correct_answer: 'Erna kommt'
        },
		{
            index: 117,
            question: 'Wie sagt der Engländer für Gast oder Wirtshaus?',
            answers: ['Main', 'Inn', 'Elbe', 'Donau'],
            correct_answer: 'Inn'
        },
		{
            index: 118,
            question: 'Wie viele Mitglieder hatte die Europäische Union EU Anfang 2016?',
            answers: ['Fünfzehn', 'Fünfundzwanzig', 'Achtundzanzig', 'Siebenundvierzig'],
            correct_answer: 'Achtundzanzig'
        },
		{
            index: 119,
            question: 'Von den weltweit 214 Millionen Migranten lebten mit 70 Millionen die meisten in?',
            answers: ['Europa', 'Asien', 'Nordamerika', 'Afrika'],
            correct_answer: 'Europa'
        },
		{
            index: 120,
            question: 'Achtung. Skurrile Frage <break time="200ms"/>Zu welchen Lockmitteln raten Experten, um Mäuse zu fangen?',
            answers: ['Schokocreme und Erdnussbutter', 'Tomatenpesto und Gurke', 'Kaugummi und Honig', 'Zwiebelringe und Senf'],
            correct_answer: 'Schokocreme und Erdnussbutter'
        },
		{
            index: 121,
            question: 'Die meisten Einwohner der Europäischen Union sind?',
            answers: ['Protestanten', 'Muslime', 'Hindus', 'Katholiken'],
            correct_answer: 'Katholiken'
        },
		{
            index: 122,
            question: 'In welchem Staat Europas ist die Wirtschaftskraft pro Kopf am höchsten?',
            answers: ['Deutschland', 'Luxemburg', 'Rumänien', 'Schweden'],
            correct_answer: 'Luxemburg'
        },
		{
            index: 123,
            question: 'In welchem dieser Staaten war die Arbeitslosigkeit der Männer niedriger als die der Frauen?',
            answers: ['Deutschland', 'Irland', 'Großbritannien', 'Griechenland'],
            correct_answer: 'Griechenland'
        },
		{
            index: 124,
            question: 'Welche Fremdsprache lernen die Schüler EU-weit am häufigsten?',
            answers: ['Deutsch', 'Französisch', 'Englisch', 'Spanisch'],
            correct_answer: 'Englisch'
        },
		{
            index: 125,
            question: 'Wo wird am meisten Energie pro Kopf verbraucht?',
            answers: ['Deutschland', 'Estland', 'Island', 'Bulgarien'],
            correct_answer: 'Island'
        },
		{
            index: 126,
            question: 'Wann wurde die Mauer in Berlin niedergerissen?',
            answers: ['Neunzehnhundertneunzig', 'Neunzehnhundertneunundachtzig', 'Neunzehnhunderteinundneunzig', 'Neunzehnhundertachtundachtzig'],
            correct_answer: 'Neunzehnhundertneunundachtzig'
        },
		{
            index: 127,
            question: 'Wann griff Japan Pearl Harbour an?',
            answers: ['Neunzehnhundertdreiundvierzig', 'Neunzehnhunderteinundvierzig', 'Neunzehnhundertvierundvierzig', 'Neunzehnhundertfünfundvierzig'],
            correct_answer: 'Neunzehnhunderteinundvierzig'
        },
		{
            index: 128,
            question: 'Wann ging die Titanic unter?',
            answers: ['Neunzehnhundertzehn', 'Neunzehnhundertelf', 'Neunzehnhundertzwölf', 'Neunzehnhundertdreizehn'],
            correct_answer: 'Neunzehnhundertzwölf'
        },
		{
            index: 129,
            question: 'Wie heisst die Griechische Göttermutter?',
            answers: ['Artemis', 'Hestia', 'Hera', 'Athene'],
            correct_answer: 'Hera'
        },
		{
            index: 130,
            question: 'Achtung. Skurrile Frage <break time="200ms"/>Womit lässt sich eine hohe, schmale Glasvase leicht von innen reinigen?',
            answers: ['Vogelsand und Föhn', 'Putzschwamm und Magnete', 'Backpapier und Milchaufschäumer', 'Serviette und Murmel'],
            correct_answer: 'Putzschwamm und Magnete'
        },
		{
            index: 131,
            question: 'Welcher Zwergstaat befindet sich zwischen Frankreich und Spanien?',
            answers: ['Liechtenstein', 'Tonga', 'Bahrain', 'Andorra'],
            correct_answer: 'Andorra'
        },
		{
            index: 132,
            question: 'An welchem Fluss liegt Kalkutta?',
            answers: ['Nil', 'Ganges', 'Mississippi', 'Wolga'],
            correct_answer: 'Ganges'
        },
		{
            index: 133,
            question: 'Was frisst ein Mungo?',
            answers: ['Affen', 'Eier', 'Früchte', 'Schlangen'],
            correct_answer: 'Schlangen'
        },
		{
            index: 134,
            question: 'Was macht fast 90% des Energieverbrauchs eines deutschen Haushalts aus?',
            answers: ['Kühlgeräte', 'Heizung und Warmwasser', 'Licht', 'Wasch und Spülmaschine'],
            correct_answer: 'Heizung und Warmwasser'
        },
		{
            index: 135,
            question: 'Was ist ein Lori?',
            answers: ['Zug', 'Papagei', 'Fluss', 'Kaffeesorte'],
            correct_answer: 'Papagei'
        },
		{
            index: 136,
            question: 'Wo fanden im Jahre 1996 die olympischen Sommerspiele statt?',
            answers: ['Paris', 'Italien', 'Atlanta', 'Russland'],
            correct_answer: 'Atlanta'
        },
		{
            index: 137,
            question: 'Was ist Dekathlon?',
            answers: ['Zehnkampf', 'Impfstoff', 'Krankheit', 'Rezept'],
            correct_answer: 'Zehnkampf'
        },
		{
            index: 138,
            question: 'Was ist ein Paso Doble?',
            answers: ['Nudelgericht', 'Spanischer Tanz', 'Eissorte', 'Gebäck'],
            correct_answer: 'Spanischer Tanz'
        },
		{
            index: 139,
            question: 'Welche Säugetiere können fliegen?',
            answers: ['Wale', 'Fliegende Fische', 'Pinguine', 'Fledermäuse'],
            correct_answer: 'Fledermäuse'
        },
		{
            index: 140,
            question: 'Achtung. Skurrile Frage <break time="200ms"/>Was lässt sich im Haushalt durch Brennnesseln ersetzen? ',
            answers: ['Fensterreiniger', 'Lorbeerblatt', 'Imprägniermittel', 'Abführmittel'],
            correct_answer: 'Fensterreiniger'
        },
		{
            index: 141,
            question: 'In welchem Monat darf man Edelweiss pflücken?',
            answers: ['Immer', 'März', 'September', 'Nie'],
            correct_answer: 'Nie'
        },
		{
            index: 142,
            question: 'Wer erfand den Blitzableiter?',
            answers: ['Levi Strauss', 'Benjamin Franklin', 'Graham Bel', 'Albert Einstein'],
            correct_answer: 'Benjamin Franklin'
        },
		{
            index: 143,
            question: 'Wer erfand die Dampfmaschine?',
            answers: ['Werner Siemens', 'Nikola Tesla', 'Henry Ford', 'James Watt'],
            correct_answer: 'James Watt'
        },
		{
            index: 144,
            question: 'Welche Lebensrettende Medizin verdanken wir Alexander Flemming?',
            answers: ['Antibiotika', 'Aspirin', 'Antibabypille', 'Penicillin'],
            correct_answer: 'Penicillin'
        },
		{
            index: 145,
            question: 'Was ist Halma?',
            answers: ['Gebäck', 'Getränk', 'Sportart', 'Brettspiel'],
            correct_answer: 'Brettspiel'
        },
		{
            index: 146,
            question: 'Wie viele Karten braucht man zum Bridgespielen?',
            answers: ['Zweiundfünfzig', 'Zweiunddreizig', 'Vierundfünfzig', 'Sechsundfünfzig'],
            correct_answer: 'Zweiundfünfzig'
        },
		{
            index: 147,
            question: 'Wie heisst die Lehre von der Sternenkunde?',
            answers: ['Chronologie', 'Astronomie', 'Archäologie', 'Meteorologie'],
            correct_answer: 'Astronomie'
        },
		{
            index: 148,
            question: 'Wie heisst die Lehre von der Zeit?',
            answers: ['Meteorologie', 'Archäologie', 'Astronomie', 'Chronologie'],
            correct_answer: 'Chronologie'
        },
		{
            index: 149,
            question: 'Wie heisst die Lehre von der Schifffahrt?',
            answers: ['Dogmatik', 'Anatomie', 'Nautik', 'Mythologie'],
            correct_answer: 'nautik'
        },
		{
            index: 150,
            question: 'Achtung. Skurrile Frage <break time="200ms"/>Ein japanisches Unternehmen schenkt seinen Mitarbeitern sechs zusätzliche Urlaubstage pro Jahr, wenn sie?',
            answers: ['nicht rauchen', 'immer erreichbar sind', 'auf Fastfood verzichten', 'in Hosen kommen'],
            correct_answer: 'nicht rauchen'
        },
		{
            index: 151,
            question: 'Wo befindet sich die Weihnachtsinsel?',
            answers: ['Bodensee', 'Indischen Ozean', 'Pazifik', 'Atlantik'],
            correct_answer: 'Indischen Ozean'
        },
		{
            index: 152,
            question: 'Was bedeutet Advent?',
            answers: ['Geburt', 'Weihnachtszeit', 'Ankunft', 'Plätzchen'],
            correct_answer: 'Ankunft'
        },
		{
            index: 153,
            question: 'Wann wurde der Adventskalender erfunden?',
            answers: ['Neunzehnhundert', 'Siebzehnhundert', 'Achtzehnhundert', 'Zweitausend'],
            correct_answer: 'Neunzehnhundert'
        },
		{
            index: 154,
            question: 'Wer erfand die Kunst des Buchdrucks?',
            answers: ['Büchele', 'Grimme', 'Siemens', 'Gutenberg'],
            correct_answer: 'Gutenberg'
        },
		{
            index: 155,
            question: 'Welcher Planet hat einen Ring?',
            answers: ['Jupiter', 'Saturn', 'Mars', 'Neptun'],
            correct_answer: 'Saturn'
        },
		{
            index: 156,
            question: 'was ist eine Schabracke?',
            answers: ['Lederschuh', 'Pferdedecke', 'Schimpfwort', 'Schuppen'],
            correct_answer: 'Pferdedecke'
        },
		{
            index: 157,
            question: 'Welcher deutsche Bundeskanzler erhielt den Friedensnobelpreis?',
            answers: ['Konrad Adenauer', 'Helmut Kohl', 'Helmut Schmid', 'Willy Brand'],
            correct_answer: 'Willy Brand'
        },
		{
            index: 158,
            question: 'Welches Land gewann die FußballWeltmeisterschaft im Jahre 1998?',
            answers: ['Italien', 'Frankreich', 'Brasilien', 'Deutschland'],
            correct_answer: 'Frankreich'
        },
		{
            index: 159,
            question: 'Wer schrieb 1897 den Roman. Weihnacht!?',
            answers: ['Karl May', 'Heinrich Mann', 'Thomas Mann', 'Henry Miller'],
            correct_answer: 'Karl May'
        },
		{
            index: 160,
            question: 'Achtung. Skurrile Frage <break time="200ms"/>Um seine Beute unschädlich zu machen, schießt der Stummelfüßer mit …?',
            answers: ['Projektilen aus Holz', 'einem Wasserstrahl', 'Zweikomponentenkleber', 'Gift'],
            correct_answer: 'Zweikomponentenkleber'
        },
		{
            index: 161,
            question: 'Seit wann gibt es keinen Kaiser mehr in Deutschland?',
            answers: ['Neunzehnhundersechzehn', 'Neunzehnhundertacht', 'Neunzehnhundertsechsundzwanzig', 'Neunzehnhundertachtzehn'],
            correct_answer: 'Neunzehnhundertachtzehn'
        },
		{
            index: 162,
            question: 'Wer schrieb das Buch. Mutter Courage?',
            answers: ['Heinrich Kleist', 'Bertolt Brecht', 'Milan Kundera'],
            correct_answer: 'Bertolt Brecht'
        },
		{
            index: 163,
            question: 'Was versteht man unter dem Begriff. Popeline?',
            answers: ['Stoffart', 'Chemische Reaktion', 'Kunststoff', 'Gasgemisch'],
            correct_answer: 'Stoffart'
        },
		{
            index: 164,
            question: 'Was bezeichnet man als Entomologie?',
            answers: ['Vogelkunde', 'Insektenkunde', 'Pflanzenkunde', 'Sternenkunde'],
            correct_answer: 'Insektenkunde'
        },
		{
            index: 165,
            question: 'Wer malte das bekannte Bild Seerosenteich?',
            answers: ['van Gogh', 'Monet', 'Picasso', 'Miró'],
            correct_answer: 'Monet'
        },
		{
            index: 166,
            question: 'Wer erfand 1878 das Mikrophon?',
            answers: ['Werner von Siemens', 'Thomas Edison', 'August Otto', 'Edward Hughes'],
            correct_answer: 'Edward Hughes'
        },
		{
            index: 167,
            question: 'Wie bezeichnet man einen Briefmarkensammler?',
            answers: ['Philatelisten', 'Numismatiker', 'Ornithologe', 'Anguisten'],
            correct_answer: 'Philatelisten'
        },
		{
            index: 168,
            question: 'Was sieht man, wenn man einen „Lycalopex“ beobachtet?',
            answers: ['Fisch', 'Fuchs', 'Stern', 'Vulkan'],
            correct_answer: 'Fuchs'
        },
		{
            index: 169,
            question: 'Welches Land gehört nicht zu Afrika?',
            answers: ['Liberia', 'Sambuta', 'Angola', 'Botswana'],
            correct_answer: 'Sambuta'
        },
		{
            index: 170,
            question: 'Achtung. Skurrile Frage <break time="200ms"/>Welches Gemüse kann im heimischen Garten angepflanzt werden?',
            answers: ['Macaronigewürz', 'Cannellonimöhre', 'Lasagnegurke', 'Spaghettikürbis'],
            correct_answer: 'Spaghettikürbis'
        },
		{
            index: 171,
            question: 'In welcher Stadt wurde Marilyn Monroe geboren?',
            answers: ['Saettle', 'Houston', 'New York', 'Los Angeles'],
            correct_answer: 'Los Angeles'
        },
		{
            index: 173,
            question: 'Welche Farbe hat das Fell eines Eisbären?',
            answers: ['Weiß', 'Grau', 'Gelb', 'Durchsichtig'],
            correct_answer: 'Durchsichtig'
        },
		{
            index: 174,
            question: 'In welcher Farbe strahlt die Sonne auf dem Mond ?',
            answers: ['Blau', 'Rot', 'Gelb', 'Weiß'],
            correct_answer: 'Weiß'
        },
		{
            index: 175,
            question: 'Zu welcher Art gehört die Tomate?',
            answers: ['Frucht', 'Gemüse', 'Obst'],
            correct_answer: 'Frucht'
        },
		{
            index: 176,
            question: 'In wieviel Ländern auf der Erde gilt derzeit Linksverkehr?',
            answers: ['Neunundfünfzig', 'Hundertachtzig', 'Fünfundzwanzig', 'Zweihundert' ],
            correct_answer: 'Neunundfünfzig'
        },
		{
            index: 177,
            question: 'Wie viel Zentimeter ist der Eiffelturm im Sommer größer als im Winter?',
            answers: ['Fünf zentimeter', 'Fünfzehn zentimeter', 'Zehn zentimeter', 'Null zentimeter'],
            correct_answer: 'Fünf zentimeter'
        },
		{
            index: 178,
            question: 'Von welchem Maler stammt das Gemälde, Der Tanz?',
            answers: ['Matisse', 'Picasso', 'van Gogh', 'Chagall'],
            correct_answer: 'Matisse'
        },
		{
            index: 179,
            question: 'Wer oder was ist Kardamom?',
            answers: ['Gewürz', 'Indischer Priester', 'Likör', 'Fischart'],
            correct_answer: 'Gewürz'
        },
		{
            index: 180,
            question: 'Achtung. Skurrile Frage <break time="200ms"/>Was lässt sich beim Campen in der Wildnis mit einer Schere aus einer Getränke und einer Suppendose basteln?',
            answers: ['Ventilator', 'Herd', 'Wasserfilter', 'Telefon'],
            correct_answer: 'Herd'
        },
		{
            index: 181,
            question: 'Wer spielte den dicken Bärtigen in, Vier Fäuste für ein Halleluja?',
            answers: ['Orson Welles', 'Mario Adorf', 'Bruce Willis', 'Bud Spencer'],
            correct_answer: 'Bud Spencer'
        },
		{
            index: 182,
            question: 'Welches Tier ist auf dem Landeswappen von Brandenburg?',
            answers: ['Grüner Frosch', 'Roter Adler', 'Weiße Maus', 'Gelber Hund'],
            correct_answer: 'Roter Adler'
        },
		{
            index: 183,
            question: 'Was versteht man unter einer Phobie?',
            answers: ['Übertriebene Hygiene', 'Homosexualität bei Tieren', 'Krankhafte Angst', 'Aberglaube'],
            correct_answer: 'Krankhafte Angst'
        },
		{
            index: 184,
            question: 'Hoppe, Hoppe Reiter, wenn er fällt, dann?',
            answers: ['Fällt er', 'Singt er', 'Schreit er', 'Plumpst er'],
            correct_answer: 'Schreit er'
        },
		{
            index: 185,
            question: 'Ene mene miste, es rappelt in der?',
            answers: ['Piste', 'Kiste', 'Liste', 'Eisenbahn'],
            correct_answer: 'Kiste'
        },
		{
            index: 186,
            question: 'Wie nennt man den Wasserstrahl eines Springbrunnens?',
            answers: ['Lava', 'Fondue', 'Fontäne', 'Fontanelle'],
            correct_answer: 'Fontäne'
        },
		{
            index: 187,
            question: 'Wer war Christoph Kolumbus?',
            answers: ['Maler', 'Dichter', 'Seefahrer', 'Physiker'],
            correct_answer: 'Seefahrer'
        },
		{
            index: 188,
            question: 'Wie nennt man die Fangarme einer Qualle?',
            answers: ['Krallen', 'Langusten', 'Schrimps', 'Tentakel'],
            correct_answer: 'Tentakel'
        },
		{
            index: 189,
            question: 'Wie bezeichnet man getrocknetes Gras?',
            answers: ['Gras', 'Heu', 'Laub', 'Schilf'],
            correct_answer: 'Heu'
        },
		{
            index: 190,
            question: 'Achtung. Skurrile Frage <break time="200ms"/>Laut einer Studie der Universität Duisburg-Essen wirken?',
            answers: ['bunte Hochhäuser höher', 'schwarze Hunde ungefährlicher', 'weiße Autos leiser', 'dunkle Haare länger'],
            correct_answer: 'weiße Autos leiser'
        },
		{
            index: 191,
            question: 'Was für ein Vogel ist das hässliche Entlein, aus dem Märchen von Hans Christian Andersen?',
            answers: ['Ente', 'Gans', 'Schwan', 'Huhn'],
            correct_answer: 'Schwan'
        },
		{
            index: 192,
            question: 'Woraus wird Sauerkraut gemacht?',
            answers: ['Weißkohl', 'Rotkohl', 'Sauerkrautpflanze', 'Eisbergsalat'],
            correct_answer: 'Weißkohl'
        },
		{
            index: 193,
            question: 'Welcher Monat hat nie 30 Tage?',
            answers: ['März', 'Juni', 'Februar', 'September'],
            correct_answer: 'Februar'
        },
		{
            index: 194,
            question: 'In welcher Stadt steht das Brandenburger Tor?',
            answers: ['Brandenburg', 'Berlin', 'Leipzig', 'Dresden'],
            correct_answer: 'Berlin'
        },
		{
            index: 195,
            question: 'Michael Herbig wurde bekannt durch?',
            answers: ['Explosiv', 'Die Bully Parade', 'Talk im Turm', 'Die Bully Show'],
            correct_answer: 'Die Bully Parade'
        },
		{
            index: 196,
            question: 'Wie heißt ein Hörspiel von Karl Valentin?',
            answers: ['Der Torwart', 'Der Kassenwart', 'Der Hauswart', 'Der Notenwart'],
            correct_answer: 'Der Notenwart'
        },
		{
            index: 197,
            question: 'Wie heißt die 1997 von der Sängerin Shakira gegründet Stiftung auf deutsch?',
            answers: ['Hemdlos Stiftung', 'Handschuh Stiftung', 'Barfuß Stiftung', 'Hosen Stiftung'],
            correct_answer: 'Barfuß Stiftung'
        },
		{
            index: 198,
            question: 'Wie nennt man den Sarg, in dem die Ägypter ihre Könige bestatteten?',
            answers: ['Container', 'Sarkophag', 'Schatztruhe', 'Tupperware'],
            correct_answer: 'Sarkophag'
        },
		{
            index: 199,
            question: 'Welche dieser Pflanzen ist ein Nachtschattengewächs?',
            answers: ['Grünkohl', 'Tomate', 'Porree', 'Möhre'],
            correct_answer: 'Tomate'
        },
		{
            index: 200,
            question: 'Achtung. Skurrile Frage <break time="200ms"/>Wer war der jüngste Beatle?',
            answers: ['John Lennon', 'George Harrison', 'Paul McCartney', 'Ringo Starr'],
            correct_answer: 'George Harrison'
        },
		{
            index: 201,
            question: 'Was wächst in vielen Gärten?',
            answers: ['Leihmütterchen', 'Stiefmütterchen', 'Rabenmütterchen', 'Puffmütterchen'],
            correct_answer: 'Stiefmütterchen'
        },
		{
            index: 202,
            question: 'Was ist ein klassisches Trainingsgerät für Boxer?',
            answers: ['Reck', 'Korb', 'Bock', 'Punchingball'],
            correct_answer: 'Punchingball'
        },
		{
            index: 203,
            question: 'Welches Land gewann 2010 erstmals den Davis Cup?',
            answers: ['Österreich', 'Schweiz', 'Serbien', 'Kroatien'],
            correct_answer: 'Serbien'
        },
		{
            index: 204,
            question: 'Was findet man oft als Verdickungsmittel in Ketchup?',
            answers: ['Xenon', 'Saccharin', 'Aspartam', 'Xanthan'],
            correct_answer: 'Xanthan'
        },
		{
            index: 205,
            question: 'Was ist Aspartam?',
            answers: ['Süssungsmittel', 'Säuerrungsmittel', 'Farbstoff', 'Bindemittel'],
            correct_answer: 'Süssungsmittel'
        },
		{
            index: 206,
            question: 'Wer war 1995 Englands Fußballer des Jahres?',
            answers: ['Christian Ziege', 'Mehmet Scholl', 'Jörg Albertz', 'Jürgen Klinsmann'],
            correct_answer: 'Jürgen Klinsmann'
        },
		{
            index: 207,
            question: 'Bei welcher Krankheit hat man, die Motten?',
            answers: ['Gelbsucht', 'Hirnhautentzündung', 'Bandscheibenvorfall', 'Tuberkulose'],
            correct_answer: 'Tuberkulose'
        },
		{
            index: 208,
            question: 'An welchem Casting nahm Bill Kaulitz mit 13 Jahren teil?',
            answers: ['D.S.D.S.', 'Popstars', 'Star Search', 'Music für Kids'],
            correct_answer: 'Star Search'
        },
		{
            index: 209,
            question: 'Wie heißt der chemische Wirkstoff, mit dem Haare blondiert werden?',
            answers: ['Schauma', 'Gard', 'Loreal', 'Wasserstoffperoxid'],
            correct_answer: 'Wasserstoffperoxid'
        },
		{
            index: 210,
            question: 'Achtung. Skurrile Frage <break time="200ms"/>Welches dieser Haushaltsgeräte findet sich nach Kühlschrank und Waschmaschine am häufigsten in deutschen Haushalten?',
            answers: ['Geschirrspülmaschine', 'Kaffeemaschine', 'Toaster', 'Mikrowelle'],
            correct_answer: 'Kaffeemaschine'
        },
		{
            index: 211,
            question: 'In welchem Minutentakt kreist die Internationale Raumstation ISS, ca um die Erde?',
            answers: ['Hundertdreizig Minuten', 'Vierundzwanzig Minuten', 'Zweihunderfünf Minuten', 'Dreiundneunzig Minuten'],
            correct_answer: 'Dreiundneunzig Minuten'
        },
		{
            index: 212,
            question: 'In welchem Land wurde James Dean geboren?',
            answers: ['USA', 'Frankreich', 'Schotland', 'Australien'],
            correct_answer: 'USA'
        },
		{
            index: 213,
            question: 'In welcher österreichischen Stadt spielt der Film „The Sound of Music“?',
            answers: ['Linz', 'Wien', 'Salzburg', 'Innsbruck'],
            correct_answer: 'Salzburg'
        },
		{
            index: 214,
            question: 'Aus wie viel Prozent Kohlenhydraten sollte die Nahrung eines Ausdauersportlers bestehen?',
            answers: ['Zehn Prozent', 'Zwanzig Prozent', 'Fünzig Prozent', 'Sechzig Prozent'],
            correct_answer: 'Sechzig Prozent'
        },
		{
            index: 215,
            question: 'Wer führte bei dem Film „The Sound of Music“ Regie?',
            answers: ['Robert Wise', 'Baz Luhrmann', 'Robert Stevenson', 'Gerorge Cukor'],
            correct_answer: 'Robert Wise'
        },
		{
            index: 216,
            question: 'Welcher Planet ist unserer Erde am nächsten?',
            answers: ['Mars', 'Saturn', 'Venus', 'Neptun'],
            correct_answer: 'Venus'
        },
		{
            index: 217,
            question: 'Welcher Salzburger See bildete in „The Sound of Music“ die Film-Kulisse für Marias Hochzeit mit Kapitän von Trapp?',
            answers: ['Jägersee', 'Mondsee', 'Eibensee', 'Böndlsee'],
            correct_answer: 'Mondsee'
        },
		{
            index: 218,
            question: 'Welche der Sportart, verbraucht bei entsprechender Anstrengung, die meisten Kalorien?',
            answers: ['Joggen', 'Radfahren', 'Reiten', 'Schwimmen'],
            correct_answer: 'Joggen'
        },
		{
            index: 219,
            question: 'Der Film “The Sound of Music” erhielt 1966 fünf Oscars. Welche Oscar-Kategorie war nicht dabei?',
            answers: ['Bester Schnitt', 'Beste Filmmusik', 'Beste Regie', 'Beste Hauptdarstellerin'],
            correct_answer: 'Beste Hauptdarstellerin'
        },
		{
            index: 220,
            question: 'Achtung. Skurrile Frage <break time="200ms"/>Womit lässt sich ein Grill oder Lagerfeuer schnell entzünden, auch wenn kein Grillanzünder zur Hand ist?',
            answers: ['Teebeutel und Essigreiniger', 'Laub und Zahnpasta', 'Wattepad und Lippenpflegestift', 'Brillenglas und Taschenlampe'],
            correct_answer: 'Wattepad und Lippenpflegestift'
        },
		{
            index: 221,
            question: 'Wann wurde die Produktion der Automarke Trabant eingestellt?',
            answers: ['Zweitausendeins', 'Zweitausendfünf', 'Neunzehnhunderteinundneunzig', 'Neunzehnhundertfünfundneunzig'],
            correct_answer: 'Neunzehnhunderteinundneunzig'
        },
		{
            index: 222,
            question: 'Wie nennt man die Sicherheitsvorrichtung im Auto, die sich bei einem Aufprall aufbläst?',
            answers: ['Airbus', 'Airbag', 'Airforce', 'Airball'],
            correct_answer: 'Airbag'
        },
		{
            index: 223,
            question: 'Welche der folgenden Früchte ist die Fetthaltigste?',
            answers: ['Banane', 'Weintraube', 'Eierpflaume', 'Avocado'],
            correct_answer: 'Avocado'
        },
		{
            index: 224,
            question: 'Wie viele Kilokalorien braucht ein gesunder Mensch Durchschnittlich pro Tag?',
            answers: ['Zweitausendfünfhundert', 'Eintausendachthundert', 'Eintausendfünfhundert', 'Viertausend'],
            correct_answer: 'Zweitausendfünfhundert'
        },
		{
            index: 225,
            question: 'Was ist Kardamom?',
            answers: ['Nudelgericht', 'Fisch', 'Gewürz', 'Wein'],
            correct_answer: 'Gewürz'
        },
		{
            index: 226,
            question: 'Wie bezeichnet man das leichte Aromatisieren von Süßspeisen mit Alkohol?',
            answers: ['Parfümieren', 'Alkieren', 'Kuvertieren', 'Balsamieren'],
            correct_answer: 'Parfümieren'
        },
		{
            index: 227,
            question: 'Was ist E 202?',
            answers: ['Gewürz', 'Farbstoff', 'Konservierungsmittel', 'Medikament'],
            correct_answer: 'Konservierungsmittel'
        },
		{
            index: 228,
            question: 'Wie bezeichnet man ein durchgebratenes Steak?',
            answers: ['Well Done', 'Rare', 'Englisch', 'Medium'],
            correct_answer: 'Well Done'
        },
		{
            index: 229,
            question: 'Die Rebsorte: "Roter Veltliner" ist...?',
            answers: ['Rosé', 'Rot', 'Weiß', 'Gibt es nicht'],
            correct_answer: 'Weiß'
        },
		{
            index: 230,
            question: 'Achtung. Skurrile Frage <break time="200ms"/>Was bringt die Flüssigkeiten in Knicklichtern zum Leuchten?',
            answers: ['Strom', 'Essigsäure', 'Seifenlauge', 'Bleichmittel'],
            correct_answer: 'Bleichmittel'
        },
		{
            index: 231,
            question: 'Mit welcher Sprache ist man sprichwörtlich am Ende, wenn man nicht mehr weiter weiß?',
            answers: ['Latein', 'Esperanto', 'Französisch', 'Chinesisch'],
            correct_answer: 'Latein'
        },
		{
            index: 232,
            question: 'Wie lautet der Poesiealbumspruch vollständig, "Marmor, Stein und Eisen bricht?',
            answers: ['Aber unsere Freundschaft nicht', 'Aber unsere Treue nicht', 'Aber unsere Ruhe nicht', 'Aber unsere Ehre nicht'],
            correct_answer: 'Aber unsere Freundschaft nicht'
        },
		{
            index: 233,
            question: 'Wie lautet das Sprichwort richtig? In der Not gehen tausend Freunde auf ein?',
            answers: ['Lot', 'Schlot', 'Boot', 'Brot'],
            correct_answer: 'Lot'
        },
		{
            index: 234,
            question: 'Welcher Vogel kann auch rückwärts fliegen?',
            answers: ['Kolibri', 'Nachtigall', 'Zwergflamingo', 'Schwarzstorch'],
            correct_answer: 'Kolibri'
        },
		{
            index: 235,
            question: 'Wie heißt die Hauptstadt von Kuba?',
            answers: ['Havanna', 'Malé', 'Port Louis', 'Santo Domingo'],
            correct_answer: 'Havanna'
        },
		{
            index: 236,
            question: 'Wie heißt die Hauptstadt von Katar?',
            answers: ['Doha', 'Malé', 'Majuro', 'Palikir'],
            correct_answer: 'Doha'
        },
		{
            index: 237,
            question: 'Welcher Name ist für eine Kohlsorte nicht geläufig?',
            answers: ['Rotkohl', 'Grünkohl', 'Weißkohl', 'Gelbkohl'],
            correct_answer: 'Gelbkohl'
        },
		{
            index: 238,
            question: 'Blume des Jahres 2005 war der Große...?',
            answers: ['Klappertopf', 'Scheppertopf', 'Friesentopf', 'Kleintopf'],
            correct_answer: 'Klappertopf'
        },
		{
            index: 239,
            question: 'Welche der folgenden Gemüsepflanzen ist nicht giftig?',
            answers: ['Möhre', 'Holunder', 'Gartenbohne', 'Kartoffel'],
            correct_answer: 'Möhre'
        },
		{
            index: 240,
            question: 'Welcher Nadelbaum wirft im Winter seine Nadeln ab?',
            answers: ['Lärche', 'Fichte', 'Blautanne', 'Eibe'],
            correct_answer: 'Lärche'
        },
		{
            index: 241,
            question: 'Was ist ein Boskop?',
            answers: ['Apfelsorte', 'Veilchenart', 'Birnensorte', 'Kaktusart'],
            correct_answer: 'Apfelsorte'
        },
		{
            index: 242,
            question: 'In welcher geologischen Zeit traten die ersten Landpflanzen auf?',
            answers: ['Silur', 'Karbon', 'Trias', 'Jura'],
            correct_answer: 'Silur'
        },
		{
            index: 243,
            question: 'Wie heißt eine Orchidee mit ährenartigem Blütenstand ?',
            answers: ['Nestklee', 'Nestwurz', 'Nestkraut', 'Nestwicke'],
            correct_answer: 'Nestwurz'
        },
		{
            index: 244,
            question: 'Wie heißt die ganze Kaffeefrucht?',
            answers: ['Kaffeebohne', 'Kaffeekirsche', 'Kaffeehülse', 'Kaffeefeige'],
            correct_answer: 'Kaffeekirsche'
        },
		{
            index: 245,
            question: 'Silvaner ist eine...?',
            answers: ['Apfelsorte', 'Rebsorte', 'Rosensorte', 'Himbeersorte'],
            correct_answer: 'Rebsorte'
        },
		{
            index: 246,
            question: 'Welche der genannten Pflanzen ist eine nahe Verwandte des Spargels?',
            answers: ['Maiglöckchen', 'Schlüsselblume', 'Vergissmeinnicht', 'Stiefmütterchen'],
            correct_answer: 'Maiglöckchen'
        },
		{
            index: 247,
            question: 'Wo befindet sich das größte Kohlanbaugebiet Europas?',
            answers: ['Rendsburg', 'Nordfriesland', 'Ostholstein', 'Dithmarschen'],
            correct_answer: 'Dithmarschen'
        },
		{
            index: 248,
            question: 'Für welches chemische Element steht das Symbol "Te" im Periodensystem?',
            answers: ['Silber', 'Kupfer', 'Tellur', 'Aluminium'],
            correct_answer: 'Tellur'
        },
		{
            index: 249,
            question: 'In welchem Jahr erschien die bekannte "Hundertwasser-Bibel"?',
            answers: ['Neunzehnhundertfünfundneunzig', 'Zweitausend', 'Neunzehnhundertneunzig', 'Neunzehnhundertfünfundachtzig'],
            correct_answer: 'Neunzehnhundertfünfundneunzig'
        },
		{
            index: 250,
            question: 'Welche der Hunderassen zählt zu einer der Größten?',
            answers: ['Greyhound', 'Foxterrier', 'Schäferhund', 'Malamut'],
            correct_answer: 'Greyhound'
        },
        {
            index: 251,
            question: 'Wie bezeichnet man den Urknall noch?',
            answers: ['Big Mäc', 'Big Band', 'Big Ben', 'Big Bang'],
            correct_answer: 'Big Bang'
        },
        {
            index: 252,
            question: 'In welcher der folgenden Städte ist Französisch nicht die offizielle Sprache?',
            answers: ['Genf', 'Paris', 'Nizza', 'Brig'],
            correct_answer: 'Brig'
        },
        {
            index: 253,
            question: 'Das Grab Tutanchamuns befindet sich im sogenannten Tal der Könige in der Nähe von Luxor. Wieviele Gräber wurden dort bisher gefunden?',
            answers: ['Sechzehn', 'Vierunddreißig', 'Dreiundsechzig', 'Hundertdreiundvierzig'],
            correct_answer: 'Dreiundsechzig'
        },
        {
            index: 254,
            question: 'Zu welchem Sternbild gehört der Stern Kochab?',
            answers: ['Großer Wagen', 'Kleiner Wagen', 'Löwe', 'Orion'],
            correct_answer: 'Kleiner Wagen'
        },
        {
            index: 255,
            question: 'Ein Orientalisches Wasserschöpfwerk nennt man?',
            answers: ['Noria', 'Okka', 'Alkazar', 'Ras'],
            correct_answer: 'Noria'
        },
        {
            index: 256,
            question: 'Womit zahlt man in Thailand?',
            answers: ['Baht', 'Stubbe', 'Kella', 'Fluhr'],
            correct_answer: 'Baht'
        },
        {
            index: 257,
            question: 'Wer konnte der Sage nach als Einziger das Rätsel der Sphinx lösen?',
            answers: ['Herakles', 'Orest', 'Theseus', 'Ödipus'],
            correct_answer: 'Ödipus'
        },
        {
            index: 258,
            question: 'Wann unternahm die Raumfähre "Columbia" ihren Jungfernflug?',
            answers: ['Neunzehnhundertneunundziebzig', 'Neunzehnhundertachtzig', 'Neunzehnhunderteinundachtzig', 'Neunzehnhundertzweiundachtzig'],
            correct_answer: 'Neunzehnhunderteinundachtzig'
        },
        {
            index: 259,
            question: 'Bezeichnet man die Himmelskunde oder die Sterndeutung als Astrologie?',
            answers: ['Himmelskunde', 'Sterndeutung'],
            correct_answer: 'Sterndeutung'
        },
        {
            index: 260,
            question: 'Welche Farbe hat der Große Fleck auf dem Jupiter?',
            answers: ['Blau', 'Gelb', 'Grün', 'Rot'],
            correct_answer: 'Rot'
        },
        {
            index: 261,
            question: 'Welches Hauptwerk verfasste Jonathan Swift?',
            answers: ['In achtzig Tagen um die Welt', 'Leviathan', 'Gullivers Reisen', 'Moby Dick'],
            correct_answer: 'Gullivers Reisen'
        },
        {
            index: 262,
            question: 'Wo wurde 1999 die berühmte Himmelsscheibe entdeckt?',
            answers: ['Berlin', 'Nebra', 'Bad Sachsa', 'Bad Düben'],
            correct_answer: 'Nebra'
        },
        {
            index: 263,
            question: 'Welcher Stern liegt nicht auf der Südhalbkugel?',
            answers: ['Aldebara', 'Rigel', 'Sirius', 'Antares'],
            correct_answer: 'Aldebara'
        },
        {
            index: 264,
            question: 'Das Zitat, Gut gebrüllt Löwe. Stammt von?',
            answers: ['Alfons Haider', 'Bruno Bruni', 'William Shakespeare'],
            correct_answer: 'William Shakespeare'
        },
        {
            index: 265,
            question: 'Wie heißt der Vater des griechischen Gottes Zeus?',
            answers: ['Uranos', 'Krones', 'Ares'],
            correct_answer: 'Krones'
        },
        {
            index: 266,
            question: 'Wie nennt man kostenlose und voll einsatzfähige Software?',
            answers: ['Shareware', 'Netware', 'Freeware', 'Demo Software'],
            correct_answer: 'Freeware'
        },
        {
            index: 267,
            question: 'Wohin reiste Erich Honecker ins Exil?',
            answers: ['Argentinien', 'Russland', 'Kuba', 'Chile'],
            correct_answer: 'Chile'
        },
        {
            index: 268,
            question: 'Dieter Bohlen und Verona Feldbusch heirateten in?',
            answers: ['Wien', 'New York', 'Nonolulu', 'Las Vegas'],
            correct_answer: 'Las Vegas'
        },
        {
            index: 269,
            question: 'In welcher Weltregion wird sich die Bevölkerungszahl bis 2060 reduzieren?',
            answers: ['Asien', 'Europa', 'Afrika', 'Nordamerika'],
            correct_answer: 'Europa'
        },
        {
            index: 270,
            question: 'Auf welcher Insel-Gruppe liegt Pearl Harbor?',
            answers: ['Honolulu', 'Grenada', 'Hawaii', 'Monito'],
            correct_answer: 'Hawaii'
        },
        {
            index: 271,
            question: 'Wo lebte Pythagoras?',
            answers: ['Rom', 'Griechenland', 'Ägypten', 'Indien'],
            correct_answer: 'Griechenland'
        },
        {
            index: 272,
            question: 'Welches Tier war 1984 Organspender für das Herz eines Babies?',
            answers: ['Pavian', 'Ferkel', 'Ponny', 'Schaf'],
            correct_answer: 'Pavian'
        },
        {
            index: 273,
            question: 'Wer war Franz Kafka?',
            answers: ['Sänger', 'Schriftsteller', 'Politiker', 'Dichter'],
            correct_answer: 'Schriftsteller'
        },
        {
            index: 274,
            question: 'Welchen Teil des menschlichen Körpers nennt man „Humerus“?',
            answers: ['Handwurzel', 'Schienbein', 'Kniescheibe', 'Oberarmknochen'],
            correct_answer: 'Oberarmknochen'
        },
        {
            index: 275,
            question: 'Tokio liegt in?',
            answers: ['Vietnam', 'China', 'Japan', 'Korea'],
            correct_answer: 'Japan'
        },
        {
            index: 276,
            question: 'Wer hat Schneewittchen den vergifteten Apfel gegeben?',
            answers: ['Tante', 'Onkel', 'Stiefmutter', 'Stiefschwester'],
            correct_answer: 'Stiefmutter'
        },
        {
            index: 277,
            question: 'Für welche seltene Tierrasse setzt sich der Unternehmer Günther Fielmann ein?',
            answers: ['Mantelschwein', 'Seidenhuhn', 'Gürtelgans', 'Brillenschaf'],
            correct_answer: 'Brillenschaf'
        },
        {
            index: 278,
            question: 'Welcher Bestandteil des Koriander wird überwiegend in der deutschen Küche verwendet?',
            answers: ['Samen', 'Stengel', 'Blätter', 'Wurzel'],
            correct_answer: 'Blätter'
        },
        {
            index: 279,
            question: 'Wo befindet sich Mitteleuropas größter Flugzeugfriedhof?',
            answers: ['Tschechien', 'Polen', 'Schweden', 'Frankreich'],
            correct_answer: 'Tschechien'
        }
    ]
});
