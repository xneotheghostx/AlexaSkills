'use strict';
/*
 * Copyright 2018 Amazon.com, Inc. and its affiliates. All Rights Reserved.
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * http://aws.amazon.com/asl/
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */


/**
 * Questions library
 * 
 * Use this file to create your own set of questions.
 * 
 * Object properties:
 *      index:          The index of the question in this list
 *      question:       The question you want Alexa to ask
 *      answers:        The list of available answers
 *      correct_answer: The correct answer to the question
 * 
 * When adding or updating questions and answers, you must take the list of all values 
 * in each of the 'answers' arrays for all questions and add them to a custom slot 
 * in your skill called 'answers'.
 * 
 * The 'answers' custom slot is be mapped to a couple of intents in the interaction model.
 * One intent, named 'AnswerOnlyIntent', contains only the slot, by itself, in order 
 * to maximize the accuracy of the model.
 * 
 * For example: 
 *      AnswerOnlyIntent {answers}
 * 
 * The other intent, 'AnswerQuestionIntent', provides more complex speech patterns
 * to match other utternaces users may include with their answers.
 * 
 * For example:
 *      AnswerQuestionIntent is it {answers}
 *      AnswerQuestionIntent it is {answers}
 *      AnswerQuestionIntent the answer is {answers}
 *      AnswerQuestionIntent I think the answer is {answers}
 * 
 * See model file at models/de-DE.json for a complete example.
 */
module.exports = Object.freeze({ 
    questions: [
        {
            index: 1,
            question: 'Ich sage, dass die Währung vor dem Euro, Deutsche Mark hieß! ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Die D-Mark war von 1948 bis 2001 die offizielle Währung in der Bundesrepublik Deutschland. '
        },
        {
            index: 2,
            question: 'Die Erde hat einen Umfang von 38 Tausend Kilometern? ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Der Umfang der Erde ist 40.075 Kilimeter. '
        },
        {
            index: 3,
            question: 'Ich glaube Heidschnucken ist ein Hefegebäck. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Heidschnucken sind eine höchst genügsame Schafrasse. '
        },
        {
            index: 4,
            question: 'Ich kann rechnen. 5 + 10 x 7 sind 105.',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Das Erbnis lautet 75, denn es heist punkt vor Strichrechnung. '
        },
        {
            index: 5,
            question: 'Ein Kilometer sind 1.000 Meter. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Ein Kilometer sind 1.000 Meter. '
        },
        {
            index: 6,
            question: 'Wenn ich einen Regenwurm teile, entstehen zwei Regenwürmer! ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Ein Wurm ohne Schwanz kann überleben ohne Kopf aber nicht. '
        },
        {
            index: 7,
            question: 'Ich behaupte, dass die Insel Föhr größer ist als Sylt! ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Die Insel Föhr ist 82,82 km² und Sylt 99,14 km² '
        },
        {
            index: 8,
            question: 'Rot, Gelb und Blau sind die Primärfarben. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Die Primärfarben oder auch Grundfarben genannt, sind Rot, Grün und Blau. '
        },
        {
            index: 9,
            question: 'In Dänemark heißt die Währung Kronen, darum kann man nicht mit Euro bezahlen. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'In vielen Touristengebieten kann auch mit Euro gezahlt werden. Das Wechselgeld wird jedoch in dänischen Kronen ausgegeben. '
        },
        {
            index: 10,
            question: 'Wir haben 15 Bundesländer in Deutschland. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Es sind 16 Bundesländer. '
        },
        {
            index: 11,
            question: 'Ich glaube, Pinguine sind keine Vögel. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Pinguine sind eine Gruppe flugunfähiger Seevögel der Südhalbkugel. '
        },
        {
            index: 12,
            question: 'Wer war zuerst da. Das Huhn oder das Ei? Ich sage das Huhn!',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Denn der Vorfahre des Huhns hat sich nicht einfach im Laufe seines Lebens verändert und war plötzlich ein Huhn. '
        },
        {
            index: 13,
            question: 'Pass auf. Ich behaupte Tomaten sind Obst! ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Paprika, Tomaten, Kürbisse, Zucchini, Auberginen und Gurken sind zwar Früchte aber gehören laut der botanischen Definition zu Obst. '
        },
        {
            index: 14,
            question: 'Meine Tante Siri behauptet, Lesen bei schlechtem Licht verdirbt die Augen. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Was wahr ist, ist dass es die Augen natürlich stärker anstrengt und ermüden. Schädigungen durch Lesen im Zwielicht, lassen sich nicht nachweisen. '
        },
        {
            index: 15,
            question: 'Ich sage, Hunde stammen von Wölfen ab. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Haushunde und der heutige graue Wolf haben den gleichen Vorfahren. Das war eine Wolfsart, die vor mindestens 45000 Jahren lebte und mittlerweile ausgestorben ist. '
        },
        {
            index: 16,
            question: 'Stimmt es, dass in Deutschland nach dem zweiten Weltkrieg, die heimliche Währung Fleisch war? ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Es wahren Zigaretten. '
        },
        {
            index: 17,
            question: 'Ich sage, wenn du bei der Arbeit aufgrund von arbeitsbedingter Übermüdung einschläft und von Bürostuhl fällst, gilt das als betrieblicher Unfall. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'So urteile das Sozialgerecht in Dortmund. '
        },
        {
            index: 18,
            question: 'Stimmt es das die Farbe der Gelb ist? ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Betrachtet man die Sonne aus dem Weltraum, erscheint sie weiß. Ihre gewohnte gelbe Farbe erklärt sich durch den Einfluss der Erdatmosphäre. '
        },
        {
            index: 19,
            question: 'Ich behaupte, dass der Schatten auf dem Mond, von der Erde kommt! ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Die Schatten der Mondphasen kommt vom Mond selbst. Wir sehen den unbeleuchteten Teil des Mondes. Da der Mond um die Erde kreist, ändert sich das Aussehen des Mondes und des Schattens. '
        },
        {
            index: 20,
            question: 'Ich bin sicher, Afrika ist ein Ort in Deutschland',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Er liegt in der Gemeinde Flieth-Stegelitz im Landkreis Uckermark in Brandenburg. '
        },
        {
            index: 21,
            question: 'Ich glaube, wir nutzen nur einen Bruchteil unserer Hirnkapazitäten.',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Magnetresonanztomographie M.R.T. Untersuchungen haben gezeigt, es gibt keinerlei inaktive Bereiche im Gehirn. '
        },
        {
            index: 22,
            question: 'Ich sage, Haare und Fingernägel wachsen auch nach dem Tod noch weiter.',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Ganzeinfach, was Tot ist kann nicht wachsen. '
        },
        {
            index: 23,
            question: 'Ich behaupte das Putenfleisch müde macht.',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Denn ein Übermaß an jeglichem Fleisch beschäftigt den Magen so sehr, dass der Kopf und der restliche Körper in dieser Zeit weniger gut versorgt. '
        },
        {
            index: 24,
            question: 'Stimmt es, dass das Wetter am Wochenende, laut Statistik eher schlechter als in der Woche ist? ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Statistisch ist es so, dass die Feinstaubbelastung in der Woche zunimmt und der Feinstaub mit zur Wolkenbildung beiträgt. '
        },
        {
            index: 25,
            question: 'Ich habe gehört, in Deutschland ist es erlaubt nackt Autozufahren.',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Aber nackt aussteigen kostet 40 Euro Bußgeld. '
        },
        {
            index: 26,
            question: 'Ich sage, in England kann man alle Briefmarken so aufkleben wie man will. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: ': Briefmarken mit der Königin drauf muss man immer richtig herum kleben. Andernfalls macht man sich strafbar. Dies gilt als Hochverrat und Respektlosigkeit. '
        },
        {
            index: 27,
            question: 'Stimmt es, ursprünglich sollten die Uhren in England andersherum laufen, so wie der Verkehr? ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Das stimmt nicht und der Verkehr läuft auch nicht rückwärts.  '
        },
        {
            index: 28,
            question: 'Ich glaube nicht, das Haare schneller wachsen und dunkler werden, wenn man sie rasiert.',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Der Rasierer verschlimmert nichts. Optisch allerdings hat man schnell den Eindruck, dass Steil stehende Stoppeln auffälliger als flach anliegende feine Härchen sind. '
        },
        {
            index: 29,
            question: 'In Russland ist das tragen eines einzigen grünen Turnschuhs, sofern der andre Fuß nackt ist, verboten.',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Aber du solltest nie deinen Personalausweis vergessen. '
        },
        {
            index: 30,
            question: 'Ich behaupte, in Indien ist die Einreise mit Satellitentelefonen strengstens untersagt.',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Denn die Besitzer können strafrechtlich verfolgt und die Geräte vom Zoll beschlagnahmt werden. '
        },
        {
            index: 31,
            question: 'Ich glaube, das Gehirn einer Katze ist besser als das vom Hund. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Das Hirn der Katze wiegt im Schnitt 25 Gramm, dort arbeiten 300 Mio. Nervenzellen. Ein Hunde-Hirn kommt im Schnitt auf 64 Gramm, aber nur 160 Mio. Nervenzellen. '
        },
        {
            index: 32,
            question: 'Ich sage, Nachtschattengewächse wie Tomaten wachsen vor allem im Dunkeln? ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Im Gegenteil, genau wie andere Nachtschattengewächse wie Auberginen, Paprika oder Tomaten, brauchen sie viel Sonnenlicht. Verregnete Sommer sind eher schlecht. '
        },
        {
            index: 33,
            question: 'Ich habe gehört, Armeisen können bis das Zehnfache ihres eigenen Gewichts tragen. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Armeisen können bei einem Körpergewicht von ca. 9 mg, das 40-fache ihres Körpergewichtes tragen. '
        },
        {
            index: 34,
            question: 'Ich behaupte, auf dem Mond gibt es Wasser. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Forscher haben 2018 einen Nachweis dafür gefunden, das es Eis an beiden Mondpolen im kühlen Schatten von Kratern gibt. '
        },
        {
            index: 35,
            question: ' ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: '',
            correct_info: ' '
        },
        {
            index: 36,
            question: ' ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: '',
            correct_info: ' '
        },
        {
            index: 37,
            question: ' ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: '',
            correct_info: ' '
        },
        {
            index: 38,
            question: ' ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: '',
            correct_info: ' '
        },
        {
            index: 39,
            question: ' ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: '',
            correct_info: ' '
        },
        {
            index: 40,
            question: ' ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: '',
            correct_info: ' '
        }
    ]
});
