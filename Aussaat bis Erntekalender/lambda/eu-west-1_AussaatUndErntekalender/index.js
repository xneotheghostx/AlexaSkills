/* NordicSkills */

const Alexa = require('ask-sdk-core');
const sorten = require('./sorten');
const i18n = require('i18next');
const sprintf = require('i18next-sprintf-postprocessor');

/* INTENT HANDLERS */
const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
  },
  handle(handlerInput) {
    const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

    const item = requestAttributes.t(getRandomItem(Object.keys(sorten.SORTE)));

    const speakOutput = requestAttributes.t('WELCOME_MESSAGE', requestAttributes.t('SKILL_NAME'), item);
    const repromptOutput = requestAttributes.t('WELCOME_REPROMPT');

    handlerInput.attributesManager.setSessionAttributes(sessionAttributes);

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(repromptOutput)
      .getResponse();
  },
};

const SorteHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'SorteIntent';
  },
  handle(handlerInput) {
    const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

    const itemSlot = handlerInput.requestEnvelope.request.intent.slots.Item;
    let itemName;
    if (itemSlot && itemSlot.value) {
      itemName = itemSlot.value.toLowerCase();
    }

    const cardTitle = requestAttributes.t('DISPLAY_CARD_TITLE', requestAttributes.t('SKILL_NAME'), itemName);
    const meineSorten = requestAttributes.t('SORTEN');
    const sorte = meineSorten[itemName];
    let speakOutput = "";

    if (sorte) {
      sessionAttributes.speakOutput = sorte;
      sessionAttributes.repromptSpeech = requestAttributes.t('SORTE_REPEAT_MESSAGE');
      handlerInput.attributesManager.setSessionAttributes(sessionAttributes);
	// Neu für die nachfrage
	  sessionAttributes.speakOutput += " <break time='250ms'/> Und? Womit kann ich dir noch helfen. Wenn nicht, sage Stopp oder beenden. ";
	  sessionAttributes.repromptSpeech += " Kann ich dir noch mit ewas anderem helfen? Wenn nicht, sage Stopp oder beenden. ";
	// Neu für die nachfrage  
      return handlerInput.responseBuilder
        .speak(sessionAttributes.speakOutput) 
	// Anfang für die nachfrage
		.reprompt(sessionAttributes.repromptSpeech)
	// Ende für die nachfrage
        .withSimpleCard(cardTitle, sorte)
        .getResponse();
    }
    else{
      speakOutput = requestAttributes.t('SORTE_NOT_FOUND_MESSAGE');
      const repromptSpeech = requestAttributes.t('SORTE_NOT_FOUND_REPROMPT');
      if (itemName) {
        speakOutput += requestAttributes.t('SORTE_NOT_FOUND_WITH_ITEM_NAME', itemName);
      } else {
        speakOutput += requestAttributes.t('SORTE_NOT_FOUND_WITHOUT_ITEM_NAME');
      }
      speakOutput += repromptSpeech;

      sessionAttributes.speakOutput = speakOutput; //saving speakOutput to attributes, so we can use it to repeat
      sessionAttributes.repromptSpeech = repromptSpeech;

      handlerInput.attributesManager.setSessionAttributes(sessionAttributes);

      return handlerInput.responseBuilder
        .speak(sessionAttributes.speakOutput)
        .reprompt(sessionAttributes.repromptSpeech)
        .getResponse();
    }
  }
};

const HelpHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
  },
  handle(handlerInput) {
    const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

    const item = requestAttributes.t(getRandomItem(Object.keys(sorten.SORTE)));

    sessionAttributes.speakOutput = requestAttributes.t('HELP_MESSAGE', item);
    sessionAttributes.repromptSpeech = requestAttributes.t('HELP_REPROMPT', item);

    return handlerInput.responseBuilder
      .speak(sessionAttributes.speakOutput)
      .reprompt(sessionAttributes.repromptSpeech)
      .getResponse();
  },
};

//Wiederholen anfang
const RepeatHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.RepeatIntent';
  },
  handle(handlerInput) {
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

    return handlerInput.responseBuilder
      .speak(sessionAttributes.speakOutput)
      .reprompt(sessionAttributes.repromptSpeech)
      .getResponse();
  },
};
// Wiederholen ende
const ExitHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent'
                || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent');
  },
  handle(handlerInput) {
    const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
    const speakOutput = requestAttributes.t('STOP_MESSAGE', requestAttributes.t('SKILL_NAME'));

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .getResponse();
  },
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    console.log("Inside SessionEndedRequestHandler");
    return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    console.log(`Session ended with reason: ${JSON.stringify(handlerInput.requestEnvelope)}`);
    return handlerInput.responseBuilder.getResponse();
  },
};


const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);

    return handlerInput.responseBuilder
      .speak('Entschuldigung, Ich konnte dich nicht verstehen. Bitte versuche es noch mal. ')
      .reprompt('Leider konnte ich dich nicht verstehen. Wiederhole es noch einmal oder versuche etwas anderes. ')
      .getResponse();
  },
};

/* CONSTANTS */
const skillBuilder = Alexa.SkillBuilders.custom();
const languageStrings = {
  de: {
    translation: {
      SORTEN: sorten.SORTE,
      SKILL_NAME: 'Aussaat bis Erntekalender',
      WELCOME_MESSAGE: 'Willkommen beim %s. Du kannst beispielsweise die Frage stellen: Wann Ernte ich %s? Nun, was möchtest du wissen? ',
      WELCOME_REPROMPT: 'Wenn du wissen möchtest, was du sagen kannst, sag einfach „Hilf mir“.',
      DISPLAY_CARD_TITLE: '%s - Zeiten und Termine für %s.',
      HELP_MESSAGE: 'Du kannst beispielsweise Fragen stellen wie, „Wann kann ich am besten %s aussäen. Oder, wann Ernte ich Eisbergsalat. Mit wiederholen lese ich dir die Informationen noch einmal vor und zum beenden, sage Beenden oder Stop... Wie kann ich dir helfen? ',
      HELP_REPROMPT: 'Du kannst beispielsweise sagen „Wann kann ich am besten %s“ aussäen. Oder du kannst „Beenden“ sagen ... Wie kann ich dir helfen?',
      STOP_MESSAGE: 'Auf Wiederhören viel Spaß im Garten und eine erfolgreiche Ernte! ',
      SORTE_REPEAT_MESSAGE: 'Sage einfach „Wiederholen“.',
      SORTE_NOT_FOUND_MESSAGE: 'Tut mir leid, ich habe derzeit ',
      SORTE_NOT_FOUND_WITH_ITEM_NAME: 'keine Information über %s. Schicke mir doch bitte eine e-mail mit dem fehlenden Obst oder Gemüse. Danke.',
      SORTE_NOT_FOUND_WITHOUT_ITEM_NAME: 'keine Informationen darüber, oder es ist ein Fehler aufgetreten. ',
      SORTE_NOT_FOUND_REPROMPT: ' Versuche es doch bitte mit etwas anderem.'
    },
  },
};

// Finding the locale of the user
const LocalizationInterceptor = {
  process(handlerInput) {
    const localizationClient = i18n.use(sprintf).init({
      lng: handlerInput.requestEnvelope.request.locale,
      overloadTranslationOptionHandler: sprintf.overloadTranslationOptionHandler,
      resources: languageStrings,
      returnObjects: true
    });

    const attributes = handlerInput.attributesManager.getRequestAttributes();
    attributes.t = function (...args) {
      return localizationClient.t(...args);
    };
  },
};

// getRandomItem
function getRandomItem(arrayOfItems) {
  // the argument is an array [] of words or phrases
  let i = 0;
  i = Math.floor(Math.random() * arrayOfItems.length);
  return (arrayOfItems[i]);
}

/* LAMBDA SETUP */
exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    SorteHandler,
    HelpHandler,
    RepeatHandler,
    ExitHandler,
    SessionEndedRequestHandler
  )
  .addRequestInterceptors(LocalizationInterceptor)
  .addErrorHandlers(ErrorHandler)
  .lambda();
