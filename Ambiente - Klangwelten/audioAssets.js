'use strict';

// Audio Source - https://nordicskills.de/audio/Ambiente//
var audioData = [
    {
        'title': 'Dusche',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Babyhilfe/Dusche.mp3',
        'category': 'Baby Hilfe'
    },
    {
        'title': 'Geschirrspüler',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Babyhilfe/Geschirrspueler.mp3',
        'category': 'Baby Hilfe'
    },
    {
        'title': 'Haartrockner',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Babyhilfe/Haartrockner.mp3',
        'category': 'Baby Hilfe'
    },
    {
        'title': 'Herzschlag',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Babyhilfe/Herzschlag.mp3',
        'category': 'Baby Hilfe'
    },
    {
        'title': 'Staubsauger',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Babyhilfe/Staubsauger.mp3',
        'category': 'Baby Hilfe'
    },
    {
        'title': 'Waschmaschine',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Babyhilfe/Waschmaschine.mp3',
        'category': 'Baby Hilfe'
    },
    {
        'title': 'Gewitter und Regenn',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Blitz_und_Donner/Gewitter_und_Regen.mp3',
        'category': 'Blitz und Donner'
    },
    {
        'title': 'Gewittersturm',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Blitz_und_Donner/Gewittersturm.mp3',
        'category': 'Blitz und Donner'
    },
    {
        'title': 'Sommergewitter',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Blitz_und_Donner/Sommergewitter.mp3',
        'category': 'Blitz und Donner'
    },
    {
        'title': 'Dauerdonner',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Blitz_und_Donner/Dauerdonner.mp3',
        'category': 'Blitz und Donner'
    },
    {
        'title': 'Kirchenglocke im Regenn',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Blitz_und_Donner/Kirchenglocke_im_Regen.mp3',
        'category': 'Blitz und Donner'
    },
    {
        'title': 'Regenn und Donner',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Blitz_und_Donner/Regen_und_Donner.mp3',
        'category': 'Blitz und Donner'
    },
    {
        'title': 'Rollender Donner',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Blitz_und_Donner/Rollender_Donner.mp3',
        'category': 'Blitz und Donner'
    },
    {
        'title': 'Donner mit Starkregen',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Blitz_und_Donner/Donner_mit_Starkregen.mp3',
        'category': 'Blitz und Donner'
    },
    {
        'title': 'Starkregen mit Gewitter',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Blitz_und_Donner/Starkregen_mit_Gewitter.mp3',
        'category': 'Blitz und Donner'
    },
    {
        'title': 'Gaskamin',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Feuer_und_Kamin/Gaskamin.mp3',
        'category': 'Feuer und Kamin'
    },
    {
        'title': 'Holzkamin',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Feuer_und_Kamin/Holzkamin.mp3',
        'category': 'Feuer und Kamin'
    },
    {
        'title': 'Knisterndes Feuer',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Feuer_und_Kamin/Knisternes_Feuer.mp3',
        'category': 'Feuer und Kamin'
    },
    {
        'title': 'Lautes knistern',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Feuer_und_Kamin/Lautes_knistern.mp3',
        'category': 'Feuer und Kamin'
    },
    {
        'title': 'Leichtes knistern',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Feuer_und_Kamin/Leichtes_knistern.mp3',
        'category': 'Feuer und Kamin'
    },
    {
        'title': 'Im Flugzeug',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Fluggeraeusche/Im_Flugzeug.mp3',
        'category': 'Flug Geräusche'
    },
    {
        'title': 'Im Hubschrauber',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Fluggeraeusche/Im_Hubschrauber.mp3',
        'category': 'Flug Geräusche'
    },
    {
        'title': 'Jet Überflug',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Fluggeraeusche/Jet_ueberflug.mp3',
        'category': 'Flug Geräusche'
    },
    {
        'title': 'Flugzeugkabine',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Fluggeraeusche/Flugzeugkabine.mp3',
        'category': 'Flug Geräusche'
    },
    {
        'title': 'Hubschrauber Überflug',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Fluggeraeusche/Hubschrauber_ueberflug.mp3',
        'category': 'Flug Geräusche'
    },
    {
        'title': 'Bach im Wald',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Fluss_und_Baeche/Bach_im_Wald.mp3',
        'category': 'Fluss und Bäche'
    },
    {
        'title': 'Bachplätschern',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Fluss_und_Baeche/Bachplaetschern.mp3',
        'category': 'Fluss und Bäche'
    },
    {
        'title': 'Ein Fluss',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Fluss_und_Baeche/Ein_Fluss.mp3',
        'category': 'Fluss und Bäche'
    },
    {
        'title': 'Fluss im Wald',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Fluss_und_Baeche/Fluss_im_Wald.mp3',
        'category': 'Fluss und Bäche'
    },
    {
        'title': 'Großer Bach',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Fluss_und_Baeche/Grosser_Bach.mp3',
        'category': 'Fluss und Bäche'
    },
    {
        'title': 'Kleiner Fluss',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Fluss_und_Baeche/Kleiner_Fluss.mp3',
        'category': 'Fluss und Bäche'
    },
    {
        'title': 'Plätschern',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Fluss_und_Baeche/Plaetschern.mp3',
        'category': 'Fluss und Bäche'
    },
    {
        'title': 'Grillen in der Nacht',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Grillen_und_Zigaden/Grillen_in_der_Nacht.mp3',
        'category': 'Grillen und Zikaden'
    },
    {
        'title': 'Grillen und Frösche',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Grillen_und_Zigaden/Grillen_und_Froesche.mp3',
        'category': 'Grillen und Zikaden'
    },
    {
        'title': 'Insekten',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Grillen_und_Zigaden/Insekten.mp3',
        'category': 'Grillen und Zikaden'
    },
    {
        'title': 'Im Dschungel',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Grillen_und_Zigaden/Im_Dschungel.mp3',
        'category': 'Grillen und Zikaden'
    },
    {
        'title': 'Zikaden in der Nacht',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Grillen_und_Zigaden/Zikaden_in_der_Nacht.mp3',
        'category': 'Grillen und Zikaden'
    },
    {
        'title': 'Zikaden am Wasser',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Grillen_und_Zigaden/Zikaden_am_Wasser.mp3',
        'category': 'Grillen und Zikaden'
    },
    {
        'title': 'Felsen im Meer',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Meer_und_Strand/Felsen_im_Meer.mp3',
        'category': 'Am Strand'
    },
    {
        'title': 'Große Wellen',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Meer_und_Strand/Grosse_Wellen.mp3',
        'category': 'Am Strand'
    },
    {
        'title': 'Klippenspiel',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Meer_und_Strand/Klippenspiel.mp3',
        'category': 'Am Strand'
    },
    {
        'title': 'Leichte Wellen',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Meer_und_Strand/Leichte_Wellen.mp3',
        'category': 'Am Strand'
    },
    {
        'title': 'Meeresbrandung',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Meer_und_Strand/Meeresbrandung.mp3',
        'category': 'Am Strand'
    },
    {
        'title': 'Möwen am Strand',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Meer_und_Strand/Moewen_am_Strand.mp3',
        'category': 'Am Strand'
    },
    {
        'title': 'Sanfte Wellen',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Meer_und_Strand/Sanfte_Wellen.mp3',
        'category': 'Am Strand'
    },
    {
        'title': 'Weißes Rauschen',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Rauschen/Weisses_Rauschen.mp3',
        'category': 'Rauschen'
    },
    {
        'title': 'Braunes Rauschen',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Rauschen/Braunes_Rauschen.mp3',
        'category': 'Rauschen'
    },
    {
        'title': 'Rosa Rauschen',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Rauschen/Rosa_Rauschen.mp3',
        'category': 'Rauschen'
    },
    {
        'title': 'Platzregen',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Regen_und_Sturm/Platzregen.mp3',
        'category': 'Regenn und Sturm'
    },
    {
        'title': 'Im Auto',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Regen_und_Sturm/Im_Auto.mp3',
        'category': 'Regenn und Sturm'
    },
    {
        'title': 'Unterm Schirm',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Regen_und_Sturm/Unterm_Schirm.mp3',
        'category': 'Regenn und Sturm'
    },
    {
        'title': 'Schwerer Regenn',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Regen_und_Sturm/Schwerer_Regen.mp3',
        'category': 'Regenn und Sturm'
    },
    {
        'title': 'Starker Regenn',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Regen_und_Sturm/Starker_Regen.mp3',
        'category': 'Regenn und Sturm'
    },
    {
        'title': 'Sturm',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Regen_und_Sturm/Sturm.mp3',
        'category': 'Regenn und Sturm'
    },
    {
        'title': 'Wellblech',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Regen_und_Sturm/Wellblech.mp3',
        'category': 'Regenn und Sturm'
    },
    {
        'title': 'Eine Nacht',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Stadtgeraeusche/Eine_Nacht.mp3',
        'category': 'Stadt Geräusche'
    },
    {
        'title': 'Froschteich',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Stadtgeraeusche/Froschteich.mp3',
        'category': 'Stadt Geräusche'
    },
    {
        'title': 'Hintergrund',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Stadtgeraeusche/Hintergrund.mp3',
        'category': 'Stadt Geräusche'
    },
    {
        'title': 'Im Park',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Stadtgeraeusche/Im_Park.mp3',
        'category': 'Stadt Geräusche'
    },
    {
        'title': 'Vögel in der Stadt',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Stadtgeraeusche/Stadtvoegel.mp3',
        'category': 'Stadt Geräusche'
    },
    {
        'title': 'Verkehr',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Stadtgeraeusche/Verkehr.mp3',
        'category': 'Stadt Geräusche'
    },
    {
        'title': 'Vögel im Park',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Stadtgeraeusche/Voegel_im_Park.mp3',
        'category': 'Stadt Geräusche'
    },
    {
        'title': 'Dschungel am Tag',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Tag_und_Nacht/Dsungel_am_Tag.mp3',
        'category': 'Tag und Nacht'
    },
    {
        'title': 'Dschungel in der Nacht',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Tag_und_Nacht/Dsungel_in_der_Nacht.mp3',
        'category': 'Tag und Nacht'
    },
    {
        'title': 'Geräusche in der Nacht',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Tag_und_Nacht/Geraeusche_in_der_Nacht.mp3',
        'category': 'Tag und Nacht'
    },
    {
        'title': 'Morgens im Dorf',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Tag_und_Nacht/Morgens_im_Dorf.mp3',
        'category': 'Tag und Nacht'
    },
    {
        'title': 'Nachts auf der Veranda',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Tag_und_Nacht/Nachts_auf_der_Veranda.mp3',
        'category': 'Tag und Nacht'
    },
    {
        'title': 'Regenwald',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Wald_und_Voegel/Regenwald.mp3',
        'category': 'Wald und Vögel'
    },
    {
        'title': 'Vögel im Wald',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Wald_und_Voegel/Voegel_im_Wald.mp3',
        'category': 'Wald und Vögel'
    },
    {
        'title': 'Vogelschwarm',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Wald_und_Voegel/Vogelschwarm.mp3',
        'category': 'Wald und Vögel'
    },
    {
        'title': 'Vögel am Fluss',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Wald_und_Voegel/Voegel_am_Fluss.mp3',
        'category': 'Wald und Vögel'
    },
    {
        'title': 'Vögel am Morgen',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Wald_und_Voegel/Voegel_am_Morgen.mp3',
        'category': 'Wald und Vögel'
    },
    {
        'title': 'Vögel in Stadtnähe',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Wald_und_Voegel/Voegel_in_Stadtnaehe.mp3',
        'category': 'Wald und Vögel'
    },
    {
        'title': 'Vögel und Grillen',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Wald_und_Voegel/Voegel_und_Grillen.mp3',
        'category': 'Wald und Vögel'
    },
    {
        'title': 'Pool befüllen',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Wassergeraeusche/Pool_befuellen.mp3',
        'category': 'Wasser Geräusche'
    },
    {
        'title': 'Abtauchen',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Wassergeraeusche/Abtauchen.mp3',
        'category': 'Wasser Geräusche'
    },
    {
        'title': 'Unter Wasser',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Wassergeraeusche/Unterwasser.mp3',
        'category': 'Wasser Geräusche'
    },
    {
        'title': 'Wassertropfen',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Wassergeraeusche/Wassertropfen.mp3',
        'category': 'Wasser Geräusche'
    },
    {
        'title': 'Tropfsteinhöhle',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Wassergeraeusche/Tropfsteinhoehle.mp3',
        'category': 'Wasser Geräusche'
    },
    {
        'title': 'Windspiel eins',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Windspiele/Windspiel_eins.mp3',
        'category': 'Windspiele'
    },
    {
        'title': 'Windspiel zwei',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Windspiele/Windspiel_zwei.mp3',
        'category': 'Windspiele'
    },
    {
        'title': 'Windspiel drei',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Windspiele/Windspiel_drei.mp3',
        'category': 'Windspiele'
    },
    {
        'title': 'New York',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Zugfahrten/NewYork_Ubahn.mp3',
        'category': 'Zugfahrten'
    },
    {
        'title': 'Zug vorbeifahrt',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Zugfahrten/Zug_vorbeifahrt.mp3',
        'category': 'Zugfahrten'
    },
    {
        'title': 'Zugfahrt mit poltern',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Zugfahrten/Zugfahrt_mit_poltern.mp3',
        'category': 'Zugfahrten'
    },
    {
        'title': 'Schienen kratzen',
        'url': 'https://nordicskills.de/audio/Ambiente/Geraeusche/Zugfahrten/Schienenkratzen.mp3',
        'category': 'Zugfahrten'
    }
];

module.exports = audioData;
