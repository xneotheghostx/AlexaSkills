"use strict";

module.exports = Object.freeze({

  // App-ID. TODO: set to your own Skill App ID from the developer portal.
    appId: 'amzn1.ask.skill.539c9d19-e10f-4133-a59c-29e8bd40cbe5',

  //  DynamoDB Table name
  dynamoDBTableName: 'KlangweltenDB',

  /*
     *  States:
     *  START_MODE : Willkommenszustand, wenn die Audio-Liste nicht begonnen hat.
     *  PLAY_MODE :  When a playlist is being played. Does not imply only active play.
     *               It remains in the state as long as the playlist is not finished.
     *  RESUME_DECISION_MODE : When a user invokes the skill in PLAY_MODE with a LaunchRequest,
     *                         the skill provides an option to resume from last position, or to start over the playlist.
     */
  states: {
    START_MODE: '',
    PLAY_MODE: '_PLAY_MODE',
    RESUME_DECISION_MODE: '_RESUME_DECISION_MODE'
  }
});