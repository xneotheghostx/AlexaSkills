﻿var Alexa = require('alexa-sdk');
var dynamo = require('./dynamoDB');
var constants = require('./constants');
var moment = require('./moment');

var locale = "";
var shouldRecieveReward = false;

exports.handler = function (event, context, callback) {

    locale = event.request.locale;

    if (locale == "de-DE") {
        slotItems = [
            {
                "name": "Eins",
                "multiplier": 8,
                "probability": 6
            },
            {
                "name": "Zwei",
                "multiplier": 16,
                "probability": 5
            },
            {
                "name": "Drei",
                "multiplier": 25,
                "probability": 4
            },
            {
                "name": "Vier",
                "multiplier": 50,
                "probability": 3
            },
            {
                "name": "Fünf",
                "multiplier": 100,
                "probability": 2
            },
            {
                "name": "Goldene Sechs",
                "multiplier": 250,
                "probability": 1
            }
        ];
    } else { //if(locale == "en-US" || locale == "en-GB")
        slotItems = [
            {
                "name": "One",
                "multiplier": 8,
                "probability": 6
            },
            {
                "name": "Two",
                "multiplier": 16,
                "probability": 5
            },
            {
                "name": "Three",
                "multiplier": 25,
                "probability": 4
            },
            {
                "name": "Four",
                "multiplier": 50,
                "probability": 3
            },
            {
                "name": "Five",
                "multiplier": 100,
                "probability": 2
            },
            {
                "name": "Golden Six",
                "multiplier": 250,
                "probability": 1
            }
        ];
    }

    var alexa = Alexa.handler(event, context);
    alexa.appId = constants.appId;
    alexa.registerHandlers(handlers.setupModeIntentHandlers, handlers.gameModeIntentHandlers);
    alexa.execute();
};

var handlers = {

    setupModeIntentHandlers: Alexa.CreateStateHandler(constants.states.SETUP_MODE, {
        'NewSession': function () {

            console.log("Session init");
            this.handler.state = constants.states.SETUP_MODE;

            var self = this;
            dynamo.getJackpot(self.event.session, function (data) {

                console.log("Jackpot: " + data.message);

                if (data.item) {
                    self.event.session.attributes['jackpot'] = data.item.score;
                    self.event.session.attributes['jackpotDelta'] = 0;
                }

                dynamo.getUserState(self.event.session, function (data) {

                    console.log("User: " + data.message);

                    if (data.item) {
                        Object.assign(self.event.session.attributes, data.item);

                        var currentDateTime = moment().add(1, 'hours');
                        if (self.event.session.attributes['timestamp'] != "") {

                            var parsedDateTime = moment(self.event.session.attributes['timestamp'], "DD/MM/YYYY HH:mm");

                            console.log("test: " + currentDateTime.date() + " .  " +  parsedDateTime.date())
                            if (currentDateTime.date() != parsedDateTime.date()) {
                                shouldRecieveReward = true;

                                // Rückkehrer-Bonus hinzufügen
                                self.event.session.attributes['score'] += 25;
                            } else {
                                shouldRecieveReward = false;
                            }
                            self.event.session.attributes['timestamp'] = currentDateTime.format("DD/MM/YYYY HH:mm");
                        }

                        
                        if (self.attributes["userName"] === "InitalName") {
                            self.emit('NoFirstName');
                        } else {
                            self.emit('WelcomeBackIntent');
                        }
                    } else {
                        self.emit('ResetIntent');
                    }
                });
            });            
        },
        'LaunchRequest': function () {

            this.emit('NewSession');
        },
        'ResetIntent': function () {

            this.attributes["userName"] = "InitalName";
            this.attributes["userType"] = "MainUser";
            this.attributes["bet"] = 1;
            this.attributes["score"] = 100;
            this.attributes["timestamp"] = moment().format("DD/MM/YYYY HH:mm");

            this.handler.state = constants.states.SETUP_MODE;

            this.emit('WelcomeIntent');
        },

        'WelcomeIntent': function () {

            this.attributes['requestName'] = true;
            if (locale == "de-DE") {
                var speech = "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/GameStart.mp3' /> Willkommen bei dem Spiel <break time='150ms'/> die Goldene Sechs. "
                    + "Ich bin dein Croupier, nimm bitte an meinem Tisch Platz. <break time='250ms'/> Wie jeder Spieler,"
                    + "erhälst du zum Anfang einen Beutel mit 100 Münzen. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Beutel_mit_Muenzen.mp3' /> Für die Hilfe im Spiel sage Hilfe. "
                    + "Für die Regeln sage Regeln. <break time='250ms'/> Und jetzt wünsche ich dir viel Spaß, Glück und Erfolg. <break time='200ms'/> "
                    + "Achso, wenn du in der Internationalen Highscore-Liste auftauchen möchtest, nenne mir jetzt einen Namen.";

                var reprompt = "Wenn du nicht namentlich in der Liste auftauchen möchtest, sage Anonym oder nenne mir jetzt einen Namen.";
            } else { //if(locale == "en-GB" || locale == "en-US")
                var speech = "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/GameStart.mp3' /> Welcome to the game <break time='150ms'/> the golden six. "
                    + "I am your croupier, please sit down at my table. <break time='250ms'/> Like every player, "
                    + "you get a bag with 100 coins at the beginning. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Beutel_mit_Muenzen.mp3' /> For help in the game say help. "
                    + "For the rules say rules. <break time='250ms'/> And now I wish you lots of fun, luck and success. <break time='200ms'/> "
                    + "If you want to appear in the international highscore list, give me a name.";

                var reprompt = "If you do not want to appear in the list, say Anonymous.";
            }

            this.emit(':ask', speech, reprompt);
        },

        'WelcomeBackIntent': function () {

            if (locale == "de-DE") {
                var speech = "Hallo " + (this.attributes.userName == "Anonym" ? "Unbekannter" : this.attributes.userName)
                    + (this.attributes.score != 100 ? ". " : ". Du hast noch 100 Münzen. ")
                    + (shouldRecieveReward ? "Willkommen zurück.<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Kl_Gewinn.mp3' /> Du hast einen Bonus von 25 Münzen bekommen. <break time='100ms'/> " : "")
                    + "Dein Einsatz " + (this.attributes.bet > 1 ? "sind " + this.attributes.bet + " Münzen. " : "ist eine Münze. ")
                    + "Wenn du soweit bist und spielen möchtest sage würfeln. ";
                var reprompt = "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' /> Ich warte. Wenn du spielen möchtest sage würfeln.";
            } else { //if(locale == "en-GB" || locale == "en-US")
                var speech = "Hello " + (this.attributes.userName == "Anonym" ? "Unknown" : this.attributes.userName)
                    + (this.attributes.score != 100 ? ". " : ". You have 100 coins left.")
                    + (shouldRecieveReward ? "Welcome back.<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Kl_Gewinn.mp3' /> You got an bonus of 25 coins. <break time='30ms'/> " : "")
                    + "Your bet is " + this.attributes.bet + (this.attributes.bet > 1 ? " coins. " : " coin. ")
                    + "If you want to start say roll the dice.";

                var reprompt = "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' /> i'm waiting. If you want to start say roll the dice.";
            }

            this.emit(':ask', speech, reprompt);
        },


        'NoFirstName': function () {
            this.attributes['requestName'] = true;
            if (locale == "de-DE") {
                var speech = "Willkommen bei dem Spiel die Goldene Sechs. "
                    + "Du hast dir noch keinen Namen gegeben. Wenn du in meiner Highscore-Liste auftauchen möchtest, nenne mir einen Namen.";
                var reprompt = "Wenn du nicht namentlich in der Liste auftauchen möchtest, "
                    + "sage Anonym oder nenne jetzt einen Namen.";
            }
            else { //if (locale == "en-GB" || locale == "en-US")
                var speech = "Welcome to the game golden six. "
                    + "You have not given yourself a name yet. If you want to appear in the highscore list, give me a name.";
                var reprompt = "If you do not want to appear in the list, say Anonymous.";
            }
            this.emit(':ask', speech, reprompt);
        },

        'VerifyFirstName': function () {
            
            if (this.attributes['requestName']) {
                if (blacklist.indexOf(this.event.request.intent.slots.firstName.value.toLowerCase()) == -1) {
                    this.attributes.userName = this.event.request.intent.slots.firstName.value;

                    if (locale == "de-DE") {
                        var speech = "Dein Name lautet nun " + this.attributes.userName + " . Ist das richtig oder falsch?";
                        var reprompt = "Dein Name lautet nun " + this.attributes.userName + " . Ist das richtig oder falsch?";
                    }
                    else { //if (locale == "en-GB" || locale == "en-US")
                        var speech = "Your name now is " + this.attributes.userName + " . Is that right or wrong?";
                        var reprompt = "Your name now is " + this.attributes.userName + " . Is that right or wrong?";
                    }
                }
                else {
                    if (locale == "de-DE") {
                        var speech = "Der genannte Name ist nicht erlaubt, bitte nenne einen anderen.";
                        var reprompt = "Nenne mir einen Namen.";
                    }
                    else { //if (locale == "en-GB" || locale == "en-US")
                        var speech = "The name mentioned is not allowed, please give me another.";
                        var reprompt = "Give me a name.";
                    }
                }
                this.emit(':ask', speech, reprompt);
            } else {
                this.emit('Unhandled');
            }
        },
        'SetFirstName': function () {
            var self = this;
            saveToDynamoDB(self, false, function () {
                self.attributes['requestName'] = false;
                if (locale == "de-DE") {
                    var speech = "Okay " + self.attributes.userName + ", möchtest du jetzt spielen ? Sage Ja oder Nein.";
                    var reprompt = "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' /> Die Würfel und ich warten. Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "Okay " + self.attributes.userName + ", do you want to play? Say yes or no.";
                    var reprompt = "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' /> The dice and I will wait. Do you want to play? Say yes or no.";
                }
                self.emit(':ask', speech, reprompt);
            });
        },
        'AbortFirstName': function () {
            this.attributes['requestName'] = true;
            if (locale == "de-DE") {
                var speech = "OK, nenne mir einen Namen.";
                var reprompt = "Nenne mir einen Namen.";
            }
            else { //if (locale == "en-GB" || locale == "en-US")
                var speech = "Okay, give me a name.";
                var reprompt = "Give me a name.";
            }
            this.emit(':ask', speech, reprompt);
        },
        'AnonymIntent': function () {
            this.attributes.userName = "Anonym";

            var self = this;
            saveToDynamoDB(self, false, function () {
                self.attributes['requestName'] = false;
                if (locale == "de-DE") {
                    var speech = "OK, ich respektiere deinen Wunsch. Möchtest du jetzt spielen?";
                    var reprompt = "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' /> Die Würfel und ich warten. Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "Okay, I respect your wish. Do you want to play?";
                    var reprompt = "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' /> The dice and I wait. Do you want to play? Say yes or no.";
                }
                self.emit(':ask', speech, reprompt);
            });
        },


        'HighScoreIntent': function () {
            var self = this;
            saveToDynamoDB(self, false, function () {
                dynamo.getHighscore(self.event.session, function (data) {
                    console.log("Highscore");

                    if (locale == "de-DE") {
                        var outp = "";
                        for (var i = 0; i < data.item.length; i++) {
                            outp += " Platz " + (i + 1) + " " + data.item[i].userName + "<break time='50ms'/> mit " + data.item[i].score + " Münzen. ";
                        }

                        var speech = outp + " Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                        var reprompt = "Der Highscore wartet darauf das du ihn verbesserst. Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                    } else { //if (locale == "en-GB" || locale == "en-US")     
                        var outp = "";
                        for (var i = 0; i < data.item.length; i++) {
                            outp += " Number " + (i + 1) + " " + data.item[i].userName + "<break time='150ms'/> with " + data.item[i].score + " coins. ";
                        }

                        var speech = outp + " Do you want to play? Say yes or no.";
                        var reprompt = "The highscore is waiting for you to improve it. Do you want to play? Say yes or no.";
                    }
                    self.emit(':ask', speech, reprompt);
                });
            });
        },


        /*'AMAZON.YesIntent': function () {

            if (this.attributes["setupState"]) {
                if (this.attributes["setupState"] === "RequestRoles") {

                    this.emit('AMAZON.HelpIntent');
                }
            }
            this.emit('Unhandled');
        },
        'AMAZON.NoIntent': function () {

            if (this.attributes["setupState"]) {
                if (this.attributes["setupState"] === "RequestRoles") {

                    this.emit('SetNameIntent');
                }
            }
            this.emit('Unhandled');
        },*/

        'StartSpinIntent': function () {

            var genNumbers = GenerateSlots(3);
            console.log(genNumbers);
            if (this.attributes.score <= 0) {
                this.attributes.score = 100;

                if (locale == "de-DE") {
                    var speech = "<say-as interpret-as='interjection'>huch</say-as>, dein Beutel ist leer. Hier <break time='200ms'/> die Bank gibt dir noch einmal einen Beutel mit 100 Münzen. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Beutel_mit_Muenzen.mp3' /> Möchtest du weiterspielen?";
                    var reprompt = "<say-as interpret-as='interjection'>na und?</say-as> ich warte. Möchtest du noch einmal Spielen ?";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "You're broke. Because you have no more coins. Here <break time='100ms'/> the bank gives you another bag of 100 coins. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Beutel_mit_Muenzen.mp3' /> Do you want to play again?";
                    var reprompt = "<say-as interpret-as='interjection'>yoo hoo</say-as> Do you want to play again?";
                }
            }
            else {
                if (this.attributes.score < this.attributes.bet)
                    this.attributes.bet = this.attributes.score;

                this.attributes.score = this.attributes.score - this.attributes.bet;
                this.attributes.score = this.attributes.score + GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers);

                this.attributes.jackpotDelta = this.attributes.jackpotDelta + 1; // Jackpot um einen erhöhen

                if (locale == "de-DE") {
                    var speech;
                    switch(Math.floor(Math.random() * 10)) {
                        case 0:
                            speech = "Dann gehts jetzt los <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> ich wünsche dir viel Glück <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> Jetzt wollen wir doch mal sehen was du hast.<break time='200ms'/>"
                                + "Ok der erste Würfel ist eine " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> und der zweite eine "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> und der dritte Würfel ist eine <break time='350ms'/> "
                                + slotItems[genNumbers[2]].name
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "de-DE");
                            break;
                        case 1:
                            speech = "Ab in den Becher <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> und <say-as interpret-as='interjection'>viel glück</say-as> <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> dann lass uns nachsehen was du hast.<break time='200ms'/>"
                                + "Tja, der erste Würfel ist eine " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> Gut und der zweite eine "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> jetzt noch der dritte Würfel mit einer <break time='250ms'/> "
                                + slotItems[genNumbers[2]].name
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "de-DE");
                            break;
                        case 2:
                            speech = "Die Würfel wollen Tanzen <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> und ich sorge für die Bewegung. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> <say-as interpret-as='interjection'>komm schon</say-as> und lächel mal. Hier ist dein ergebnis <break time='200ms'/>"
                                + "<say-as interpret-as='interjection'>oh oh</say-as>, das hier ist eine " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> und der hier eine "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> und der letzte Würfel ist eine <break time='300ms'/> "
                                + slotItems[genNumbers[2]].name
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "de-DE");
                            break;
                        case 3:
                            speech = "Dann spiel ich mal die Glücksfee <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> und wünsche dir viel Glück <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> Die Würfel sind gefallen.<break time='200ms'/>"
                                + "<say-as interpret-as='interjection'>oh nein</say-as>, das ist nicht meine Schuld, hier ist eine " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> und der hier, ist eine "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> und mit dem letzten Würfel bekommst du eine <break time='250ms'/> "
                                + slotItems[genNumbers[2]].name
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "de-DE");
                            break;
                        case 4:
                            speech = "Diese Runde wird ein Quickie <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> ab dafür <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> Kurz und knapp <break time='200ms'/>"
                                + "eine " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='250ms'/> eine "
                                + slotItems[genNumbers[1]].name
                                + "<break time='250ms'/> und eine <break time='100ms'/> "
                                + slotItems[genNumbers[2]].name
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "de-DE");
                            break;
                        case 5:
                            speech = "Wenn ich wüsste wie es geht, <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> würde ich dich gewinnen lassen. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> Und ohne zu schummeln, <break time='100ms'/> "
                                + "wird aus aus dem erten Würfel eine " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> und aus dem zweiten eine "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> und der dritte glänzt mit einer <break time='300ms'/> "
                                + slotItems[genNumbers[2]].name
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "de-DE");
                            break;
                        case 6:
                            speech = "<say-as interpret-as='interjection'>ey</say-as> gedrängelt wird nicht. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> Ich mach ja schon. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> <break time='100ms'/> "
                                + "und was hat es dir gebracht? <break time='200ms'/> hier ist eine " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> auf dem zweiten Würfel siehst du eine "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> und der dritte glänzt mit einer <break time='300ms'/> "
                                + slotItems[genNumbers[2]].name
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "de-DE");
                            break;
                        case 7:
                            speech = "Du möchtest also den Jackpot knacken? <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> gut. Ich streng mich an. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "aber eins ist dir doch klar, <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> <break time='100ms'/> "
                                + "wenn es nichts wird, liegt es an den Würfeln. <break time='250ms'/> Da liegt eine " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> und eine "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> der dritte Würfel, versucht mit einer <break time='100ms'/>"
                                + slotItems[genNumbers[2]].name 
                                + "zu punkten. "
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "de-DE");
                            break;
                        case 8:
                            speech = "Der Croupier bittet zu Tisch. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> Ich würfel und du drückst die Daumen. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> <break time='100ms'/> "
                                + "und hat es etwas gebracht? <break time='200ms'/> OK, da liegt eine " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> und eine "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> tja, der dritte Würfel ist eine <break time='200ms'/>"
                                + slotItems[genNumbers[2]].name 
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "de-DE");
                            break;
                        case 9:
                            speech = "Mein Name ist Alexa und ich verzaubere deine Würfel. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> <say-as interpret-as='interjection'>hihi</say-as> Sorry, ich mach ja schon. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> <say-as interpret-as='interjection'>ähm</say-as><break time='200ms'/> "
                                + "und hat es geholfen? <break time='250ms'/> OK, da liegt eine " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> und eine "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> der letzte Würfel ist eine <break time='200ms'/>"
                                + slotItems[genNumbers[2]].name 
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "de-DE");
                            break;
                    }

                    speech += " Möchtest du noch einmal Spielen ?";
                    var reprompt = "Wenn du willst gehts jetzt weiter. Möchtest du noch einmal Spielen ?";
                    
                }
                else //if (locale == "en-GB" || locale == "en-US")
                {
                    var speech;
                    switch(Math.floor(Math.random() * 10)) {
                        case 0:
                            speech = "Okay go now <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> i wish you luck <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> Now let's see what you have <break time='200ms'/>"
                                + "Okay. The first dice is a " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> and the second a "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> and the third dice is a <break time='350ms'/> "
                                + slotItems[genNumbers[2]].name
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "en-US");
                            break;
                        case 1:
                            speech = "Into the cup <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> and <say-as interpret-as='interjection'>good luck</say-as> <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> lets see what we got. <break time='200ms'/>"
                                + "Well, the first dice is a " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> good and the second a "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> now the third dice with a <break time='250ms'/> "
                                + slotItems[genNumbers[2]].name
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "en-US");
                            break;
                        case 2:
                            speech = "The dice want to dance <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> and I take care of the movement. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> <say-as interpret-as='interjection'>come on</say-as> and smile. Here is your result <break time='200ms'/>"
                                + "<say-as interpret-as='interjection'>oh dear</say-as>, this is a " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> and this a "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> and the last dice is a <break time='300ms'/> "
                                + slotItems[genNumbers[2]].name
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "en-US");
                            break;
                        case 3:
                            speech = "Then I'll play the lucky fairy <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> and wish you good luck <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> The dice have fallen. <break time='200ms'/>"
                                + "<say-as interpret-as='interjection'>ouch</say-as>, this is not my fault, here is a " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> and the second is a "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> and with the last dice you get a <break time='250ms'/> "
                                + slotItems[genNumbers[2]].name
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "en-US");
                            break;
                        case 4:
                            speech = "This round will be a quickie <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> for that <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> Short and sweet <break time='200ms'/>"
                                + "a " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='250ms'/> and "
                                + slotItems[genNumbers[1]].name
                                + "<break time='250ms'/> and a <break time='100ms'/> "
                                + slotItems[genNumbers[2]].name
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "en-US");
                            break;
                        case 5:
                            speech = "If I knew how to do it, <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> I would let you win. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> And without cheating, <break time='100ms'/> "
                                + "out of the first dice comes a " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> and the second one is a "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> and the third one shines with a <break time='300ms'/> "
                                + slotItems[genNumbers[2]].name
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "en-US");
                            break;
                        case 6:
                            speech = "<say-as interpret-as='interjection'>hey</say-as> don't push me. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> I'll do it. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> <break time='100ms'/> "
                                + "and what did it bring you? <break time='200ms'/> here is a " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> on the second dice you see a "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> and the third one shines with a <break time='300ms'/> "
                                + slotItems[genNumbers[2]].name
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "en-US");
                            break;
                        case 7:
                            speech = "So you want to crack the jackpot? <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> Well. I dress myself. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "but one thing is clear to you, <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> <break time='100ms'/> "
                                + "if it does not, it's up to the dice. <break time='250ms'/> There is a " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> and a "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> the third dice, tried with a <break time='100ms'/>"
                                + slotItems[genNumbers[2]].name 
                                + "to score. "
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "en-US");
                            break;
                        case 8:
                            speech = "The croupier asks for a table. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> I will roll the dice and you keep your fingers crossed <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> <break time='100ms'/> "
                                + "and did it do something? <break time='200ms'/> OK, there's a " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> and a "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> well, the third dice is a <break time='200ms'/>"
                                + slotItems[genNumbers[2]].name 
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "en-US");
                            break;
                        case 9:
                            speech = "My name is Alexa and I enchant your dice. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_in_den_Becher.mp3' /> <say-as interpret-as='interjection'>hi hi</say-as> Sorry, I'll continue. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfelbecher_schuetteln.mp3' />"
                                + "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_auf_den_Tisch.mp3' /> <say-as interpret-as='interjection'>ähm</say-as><break time='200ms'/> "
                                + "and has it helped? <break time='250ms'/> OK, there's a " 
                                + slotItems[genNumbers[0]].name
                                + "<break time='300ms'/> and a "
                                + slotItems[genNumbers[1]].name
                                + "<break time='300ms'/> the last dice is a <break time='200ms'/>"
                                + slotItems[genNumbers[2]].name 
                                + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "en-US");
                            break;
                    }
                        speech += " Do you want to play again?";
                        var reprompt = "Do you want to play again?";
                }

                // Reset jackpot
                if (GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers) == this.attributes.bet + Math.floor(this.attributes.jackpot / 10)) {
                    
					console.log(this.attributes.jackpot + " Münzen Jackpot geknackt von " + this.attributes["userName"]);
					
					var self = this;
                    saveToDynamoDB(self, true, function () {
                        self.emit(':ask', speech, reprompt);
                    });
                }
            }
            this.emit(':ask', speech, reprompt);
        },

        'RaiseBetIntent': function () {

            if (this.attributes.bet < 5) {
                if ((this.attributes.score - this.attributes.bet) <= 0) {

                    if (locale == "de-DE") {
                        var speech = "Du kannst deinen Einsatz nicht weiter erhöhen. Möchtest du weiterspielen?";
                    }
                    else { //if (locale == "en-GB" || locale == "en-US")
                        var speech = "You can not increase your bet any further. Do you want to continue playing?";
                    }
                }
                else {
                    this.attributes.bet = this.attributes.bet + 1;

                    if (locale == "de-DE") {
                        var speech = "Dein Einsatz ist jetzt mal <say-as interpret-as='number'>" + this.attributes.bet + "</say-as>. Möchtest du erhöhen oder weiterspielen?";
                    }
                    else { //if (locale == "en-GB" || locale == "en-US")
                        var speech = "Your bet is now <say-as interpret-as='number'>" + this.attributes.bet + "</say-as>. Do you want to raise or continue playing?";
                    }
                }
            }
            else {
                if (locale == "de-DE") {
                    var speech = "Maximaleinsatz sind 5 Münzen. Möchtest du weiterspielen?";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "Maximum bet is 5 coins. Do you want to continue playing?";
                }
            }

            if (locale == "de-DE") {
                var reprompt = "Möchtest du weiterspielen?";
            }
            else { //if (locale == "en-GB" || locale == "en-US")
                var reprompt = "Do you want to continue playing?";
            }
            this.emit(':ask', speech, reprompt);
        },
        'LowerBetIntent': function () {

            if (this.attributes.bet > 1) {
                this.attributes.bet = this.attributes.bet - 1;

                if (locale == "de-DE") {
                    var speech = "Dein Einsatz ist jetzt mal <say-as interpret-as='number'>" + this.attributes.bet + "</say-as>. Möchtest du verringern oder weiterspielen?";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "Your bet is now <say-as interpret-as='number'>" + this.attributes.bet + "</say-as>. Do you want to lower or continue playing?";
                }
            }
            else {
                if (locale == "de-DE") {
                    var speech = "Mindesteinsatz ist eine Münze. Möchtest du weiterspielen?";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "Minimum bet is one coin. Do you want to continue playing?";
                }
            }

            if (locale == "de-DE") {
                var reprompt = "Möchtest du weiterspielen?";
            }
            else { //if (locale == "en-GB" || locale == "en-US")
                var reprompt = "Do you want to continue playing?";
            }
            this.emit(':ask', speech, reprompt);
        },


        'CurrentScoreIntent': function () {
            var self = this;
            saveToDynamoDB(self, false, function () {
                if (locale == "de-DE") {
                    var speech = "Du hast noch " + self.attributes.score + " Münzen.<break time='50ms'/> Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                    var reprompt = "Die Würfel, der Becher und ich warten. Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "You have " + self.attributes.score + " coins left. <break time='50ms'/> Do you want to play? Say yes or no.";
                    var reprompt = "The dice, the cup and I are waiting. Do you want to play? Say yes or no.";
                }
                self.emit(':ask', speech, reprompt);
            });
        },
        'CurrentJackpotIntent': function () {
            var self = this;
            saveToDynamoDB(self, false, function () {
                if (locale == "de-DE") {
                    var speech = "Derzeit sind im Jackpot " + Math.floor((self.attributes.jackpot + self.attributes.jackpotDelta) / 10) + " Münzen.<break time='150ms'/> Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                    var reprompt = "Die Würfel, der Becher und ich warten. Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "Currently, the jackpot is " + Math.floor((self.attributes.jackpot + self.attributes.jackpotDelta) / 10) + " coins.<break time='150ms'/> Do you want to play? Say yes or no.";
                    var reprompt = "The dice, the cup and I are waiting. Do you want to play? Say yes or no.";
                }
                self.emit(':ask', speech, reprompt);
            });
        },



        'RoleIntent': function () {

            if (locale == "de-DE") {

                var speech = "Hier sind die Regeln für die Goldene Sechs.<break time='50ms'/> Bei Übereinstimmung von zwei Würfeln, bekommst du deinen Einsatz zurück. <break time='50ms'/>Bei drei gleichen Würfeln,"
                    + " wie zum beispiel die Eins <break time='50ms'/>bekommst du 8 Münzen. Bei drei mal Zwei 16 Münzen <break time='150ms'/> für drei mal die Drei 25, drei mal die Vier 50, "
                    + "und bei 3 mal die Fünf 100 Münzen. <break time='50ms'/> Der größte Gewinn ist drei mal die Goldene Sechs <break time='200ms'/> dann Zahlt dir die Bank den Jackpot aus. <break time='250ms'/> Bei den gewinnen mit "
                    + "drei Übereinstimmungen, erhöht sich dein Gewinn, je Faktor, eins bis maximal fünf. Abhängig von deinem Einsatz. Hier ein Beispiel <break time='250ms'/>. Du hast deinen Einsatz auf vier Münzen erhöht "
                    + "und gewinnst. Bei drei mal Vier, also 50 Münzen mal vier, das sind dann 200 Münzen. Der Jackpot erhöht sich alle zehn Spiele um eine Münze. Er füllt sich durch das spielen aller Spieler. <break time='50ms'/>"
                    + "Ich wünsche dir viel Spaß und viel Glück, bei meinem Spiel Drei mal Sechs.<break time='200ms'/> Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                var reprompt = "Die Würfel sind im Becher. Möchtest du jetzt spielen? Sage Ja oder Nein.";
            } else { //if (locale == "en-GB" || locale == "en-US")

                var speech = "Here are the rules for the golden six.<break time='50ms'/> If two dice match, you will get your bet back. <break time='50ms'/>With three equal dice,"
                    + " for example three ones <break time='50ms'/>you get 8 coins. With three twos 16 coins, <break time='150ms'/> for three times threes 25, three times four 50, "
                    + "and at three times five 100 coins. <break time='50ms'/> The biggest win is three times the golden six <break time='200ms'/> then the bank pays out the jackpot. <break time='250ms'/> If you win with three matches, "
                    + "your prize will increases by a factor of one to a maximum of five, depending on your bet. Here's an example <break time='50ms'/>. You have increased your bet to four coins and win. "
                    + "With three times four, you will get 50 coins multiplied by four, so you will earn 200 coins. The jackpot increases all ten games by a coin, it fills itself with the playing of all players. <break time='50ms'/>"
                    + "Now, I wish you good luck and lots of fun.<break time='200ms'/> Do you want to play? Say yes or no.";
                var reprompt = "The dice are in the cup. Do you want to play? Say yes or no.";
            }
            this.emit(':ask', speech, reprompt);
        },
        'AMAZON.HelpIntent': function () {

            if (locale == "de-DE") {

                var speech = "Beim dem Spiel die Goldene Sechs, hast du drei Würfel. Jeder Würfel hat sechs Zahlen. Beim Start bekommst du von der Bank 100 Münzen. Mit dem Befehl würfel, startest du dein erstes Spiel ."
                    + "Bei Übereinstimmung von zwei Würfeln, bekommst du deinen Einsatz zurück. Bei Übereinstimmung von drei Würfeln, bekommst du je nach der gewürfelten Zahl, deinen Gewinn gut geschrieben. <break time='150ms'/>"
                    + "Du kannst deinen Einsatz erhöhen oder verringern. Dafür sagst du Einsatz erhöhen oder Einsatz verringern. Der Maximal Einsatz pro Spiel beträgt fünf Münzen<break time='150ms'/> und der kleinste Einsatz eine Münze ."
                    + "Beim beenden des Spiels wird dein Punktestand in die Highscoreliste eingetragen. Bei einem Neustart startest du mit deinem alten Guthaben und spielst weiter.<break time='150ms'/>"
                    + "Du kannst diese Hilfe jederzeit mit dem befehl Hilfe<break time='150ms'/> wieder aufrufen. Und jetzt, wünsche ich dir viel Spaß und viel Glück, bei meinem Spiel die Goldene Sechs."
                    + "<break time='50ms'/> Möchtest du jetzt spielen? Sage Ja oder Nein.";
                var reprompt = "Die Würfel sind im Becher. Möchtest du jetzt spielen? Sage Ja oder Nein.";
            } else { //if (locale == "en-GB" || locale == "en-US")

                var speech = "In the golden six, you have three dice. Each dice has six different symbols. At the start you get 100 coins from the bank. With the command pull to start your first game. "
                    + "If two dice match, you will get your bet back. If three dice match, you'll get a prize, depending on the symbol. <break time='150ms'/>"
                    + "You can increase or decrease your bet. You can say raise bet or reduce bet. The maximum bet per game is five coins<break time='150ms'/> and the minimum bet one coin. "
                    + "When you complete a round, your score is entered in the highscore list. When you restart, you continue with your old credit.<break time='150ms'/> "
                    + "You can replay the help at any time using the command help. And now, I wish you good luck and lots of fun. "
                    + "<break time='150ms'/>Do you want to play? Say yes or no.";
                var reprompt = "The dice are in the cup. Do you want to play? Say yes or no.";
            }
            this.emit(':ask', speech, reprompt);
        },
        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            this.attributes = updateRatingAttributes(this.attributes, locale);
            var speech = "";

            var self = this;
            saveToDynamoDB(self, false, function () {

                dynamo.getHighscore(self.event.session, function (data) {
                    console.log("Highscore-End: " + data.message);

                    if (data.item) {
                        if (locale == "de-DE") {

                            
                            speech = "Vielen Dank, das du mit mir gespielt hast. Bis zum nächsten mal. "
                                + "Du bist nicht unter den Zehn besten.";
                            var cardTitle = "Auf wiederhören und vielen Dank für´s Spielen.";
                            var cardText = "Danke, dass du mit mir gespielt hast.\n\nWenn dir dieser Skill gefallen hat, freut sich das Team von \"Nordic Skills\" über eine positive Bewertung von dir!\nDies hilft ihnen zukünftig weitere Skills zu entwickeln. \nUnd so bewertest du einen Skill:\n\n1. Öffne die Alexa App auf deinem Smartphone, Laptop oder PC\n\n2. Gehe in den Bereich „Skills“\n\n3. Tippe auf „Ihre Skills“ in der rechten oberen Ecke\n\n4. Suche nach „Die Goldene Sechs“\n\n5. Scrolle bis nach unten und tippe auf „Schreiben Sie eine Rezension“\n\n6. Unterstütze das Team von \"Nordic Skills\" mit deiner 5-Sterne-Bewertung\n\nDanke!";
                            var imageObj = { smallImageUrl: "https://nordicskills.de/bilder/DerZauberwaldKendell/Danke.jpg", largeImageUrl: "https://nordicskills.de/bilder/DerZauberwaldKendell/Danke.jpg" };
                        } else { //if (locale == "en-GB" || locale == "en-US")

                            speech = "Thank you for playing with me. Until next time. "
                                + "You're not in the top ten.";
                            var cardTitle = "Thank you for playing with me. Until next time.";
                            var cardText = "Thank you for playing with me. Until next time.";
                            var imageObj = null;
                        }

                        for (var i = 0; i < data.item.length; i++) {
                            if (data.item[i].userId == self.event.session.user.userId) {

                                if (locale == "de-DE") {
                                    speech = "Du bist zurzeit auf Platz <say-as interpret-as='number'>" + (i + 1) + "</say-as>. "
                                        + "Vielen Dank, dass du mit mir gespielt hast. Bis zum nächsten mal. ";
                                } else { //if (locale == "en-GB" || locale == "en-US")
                                    speech = "You are currently at place <say-as interpret-as='number'>" + (i + 1) + "</say-as>. "
                                        + "Thank you for playing with me. Until next time.";
                                }
                            }
                        }

                        if(self.event.session.attributes["ratingSpeech"] != "")
                            speech = self.event.session.attributes["ratingSpeech"];

                        self.emit(':tellWithCard', speech, cardTitle, cardText, imageObj);
                    } else {
                        self.emit('Unhandled');
                    }
                });

            });
        },
        'Unhandled': function () {

            if (locale == "de-DE") {
                var speech = 'Entschuldigung, Ich habe dich leider nicht verstanden.';
            } else { //if (locale == "en-GB" || locale == "en-US")     
                var speech = 'Sorry, I did not understand you.';
            }
            this.emit(':ask', speech, speech);
        }
    }),
    gameModeIntentHandlers: Alexa.CreateStateHandler(constants.states.GAME_MODE, {

    })
};

function updateRatingAttributes(attributes, locale) {

    var returnRating = false;

    if (!attributes["firstTimestamp"])
        attributes["firstTimestamp"] = moment().valueOf();

    attributes["lastTimestamp"] = moment().valueOf();

    if (!attributes["sessionCount"]) {
        attributes["sessionCount"] = 0;
        returnRating = true;
    }

    if (attributes["sessionCount"] >= 3 && moment(attributes["lastTimestamp"]).diff(moment(attributes["firstTimestamp"]), 'days') > 7) {
        attributes["sessionCount"] = 0;
    }

    if (!returnRating && attributes["sessionCount"] < 3 && moment(attributes["lastTimestamp"]).diff(moment(attributes["firstTimestamp"]), 'days') <= 14)
        if (Math.floor(Math.random() * 5) == 0) // 20% Wahrscheinlichkeit innerhalb von 2 Wochen
            returnRating = true;

    if (returnRating) {
        attributes["sessionCount"] = attributes["sessionCount"] + 1;

        if (locale == "de-DE")
            attributes["ratingSpeech"] = "Hat Dir mein Spiel die Goldene Sechs gefallen? Dann bewerte es doch auf der Amazon Webseite oder über Deine Alexa-App! ";
        else //if (locale == "en-GB" || locale == "en-US")
            attributes["ratingSpeech"] = "Did you like the Golden Six? Then rate it on the Amazon website or via your Alexa app! ";
    } else {
        attributes["ratingSpeech"] = "";
    }
    return attributes;
}

function saveToDynamoDB(self, newJackpot, cb) {

    dynamo.getJackpot(self.event.session, function (data) {
        console.log("GetJackpot: " + data.message);

        if (newJackpot) {
            self.event.session.attributes['jackpot'] = 1000;
            self.event.session.attributes['jackpotDelta'] = 0;
        } else {
            self.event.session.attributes['jackpot'] = data.item.score + self.event.session.attributes['jackpotDelta'];
            self.event.session.attributes['jackpotDelta'] = 0;
        }
 
        dynamo.setJackpot(self.event.session, function (data) {
            console.log("SetJackpot: " + data.message);

            dynamo.putUserState(self.event.session, function (data) {
                console.log("PutUserState: " + data.message);

                console.log("All done!");
                cb();
            });
        });
    });
}

function BuildResultString(prize, bet, jackpot, locale)
{
    if (locale == "de-DE") {
        switch(Math.floor(Math.random() * 3)) {
            case 0:
                if (prize == bet + Math.floor(jackpot / 10)) // Seven-Jackpot
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Jackpot.mp3' /> <say-as interpret-as='interjection'>Glückwunsch</say-as>, du hast den Jackpot geknackt, du gewinnst " + Math.floor(jackpot / 10) + " Münzen. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else if (prize > bet) // Gewonnen (kleiner Gewinn)
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Kl_Gewinn.mp3' /> <say-as interpret-as='interjection'>Glückwunsch</say-as>, du hast " + prize + " Münzen gewonnen. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else if (prize == bet) // Einsatz wiederbekommen
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Einsatz.mp3' /> Du hast deinen Einsatz wiederbekommen. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else // Verloren
                {       
                return " <break time='200ms'/> <audio src='https://alexanews.de/audio/SlotMachine/verloren.mp3' /> Schade, du hast verloren. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
            case 1:
                if (prize == bet + Math.floor(jackpot / 10)) // Seven-Jackpot
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Jackpot.mp3' /> <say-as interpret-as='interjection'>na sieh mal einer an</say-as>, du hast ja den Jackpot geknackt und gewinnst " + Math.floor(jackpot / 10) + " Münzen. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else if (prize > bet) // Gewonnen (kleiner Gewinn)
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Kl_Gewinn.mp3' /> <say-as interpret-as='interjection'>juhu</say-as>, du hast " + prize + " Münzen gewonnen. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else if (prize == bet) // Einsatz wiederbekommen
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Einsatz.mp3' />Tja, da bekommst du deinen Einzatz wieder. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else // Verloren
                {       
                return " <break time='200ms'/> <audio src='https://alexanews.de/audio/SlotMachine/verloren.mp3' />Mann kann auch nicht immer gewinnen. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
            case 2:
                if (prize == bet + Math.floor(jackpot / 10)) // Seven-Jackpot
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Jackpot.mp3' /> <say-as interpret-as='interjection'>hach ja</say-as>, du bekommst den Jackpot und ich geh leer aus. Du gewinnst " + Math.floor(jackpot / 10) + " Münzen. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else if (prize > bet) // Gewonnen (kleiner Gewinn)
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Kl_Gewinn.mp3' /> Hier kommt dein Gewinn, du hast " + prize + " Münzen gewonnen. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else if (prize == bet) // Einsatz wiederbekommen
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Einsatz.mp3' />Wie ein Bumerrang, hier ist dein Einzatz zurück. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else // Verloren
                {       
                return " <break time='200ms'/> <audio src='https://alexanews.de/audio/SlotMachine/verloren.mp3' />Manchmal gewinnt man, manchmal verliert man. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
            }
    }
    else // if (locale == "en-GB" || locale = "en-US") 
    {
        switch(Math.floor(Math.random() * 3)) {
            case 0:
                if (prize / bet == slotItems[slotItems.length - 1].multiplier) // Seven-Jackpot
                {
                return "<audio src='https://alexanews.de/audio/SlotMachine/jackpot.mp3' /> <say-as interpret-as='interjection'>Congratulations</say-as>, you hit the Jackpot. You won " + Math.floor(jackpot / 10) + " coins. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else if (prize > bet) // Gewonnen (kleiner Gewinn)
                {
                return "<audio src='https://alexanews.de/audio/SlotMachine/muenzen_pot.mp3' /> <say-as interpret-as='interjection'>Congratulations</say-as> you won " + prize + " coins. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else if (prize == bet) // Einsatz wiederbekommen
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Einsatz.mp3' /> You got your bet back. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else // Verloren
                {
                return " <break time='200ms'/> <audio src='https://alexanews.de/audio/SlotMachine/verloren.mp3' /> Too bad, you lost. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
            case 1:
                if (prize == bet + Math.floor(jackpot / 10)) // Seven-Jackpot
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Jackpot.mp3' /> <say-as interpret-as='interjection'>cha ching</say-as>, you cracked the jackpot and won " + Math.floor(jackpot / 10) + " coins. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else if (prize > bet) // Gewonnen (kleiner Gewinn)
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Kl_Gewinn.mp3' /> <say-as interpret-as='interjection'>ta da</say-as>, You won " + prize + " coins. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else if (prize == bet) // Einsatz wiederbekommen
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Einsatz.mp3' /> Well, you'll get your one-shot again. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else // Verloren
                {       
                return " <break time='200ms'/> <audio src='https://alexanews.de/audio/SlotMachine/verloren.mp3' /> Man can not always win. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
            case 2:
                if (prize == bet + Math.floor(jackpot / 10)) // Seven-Jackpot
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Jackpot.mp3' /> <say-as interpret-as='interjection'>aw man</say-as>, you get the jackpot and I go out empty. <break time='200ms'/> You win " + Math.floor(jackpot / 10) + " coins. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else if (prize > bet) // Gewonnen (kleiner Gewinn)
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Kl_Gewinn.mp3' /> Here comes your profit, you won " + prize + " coins. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else if (prize == bet) // Einsatz wiederbekommen
                {
                return "<audio src='https://nordicskills.de/audio/DieGoldeneSechs/Einsatz.mp3' /> Like a boomerang, here's your bet back. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
                else // Verloren
                {       
                return " <break time='200ms'/> <audio src='https://alexanews.de/audio/SlotMachine/verloren.mp3' /> Sometimes you win, sometimes you lose. <audio src='https://nordicskills.de/audio/DieGoldeneSechs/Wuerfel_vom_Tisch.mp3' />";
                }
            }
    }
}

function GenerateSlots(count) // Random randNumGen,
{
    var randNums = new Array(count).fill(0);

    for (var i = 0; i < randNums.length; i++)
    {
        var randNum = Math.floor(Math.random() * 22);

        var counter = 0;

        for (var j = 0; j < slotItems.length; j++)
        {
            if (randNum >= counter && randNum < counter + slotItems[j].probability) {
                randNums[i] = j;
                break;
            }
            counter += slotItems[j].probability;
        }
    }

    return randNums;
}

function GetPrize(bet, jackpot, genNums)
{
    if (genNums[0] == genNums[1] && genNums[1] == genNums[2]) // Drei Übereinstimmungen
    {
        if (genNums[0] == 5)
            return bet + Math.floor(jackpot / 10);

        for (var i = 0; i < slotItems.length; i++)
        {
            if (genNums[0] == i)
                return bet * slotItems[i].multiplier;
        }
    }
    else if (genNums[0] == genNums[1] || genNums[1] == genNums[2] || genNums[0] == genNums[2]) // Zwei Übereinstimmungen
    {
        return bet;
    }

    return 0; // Keine Übereinstimmung
}


function supportsDisplay() {
    var hasDisplay =
        this.event.context &&
        this.event.context.System &&
        this.event.context.System.device &&
        this.event.context.System.device.supportedInterfaces &&
        this.event.context.System.device.supportedInterfaces.Display

    return hasDisplay;
}

function isSimulator() {
    var isSimulator = !this.event.context; //simulator doesn't send context
    return isSimulator;
}


var slotItems;

var blacklist =
[
    "hitler",
    "nazi",
    "neonazi",
    "hure",
    "porno",
    "pornostar",
    "kinderporno",
    "kindersex",
    "kinderfick",
    "kinderficker",
    "kinderschänder",
    "kindergewalt",
    "vergewaltigung",
    "vergewaltiger",
    "start",
    "jackpot",
    "bitch",
    "whore",
    "fuck",
    "porn",
    "rapist"
]