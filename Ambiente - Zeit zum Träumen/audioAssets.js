'use strict';

// Audio Source - https://nordicskills.de/audio/ZeitZumTraeumen/
var audioData = [
    {
        'title': 'Alte Bilder von dir',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Alte_bilder_von_dir.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Ave Maria',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Ave_maria.mp3',
        'category': 'Kategorie 1'
    },
	{
        'title': 'Ballade',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Ballade.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Deine Anmut',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Deine_anmut.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Ein glückliches Ende',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Ein_glueckliches_ende.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Ein Tag im Park',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Ein_tag_im_park.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Gestorben für die Liebe',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Gestorben_fuer_die_liebe.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Glücklich',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Gluecklich.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Helles Licht',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Helles_licht.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Herbst des Lebens',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Herbst_des_lebens.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Klavier Solo',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Klavier_solo.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Klavierzauber',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Klavierzauber.mp3',
        'category': 'Kategorie 1'
    },
	{
        'title': 'Mein Herz tanzt',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Mein_herz_tanzt.mp3',
        'category': 'Kategorie 1'
    },
	{
        'title': 'Mondscheinsonate',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Mondscheinsonate.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Morgensonne',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/morgensonne.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Novemberregen',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Novemberregen.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Sanfte Berührung',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Sanfte_beruehrung.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Sich verlieben',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Sich_verlieben.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Sonnenaufgang ohne dich',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Sonnenaufgang_ohne_dich.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Süsse Melodie',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Suesse_melodie.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Tränen in Venedig',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Traenen_in_venedig.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Vorher',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Vorher.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Wolkenballett',
        'url': 'https://nordicskills.de/audio/ZeitZumTraeumen/Wolkenballett.mp3',
        'category': 'Kategorie 1'
    }
];

module.exports = audioData;
