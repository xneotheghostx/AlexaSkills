// Copyright <YEAR> Amazon.com, Inc. or its affiliates. All Rights Reserved.
// Licensed under the Amazon Software License
// http://aws.amazon.com/asl/

module.exports = {
  translation: {
    HINTS_AVAILABLE: 'Du hast noch %i Hinweise übrig.',
    WELCOME_MESSAGE: 'Willkommen bei !  Ich werde dir den Namen eines Schauspielers oder einer Schauspielerin geben, und du musst mir sagen, an welche Serie oder Film ich denke. ' +
	'Wenn du es nicht weißt, kannst du einen weiteren Hinweis kaufen, dann nenne ich den Namen von einem weiteren Schauspieler aus dem gleichen Film oder der gleichen Serie. %s Berreit für deine erste Frage?',
    CANNOT_BUY_RIGHT_NOW: 'I am sorry. The hint pack is not available for purchase at this time.',
    HELP_PROMPT: 'Ich gebe dir den Namen einer Schauspielerin oder Schauspielers, und du musst mir sagen, an welchen Film oder Serie ich denke. Du kannst Tipps kaufen, wenn du den Namen eines zweiten oder dritten Schauspielers benötigst... frage mich einfach! Bist du bereit für eine Frage?',
    NO_HINTS_FOR_NOW: 'Keine Hinweise für jetzt. Ich habs. %S',
    ERROR_MESSAGE: 'Derzeit liegt eine Störung vor. Bitte versuche es später noch einmal. ',
    GAVE_UP: 'OK. Das war auch schwierig. Der Film oder die Serie, an die ich dachte, war %s. Möchtest du eine andere Frage versuchen?',
    THANK_YOU: 'Danke das du neue Hinweise gekauft hast! %s',
    UNABLE_TO_SELL: 'Es sieht so aus, als könnten derzeit keine Hinweise verkauft werden. Tut mir leid. Vielleicht bekommst du später wieder. %s',
    NOT_CORRECT: 'Es tut mir Leid. Das ist nicht der Film oder die Serie, an die ich denke. Du kannst weiter raten oder sagen, ich weiß es nicht. Wenn du einen weiteren Hinweis möchtest, sage, einen Hinweis. ',
    CORRECT_ANSWER: 'Das ist richtig! Ich dachte an %s. Möchtest du einen anderen Film oder eine andere Serie erraten?',
    REPLAY_PROMPT: 'Auf deinem Punktekonto stehen dir noch %i Hinweise zur verfügung. Bist du bereit für deine nächste Frage?',
    NO_MORE_CLUES: 'Du hast bereits zwei Hinweise für diesen Film oder Serie bekommen. Ich habe keine weiteren Hinweise für Dich. %s',
    NEW_CLUE: 'OK. Hier kommt dein neuer Hinweis. Und der Schauspieler lautet %s. ',
    UPSELL_MESSAGE: 'Du hast keine nutzbaren Hinweise mehr. Möchtest du etwas über das "Fünf Tipp Packet" erfahren?',
    CURRENTLY_UNAVAILABLE: 'Es tut mir leid. Dieses Hinweispaket kann derzeit nicht gekauft werden.',
    GAME_QUESTION: 'Errate den Film oder die Serie, an die ich denke, diese Star´s %s. <break time=\'.5s\'/> %s',
    ARE_YOU_READY: ' Bist du bereit für eine Frage? ',
  },
};
