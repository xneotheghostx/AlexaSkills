"use strict";

module.exports = Object.freeze({

  // App-ID. TODO: set to your own Skill App ID from the developer portal.
    appId: 'amzn1.ask.skill.815f160e-7e65-4c18-8953-5f5c9ea46069',

  /*
     *  States:
     *  START_MODE : Willkommenszustand, wenn die Audio-Liste nicht begonnen hat.
     *  PLAY_MODE :  When a playlist is being played. Does not imply only active play.
     *               It remains in the state as long as the playlist is not finished.
     *  RESUME_DECISION_MODE : When a user invokes the skill in PLAY_MODE with a LaunchRequest,
     *                         the skill provides an option to resume from last position, or to start over the playlist.
     */
  states: {
    SETUP_MODE: '',
    QUIZ_MODE: '_QUIZ_MODE',
    QUIZ_BUTTON_MODE: '_QUIZ_BUTTON_MODE',
    MINIGAME_ROCKET: '_MINIGAME_ROCKET',
    MINIGAME_SHELL: '_MINIGAME_SHELL',
    MINIGAME_MEMORY: '_MINIGAME_MEMORY',
    MINIGAME_QUICK: '_MINIGAME_QUICK',
    MINIGAME_BUTTON_QUICK: '_MINIGAME_BUTTON_QUICK',
    MINIGAME_WHOAMI: '_MINIGAME_WHOAMI',
    MINIGAME_BUTTON_WHOAMI: '_MINIGAME_BUTTON_WHOAMI'
  }
});