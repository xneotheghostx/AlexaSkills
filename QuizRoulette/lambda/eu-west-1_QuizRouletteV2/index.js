﻿var Alexa = require('alexa-sdk');
var dynamo = require('./dynamoDB');
var quiz = require('./quiz');
var constants = require('./constants');
var moment = require('./moment');

var quick = require('./quick.json');
var whoAmI = require('./whoAmI.json');

// Gadget Directives Builder
const GadgetDirectives = require('utils/gadgetDirectives.js');
// Basic Animation Helper Library
const BasicAnimations = require('utils/basicAnimations.js');

const ANIMATIONS = {
    'Roulette': {
        'targetGadgets': [],
        'animations': BasicAnimations.RouletteAnimation()
    },
    'Button1Idle': {
        'targetGadgets': [],
        'animations': BasicAnimations.SolidAnimation(1, "orange", 60000)
    },
    'Button2Idle': {
        'targetGadgets': [],
        'animations': BasicAnimations.SolidAnimation(1, "blue", 60000)
    },
    'Button3Idle': {
        'targetGadgets': [],
        'animations': BasicAnimations.SolidAnimation(1, "purple", 60000)
    },
    'Button4Idle': {
        'targetGadgets': [],
        'animations': BasicAnimations.SolidAnimation(1, "cyan", 60000)
    },
    'Pressed': {
        'targetGadgets': [],
        'animations': BasicAnimations.BreatheAnimation(20, "yellow", 1000)
    },
    'Correct': {
        'targetGadgets': [],
        'animations': BasicAnimations.CorrectAnimation()
    },
    'Wrong': {
        'targetGadgets': [],
        'animations': BasicAnimations.WrongAnimation()
    },
    'TurnOff': {
        'targetGadgets': [],
        'animations': BasicAnimations.SolidAnimation(1, "black", 1000)
    }
};

const ROLL_CALL_RECOGNIZER = {
    "roll_call_button_recognizer": {
        "type": "match",
        "fuzzy": false,
        "anchor": "end",
        "pattern": [{
            "action": "down"
        }]
    }
};

const ROLL_CALL_EVENTS = {
    "button_checked_in": {
        "meets": ["roll_call_button_recognizer"],
        "reports": "matches",
        "shouldEndInputHandler": true,
        "maximumInvocations": 1
    },
    "timeout": {
        "meets": ["timed out"],
        "reports": "history",
        "shouldEndInputHandler": true
    }
};

const ROLL_CALL_BACK_EVENTS = {
    "button_checked_back": {
        "meets": ["roll_call_button_recognizer"],
        "reports": "matches",
        "shouldEndInputHandler": true,
        "maximumInvocations": 1
    },
    "timeout": {
        "meets": ["timed out"],
        "reports": "history",
        "shouldEndInputHandler": true
    }
};

exports.handler = function (event, context, callback) {
    var alexa = Alexa.handler(event, context);

    alexa.appId = constants.appId;
    alexa.registerHandlers(handlers.setupModeIntentHandlers, handlers.quizModeIntentHandlers, handlers.quizButtonModeIntentHandlers, handlers.rocketIntentHandlers, handlers.shellIntentHandlers, handlers.memoryIntentHandlers, handlers.quickIntentHandlers, handlers.quickButtonIntentHandlers, handlers.whoAmIIntentHandlers, handlers.whoAmIButtonIntentHandlers);
    alexa.execute();
};

var handlers = {

    setupModeIntentHandlers: Alexa.CreateStateHandler(constants.states.SETUP_MODE, {
        'NewSession': function () {

            console.log("Session init");
            this.handler.state = constants.states.SETUP_MODE;

            var self = this;
            dynamo.getUserState(self.event.session, function (data) {

                console.log(data);

                if (data.item) {
                    Object.assign(self.event.session.attributes, data.item);

                    if (self.attributes["players"].length > 0) {
                        self.emit('SaveFoundIntent');
                    } else {
                        self.emit('ResetIntent');
                    }
                } else {
                    self.emit('ResetIntent');
                }
            });
        },
        'LaunchRequest': function () {

            this.emitWithState('NewSession');
        },
        'ResetIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            this.attributes['players'] = [];

            this.attributes['used'] = '{}';

            this.attributes["echoButtons"] = false;
            this.attributes["minigames"] = false;
            this.attributes["round"] = 0;
            this.attributes["maxRounds"] = 20;

            this.attributes["currentPlayer"] = 0;

            this.emitWithState('NewGameIntent', speech);
        },
        'NewGameIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            speech += "Willkommen beim Quiz-Roulette, ich bin Alexa und werde dich im Spiel begleiten. Möchtest du die Regeln hören?";
            this.attributes["setupState"] = "RequestRoles";
            this.emit(':ask', speech, speech);
        },
        'SaveFoundIntent': function () {

            var speech = "Willkommen zurück beim Quiz-Roulette. Ich habe einen alten Spielstand von ";

            for (var i = 0; i < this.attributes["players"].length; i++) {

                if (this.attributes["players"].length == 1) {
                    speech += this.attributes["players"][i].name;
                } else if (i == this.attributes["players"].length - 1) {
                    speech += " und " + this.attributes["players"][i].name;
                } else {
                    speech += this.attributes["players"][i].name + ", ";
                }
            }

            speech += " gefunden. Wenn du von dort aus weiterspielen möchtest, sage weiter spielen. Wenn du eine neue Runde starten willst sage neues spiel. "

            this.emit(':ask', speech, speech);
        },
        'ContinueIntent': function () {

            if (this.attributes["echoButtons"]) {

                this.attributes["registeredPlayer"] = 0;
                var speech = "Okay, " + this.attributes["players"][this.attributes["registeredPlayer"]].name + " bitte drücke deinen Echo Button. ";

                this.response.speak(speech);

                // StartInputHandler
                this.response._addDirective(GadgetDirectives.startInputHandler({
                    "timeout": 40000,
                    "recognizers": ROLL_CALL_RECOGNIZER,
                    "events": ROLL_CALL_BACK_EVENTS
                }));

                this.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.Button1Idle, { 'targetGadgets': [] }));
                this.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.Button1Idle, { 'targetGadgets': [] }));
                this.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.Button1Idle, { 'targetGadgets': [] }));

                // start keeping track of some state
                // see: https://developer.amazon.com/docs/gadget-skills/save-state-echo-button-skill.html
                this.attributes.buttonCount = 0;
                this.attributes.isRollCallComplete = false;
                this.attributes.expectingEndSkillConfirmation = false;
                // setup an array of DeviceIDs to hold IDs of buttons that will be used in the skill
                this.attributes.DeviceIDs = [];
                this.attributes.DeviceIDs[0] = "Device ID listings";
                // Save StartInput Request ID
                this.attributes.CurrentInputHandlerID = this.event.request.requestId;

                this.emit('GlobalResponseReady', { 'openMicrophone': false });
            } else {
                this.handler.state = constants.states.QUIZ_MODE;
                this.emitWithState('NextQuestionIntent');
            }
        },
        'EchoButtonsIntent': function () {

            this.attributes["setupState"] = "Buttons";
            var speech = "Quiz-Roulette unterstützt ab jetzt Echo Buttons! Möchtest du mit Echo Buttons spielen? Sage Ja oder Nein. "
            this.emit(':ask', speech, speech);
        },
        'SetNameIntent': function () {

            if (this.attributes["setupState"] && this.attributes["setupState"] === "Buttons") {

                this.attributes["setupState"] = "AssignName";
                var speech = "";

                if (this.attributes["echoButtons"]) {
                    speech = "Der erste Spieler drückt bitte seinen Echo Button. ";

                    this.response.speak(speech);

                    // StartInputHandler
                    this.response._addDirective(GadgetDirectives.startInputHandler({
                        "timeout": 40000,
                        "recognizers": ROLL_CALL_RECOGNIZER,
                        "events": ROLL_CALL_EVENTS
                    }));

                    // start keeping track of some state
                    // see: https://developer.amazon.com/docs/gadget-skills/save-state-echo-button-skill.html
                    this.attributes.buttonCount = 0;
                    this.attributes.isRollCallComplete = false;
                    this.attributes.expectingEndSkillConfirmation = false;
                    // setup an array of DeviceIDs to hold IDs of buttons that will be used in the skill
                    this.attributes.DeviceIDs = [];
                    this.attributes.DeviceIDs[0] = "Device ID listings";
                    // Save StartInput Request ID
                    this.attributes.CurrentInputHandlerID = this.event.request.requestId;

                    this.emit('GlobalResponseReady', { 'openMicrophone': false });
                } else {
                    speech = "Nenne den Namen des ersten Spielers.";
                    this.emit(':ask', speech, speech);
                }
            }
            this.emit('Unhandled');
        },

        'GameEngine.InputHandlerEvent': function () {
            console.log("rollCallModeIntentHandlers::InputHandlerEvent");
            if (this.event.request.originatingRequestId != this.attributes.CurrentInputHandlerID) {
                console.log("Stale input event received: received event from " + this.event.request.originatingRequestId + "; expecting " + this.attributes.CurrentInputHandlerID);

                this.emit('GlobalResponseReady', { 'openMicrophone': false });
                return;
            }

            var gameEngineEvents = this.event.request.events || [];
            for (var i = 0; i < gameEngineEvents.length; i++) {
                // In this request type, we'll see one or more incoming events
                // that correspond to the StartInputHandler we sent above.

                if (gameEngineEvents[i].name == 'button_checked_back') {

                    if (this.attributes.DeviceIDs.indexOf(gameEngineEvents[i].inputEvents[0].gadgetId) > -1) {
                        var speech = "Dieser Echo Button wurde bereits registriert. " + this.attributes["players"][this.attributes["registeredPlayer"]].name + " bitte drücke deinen Echo Button. ";

                        this.response.speak(speech);

                        // StartInputHandler
                        this.response._addDirective(GadgetDirectives.startInputHandler({
                            "timeout": 40000,
                            "recognizers": ROLL_CALL_RECOGNIZER,
                            "events": ROLL_CALL_BACK_EVENTS
                        }));

                        // Save StartInput Request ID
                        this.attributes.CurrentInputHandlerID = this.event.request.requestId;

                        this.emit('GlobalResponseReady', { 'openMicrophone': false });
                    } else {
                        if (this.attributes["registeredPlayer"] <= this.attributes["players"].length - 1) {

                            this.attributes["players"][this.attributes["registeredPlayer"]].device = gameEngineEvents[i].inputEvents[0].gadgetId;

                            this.attributes.DeviceIDs.push(gameEngineEvents[i].inputEvents[0].gadgetId);
                            this.attributes.buttonCount = this.attributes.buttonCount + 1;

                            if (this.attributes["registeredPlayer"] == this.attributes["players"].length - 1) {
                                this.handler.state = constants.states.QUIZ_BUTTON_MODE;
                                this.emitWithState('NextQuestionIntent');
                            } else {
                                setDefaultButtonColor(this);

                                this.attributes["registeredPlayer"]++;
                                var speech = "Okay, " + this.attributes["players"][this.attributes["registeredPlayer"]].name + " bitte drücke deinen Echo Button. ";

                                this.response.speak(speech).listen(speech);

                                // StartInputHandler
                                this.response._addDirective(GadgetDirectives.startInputHandler({
                                    "timeout": 40000,
                                    "recognizers": ROLL_CALL_RECOGNIZER,
                                    "events": ROLL_CALL_BACK_EVENTS
                                }));

                                // Save StartInput Request ID
                                this.attributes.CurrentInputHandlerID = this.event.request.requestId;

                                //this.attributes.isRollCallComplete = true;
                                //this.handler.state = SKILL_STATES.PLAY_MODE;

                                this.emit('GlobalResponseReady', { 'openMicrophone': false });
                            }
                        }
                    }
                } else if (gameEngineEvents[i].name == 'button_checked_in') {

                    if (this.attributes.DeviceIDs.indexOf(gameEngineEvents[i].inputEvents[0].gadgetId) > -1) {

                        var speech = "Dieser Echo Button wurde bereits registriert. Der nächste Spieler drückt bitte seinen Echo Button. ";
                        this.response.speak(speech);

                        // StartInputHandler
                        this.response._addDirective(GadgetDirectives.startInputHandler({
                            "timeout": 40000,
                            "recognizers": ROLL_CALL_RECOGNIZER,
                            "events": ROLL_CALL_EVENTS
                        }));

                        // Save StartInput Request ID
                        this.attributes.CurrentInputHandlerID = this.event.request.requestId;

                        this.emit('GlobalResponseReady', { 'openMicrophone': false });
                    } else {

                        // reset button press animations until the user chooses a color
                        //this.response._addDirective(GadgetDirectives.setButtonDownAnimation(DEFUALT_ANIMATIONS.ButtonDown));
                        //this.response._addDirective(GadgetDirectives.setButtonUpAnimation(DEFUALT_ANIMATIONS.ButtonUp));

                        //this.attributes.isRollCallComplete = true;
                        //this.handler.state = SKILL_STATES.PLAY_MODE;

                        this.attributes.DeviceIDs.push(gameEngineEvents[i].inputEvents[0].gadgetId);

                        this.attributes.buttonCount = this.attributes.buttonCount + 1;

                        let outputSpeech = "Okay, bitte nenne deinen Namen";

                        this.response.speak(outputSpeech).listen(outputSpeech);

                        setDefaultButtonColor(this);
                        this.emit('GlobalResponseReady', { 'openMicrophone': true });
                    }

                } else if (gameEngineEvents[i].name == 'timeout') {
                    const outputSpeech = "Es wurde kein Echo Button gedrückt. Bitte sage neustarten oder weiterspielen, um es nochmal zu versuchen oder ohne Echo Buttons zu spielen. ";

                    this.response.speak(outputSpeech).listen(outputSpeech);

                    //let deviceIds = this.attributes.DeviceIDs;
                    //deviceIds = deviceIds.slice(-2);

                    // Reset button animations while we figure out what the user wants to do next
                    //this.response._addDirective(GadgetDirectives.setIdleAnimation(ROLL_CALL_ANIMATIONS.Timeout, { 'targetGadgets': deviceIds }));
                    //this.response._addDirective(GadgetDirectives.setButtonDownAnimation(DEFUALT_ANIMATIONS.ButtonDown, { 'targetGadgets': deviceIds }));
                    //this.response._addDirective(GadgetDirectives.setButtonUpAnimation(DEFUALT_ANIMATIONS.ButtonUp, { 'targetGadgets': deviceIds }));

                    // set a flag that we can use to determine that the confirmation is for a timeout
                    //this.attributes.expectingEndSkillConfirmation = true;

                    this.emit('GlobalResponseReady', { 'openMicrophone': true });
                    return;
                }
            }
        },
        'GlobalResponseReady': function ({ openMicrophone = false } = {}) {
            console.log('globalHandlers::GlobalResponseReady (openMicrophone = ' + openMicrophone + ')');
            // we trigger the `GlobalResponseReady` event from other handlers - the openMicrophone parameter controls the microphone behavior

            if (openMicrophone) { // setting shouldEndSession = fase  -  lets Alexa know that we're looking for an answer from the user 
                this.handler.response.response.shouldEndSession = false;
            } else { // deleting shouldEndSession will keep the skill session going, while the input handler is active, waiting for button presses
                delete this.handler.response.response.shouldEndSession;
            }

            // log the response to CloudWatch to make it easier to debug the skill
            console.log("==Response== " + JSON.stringify(this.handler.response));
            this.emit(':responseReady');
        },

        'NameIntent': function () {

            var name = this.event.request.intent.slots.name.value;
            
            if(typeof name === 'undefined')
                var name = "spieler";

            name = name.replace(/\W/g, '');
            name = name.toLowerCase();

            if (this.attributes["setupState"] && (this.attributes["setupState"] === "AssignName" || this.attributes["setupState"] === "ChangeName")) {

                if (this.attributes["setupState"] === "AssignName")
                    if (this.attributes["echoButtons"])
                        this.attributes['players'].push({ name: this.event.request.intent.slots.name.value, score: 0, device: this.attributes.DeviceIDs[this.attributes.DeviceIDs.length - 1] });
                    else
                        this.attributes['players'].push({ name: this.event.request.intent.slots.name.value, score: 0 });
                else if (this.attributes["setupState"] === "ChangeName")
                    this.attributes['players'][this.attributes['players'].length - 1].name = this.event.request.intent.slots.name.value;

                this.attributes["setupState"] = "IsNameCorrect";

                var speech = "Okay, Spieler " + this.attributes['players'].length + " heißt jetzt " + this.attributes['players'][this.attributes['players'].length - 1].name + ". Ist das richtig?";
                this.emit(':ask', speech, speech);
            }
            this.emit('Unhandled');
        },

        'NumberIntent': function () {

            var number = this.event.request.intent.slots.number.value;

            if (!isNaN(number) && this.attributes["setupState"] == "maxRounds") {
                if (number >= 5) {
                    this.attributes["maxRounds"] = number;
                    speech = "Ich habe " + number + " Runden verstanden. Ist das richtig? ";
                    this.emit(':ask', speech, speech);
                } else {
                    speech = "Es müssen mindestens 5 Runden gespielt werden. ";
                    this.emit(':ask', speech, speech);
                }
            }
            this.emit('Unhandled');
        },

        'MinigamesIntent': function () {
            this.attributes["setupState"] = "RequestMinigames";

            var speech = "Möchtest du mit Minispielen zwischen den Quiz-Runden spielen.";
            this.emit(':ask', speech, speech);
        },

        'ReadyIntent': function () {

            if (this.attributes["setupState"] && this.attributes["setupState"] === "PlayerReady") {

                if (this.attributes["echoButtons"])
                    this.handler.state = constants.states.QUIZ_BUTTON_MODE;
                else
                    this.handler.state = constants.states.QUIZ_MODE;

                this.emitWithState('NextRoundIntent');
            }
            this.emit('Unhandled');
        },

        'AMAZON.YesIntent': function () {

            if (this.attributes["setupState"]) {
                if (this.attributes["setupState"] === "Buttons") {

                    this.attributes["echoButtons"] = true;
                    this.emit('SetNameIntent');
                } else if (this.attributes["setupState"] === "RequestRoles") {

                    this.emit('AMAZON.HelpIntent');
                } else if (this.attributes["setupState"] === "IsNameCorrect") {

                    if ((this.attributes["echoButtons"] && this.attributes["players"].length >= 4) || (!this.attributes["echoButtons"] && this.attributes["players"].length >= 10)) {

                        this.attributes["setupState"] = "maxRounds";
                        var speech = "Du hast die maximale Spielerzahl erreicht. Wie viele Runden möchtest du spielen. 10 Runden dauern, bei einer Gruppen-Größe von 2 Spielern, 15 Minuten. ";
                        this.emit(':ask', speech, speech);
                    }
                    this.attributes["setupState"] = "AssignName";
                    
                    var speech = "Okay, möchtest du einen weiteren Spieler hinzufügen.";
                    this.emit(':ask', speech, speech);
                } else if (this.attributes["setupState"] === "AssignName") {
                    
                    if (this.attributes["echoButtons"]) {
                        var speech = "Der nächste Spieler drückt bitte seinen Echo Button. ";
                        this.response.speak(speech);

                        // StartInputHandler
                        this.response._addDirective(GadgetDirectives.startInputHandler({
                            "timeout": 40000,
                            "recognizers": ROLL_CALL_RECOGNIZER,
                            "events": ROLL_CALL_EVENTS
                        }));

                        // Save StartInput Request ID
                        this.attributes.CurrentInputHandlerID = this.event.request.requestId;

                        this.emit('GlobalResponseReady', { 'openMicrophone': false });
                    } else {
                        var speech = "Nenne seinen Namen. ";
                        this.emit(':ask', speech, speech);
                    }
                } else if (this.attributes["setupState"] === "maxRounds") {

                    if (this.attributes['players'].length == 1) {

                        this.attributes["setupState"] = "PlayerReady";

                        var speech = "Okay. ";
                        for (var i = 0; i < this.attributes["players"].length; i++) {
                            speech += this.attributes["players"][i].name + ", ";
                        }
                        speech += " wenn du bereit bist, sag bereit.";
                        this.emit(':ask', speech, speech);
                    }

                    this.emit('MinigamesIntent');
                } else if (this.attributes["setupState"] === "RequestMinigames") {

                    this.attributes["minigames"] = true;
                    this.attributes["setupState"] = "PlayerReady";

                    var speech = "Okay. ";
                    for (var i = 0; i < this.attributes["players"].length; i++) {
                        speech += this.attributes["players"][i].name + ", ";
                    }
                    speech += " wenn ihr bereit seid, sag bereit.";
                    this.emit(':ask', speech, speech);
                }
            }
            this.emit('Unhandled');
        },
        'AMAZON.NoIntent': function () {

            if (this.attributes["setupState"]) {
                if (this.attributes["setupState"] === "Buttons") {

                    this.attributes["echoButtons"] = false;
                    this.emit('SetNameIntent');
                } else if (this.attributes["setupState"] === "RequestRoles") {

                    this.emit('EchoButtonsIntent');
                } else if (this.attributes["setupState"] === "IsNameCorrect") {

                    this.attributes["setupState"] = "ChangeName";

                    var speech = "Nenne mir seinen Namen.";
                    this.emit(':ask', speech, speech);
                } else if (this.attributes["setupState"] === "AssignName") {

                    this.attributes["setupState"] = "maxRounds";
                    var speech = "Wie viele Runden möchtest du spielen. 10 Runden dauern, bei einer Gruppen-Größe von 2 Spielern, 15 Minuten. ";
                    this.emit(':ask', speech, speech);
                    //this.emit('MinigamesIntent')
                } else if (this.attributes["setupState"] === "maxRounds") {

                    var speech = "Wie viele Runden soll gespielt werden. ";
                    this.emit(':ask', speech, speech);
                } else if (this.attributes["setupState"] === "RequestMinigames") {

                    this.attributes["minigames"] = false;
                    this.attributes["setupState"] = "PlayerReady";

                    var speech = "Okay. ";
                    for (var i = 0; i < this.attributes["players"].length; i++) {
                        speech += this.attributes["players"][i].name + ", ";
                    }
                    speech += " wenn ihr bereit seid, sag bereit.";
                    this.emit(':ask', speech, speech);
                }
            }
            this.emit('Unhandled');
        },

        'AMAZON.HelpIntent': function () {
            
            var speech = "Quiz-Roulette ist ein Quiz mit allgemeinen Fragen und Mini Spielen nach jeder 5. Runde. Je Frage gibt es 3 Antwortmöglichkeiten. Du musst mit 1, 2 oder 3 antworten. <break time='100ms' />";
            speech += "Solltest du mal eine Frage nicht verstanden haben, kannst du wiederholen sagen und ich werde die Frage wiederholen. <break time='100ms' />";
            speech += "Du kannst alleine oder in einer Gruppe von bis zu 10 Spielern spielen. Mini Spiele kannst du erst ab einer Anzahl von 2 Spielern spielen. <break time='100ms' />";
            speech += "Du erhältst für jede richtige Antwort einen Punkt. Der Spieler mit dem meisten Punkten, nach Ende der letzten Runde, gewinnt. <break time='100ms' />";
            speech += "In den Mini Spielen gibt es zusätzlich Punkte, damit kann ein zurückliegender Spieler schnell aufholen. <break time='100ms' />";
            speech += "Wenn du möchtest kann ich dir jederzeit, den  Punktestand nennen. Sage einfach Alexa Punktestand. <break time='100ms' />";
            speech += "Du kannst das Spiel jeder Zeit pausieren, indem du Alexa Stop sagst. Weiterspielen kannst du, indem du den Skill erneut startest. <break time='200ms' />";
            speech += "Es kommen noch einige Informationen, wenn du mit Echo Buttons spielen möchtest. Um zu überspringen sage Alexa fortfahren. <break time='100ms' />";
            speech += "Beim ersten Anmelden der Echo Buttons kann es zu Verzögerungen kommen. Habe also ein wenig Geduld. <break time='100ms' />";
            speech += "Bitte seid fair und drückt den Echo Button nicht ohne Grund, denn ihr unterbrecht mich damit. So bekommt auch jeder die Fragen mit und hat eine Chance. <break time='100ms' />";
            speech += "Viel Spaß beim Spielen. Sage fortfahren.";
            this.emit(':ask', speech, "Sage fortfahren.");
        },
        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            this.attributes = updateRatingAttributes(this.attributes);
            var speech = this.attributes["ratingSpeech"];

            console.log("should end......");

            var self = this;
            dynamo.putUserState(self.event.session, function (data) {
                console.log(data.message);

                if(speech == "")
                    speech = "Okay, auf wiederhören.";
                self.response.speak(speech);

                self.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.TurnOff));
                self.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.TurnOff));
                self.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.TurnOff));
                self.handler.response.response.shouldEndSession = true;
                self.emit(':responseReady');
            });
        },
        'Unhandled': function () {
            var speech = 'Entschuldigung, Ich habe dich nicht verstanden.';
            this.emit(':ask', speech, speech);
        }
    }),
    quizModeIntentHandlers: Alexa.CreateStateHandler(constants.states.QUIZ_MODE, {

        'AnswerIntent': function () {

            var name = this.event.request.intent.slots.answer.value;
            name = name.replace(/\W/g, '');
            name = name.toLowerCase();

            if (name == "eins" || name == "zwei" || name == "drei") {
                if (name == "eins")
                    this.attributes["currentAnswer"] = "1";
                else if (name == "zwei")
                    this.attributes["currentAnswer"] = "2";
                else if (name == "drei")
                    this.attributes["currentAnswer"] = "3";
                this.emitWithState('NextQuestionIntent');
            } else {
                this.emit('Unhandled');
            }
        },
        'NextRoundIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            if (this.attributes["round"] >= this.attributes["maxRounds"]) {
                this.emitWithState('EndIntent', speech);
            }

            this.attributes["round"] = this.attributes["round"] + 1;

            if (this.attributes["players"].length > 1)
                speech += "Runde <say-as interpret-as='number'>" + this.attributes["round"] + "</say-as>. <break time='500ms' />";

            this.emitWithState('NextQuestionIntent', speech);
        },
        'NextQuestionIntent': function (speech) {
            console.log("Intent: NextQuestionIntent");

            if (typeof this.attributes["players"][this.attributes["currentPlayer"]] === 'undefined')
                this.emitWithState('Unhandled');

            if (typeof speech === 'undefined')
                var speech = "";

            var used = JSON.parse(this.attributes['used'] || '{}');
            var currentQuestionId = this.attributes["q"];
            //console.log('answer question=' + currentQuestionId + ' session=', session);

            // Wenn eine Antwort gegeben wurde. Erst das Richtig oder Falsch ausgeben.
            if (this.attributes["currentAnswer"] && this.attributes["currentAnswer"] != "") {

                var q = currentQuestionId ? quiz.getQuestion(currentQuestionId) : null;
                // found question in session; check answer
                if (q) {
                    var answer = this.attributes["currentAnswer"] || 'X';
                    answer = answer.slice(0, 1).toUpperCase();
                    if (q.validAnswers().indexOf(answer) < 0) {
                        answer = 'X';
                    }
                    console.log('answer normalized=' + answer);
                    if (q.isCorrect(answer)) {
                        speech += "<audio src='https://nordicskills.de/audio/MiniGame/correct.mp3' /> Die Antwort ist richtig. Du erhältst einen Punkt .";
                        if(this.attributes["players"][this.attributes["currentPlayer"]])
                            this.attributes["players"][this.attributes["currentPlayer"]].score += 1;
                    } else {
                        speech += "<audio src='https://nordicskills.de/audio/MiniGame/incorrect.mp3' /> Das war falsch, die richtige Antwort wäre " + q.answerText() + ".";
                    }
                    //say.push(q.explanation());
                    // save question and answer to used questions
                    used[currentQuestionId] = answer;
                }
                this.attributes['used'] = JSON.stringify(used);
                console.log('questions=', Object.keys(used).length);
                this.attributes["currentAnswer"] = "";

                this.attributes["currentPlayer"] = this.attributes["currentPlayer"] + 1;
                if (this.attributes["currentPlayer"] == this.attributes["players"].length) {
                    this.attributes["currentPlayer"] = 0;

                    if (this.attributes["players"].length > 1) {
                        speech += "Runde " + this.attributes["round"] + " ist vorbei. <audio src='https://nordicskills.de/audio/MiniGame/short-transition.mp3' />";

                        if (this.attributes["minigames"] && this.attributes["round"] != 0 && this.attributes["round"] % 5 == 0) {
                            this.attributes = randomMinigame(this.attributes);

                            speech += "Mini Spiel-Runde! <audio src='https://nordicskills.de/audio/MiniGame/roulette.mp3' /> <audio src='https://nordicskills.de/audio/MiniGame/ding.mp3' /> " + minigames[this.attributes["currentMinigameID"]].desc + ". ";

                            var minigame = minigames[this.attributes['currentMinigameID']].name;

                            this.handler.state = "_MINIGAME_" + minigame.toUpperCase();
                            this.emitWithState(minigame + "StartIntent", speech);
                        }

                        if (this.attributes["round"] != 0 && this.attributes["round"] % 6 == 0) {
                            speech += scoreboard(this.attributes);
                        }
                    }
                    this.emitWithState("NextRoundIntent", speech);
                }
            }

            // get next question
            var next = quiz.getNextQuestion(Object.keys(used));
            if (next) {
                //say.push('<s>Question ' + (numQuestions + 1) + '. <break strength="x-strong" /></s>');
                if (this.attributes["players"].length > 1) {
                    speech += "Frage an " + this.attributes["players"][this.attributes["currentPlayer"]].name + ". <break time='500ms' />";
                }
                speech += next.questionAndAnswers();
                this.attributes["q"] = next.id;
                //response.shouldEndSession(false, 'What do you think? Is it ' + next.choices() + '?');
            } else {
                speech += "Das waren alle Fragen. Ab jetzt könnte es zu Wiederholungen kommen. ";
            }

            var reprompt = "Wenn ich die Frage wiederholen soll, sage wiederholen.";

            this.emit(':ask', speech, reprompt);
        },
        'ScoresIntent': function () {
    
            var speech = scoreboard(this.attributes);
            speech += "<break time='500ms' />";
            this.emitWithState("NextQuestionIntent", speech);
        },
        'RepeatIntent': function () {

            var q = quiz.getQuestion(this.attributes["q"]);
            if (q) {
                var speech = "Ich wiederhole. " + q.questionAndAnswers();
                this.emit(':ask', speech, speech);
            }
        },

        'EndIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            speech += "<audio src='https://nordicskills.de/audio/MiniGame/short-transition.mp3' /> Endstand: <break time='500ms' />";

            var sortedScores = this.attributes["players"].concat().sort(function (a, b) { return (a.score > b.score) ? 1 : ((b.score < a.score) ? -1 : 0); });
            for (var i = 0; i < sortedScores.length; i++) {
                if (i == 0) {
                    speech += "Den letzten Platz belegt ";
                } else if (i == sortedScores.length - 1) {
                    speech += "Der Gewinner des Quiz-Roulettes ist damit ";
                } else {
                    speech += "Platz " + (sortedScores.length - i) + " belegt ";
                } 

                speech += sortedScores[i].name + " mit " + (sortedScores[i].score == 1 ? "einem Punkt" : (sortedScores[i].score + " Punkten")) + ". <break time='500ms' />";
            }
            speech += "Glückwunsch! <break time='500ms' /> Warte kurz, um ein neues Spiel zu starten oder sage beenden, um das Quiz zu beenden. <break time='1s' />"

            this.handler.state = constants.states.SETUP_MODE;
            delete this.attributes['STATE'];
            this.emitWithState("ResetIntent", speech);
        },

        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            this.attributes = updateRatingAttributes(this.attributes);
            var speech = this.attributes["ratingSpeech"];

            console.log("should end......");

            var self = this;
            dynamo.putUserState(self.event.session, function (data) {
                console.log(data.message);

                if (speech == "")
                    speech = "Okay, auf wiederhören.";
                self.emit(':tell', speech);
            });
        },
        'Unhandled': function () {
            var speech = 'Entschuldigung, Ich habe dich nicht verstanden.';
            this.emit(':ask', speech, speech);
        }
    }),
    quizButtonModeIntentHandlers: Alexa.CreateStateHandler(constants.states.QUIZ_BUTTON_MODE, {

        'GameEngine.InputHandlerEvent': function () {
            console.log("rollCallModeIntentHandlers::InputHandlerEvent");
            if (this.event.request.originatingRequestId != this.attributes.CurrentInputHandlerID) {
                console.log("Stale input event received: received event from " + this.event.request.originatingRequestId + "; expecting " + this.attributes.CurrentInputHandlerID);

                this.emit('GlobalResponseReady', { 'openMicrophone': false });
                return;
            }

            var gameEngineEvents = this.event.request.events || [];
            for (var i = 0; i < gameEngineEvents.length; i++) {
                // In this request type, we'll see one or more incoming events
                // that correspond to the StartInputHandler we sent above.

                if (gameEngineEvents[i].name == 'button_checked_in') {

                    //this.attributes.DeviceIDs.push(gameEngineEvents[i].inputEvents[0].gadgetId);

                    //this.attributes.buttonCount = this.attributes.buttonCount + 1;
                    var index = 0;
                    for (var j = 0; j < this.attributes["players"].length; j++) {
                        if (this.attributes["players"][j].device == gameEngineEvents[i].inputEvents[0].gadgetId)
                            index = j;
                    }
                    
                    this.attributes["playerIndex"] = index;

                    this.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.TurnOff));
                    this.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.TurnOff));
                    this.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.TurnOff));

                    this.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.Pressed, { 'targetGadgets': [this.attributes["players"][index].device] }));
                    this.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.Pressed, { 'targetGadgets': [this.attributes["players"][index].device] }));
                    this.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.Pressed, { 'targetGadgets': [this.attributes["players"][index].device] }));

                    let outputSpeech = "<audio src='https://nordicskills.de/audio/MiniGame/ding.mp3' /> " + this.attributes["players"][index].name + ", kennst du die Lösung? Sage eins, zwei oder drei. ";

                    this.response.speak(outputSpeech).listen(outputSpeech);

                    /*let deviceIds = this.attributes.DeviceIDs;
                    deviceIds = deviceIds.slice(-this.attributes.buttonCount);

                    // send an idle animation to registered buttons
                    this.response._addDirective({
                        "type": "GadgetController.SetLight",
                        "version": 1,
                        "targetGadgets": deviceIds,
                        "parameters": {
                            "animations": BasicAnimations.FadeInAnimation(1, "green", 5000),
                            "triggerEvent": "none",
                            "triggerEventTimeMs": 0,
                        }
                    });*/
                    // reset button press animations until the user chooses a color
                    //this.response._addDirective(GadgetDirectives.setButtonDownAnimation(DEFUALT_ANIMATIONS.ButtonDown));
                    //this.response._addDirective(GadgetDirectives.setButtonUpAnimation(DEFUALT_ANIMATIONS.ButtonUp));

                    //this.attributes.isRollCallComplete = true;
                    //this.handler.state = SKILL_STATES.PLAY_MODE;

                    this.emit('GlobalResponseReady', { 'openMicrophone': true });

                } else if (gameEngineEvents[i].name == 'timeout') {
                    const outputSpeech = "Es wurde kein Echo Button gedrückt. Ich werde die Frage gleich nochnmal wiederholen. Wenn ihr die Lösung nicht kennt, ratet einfach, es gibt keinen Punktabzug für falsche Antworten. ";

                    //this.response.speak(outputSpeech).listen(outputSpeech);

                    //let deviceIds = this.attributes.DeviceIDs;
                    //deviceIds = deviceIds.slice(-2);

                    // Reset button animations while we figure out what the user wants to do next
                    //this.response._addDirective(GadgetDirectives.setIdleAnimation(ROLL_CALL_ANIMATIONS.Timeout, { 'targetGadgets': deviceIds }));
                    //this.response._addDirective(GadgetDirectives.setButtonDownAnimation(DEFUALT_ANIMATIONS.ButtonDown, { 'targetGadgets': deviceIds }));
                    ///this.response._addDirective(GadgetDirectives.setButtonUpAnimation(DEFUALT_ANIMATIONS.ButtonUp, { 'targetGadgets': deviceIds }));

                    // set a flag that we can use to determine that the confirmation is for a timeout
                    //this.attributes.expectingEndSkillConfirmation = true;

                    //this.emit('GlobalResponseReady', { 'openMicrophone': true });
                    this.emitWithState('RepeatIntent', outputSpeech)
                    return;
                }
            }
        },
        'GlobalResponseReady': function ({ openMicrophone = false } = {}) {
            console.log('globalHandlers::GlobalResponseReady (openMicrophone = ' + openMicrophone + ')');
            // we trigger the `GlobalResponseReady` event from other handlers - the openMicrophone parameter controls the microphone behavior

            if (openMicrophone) { // setting shouldEndSession = fase  -  lets Alexa know that we're looking for an answer from the user 
                this.handler.response.response.shouldEndSession = false;
            } else { // deleting shouldEndSession will keep the skill session going, while the input handler is active, waiting for button presses
                delete this.handler.response.response.shouldEndSession;
            }

            // log the response to CloudWatch to make it easier to debug the skill
            console.log("==Response== " + JSON.stringify(this.handler.response));
            this.emit(':responseReady');
        },



        'AnswerIntent': function () {

            var name = this.event.request.intent.slots.answer.value;
            name = name.replace(/\W/g, '');
            name = name.toLowerCase();

            if (name == "eins" || name == "zwei" || name == "drei") {
                if (name == "eins")
                    this.attributes["currentAnswer"] = "1";
                else if (name == "zwei")
                    this.attributes["currentAnswer"] = "2";
                else if (name == "drei")
                    this.attributes["currentAnswer"] = "3";
                this.emitWithState('NextQuestionIntent');
            } else {
                this.emit('Unhandled');
            }
        },
        'NextRoundIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            if (this.attributes["round"] >= this.attributes["maxRounds"]) {
                this.emitWithState('EndIntent', speech);
            }

            this.attributes["round"] = this.attributes["round"] + 1;

            if (this.attributes["players"].length > 1)
                speech += "Runde <say-as interpret-as='number'>" + this.attributes["round"] + "</say-as>. <break time='500ms' />";

            this.emitWithState('NextQuestionIntent', speech);
        },
        'NextQuestionIntent': function (speech) {
            console.log("Intent: NextQuestionIntent");

            // Uncomment for testing minigames
            //this.attributes["currentPlayer"] = this.attributes["players"].length - 1;
            //this.attributes["round"] = 5;
            //this.attributes["currentAnswer"] = "1";

            if (typeof this.attributes["players"][this.attributes["currentPlayer"]] === 'undefined')
                this.emitWithState('Unhandled');

            if (typeof speech === 'undefined')
                var speech = "";

            var used = JSON.parse(this.attributes['used'] || '{}');
            var currentQuestionId = this.attributes["q"];
            //console.log('answer question=' + currentQuestionId + ' session=', session);

            // Wenn eine Antwort gegeben wurde. Erst das Richtig oder Falsch ausgeben.
            if (this.attributes["currentAnswer"] && this.attributes["currentAnswer"] != "") {

                var q = currentQuestionId ? quiz.getQuestion(currentQuestionId) : null;
                // found question in session; check answer
                if (q) {
                    var answer = this.attributes["currentAnswer"] || 'X';
                    answer = answer.slice(0, 1).toUpperCase();
                    if (q.validAnswers().indexOf(answer) < 0) {
                        answer = 'X';
                    }
                    console.log('answer normalized=' + answer);
                    if (q.isCorrect(answer)) {
                        speech += "<audio src='https://nordicskills.de/audio/MiniGame/correct.mp3' /> Die Antwort ist richtig. Du erhältst einen Punkt. ";
                        this.attributes["players"][this.attributes["playerIndex"]].score += 1;
                    } else {
                        speech += "<audio src='https://nordicskills.de/audio/MiniGame/incorrect.mp3' /> Das war falsch, die richtige Antwort wäre " + q.answerText() + ".";
                    }
                    //say.push(q.explanation());
                    // save question and answer to used questions
                    used[currentQuestionId] = answer;
                }
                this.attributes['used'] = JSON.stringify(used);
                console.log('questions=', Object.keys(used).length);
                this.attributes["currentAnswer"] = "";

                this.attributes["currentPlayer"] = this.attributes["currentPlayer"] + 1;
                if (this.attributes["currentPlayer"] == this.attributes["players"].length) {
                    this.attributes["currentPlayer"] = 0;

                    if (this.attributes["players"].length > 1) {
                        speech += "Runde " + this.attributes["round"] + " ist vorbei. <audio src='https://nordicskills.de/audio/MiniGame/short-transition.mp3' />";

                        if (this.attributes["minigames"] && this.attributes["round"] != 0 && this.attributes["round"] % 5 == 0) {
                            this.attributes = randomMinigame(this.attributes);

                            speech += "Mini Spiel-Runde! <audio src='https://nordicskills.de/audio/MiniGame/roulette.mp3' /> <audio src='https://nordicskills.de/audio/MiniGame/ding.mp3' /> " + minigames[this.attributes["currentMinigameID"]].desc + ". ";

                            var minigame = minigames[this.attributes['currentMinigameID']].name;

                            this.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.Roulette));
                            this.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.Roulette));
                            this.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.Roulette));

                            if (minigame == "WhoAmI" || minigame == "Quick") {
                                this.handler.state = "_MINIGAME_BUTTON_" + minigame.toUpperCase();
                                this.emitWithState(minigame + "StartIntent", speech);
                            } else {
                                this.handler.state = "_MINIGAME_" + minigame.toUpperCase();
                                this.emitWithState(minigame + "StartIntent", speech);
                            }
                        }

                        if (this.attributes["round"] != 0 && this.attributes["round"] % 6 == 0) {
                            speech += scoreboard(this.attributes);
                        }
                    }
                    this.emitWithState("NextRoundIntent", speech);
                }
            }

            setDefaultButtonColor(this);

            // get next question
            var next = quiz.getNextQuestion(Object.keys(used));
            if (next) {
                //say.push('<s>Question ' + (numQuestions + 1) + '. <break strength="x-strong" /></s>');
                /*if (this.attributes["players"].length > 1) {
                    speech += "Frage an " + this.attributes["players"][this.attributes["currentPlayer"]].name + ". <break time='500ms' />";
                }*/
                speech += next.questionAndAnswers();
                this.attributes["q"] = next.id;
                //response.shouldEndSession(false, 'What do you think? Is it ' + next.choices() + '?');

                // StartInputHandler
                this.response._addDirective(GadgetDirectives.startInputHandler({
                    "timeout": 35000,
                    "recognizers": ROLL_CALL_RECOGNIZER,
                    "events": ROLL_CALL_EVENTS
                }));
                // Save StartInput Request ID
                this.attributes.CurrentInputHandlerID = this.event.request.requestId;

            } else {
                speech += "Das waren alle Fragen. Ab jetzt könnte es zu Wiederholungen kommen. ";
            }
            var reprompt = "Wenn ich die Frage wiederholen soll, sage wiederholen.";

            this.response.speak(speech).listen(reprompt);
            this.emit('GlobalResponseReady', { 'openMicrophone': false });
            //this.emit(':ask', speech, reprompt);
        },
        'ScoresIntent': function () {

            var speech = scoreboard(this.attributes);
            speech += "<break time='500ms' />";
            this.emitWithState("NextQuestionIntent", speech);
        },
        'RepeatIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            var q = quiz.getQuestion(this.attributes["q"]);
            if (q) {
                // StartInputHandler
                this.response._addDirective(GadgetDirectives.startInputHandler({
                    "timeout": 35000,
                    "recognizers": ROLL_CALL_RECOGNIZER,
                    "events": ROLL_CALL_EVENTS
                }));
                // Save StartInput Request ID
                this.attributes.CurrentInputHandlerID = this.event.request.requestId;

                speech += q.questionAndAnswers();

                //var speech = "Ich wiederhole. " + q.questionAndAnswers();
                //this.emit(':ask', speech, speech);
                this.response.speak(speech);
                this.emit('GlobalResponseReady', { 'openMicrophone': false });
            }
        },

        'EndIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            speech += "<audio src='https://nordicskills.de/audio/MiniGame/short-transition.mp3' /> Endstand: <break time='500ms' />";

            var sortedScores = this.attributes["players"].concat().sort(function (a, b) { return (a.score > b.score) ? 1 : ((b.score < a.score) ? -1 : 0); });
            for (var i = 0; i < sortedScores.length; i++) {
                if (i == 0) {
                    speech += "Den letzten Platz belegt ";
                } else if (i == sortedScores.length - 1) {
                    speech += "Der Gewinner des Quiz-Roulettes ist damit ";
                } else {
                    speech += "Platz " + (sortedScores.length - i) + " belegt ";
                }

                speech += sortedScores[i].name + " mit " + (sortedScores[i].score == 1 ? "einem Punkt" : (sortedScores[i].score + " Punkten")) + ". <break time='500ms' />";
            }
            speech += "Glückwunsch! <break time='500ms' /> Warte kurz, um ein neues Spiel zu starten oder sage beenden, um das Quiz zu beenden. <break time='1s' />"

            this.handler.state = constants.states.SETUP_MODE;
            this.emitWithState("ResetIntent", speech);
        },

        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            this.attributes = updateRatingAttributes(this.attributes);
            var speech = this.attributes["ratingSpeech"];

            console.log("should end......");

            var self = this;
            dynamo.putUserState(self.event.session, function (data) {
                console.log(data.message);

                if (speech == "")
                    speech = "Okay, auf wiederhören.";
                self.response.speak(speech);

                self.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.TurnOff));
                self.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.TurnOff));
                self.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.TurnOff));
                self.handler.response.response.shouldEndSession = true;
                self.emit(':responseReady');
            });
        },
        'Unhandled': function () {
            var speech = 'Entschuldigung, Ich habe dich nicht verstanden.';
            this.emit(':ask', speech, speech);
        }
    }),
    rocketIntentHandlers: Alexa.CreateStateHandler(constants.states.MINIGAME_ROCKET, {

        'RocketStartIntent': function (speech) {

            this.attributes["rocket"] = [];
            this.attributes["rocketCorrect"] = false;

            this.attributes["currentPlayer"] = 0;
            speech += "Jeder Spieler rät auf welcher Höhe die Rakete explodiert, indem er mir eine Zahl zwischen eins und 500 nennt. Danach wird eine Rakete abgefeuert. Wer am nächsten dran ist gewinnt. ";
            speech += this.attributes["players"][this.attributes["currentPlayer"]].name + ", nenne mir eine Zahl zwischen eins und 500. ";
            this.emit(':ask', speech, speech);
        },
        'RocketNextIntent': function () {

            if (this.attributes["currentPlayer"] == this.attributes["players"].length - 1) {
                this.emitWithState('RocketEndIntent');
            } else {
                this.attributes["currentPlayer"] = this.attributes["currentPlayer"] + 1;
                this.attributes["rocketCorrect"] = true;
                var speech = this.attributes["players"][this.attributes["currentPlayer"]].name + " nenne mir eine Zahl zwischen eins und 500. ";
                this.emit(':ask', speech, speech);
            }
        },
        'RocketEndIntent': function () {

            this.attributes["currentPlayer"] = 0;

            speech = "Okay, wir starten nun die Rakete. <audio src='https://nordicskills.de/audio/MiniGame/rocket-launch.mp3' />";

            // Gewinn-Höhe generieren
            var explosionHeight = Math.floor(Math.random() * 500) + 1;

            // Wer war am nächsten dran
            var distance = Math.abs(this.attributes["rocket"][0] - explosionHeight);
            var idx = 0;
            for (var i = 0; i < this.attributes["rocket"].length; i++) {
                var cdistance = Math.abs(this.attributes["rocket"][i] - explosionHeight);
                if (cdistance < distance) {
                    idx = i;
                    distance = cdistance;
                }
            }

            var height = 50;
            while (height < explosionHeight) {
                speech += "<break time='1s'/> " + height + " Meter. "; // <audio src='https://nordicskills.de/audio/MiniGame/rocket-flying.mp3' />
                height = height + 50;
            }

            speech += "<audio src='https://nordicskills.de/audio/MiniGame/explosion-fireworks.mp3' /> Die Rakete ist in " + explosionHeight + " Metern explodiert. <audio src='https://nordicskills.de/audio/MiniGame/correct.mp3' /> " + this.attributes["players"][idx].name + " war mit " + this.attributes["rocket"][idx] + " Metern am nächsten. Glückwunsch. ";

            this.attributes["players"][idx].score = this.attributes["players"][idx].score + 5;

            speech += "Du erhältst zusätzlich 5 Punkte. ";

            if (this.attributes["echoButtons"])
                this.handler.state = constants.states.QUIZ_BUTTON_MODE;
            else
                this.handler.state = constants.states.QUIZ_MODE;
            this.emitWithState('NextRoundIntent', speech);
        },
        'AnswerIntent': function () {

          var name = this.event.request.intent.slots.answer.value;
          name = name.replace(/\W/g, '');
          name = name.toLowerCase();

          if (name == "eins" || name == "zwei" || name == "drei") {
              if (name == "eins")
                this.emitWithState('NumberIntent', 1);
              else if (name == "zwei")
                this.emitWithState('NumberIntent', 2);
              else if (name == "drei")
                this.emitWithState('NumberIntent', 3);
          } else {
              this.emit('Unhandled');
          }
        },
        'NumberIntent': function (number) {

          if (typeof number === 'undefined')
            var number = this.event.request.intent.slots.number.value;

            if (!isNaN(number)) {
                if (number > 0 && number <= 500) {
                    if (this.attributes["rocket"].indexOf(number) === -1) {
                        this.attributes["rocket"][this.attributes["currentPlayer"]] = number;
                        this.attributes["rocketCorrect"] = true;
                        var speech = "Ich habe " + number + " verstanden ist das richtig? ";
                        this.emit(':ask', speech, speech);
                    } else {
                        speech = "Tut mir Leid. Die Zahl wurde leider schon genannt. Bitte nenne eine andere. ";
                        this.emit(':ask', speech, speech);
                    }
                } else {
                    speech = "Die Zahl muss zwischen eins und 500 liegen. ";
                    this.emit(':ask', speech, speech);
                }
            }
            this.emit('Unhandled');
        },

        'AMAZON.YesIntent': function () {

            if (this.attributes["rocketCorrect"]) {
                this.attributes["rocketCorrect"] = false;
                this.emitWithState('RocketNextIntent');
            }
            this.emit('Unhandled');
        },
        'AMAZON.NoIntent': function () {

            if (this.attributes["rocketCorrect"]) {
                this.attributes["rocketCorrect"] = false;
                var speech = this.attributes["players"][this.attributes["currentPlayer"]].name + ", nenne mir eine Zahl zwischen eins und 500. ";
                this.emit(':ask', speech, speech);
            }
            this.emit('Unhandled');
        },

        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            this.attributes = updateRatingAttributes(this.attributes);
            var speech = this.attributes["ratingSpeech"];

            console.log("should end......");

            var self = this;
            dynamo.putUserState(self.event.session, function (data) {
                console.log(data.message);

                if (speech == "")
                    speech = "Okay, auf wiederhören.";
                self.emit(':tell', speech);
            });
        },
        'Unhandled': function () {
            var speech = 'Entschuldigung, Ich habe dich nicht verstanden.';
            this.emit(':ask', speech, speech);
        }
    }),
    shellIntentHandlers: Alexa.CreateStateHandler(constants.states.MINIGAME_SHELL, {

        'ShellStartIntent': function (speech) {

            this.attributes["shell"] = [];

            this.attributes["currentPlayer"] = 0;

            speech += "Ich lege gleich eine Kugel unter eines von 3 Hütchen und tausche sie hin und her. Danach nennt mir jeder Spieler eine Zahl zwischen 1 und 3. <break time='1s'/>";
            speech += "Okay, legen wir los. <audio src='https://nordicskills.de/audio/MiniGame/shell.mp3' />"
            speech += this.attributes["players"][this.attributes["currentPlayer"]].name + ", unter welchem Hütchen liegt die Kugel. Nenne mir eine Zahl zwischen eins und 3. ";
            this.emit(':ask', speech, speech);
        },
        'ShellNextIntent': function () {

            if (this.attributes["currentPlayer"] == this.attributes["players"].length - 1) {
                this.emitWithState('ShellEndIntent');
            } else {
                this.attributes["currentPlayer"] = this.attributes["currentPlayer"] + 1;
                var speech = this.attributes["players"][this.attributes["currentPlayer"]].name + ", nenne mir eine Zahl zwischen eins und 3. ";
                this.emit(':ask', speech, speech);
            }
        },
        'ShellEndIntent': function (speech) {

            this.attributes["currentPlayer"] = 0;

            var winningShell = Math.floor(Math.random() * 3) + 1;

            speech = "Okay, <audio src='https://nordicskills.de/audio/MiniGame/shell_end.mp3' /> Die Kugel war unter dem " + winningShell + ". Hütchen. ";

            var winners = 0;
            for (var i = 0; i < this.attributes["shell"].length; i++) {
                if (this.attributes["shell"][i] == winningShell) {
                    winners = winners + 1;

                    speech += this.attributes["players"][i].name + ", ";

                    this.attributes["players"][i].score = this.attributes["players"][i].score + 5;
                }
            }

            if (winners == 1) {
                speech += "Du hast richtig geraten. Du erhältst 5 Punkte. ";
            } else if (winners > 1) {
                speech += "Ihr habt richtig geraten. Ihr erhaltet je 5 Punkte. ";
            } else {
                speech += "<audio src='https://nordicskills.de/audio/MiniGame/incorrect.mp3' /> Niemand hat richtig geraten. Also gibts auch keine Punkte. ";
            }

            if (this.attributes["echoButtons"])
                this.handler.state = constants.states.QUIZ_BUTTON_MODE;
            else
                this.handler.state = constants.states.QUIZ_MODE;
            this.emitWithState('NextRoundIntent', speech);
        },
        'AnswerIntent': function () {

          var name = this.event.request.intent.slots.answer.value;
          name = name.replace(/\W/g, '');
          name = name.toLowerCase();

          if (name == "eins" || name == "zwei" || name == "drei") {
              if (name == "eins")
                this.emitWithState('NumberIntent', 1);
              else if (name == "zwei")
                this.emitWithState('NumberIntent', 2);
              else if (name == "drei")
                this.emitWithState('NumberIntent', 3);
          } else {
              this.emit('Unhandled');
          }
        },
        'NumberIntent': function (number) {

          if (typeof number === 'undefined')
            var number = this.event.request.intent.slots.number.value;

          if (!isNaN(number)) {
              if (number > 0 && number <= 3) {
                  this.attributes["shell"][this.attributes["currentPlayer"]] = number;
                  this.emitWithState('ShellNextIntent');
              } else {
                  speech = "Die Zahl muss zwischen eins und 3 liegen.";
                  this.emit(':ask', speech, speech);
              }
          }
          this.emit('Unhandled');
        },

        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            this.attributes = updateRatingAttributes(this.attributes);
            var speech = this.attributes["ratingSpeech"];

            console.log("should end......");

            var self = this;
            dynamo.putUserState(self.event.session, function (data) {
                console.log(data.message);

                if (speech == "")
                    speech = "Okay, auf wiederhören.";
                self.emit(':tell', speech);
            });
        },
        'Unhandled': function () {
            var speech = 'Entschuldigung, Ich habe dich nicht verstanden.';
            this.emit(':ask', speech, speech);
        }
    }),
    memoryIntentHandlers: Alexa.CreateStateHandler(constants.states.MINIGAME_MEMORY, {

        'MemoryStartIntent': function (speech) {

            var cards = ["Hund", "Hund", "Katze", "Katze", "Maus", "Maus", "Pferd", "Pferd", "Ratte"];

            this.attributes["memoryCards"] = shuffleArray(cards);
            console.log(this.attributes["memoryCards"]);
            this.attributes["memoryGuesses"] = [];
            this.attributes["memoryPoints"] = new Array(this.attributes["players"].length).fill(0);

            this.attributes["currentPlayer"] = 0;

            speech += "Ihr habt 9 verdeckte Karten vor euch und ratet nacheinander zwei Zahlen. Die genannten Karten werden dann aufgedeckt. Ziel ist es Paare zu finden. <break time='500ms'/>";
            speech += this.attributes["players"][this.attributes["currentPlayer"]].name + ", nenne mir zwei Zahlen zwischen 1 und 9. ";
            this.emit(':ask', speech, speech);
        },
        'MemoryNextIntent': function () {

            console.log(this.attributes["memoryCards"]);

            var speech = "<audio src='https://nordicskills.de/audio/MiniGame/card-flip-" + Math.floor(Math.random() * 4) + ".mp3' />  " + this.attributes["memoryCards"][this.attributes["memoryGuesses"][0] - 1] + " liegt unter der " + this.attributes["memoryGuesses"][0] + ". Karte. <break time='500ms'/>";
            speech += "<audio src='https://nordicskills.de/audio/MiniGame/card-flip-" + Math.floor(Math.random() * 4) + ".mp3' />  " + this.attributes["memoryCards"][this.attributes["memoryGuesses"][1] - 1] + " liegt unter der " + this.attributes["memoryGuesses"][1] + ". Karte. <break time='500ms'/>";
            if (this.attributes["memoryCards"][this.attributes["memoryGuesses"][0] - 1] === this.attributes["memoryCards"][this.attributes["memoryGuesses"][1] - 1]) {

                this.attributes["memoryCards"][this.attributes["memoryGuesses"][0] - 1] = "";
                this.attributes["memoryCards"][this.attributes["memoryGuesses"][1] - 1] = "";

                this.attributes["memoryPoints"][this.attributes["currentPlayer"]] = this.attributes["memoryPoints"][this.attributes["currentPlayer"]] + 2;
                speech += "<audio src='https://nordicskills.de/audio/MiniGame/correct.mp3' /> 2 Punkte für " + this.attributes["players"][this.attributes["currentPlayer"]].name + ". <break time='500ms' />";
            } else {
                speech += "Leider kein Paar. <break time='500ms'/>";
            }

            if (arrayIsBlank(this.attributes["memoryCards"])) {
                this.emitWithState('MemoryEndIntent', speech);
            } else {
                if (this.attributes["currentPlayer"] == this.attributes["players"].length - 1) {
                    this.attributes["currentPlayer"] = 0;
                } else {
                    this.attributes["currentPlayer"] = this.attributes["currentPlayer"] + 1;
                }
                speech += this.attributes["players"][this.attributes["currentPlayer"]].name + ", nenne mir zwei Zahlen zwischen eins und 9. ";
                this.emit(':ask', speech, speech);
            }
        },
        'MemoryEndIntent': function (speech) {

            this.attributes["currentPlayer"] = 0;

            speech += "Okay, das waren alle Karten. ";

            for (var i = 0; i < this.attributes["players"].length; i++) {
                this.attributes["players"][i].score = this.attributes["players"][i].score + this.attributes["memoryPoints"][i]

                if (i == 0) {
                    speech += this.attributes["players"][i].name + " erhält " + this.attributes["memoryPoints"][i] + " Punkte. ";
                } else if (i == this.attributes["players"].length - 1) {
                    speech += " und " + this.attributes["players"][i].name + " bekommt " + this.attributes["memoryPoints"][i] + " Punkte auf sein Konto. ";
                } else {
                    speech += this.attributes["players"][i].name + " " + this.attributes["memoryPoints"][i] + " Punkte. ";
                }
            }

            if (this.attributes["echoButtons"])
                this.handler.state = constants.states.QUIZ_BUTTON_MODE;
            else
                this.handler.state = constants.states.QUIZ_MODE;
            this.emitWithState('NextRoundIntent', speech);
        },
        'NumberIntent': function () {

            var number = this.event.request.intent.slots.number.value;
            var numberTwo = this.event.request.intent.slots.numberTwo.value;

            if (!isNaN(number) && !isNaN(numberTwo) && number > 0 && number <= 9 && numberTwo > 0 && numberTwo <= 9) {
                if (this.attributes["memoryCards"][number - 1] == "") {
                    speech = number + " wurde bereits aufgedeckt. ";
                    this.emit(':ask', speech, speech);
                }
                if (this.attributes["memoryCards"][numberTwo - 1] == "") {
                    speech = numberTwo + " wurde bereits aufgedeckt. ";
                    this.emit(':ask', speech, speech);
                }

                this.attributes["memoryGuesses"][0] = number;
                this.attributes["memoryGuesses"][1] = numberTwo;
                this.emitWithState('MemoryNextIntent');
            } else {
                speech = "Die Zahlen müssen zwischen eins und 9 liegen.";
                this.emit(':ask', speech, speech);
            }
        },
        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            this.attributes = updateRatingAttributes(this.attributes);
            var speech = this.attributes["ratingSpeech"];

            console.log("should end......");

            var self = this;
            dynamo.putUserState(self.event.session, function (data) {
                console.log(data.message);

                if (speech == "")
                    speech = "Okay, auf wiederhören.";
                self.emit(':tell', speech);
            });
        },
        'Unhandled': function () {
            var speech = 'Entschuldigung, Ich habe dich nicht verstanden.';
            this.emit(':ask', speech, speech);
        }
    }),
    quickIntentHandlers: Alexa.CreateStateHandler(constants.states.MINIGAME_QUICK, {

        'QuickStartIntent': function (speech) {

            this.attributes["quickPoints"] = new Array(this.attributes["players"].length).fill(0);
            this.attributes["quickQuestions"] = [];
            this.attributes["quickIndex"] = 0;

            this.attributes["quickNameWanted"] = false;
            
            var arr = []
            while (arr.length < 3) {
                var randomnumber = Math.floor(Math.random() * quick.length);
                if (arr.indexOf(randomnumber) > -1) continue;
                arr[arr.length] = randomnumber;
                this.attributes["quickQuestions"].push(quick[randomnumber]);
            }

            speech += "Ich stelle gleich nacheinander 3 Fragen. Wenn jemand von euch die Lösung kennt muss er sie nur nennen. Für jede richtige Antwort gibt es 3 Punkte. ";
            this.emitWithState('QuickNextIntent', speech);
        },
        'QuickNextIntent': function (speech) {

            if (typeof this.attributes["quickQuestions"][this.attributes["quickIndex"]] === 'undefined')
                this.emitWithState('QuickStartIntent');

            if (this.attributes["quickIndex"] == 3) {
                this.emitWithState('QuickEndIntent', speech);
            }

            if (this.attributes["quickIndex"] == 0) {
                speech += "Erste Frage. ";
            } else if (this.attributes["quickIndex"] == 1) {
                speech += "Zweite Frage. ";
            } else {
                speech += "Dritte Frage. ";
            }

            speech += this.attributes["quickQuestions"][this.attributes["quickIndex"]].question;
            var reprompt = "Wenn ihr die Lösung nicht kennt. Sagt keine Ahnung. ";
            this.emit(':ask', speech, reprompt);
        },
        'QuickEndIntent': function (speech) {

            if (typeof this.attributes["quickQuestions"][this.attributes["quickIndex"]] === 'undefined')
                this.emitWithState('QuickStartIntent');

            this.attributes["currentPlayer"] = 0;

            speech += "Okay, das waren alle Fragen. ";

            for (var i = 0; i < this.attributes["players"].length; i++) {
                this.attributes["players"][i].score = this.attributes["players"][i].score + this.attributes["quickPoints"][i];

                if (i == 0) {
                    speech += this.attributes["players"][i].name + " erhält " + this.attributes["quickPoints"][i] + " Punkte. ";
                } else if (i == this.attributes["players"].length - 1) {
                    speech += " und " + this.attributes["players"][i].name + " bekommt " + this.attributes["quickPoints"][i] + " Punkte auf sein Konto. ";
                } else {
                    speech += this.attributes["players"][i].name + " " + this.attributes["quickPoints"][i] + " Punkte. ";
                }
            }

            if (this.attributes["echoButtons"])
                this.handler.state = constants.states.QUIZ_BUTTON_MODE;
            else
                this.handler.state = constants.states.QUIZ_MODE;
            this.emitWithState('NextRoundIntent', speech);
        },
        'AnswerIntent': function () {

            var answer = "";
            if (this.event.request.intent.slots.answer)
                answer = this.event.request.intent.slots.answer.value;

            if (this.attributes["quickNameWanted"]) {
                this.emitWithState('NameIntent', answer);
            }

            if (answer.toLowerCase() == "keine ahnung") {
                var speech = "Die Lösung war " + this.attributes["quickQuestions"][this.attributes["quickIndex"]].answer + ". <break time='500ms'/> Keine Punkte für diese Frage. ";
                this.attributes["quickIndex"] = this.attributes["quickIndex"] + 1;
                this.emitWithState('QuickNextIntent', speech);
            }

            if (answer.toLowerCase() == this.attributes["quickQuestions"][this.attributes["quickIndex"]].answer.toLowerCase()) {
                var speech = "<audio src='https://nordicskills.de/audio/MiniGame/correct.mp3' /> Die Antwort war richtig! Wer hat die richtige Antwort gegeben? ";
                this.attributes["quickNameWanted"] = true;
                this.emit(':ask', speech, speech);
            } else {
                var speech = "<audio src='https://nordicskills.de/audio/MiniGame/incorrect.mp3' /> Die Antwort ist leider falsch. ";
                this.emitWithState('QuickNextIntent', speech);
            }
        },
        'NameIntent': function (answer) {

            var name = "";

            if (this.event.request.intent.slots.name)
                var name = this.event.request.intent.slots.name.value;
            
            if(typeof name === 'undefined')
                var name = "";

            name = name.replace(/\W/g, '');

            if (typeof answer !== 'undefined')
                name = answer;

            if (this.attributes["quickNameWanted"]) {
                for (var i = 0; i < this.attributes["players"].length; i++) {
                    if (this.attributes["players"][i].name.toLowerCase() == name.toLowerCase()) {

                        var speech = name + " erhält 3 Punkte. ";
                        this.attributes["quickPoints"][i] = this.attributes["quickPoints"][i] + 3;
                        this.attributes["quickIndex"] = this.attributes["quickIndex"] + 1;
                        this.attributes["quickNameWanted"] = false;
                        this.emitWithState('QuickNextIntent', speech);
                    }
                }

                var speech = name + ' ist kein Mitspieler. ';
                var repromt = 'Wer hat die Antwort gegeben? ';
                this.emit(':ask', speech, repromt);
            } else {
                this.emitWithState('AnswerIntent');
            }
        },

        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            this.attributes = updateRatingAttributes(this.attributes);
            var speech = this.attributes["ratingSpeech"];

            console.log("should end......");

            var self = this;
            dynamo.putUserState(self.event.session, function (data) {
                console.log(data.message);

                if (speech == "")
                    speech = "Okay, auf wiederhören.";
                self.emit(':tell', speech);
            });
        },
        'Unhandled': function () {
            var speech = 'Entschuldigung, Ich habe dich nicht verstanden.';
            this.emit(':ask', speech, speech);
        }
    }),
    quickButtonIntentHandlers: Alexa.CreateStateHandler(constants.states.MINIGAME_BUTTON_QUICK, {

        'GameEngine.InputHandlerEvent': function () {
            console.log("rollCallModeIntentHandlers::InputHandlerEvent");
            if (this.event.request.originatingRequestId != this.attributes.CurrentInputHandlerID) {
                console.log("Stale input event received: received event from " + this.event.request.originatingRequestId + "; expecting " + this.attributes.CurrentInputHandlerID);

                this.emit('GlobalResponseReady', { 'openMicrophone': false });
                return;
            }

            var gameEngineEvents = this.event.request.events || [];
            for (var i = 0; i < gameEngineEvents.length; i++) {
                // In this request type, we'll see one or more incoming events
                // that correspond to the StartInputHandler we sent above.

                if (gameEngineEvents[i].name == 'button_checked_in') {

                    //this.attributes.DeviceIDs.push(gameEngineEvents[i].inputEvents[0].gadgetId);

                    //this.attributes.buttonCount = this.attributes.buttonCount + 1;
                    var index = 0;
                    for (var j = 0; j < this.attributes["players"].length; j++) {
                        if (this.attributes["players"][j].device == gameEngineEvents[i].inputEvents[0].gadgetId)
                            index = j;
                    }
                    this.attributes["playerIndex"] = index;

                    this.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.TurnOff));
                    this.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.TurnOff));
                    this.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.TurnOff));

                    this.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.Pressed, { 'targetGadgets': [this.attributes["players"][index].device] }));
                    this.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.Pressed, { 'targetGadgets': [this.attributes["players"][index].device] }));
                    this.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.Pressed, { 'targetGadgets': [this.attributes["players"][index].device] }));
                    
                    let outputSpeech = "<audio src='https://nordicskills.de/audio/MiniGame/ding.mp3' /> " + this.attributes["players"][index].name + ", kennst du die Lösung? ";

                    this.response.speak(outputSpeech).listen(outputSpeech);

                    /*let deviceIds = this.attributes.DeviceIDs;
                    deviceIds = deviceIds.slice(-this.attributes.buttonCount);

                    // send an idle animation to registered buttons
                    this.response._addDirective({
                        "type": "GadgetController.SetLight",
                        "version": 1,
                        "targetGadgets": deviceIds,
                        "parameters": {
                            "animations": BasicAnimations.FadeInAnimation(1, "green", 5000),
                            "triggerEvent": "none",
                            "triggerEventTimeMs": 0,
                        }
                    });*/
                    // reset button press animations until the user chooses a color
                    //this.response._addDirective(GadgetDirectives.setButtonDownAnimation(DEFUALT_ANIMATIONS.ButtonDown));
                    //this.response._addDirective(GadgetDirectives.setButtonUpAnimation(DEFUALT_ANIMATIONS.ButtonUp));

                    //this.attributes.isRollCallComplete = true;
                    //this.handler.state = SKILL_STATES.PLAY_MODE;

                    this.emit('GlobalResponseReady', { 'openMicrophone': true });

                } else if (gameEngineEvents[i].name == 'timeout') {
                    this.emitWithState('AnswerIntent', "keine ahnung");
                }
            }
        },
        'GlobalResponseReady': function ({ openMicrophone = false } = {}) {
            console.log('globalHandlers::GlobalResponseReady (openMicrophone = ' + openMicrophone + ')');
            // we trigger the `GlobalResponseReady` event from other handlers - the openMicrophone parameter controls the microphone behavior

            if (openMicrophone) { // setting shouldEndSession = fase  -  lets Alexa know that we're looking for an answer from the user 
                this.handler.response.response.shouldEndSession = false;
            } else { // deleting shouldEndSession will keep the skill session going, while the input handler is active, waiting for button presses
                delete this.handler.response.response.shouldEndSession;
            }

            // log the response to CloudWatch to make it easier to debug the skill
            console.log("==Response== " + JSON.stringify(this.handler.response));
            this.emit(':responseReady');
        },

        'QuickStartIntent': function (speech) {

            this.attributes["quickPoints"] = new Array(this.attributes["players"].length).fill(0);
            this.attributes["quickQuestions"] = [];
            this.attributes["quickIndex"] = 0;

            this.attributes["quickNameWanted"] = false;

            var arr = []
            while (arr.length < 3) {
                var randomnumber = Math.floor(Math.random() * quick.length);
                if (arr.indexOf(randomnumber) > -1) continue;
                arr[arr.length] = randomnumber;
                this.attributes["quickQuestions"].push(quick[randomnumber]);
            }

            speech += "Ich stelle gleich nacheinander 3 Fragen. Wer die Lösung kennt, drückt seinen Echo Button. Ihr habt 10 Sekunden Zeit die Frage zu beantworten und für jede richtige Antwort gibt es 3 Punkte. ";
            this.emitWithState('QuickNextIntent', speech);
        },
        'QuickNextIntent': function (speech) {

            if (this.attributes["quickIndex"] == 3) {
                this.emitWithState('QuickEndIntent', speech);
            }

            if (typeof this.attributes["quickQuestions"][this.attributes["quickIndex"]] === 'undefined')
                this.emitWithState('QuickStartIntent');
            
            if (this.attributes["quickIndex"] == 0) {
                speech += "Erste Frage. ";
            } else if (this.attributes["quickIndex"] == 1) {
                speech += "Zweite Frage. ";
            } else {
                speech += "Dritte Frage. ";
            }
            
            speech += this.attributes["quickQuestions"][this.attributes["quickIndex"]].question;
            //var reprompt = "Wenn ihr die Lösung nicht kennt. Sagt keine Ahnung. ";

            if (this.attributes["quickIndex"] > 0) {
                setDefaultButtonColor(this);
                // StartInputHandler
                this.response._addDirective(GadgetDirectives.startInputHandler({
                    "timeout": 15000,
                    "recognizers": ROLL_CALL_RECOGNIZER,
                    "events": ROLL_CALL_EVENTS
                }));
            } else{
                // StartInputHandler
                this.response._addDirective(GadgetDirectives.startInputHandler({
                    "timeout": 45000,
                    "recognizers": ROLL_CALL_RECOGNIZER,
                    "events": ROLL_CALL_EVENTS
                }));
            }
            
            // Save StartInput Request ID
            this.attributes.CurrentInputHandlerID = this.event.request.requestId;

            this.response.speak(speech);
            this.emit('GlobalResponseReady', { 'openMicrophone': false });
            //this.emit(':ask', speech, reprompt);
        },
        'QuickEndIntent': function (speech) {

            if (this.attributes["quickIndex"] != 3 && typeof this.attributes["quickQuestions"][this.attributes["quickIndex"]] === 'undefined')
                this.emitWithState('QuickStartIntent');

            this.attributes["currentPlayer"] = 0;

            speech += "Okay, das waren alle Fragen. ";

            for (var i = 0; i < this.attributes["players"].length; i++) {
                this.attributes["players"][i].score = this.attributes["players"][i].score + this.attributes["quickPoints"][i];

                if (i == 0) {
                    speech += this.attributes["players"][i].name + " erhält " + this.attributes["quickPoints"][i] + " Punkte. ";
                } else if (i == this.attributes["players"].length - 1) {
                    speech += " und " + this.attributes["players"][i].name + " bekommt " + this.attributes["quickPoints"][i] + " Punkte auf sein Konto. ";
                } else {
                    speech += this.attributes["players"][i].name + " " + this.attributes["quickPoints"][i] + " Punkte. ";
                }
            }

            setDefaultButtonColor(this);

            if (this.attributes["echoButtons"])
                this.handler.state = constants.states.QUIZ_BUTTON_MODE;
            else
                this.handler.state = constants.states.QUIZ_MODE;
            this.emitWithState('NextRoundIntent', speech);
        },
        'AnswerIntent': function (answer) {

            if (typeof answer === 'undefined')
                var answer = "";

            if (answer == "" && this.event.request.intent.slots.answer)
                answer = this.event.request.intent.slots.answer.value;

            if (this.attributes["quickNameWanted"]) {
                this.emitWithState('NameIntent', answer);
            }

            if (answer.toLowerCase() == "keine ahnung") {
                var speech = "Die Lösung war " + this.attributes["quickQuestions"][this.attributes["quickIndex"]].answer + ". <break time='500ms'/> Keine Punkte für diese Frage. ";
                this.attributes["quickIndex"] = this.attributes["quickIndex"] + 1;
                this.emitWithState('QuickNextIntent', speech);
            }

            if (answer.toLowerCase() == this.attributes["quickQuestions"][this.attributes["quickIndex"]].answer.toLowerCase()) {
                var speech = "<audio src='https://nordicskills.de/audio/MiniGame/correct.mp3' /> Die Antwort war richtig! ";

                speech += "Du erhältst 3 Punkte. ";
                this.attributes["quickPoints"][this.attributes["playerIndex"]] = this.attributes["quickPoints"][this.attributes["playerIndex"]] + 3;
                this.attributes["quickIndex"] = this.attributes["quickIndex"] + 1;
                this.emitWithState('QuickNextIntent', speech);

                //this.attributes["quickNameWanted"] = true;
                //this.emit(':ask', speech, speech);
            } else {
                var speech = "<audio src='https://nordicskills.de/audio/MiniGame/incorrect.mp3' /> Die Antwort ist leider falsch. ";
                this.emitWithState('QuickNextIntent', speech);
            }
        },
        /*'NameIntent': function (answer) {

            var name = "";
            if (this.event.request.intent.slots.name)
                name = this.event.request.intent.slots.name.value;
            name = name.replace(/\W/g, '');

            if (typeof answer !== 'undefined')
                name = answer;

            if (this.attributes["quickNameWanted"]) {
                for (var i = 0; i < this.attributes["players"].length; i++) {
                    if (this.attributes["players"][i].name.toLowerCase() == name.toLowerCase()) {

                        var speech = name + " erhält 3 Punkte. ";
                        this.attributes["quickPoints"][i] = this.attributes["quickPoints"][i] + 3;
                        this.attributes["quickIndex"] = this.attributes["quickIndex"] + 1;
                        this.attributes["quickNameWanted"] = false;
                        this.emitWithState('QuickNextIntent', speech);
                    }
                }

                var speech = name + ' ist kein Mitspieler. ';
                var repromt = 'Wer hat die Antwort gegeben? ';
                this.emit(':ask', speech, repromt);
            } else {
                this.emitWithState('AnswerIntent');
            }
        },*/

        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            this.attributes = updateRatingAttributes(this.attributes);
            var speech = this.attributes["ratingSpeech"];

            console.log("should end......");

            var self = this;
            dynamo.putUserState(self.event.session, function (data) {
                console.log(data.message);

                if (speech == "")
                    speech = "Okay, auf wiederhören.";
                self.response.speak(speech);

                self.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.TurnOff));
                self.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.TurnOff));
                self.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.TurnOff));
                self.handler.response.response.shouldEndSession = true;
                self.emit(':responseReady');
            });
        },
        'Unhandled': function () {
            var speech = 'Entschuldigung, Ich habe dich nicht verstanden.';
            this.emit(':ask', speech, speech);
        }
    }),
    whoAmIIntentHandlers: Alexa.CreateStateHandler(constants.states.MINIGAME_WHOAMI, {

        'WhoAmIStartIntent': function (speech) {

            this.attributes["whoAmIPoints"] = new Array(this.attributes["players"].length).fill(0);
            this.attributes["whoAmIIndex"] = Math.floor(Math.random() * whoAmI.length);
            this.attributes['whoAmIHint'] = 0;

            this.attributes["whoAmINameWanted"] = false;

            speech += 'In diesem Spiel geht es darum eine Person oder einen Gegenstand zu erraten. Ich werde nach und nach <phoneme alphabet="ipa" ph="tɪps">Tipps</phoneme> geben, bis einer von euch auf die Lösung kommt. ';
            this.emitWithState('WhoAmINextIntent', speech);
        },
        'WhoAmINextIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            

            if (this.attributes["whoAmIHint"] == 0) {
                speech += "Erster Hinweis. ";
            } else if (this.attributes["whoAmIHint"] == whoAmI[this.attributes["whoAmIIndex"]].hints.length - 1) {
                speech += "Letzter Hinweis. ";
            } else if (this.attributes["whoAmIHint"] == whoAmI[this.attributes["whoAmIIndex"]].hints.length) {
                var speech = "Die Lösung war " + whoAmI[this.attributes["whoAmIIndex"]].answers[0] + ". Leider keine Punkte für euch. ";
                this.emitWithState('WhoAmIEndIntent', speech);
            }

            speech += whoAmI[this.attributes["whoAmIIndex"]].hints[this.attributes['whoAmIHint']];
            var reprompt = "Wenn ihr die Lösung nicht kennt. Sagt keine Ahnung. ";
            this.emit(':ask', speech, reprompt);
        },
        'WhoAmIEndIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            if (this.attributes["whoAmIHint"] < whoAmI[this.attributes["whoAmIIndex"]].hints.length) {
                this.attributes["players"][this.attributes["currentPlayer"]].score = this.attributes["players"][this.attributes["currentPlayer"]].score + 5;
                speech += this.attributes["players"][this.attributes["currentPlayer"]].name + " erhält 5 Punkte. ";
            }

            this.attributes["currentPlayer"] = 0;

            if (this.attributes["echoButtons"])
                this.handler.state = constants.states.QUIZ_BUTTON_MODE;
            else
                this.handler.state = constants.states.QUIZ_MODE;
            this.emitWithState('NextRoundIntent', speech);
        },
        'AnswerIntent': function () {

            var answer = "";
            if (this.event.request.intent.slots.answer)
                answer = this.event.request.intent.slots.answer.value;

            if (this.attributes["whoAmINameWanted"]) {
                this.emitWithState('NameIntent', answer);
            }

            if (answer.toLowerCase() == "keine ahnung") {
                
                this.attributes["whoAmIHint"] = this.attributes["whoAmIHint"] + 1;
                this.emitWithState('WhoAmINextIntent', speech);
            }

            if (answer.toLowerCase() == whoAmI[this.attributes["whoAmIIndex"]].answers[0].toLowerCase()) {
                var speech = "<audio src='https://nordicskills.de/audio/MiniGame/correct.mp3' /> Die Antwort war richtig! Wer hat die richtige Antwort gegeben? ";
                this.attributes["whoAmINameWanted"] = true;
                this.emit(':ask', speech, speech);
            } else {
                var speech = " <audio src='https://nordicskills.de/audio/MiniGame/incorrect.mp3' /> Die Antwort ist leider falsch. ";
                this.emitWithState('WhoAmINextIntent', speech);
            }
        },
        'NameIntent': function (answer) {

            var name = "";
            if (this.event.request.intent.slots.name)
                name = this.event.request.intent.slots.name.value;

            if (typeof answer !== 'undefined')
                name = answer;

            if (this.attributes["whoAmINameWanted"]) {
                for (var i = 0; i < this.attributes["players"].length; i++) {
                    if (this.attributes["players"][i].name.toLowerCase() == name.toLowerCase()) {

                        this.attributes["currentPlayer"] = i;
                        this.attributes["whoAmINameWanted"] = false;
                        this.emitWithState('WhoAmIEndIntent');
                    }
                }

                var speech = name + ' ist kein Mitspieler. ';
                var repromt = 'Wer hat die Antwort gegeben? ';
                this.emit(':ask', speech, repromt);
            } else {
                this.emitWithState('AnswerIntent');
            }
        },

        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            this.attributes = updateRatingAttributes(this.attributes);
            var speech = this.attributes["ratingSpeech"];

            console.log("should end......");

            var self = this;
            dynamo.putUserState(self.event.session, function (data) {
                console.log(data.message);

                if (speech == "")
                    speech = "Okay, auf wiederhören.";
                self.emit(':tell', speech);
            });
        },
        'Unhandled': function () {
            var speech = 'Entschuldigung, Ich habe dich nicht verstanden.';
            this.emit(':ask', speech, speech);
        }
    }),
    whoAmIButtonIntentHandlers: Alexa.CreateStateHandler(constants.states.MINIGAME_BUTTON_WHOAMI, {

        'GameEngine.InputHandlerEvent': function () {
            console.log("rollCallModeIntentHandlers::InputHandlerEvent");
            if (this.event.request.originatingRequestId != this.attributes.CurrentInputHandlerID) {
                console.log("Stale input event received: received event from " + this.event.request.originatingRequestId + "; expecting " + this.attributes.CurrentInputHandlerID);

                this.emit('GlobalResponseReady', { 'openMicrophone': false });
                return;
            }

            var gameEngineEvents = this.event.request.events || [];
            for (var i = 0; i < gameEngineEvents.length; i++) {
                // In this request type, we'll see one or more incoming events
                // that correspond to the StartInputHandler we sent above.

                if (gameEngineEvents[i].name == 'button_checked_in') {

                    //this.attributes.DeviceIDs.push(gameEngineEvents[i].inputEvents[0].gadgetId);

                    //this.attributes.buttonCount = this.attributes.buttonCount + 1;
                    var index = 0;
                    for (var j = 0; j < this.attributes["players"].length; j++) {
                        if (this.attributes["players"][j].device == gameEngineEvents[i].inputEvents[0].gadgetId)
                            index = j;
                    }

                    this.attributes["playerIndex"] = index;

                    this.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.TurnOff));
                    this.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.TurnOff));
                    this.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.TurnOff));

                    this.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.Pressed, { 'targetGadgets': [this.attributes["players"][index].device] }));
                    this.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.Pressed, { 'targetGadgets': [this.attributes["players"][index].device] }));
                    this.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.Pressed, { 'targetGadgets': [this.attributes["players"][index].device] }));

                    let outputSpeech = "<audio src='https://nordicskills.de/audio/MiniGame/ding.mp3' /> " + this.attributes["players"][index].name + ", kennst du die Lösung? ";

                    this.response.speak(outputSpeech).listen(outputSpeech);

                    /*let deviceIds = this.attributes.DeviceIDs;
                    deviceIds = deviceIds.slice(-this.attributes.buttonCount);

                    // send an idle animation to registered buttons
                    this.response._addDirective({
                        "type": "GadgetController.SetLight",
                        "version": 1,
                        "targetGadgets": deviceIds,
                        "parameters": {
                            "animations": BasicAnimations.FadeInAnimation(1, "green", 5000),
                            "triggerEvent": "none",
                            "triggerEventTimeMs": 0,
                        }
                    });*/
                    // reset button press animations until the user chooses a color
                    //this.response._addDirective(GadgetDirectives.setButtonDownAnimation(DEFUALT_ANIMATIONS.ButtonDown));
                    //this.response._addDirective(GadgetDirectives.setButtonUpAnimation(DEFUALT_ANIMATIONS.ButtonUp));

                    //this.attributes.isRollCallComplete = true;
                    //this.handler.state = SKILL_STATES.PLAY_MODE;

                    this.emit('GlobalResponseReady', { 'openMicrophone': true });

                } else if (gameEngineEvents[i].name == 'timeout') {

                    this.emitWithState('AnswerIntent', "keine ahnung");
                }
            }
        },
        'GlobalResponseReady': function ({ openMicrophone = false } = {}) {
            console.log('globalHandlers::GlobalResponseReady (openMicrophone = ' + openMicrophone + ')');
            // we trigger the `GlobalResponseReady` event from other handlers - the openMicrophone parameter controls the microphone behavior

            if (openMicrophone) { // setting shouldEndSession = fase  -  lets Alexa know that we're looking for an answer from the user 
                this.handler.response.response.shouldEndSession = false;
            } else { // deleting shouldEndSession will keep the skill session going, while the input handler is active, waiting for button presses
                delete this.handler.response.response.shouldEndSession;
            }

            // log the response to CloudWatch to make it easier to debug the skill
            console.log("==Response== " + JSON.stringify(this.handler.response));
            this.emit(':responseReady');
        },

        'WhoAmIStartIntent': function (speech) {

            this.attributes["whoAmIPoints"] = new Array(this.attributes["players"].length).fill(0);
            this.attributes["whoAmIIndex"] = Math.floor(Math.random() * whoAmI.length);
            this.attributes['whoAmIHint'] = 0;

            this.attributes["whoAmINameWanted"] = false;

            speech += 'In diesem Spiel geht es darum eine Person oder einen Gegenstand zu erraten. Ich werde nach und nach <phoneme alphabet="ipa" ph="tɪps">Tipps</phoneme> geben, bis einer von euch auf die Lösung kommt. Ihr bekommt alle 10 Sekunden einen neuen <phoneme alphabet="ipa" ph="tɪp">Tipp</phoneme>. ';
            this.emitWithState('WhoAmINextIntent', speech);
        },
        'WhoAmINextIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";



            if (this.attributes["whoAmIHint"] == 0) {
                speech += "Erster Hinweis. ";
            } else if (this.attributes["whoAmIHint"] == whoAmI[this.attributes["whoAmIIndex"]].hints.length - 1) {
                speech += "Letzter Hinweis. ";
            } else if (this.attributes["whoAmIHint"] == whoAmI[this.attributes["whoAmIIndex"]].hints.length) {
                var speech = "Die Lösung war " + whoAmI[this.attributes["whoAmIIndex"]].answers[0] + ". Leider keine Punkte für euch. ";
                this.emitWithState('WhoAmIEndIntent', speech);
            }

            speech += whoAmI[this.attributes["whoAmIIndex"]].hints[this.attributes['whoAmIHint']];

            if (this.attributes["whoAmIHint"] > 0) {
                setDefaultButtonColor(this);
                // StartInputHandler
                this.response._addDirective(GadgetDirectives.startInputHandler({
                    "timeout": 10000,
                    "recognizers": ROLL_CALL_RECOGNIZER,
                    "events": ROLL_CALL_EVENTS
                }));
            } else {
                // StartInputHandler
                this.response._addDirective(GadgetDirectives.startInputHandler({
                    "timeout": 40000,
                    "recognizers": ROLL_CALL_RECOGNIZER,
                    "events": ROLL_CALL_EVENTS
                }));
            }

            // Save StartInput Request ID
            this.attributes.CurrentInputHandlerID = this.event.request.requestId;

            this.response.speak(speech);
            this.emit('GlobalResponseReady', { 'openMicrophone': false });
            //this.emit(':ask', speech, reprompt);
        },
        'WhoAmIEndIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            if (this.attributes["whoAmIHint"] < whoAmI[this.attributes["whoAmIIndex"]].hints.length) {
                this.attributes["players"][this.attributes["currentPlayer"]].score = this.attributes["players"][this.attributes["currentPlayer"]].score + 5;
                speech += "Du erhältst 5 Punkte. ";
            }

            this.attributes["currentPlayer"] = 0;

            setDefaultButtonColor(this);

            if (this.attributes["echoButtons"])
                this.handler.state = constants.states.QUIZ_BUTTON_MODE;
            else
                this.handler.state = constants.states.QUIZ_MODE;
            this.emitWithState('NextRoundIntent', speech);
        },
        'AnswerIntent': function (answer) {

            if (typeof answer === 'undefined')
                var answer = "";

            if (answer == "" && this.event.request.intent.slots.answer)
                answer = this.event.request.intent.slots.answer.value;

            if (this.attributes["whoAmINameWanted"]) {
                this.emitWithState('NameIntent', answer);
            }

            if (answer.toLowerCase() == "keine ahnung") {

                this.attributes["whoAmIHint"] = this.attributes["whoAmIHint"] + 1;
                this.emitWithState('WhoAmINextIntent', speech);
            }

            if (answer.toLowerCase() == whoAmI[this.attributes["whoAmIIndex"]].answers[0].toLowerCase()) {
                var speech = "<audio src='https://nordicskills.de/audio/MiniGame/correct.mp3' /> Die Antwort war richtig! ";
                this.attributes["currentPlayer"] = this.attributes["playerIndex"];
                this.emitWithState('WhoAmIEndIntent');
                //this.attributes["whoAmINameWanted"] = true;
                //this.emit(':ask', speech, speech);
            } else {
                var speech = " <audio src='https://nordicskills.de/audio/MiniGame/incorrect.mp3' /> Die Antwort ist leider falsch. ";
                this.emitWithState('WhoAmINextIntent', speech);
            }
        },
        /*'NameIntent': function (answer) {

            var name = "";
            if (this.event.request.intent.slots.name)
                name = this.event.request.intent.slots.name.value;

            if (typeof answer !== 'undefined')
                name = answer;

            if (this.attributes["whoAmINameWanted"]) {
                for (var i = 0; i < this.attributes["players"].length; i++) {
                    if (this.attributes["players"][i].name.toLowerCase() == name.toLowerCase()) {

                        this.attributes["currentPlayer"] = i;
                        this.attributes["whoAmINameWanted"] = false;
                        this.emitWithState('WhoAmIEndIntent');
                    }
                }

                var speech = name + ' ist kein Mitspieler. ';
                var repromt = 'Wer hat die Antwort gegeben? ';
                this.emit(':ask', speech, repromt);
            } else {
                this.emitWithState('AnswerIntent');
            }
        },*/

        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            this.attributes = updateRatingAttributes(this.attributes);
            var speech = this.attributes["ratingSpeech"];

            console.log("should end......");

            var self = this;
            dynamo.putUserState(self.event.session, function (data) {
                console.log(data.message);

                if (speech == "")
                    speech = "Okay, auf wiederhören.";
                self.response.speak(speech);

                self.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.TurnOff));
                self.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.TurnOff));
                self.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.TurnOff));
                self.handler.response.response.shouldEndSession = true;
                self.emit(':responseReady');
            });
        },
        'Unhandled': function () {
            var speech = 'Entschuldigung, Ich habe dich nicht verstanden.';
            this.emit(':ask', speech, speech);
        }
    })
};

function updateRatingAttributes(attributes) {

    var returnRating = false;

    if (!attributes["firstTimestamp"])
        attributes["firstTimestamp"] = moment().valueOf();

    attributes["lastTimestamp"] = moment().valueOf();  

    if (!attributes["sessionCount"]) {
        attributes["sessionCount"] = 0;
        returnRating = true;
    }

    if (attributes["sessionCount"] >= 3 && moment(attributes["lastTimestamp"]).diff(moment(attributes["firstTimestamp"]), 'days') > 7) {
        attributes["sessionCount"] = 0;
    }

    if (!returnRating && attributes["sessionCount"] < 3 && moment(attributes["lastTimestamp"]).diff(moment(attributes["firstTimestamp"]), 'days') <= 14)
        if (Math.floor(Math.random() * 5) == 0) // 20% Wahrscheinlichkeit innerhalb von 2 Wochen
            returnRating = true;

    if (returnRating) {
        attributes["sessionCount"] = attributes["sessionCount"] + 1;
        attributes["ratingSpeech"] = "Hat Dir das Quiz Roulette gefallen? Dann bewerte es doch auf der Amazon Webseite oder über Deine Alexa-App! ";
    } else {
        attributes["ratingSpeech"] = "";
    }
    return attributes;
}

function setDefaultButtonColor(self) {
    let deviceIds = self.attributes.DeviceIDs;
    deviceIds = deviceIds.slice(-self.attributes.buttonCount);

    self.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.Button1Idle, { 'targetGadgets': [deviceIds[0]] }));
    self.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.Button1Idle, { 'targetGadgets': [deviceIds[0]] }));
    self.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.Button1Idle, { 'targetGadgets': [deviceIds[0]] }));

    if (deviceIds.length > 1) {
        self.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.Button2Idle, { 'targetGadgets': [deviceIds[1]] }));
        self.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.Button2Idle, { 'targetGadgets': [deviceIds[1]] }));
        self.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.Button2Idle, { 'targetGadgets': [deviceIds[1]] }));
    }

    if (deviceIds.length > 2) {
        self.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.Button3Idle, { 'targetGadgets': [deviceIds[2]] }));
        self.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.Button3Idle, { 'targetGadgets': [deviceIds[2]] }));
        self.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.Button3Idle, { 'targetGadgets': [deviceIds[2]] }));
    }

    if (deviceIds.length > 3) {
        self.response._addDirective(GadgetDirectives.setIdleAnimation(ANIMATIONS.Button4Idle, { 'targetGadgets': [deviceIds[3]] }));
        self.response._addDirective(GadgetDirectives.setButtonDownAnimation(ANIMATIONS.Button4Idle, { 'targetGadgets': [deviceIds[3]] }));
        self.response._addDirective(GadgetDirectives.setButtonUpAnimation(ANIMATIONS.Button4Idle, { 'targetGadgets': [deviceIds[3]] }));
    }
    return self;
}

function scoreboard(attributes) {

    var sortedScores = attributes["players"].concat().sort(function (a, b) { return (a.score < b.score) ? 1 : ((b.score > a.score) ? -1 : 0); });

    var speech = "";
    for (var i = 0; i < sortedScores.length; i++) {
        if (i == 0) {
            speech += "Momentan führt ";
        } else if (i == 1) {
            speech += "gefolgt von ";
        } else if (i == sortedScores.length - 1) {
            speech += "Schlusslicht ist ";
        }

        speech += sortedScores[i].name + " mit " + (sortedScores[i].score == 1 ? "einem Punkt" : (sortedScores[i].score + " Punkten")) + ", ";

        if (i > 1 && i == sortedScores.length - 1) {
            speech += "aber Kopf hoch es ist noch nicht vorbei. ";
        }
    }
    speech += "<break time='500ms' />"
    return speech;
}

function randomMinigame(attributes) {
    var randNum = Math.floor(Math.random() * minigames.length);
    attributes["currentMinigameID"] = randNum;
    return attributes;
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

function arrayIsBlank(array) {

    for (var i = 0; i < array.length; i++) {
        if (array[i] != "" && array[i] != "Ratte") {
            return false;
        }
    }
    return true;
}

var minigames =
    [
        {
            "name": "Rocket",
            "desc": "Glücksraketen"
        },
        {
            "name": "Shell",
            "desc": "Hütchenspiel"
        },
        {
            "name": "Memory",
            "desc": "Memory"
        },
        {
            "name": "Quick",
            "desc": "Schnellfrage-Runde"
        },
        {
            "name": "WhoAmI",
            "desc": "Wer bin ich?"
        }
    ];