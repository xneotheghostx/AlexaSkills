'use strict';

// Audio Source - https://nordicskills.de/audio/ZeitDerEntspannung/
var audioData = [
    {
        'title': 'Ambiente',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Ambiente.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Ave Maria',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Ave_Maria.mp3',
        'category': 'Kategorie 1'
    },
	{
        'title': 'Ein Lied für Mama',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Ein_Lied_fuer_Mama.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Ein neuer Anfang',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Ein_neuer_Anfang.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Ein langer Tag',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Ein_langer_Tag.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Folge deinem Herzen',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Folge_deinem_Herzen.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Geh mit mir',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Geh_mit_mir.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Gemeinsam mit dir',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Gemeinsam_mit_dir.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Glückliche Zeiten',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Glueckliche_Zeiten.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Graswiese',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Grasswiese.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Im Hintergrund',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Im_Hintergrund.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Klareswasser',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Klareswasser.mp3',
        'category': 'Kategorie 1'
    },
	{
        'title': 'Lass dich fallen',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Lass_dich_fallen.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Milchstrasse',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Milchstrasse.mp3',
        'category': 'Kategorie 1'
    },
	{
        'title': 'Öffne deine Augen',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Oeffne_deine_Augen.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Romanze am Klavier',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Romance_am_Klavier.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Schlaf schön',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Schlaf_schoen.mp3',
        'category': 'Kategorie 1'
    },
	{
        'title': 'Smaragdfluss',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Smaragdfluss.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Stimmungsvoll',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Stimmungsvoll.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Traumland',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Traumland.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Warme Herzen',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Warme_Herzen.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Windige Tage',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Windige_Tage.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Zuhause',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Zuhause.mp3',
        'category': 'Kategorie 1'
    }
];

module.exports = audioData;