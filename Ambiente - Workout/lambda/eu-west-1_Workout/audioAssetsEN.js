'use strict';

// Audio Source - https://nordicskills.de/audio/ZeitDerEntspannung/
var audioData = [
    {
        'title': 'Ambiance',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Ambiente.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Ave Maria',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Ave_Maria.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'A song for Mama',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Ein_Lied_fuer_Mama.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'A new beginning',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Ein_neuer_Anfang.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'A long day',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Ein_langer_Tag.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Follow your heart',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Folge_deinem_Herzen.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Walk with me',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Geh_mit_mir.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Together with you',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Gemeinsam_mit_dir.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Happy times',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Glueckliche_Zeiten.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Grass meadow',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Grasswiese.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'In the background',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Im_Hintergrund.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Clear water',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Klareswasser.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Let it go',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Lass_dich_fallen.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Milky Way',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Milchstrasse.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Open your eyes',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Oeffne_deine_Augen.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Romance at the piano',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Romance_am_Klavier.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Sleep well',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Schlaf_schoen.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Emerald river',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Smaragdfluss.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Atmospheric',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Stimmungsvoll.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Dreamland',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Traumland.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Warm heart',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Warme_Herzen.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'Windy days',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Windige_Tage.mp3',
        'category': 'Kategorie 1'
    },
    {
        'title': 'At home',
        'url': 'https://nordicskills.de/audio/ZeitDerEntspannung/Zuhause.mp3',
        'category': 'Kategorie 1'
    }
];

module.exports = audioData;