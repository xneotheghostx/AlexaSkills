'use strict';
/*
 * Copyright 2018 Amazon.com, Inc. and its affiliates. All Rights Reserved.
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * http://aws.amazon.com/asl/
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */


/**
 * Questions library
 * 
 * Use this file to create your own set of questions.
 * 
 * Object properties:
 *      index:          The index of the question in this list
 *      question:       The question you want Alexa to ask
 *      answers:        The list of available answers
 *      correct_answer: The correct answer to the question
 * 
 * When adding or updating questions and answers, you must take the list of all values 
 * in each of the 'answers' arrays for all questions and add them to a custom slot 
 * in your skill called 'answers'.
 * 
 * The 'answers' custom slot is be mapped to a couple of intents in the interaction model.
 * One intent, named 'AnswerOnlyIntent', contains only the slot, by itself, in order 
 * to maximize the accuracy of the model.
 * 
 * For example: 
 *      AnswerOnlyIntent {answers}
 * 
 * The other intent, 'AnswerQuestionIntent', provides more complex speech patterns
 * to match other utternaces users may include with their answers.
 * 
 * For example:
 *      AnswerQuestionIntent is it {answers}
 *      AnswerQuestionIntent it is {answers}
 *      AnswerQuestionIntent the answer is {answers}
 *      AnswerQuestionIntent I think the answer is {answers}
 * 
 * See model file at models/de-DE.json for a complete example.
 */
module.exports = Object.freeze({ 
    questions: [
        {
            index: 1,
            question: 'Ich sage, dass die Währung vor dem Euro, Deutsche Mark hieß! ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Die D-Mark war von 1948 bis 2001 die offizielle Währung in der Bundesrepublik Deutschland. '
        },
        {
            index: 2,
            question: 'Die Erde hat einen Umfang von 38 Tausend Kilometern? ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Der Umfang der Erde ist 40.075 Kilimeter. '
        },
        {
            index: 3,
            question: 'Rot, Gelb und Blau sind die Primärfarben. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Die Primärfarben oder auch Grundfarben genannt, sind Rot, Grün und Blau. '
        },
        {
            index: 4,
            question: 'Ich kann rechnen. 5 + 10 x 7 sind 350.',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Das Erbnis lautet 75, denn es heist, punkt vor Strichrechnung. '
        },
        {
            index: 5,
            question: 'Ein Kilometer sind 1.000 Meter. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Ein Kilometer sind 1.000 Meter. '
        },
        {
            index: 6,
            question: 'Wenn ich einen Regenwurm teile, entstehen zwei Regenwürmer! ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Ein Wurm ohne Schwanz kann überleben ohne Kopf, aber nicht. '
        },
        {
            index: 7,
            question: ' ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: ' '
        },
        {
            index: 8,
            question: ' ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: ' '
        },
        {
            index: 9,
            question: ' ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: ' '
        },
        {
            index: 10,
            question: ' ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: ' '
        },
        {
            index: 11,
            question: 'Ich glaube, Pinguine sind keine Vögel. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Pinguine sind eine Gruppe flugunfähiger Seevögel der Südhalbkugel. '
        },
        {
            index: 5,
            question: 'Wer war zuerst da. Das Huhn oder das Ei? Ich sage das Huhn!',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Denn der Vorfahre des Huhns hat sich nicht einfach im Laufe seines Lebens verändert und war plötzlich ein Huhn. '
        },
        {
            index: 4,
            question: 'Pass auf. Ich behaupte Tomaten sind Obst! ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Paprika, Tomaten, Kürbisse, Zucchini, Auberginen und Gurken sind zwar Früchte aber gehören laut der botanischen Definition zu Obst. '
        },
        {
            index: 4,
            question: 'Ich glaube, das Gehirn einer Katze ist besser als das vom Hund. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Das Hirn der Katze wiegt im Schnitt 25 Gramm, dort arbeiten 300 Mio. Nervenzellen. Ein Hunde-Hirn kommt im Schnitt auf 64 Gramm, aber nur 160 Mio. Nervenzellen. '
        },
        {
            index: 5,
            question: 'Ich sage, Hunde stammen von Wölfen ab. ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Haushunde und der heutige graue Wolf haben den gleichen Vorfahren. Das war eine Wolfsart, die vor mindestens 45000 Jahren lebte und mittlerweile ausgestorben ist. '
        },
        {
            index: 6,
            question: 'Ich behaupte, dass die Insel Föhr größer ist als Sylt! ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Die Insel Föhr ist 82,82 km² und Sylt 99,14 km² '
        },
        {
            index: 7,
            question: 'Stimmt es, dass in Deutschland nach dem zweiten Weltkrieg, die heimliche Währung Fleisch war? ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Es wahren Zigaretten. '
        },
        {
            index: 8,
            question: 'Stimmt es das die Farbe der Gelb ist? ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Richtig',
            correct_info: 'Betrachtet man die Sonne aus dem Weltraum, erscheint sie weiß. Ihre gewohnte gelbe Farbe erklärt sich durch den Einfluss der Erdatmosphäre. '
        },
        {
            index: 9,
            question: 'Ich behaupte, dass der Schatten auf dem Mond, kommt von der Erde kommt! ',
            answers: ['Richtig', 'Falsch'],
            correct_answer: 'Falsch',
            correct_info: 'Die Schatten der Mondphasen kommt vom Mond selbst. Wir sehen den unbeleuchteten Teil des Mondes. Da der Mond um die Erde kreist, ändert sich das Aussehen des Mondes und des Schattens. '
        }
    ]
});
