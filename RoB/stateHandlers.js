'use strict';

var Alexa = require('alexa-sdk');
var constants = require('./constants');

var https = require('https');

var stateHandlers = {
    startModeIntentHandlers: Alexa.CreateStateHandler(constants.states.START_MODE, {
        /*
         *  All Intent Handlers for state : START_MODE
         */
        'LaunchRequest': function () {

            https.get("https://nordicskills.de/RoB/categories.json", res => { // JSON-Datei aus RoB-Tool herunterladen und in Session speichern
                res.setEncoding("utf8");
                let body = "";
                res.on("data", data => {
                    body += data;
                });
                res.on("end", () => {
                    this.attributes['hierarchy'] = removeEmptyStringElements(JSON.parse(body));

                    this.attributes['index'] = -1;
                    this.handler.state = constants.states.START_MODE;
                    /*

                    var reprompt = 'Hier ist eine kleine Auswahl an Beiträgen für dich: '
                        + buildRandomIndex(audioDataDE, 5) + " Du findest alle verfügbaren Blogtitel in deiner Alexa-App. "
                        + "Sage 'Anfangen' oder einen Titel deiner Wahl, um zu starten.";*/

                    var speech = ' Herzlich Willkommen bei RoB, deinem "Reiseblog ohne Bilder". <break time="200ms" /> Wieso ein Reiseblog ohne Bilder`? ' 
                        + '<break time="150ms" /> Weil nichts bunter, großartiger und einzigartiger ist <break time="200ms" /> als unsere eigene Fantasie.'
                        + 'Weil nichts so schön gemalt, fotografiert oder gefilmt ist <break time="200ms" /> wie unser eigenes Kopfkino. Weil uns nichts so wundervolle Länder, '
                        + 'Städte, Landschaften und Reisen beschert <break time="200ms" /> wie unsere Einbildungskraft. Weil es einfach nichts Perfekteres gibt. <break time="150ms" /> '
                        + 'Deshalb ein Reiseblog ohne Bilder. '
                        + 'Hör einfach nur zu! <break time="200ms" /> Sage "Menü" oder einen Blogtitel deiner Wahl, um zu starten. ';

                    this.emitWithState('MenuIntent', speech);
                });
            });
        },
        'PlayAudio': function () {

            if (!this.attributes['playOrder']) {
                // Initialize Attributes if undefined.
                this.attributes['playOrder'] = "placeholder";
                this.attributes['index'] = -1;
                this.attributes['offsetInMilliseconds'] = 0;
                this.attributes['loop'] = false;
                this.attributes['shuffle'] = false;
                this.attributes['playbackIndexChanged'] = true;
                //  Change state to START_MODE
                this.handler.state = constants.states.START_MODE;
            }
            controller.play.call(this);
        },
        'MenuIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            this.handler.state = constants.states.START_MODE;

            speech += 'Folgende Kategorien stehen dir zur Auswahl: ';
            var reprompt = 'Was möchtest du jetzt hören? Sage ';
            for (var i = 0; i < this.attributes['hierarchy'].length; i++) {
                speech += this.attributes['hierarchy'][i].title + (this.attributes['hierarchy'].length - 2 == i ? ' oder ' : (this.attributes['hierarchy'].length - 1 == i ? '. ' : ', '));
                reprompt += this.attributes['hierarchy'][i].intent + (this.attributes['hierarchy'].length - 2 == i ? ' oder ' : (this.attributes['hierarchy'].length - 1 == i ? '. ' : ', '));
            }
            speech += reprompt;

            this.response.audioPlayerStop();
            this.response.speak(speech).listen(reprompt);
            this.emit(':responseReady');
        },
        'AMAZON.PreviousIntent': function () {
            if (!this.attributes['breadcrumbs'])
                this.attributes['breadcrumbs'] = [];

            if (this.attributes['breadcrumbs'].length > 1) {
                var lastPodcast = searchArrayWithId(this.attributes['hierarchy'], this.attributes['breadcrumbs'][this.attributes['breadcrumbs'].length - 2]);

                this.attributes['breadcrumbs'].splice(-1, 1);

                var message = '';
                var reprompt = 'Was möchtest du jetzt hören? Sage ';
                var appendReprompt = false;

                for (var i = 0; i < lastPodcast.children.length; i++) {
                    if (lastPodcast.children[i].title.toLowerCase() !== lastPodcast.children[i].intent.toLowerCase())
                        appendReprompt = true;

                    message += lastPodcast.children[i].title + (lastPodcast.children.length - 2 == i ? ' oder ' : (lastPodcast.children.length - 1 == i ? '. ' : ', '));
                    reprompt += lastPodcast.children[i].intent + (lastPodcast.children.length - 2 == i ? ' oder ' : (lastPodcast.children.length - 1 == i ? '. ' : ', '));
                }
                if (appendReprompt)
                    message += reprompt;

                this.response.speak(message).listen(reprompt + " Wenn du zurück möchtest sage Zurück oder Menü");
                this.emit(':responseReady');
            } else {
                this.handler.state = constants.states.START_MODE;
                this.emitWithState('LaunchRequest');
            }
        },
        'AMAZON.HelpIntent': function () {
            var message = 'Mit „Alexa Pause“ pausierst du deinen Blogbeitrag. <break time="100ms"/> Mit "Alexa fortsetzen" spielst du ihn weiter ab <break time="100ms"/> ' +
                'und mit „Alexa beenden“ beendest du ihn. <break time="100ms"/> Du kannst in der Blogliste mit "Alexa weiter" oder "Alexa zurück" die Beiträge wechseln. ' +
                '<break time="100ms"/> Sage "Anfang" oder einen Titel deiner Wahl um zu starten.';
            var reprompt = 'Sage "Anfangen" oder einen Titel deiner Wahl um zu starten. ';
            this.response.speak(message).listen(reprompt);
            this.emit(':responseReady');
        },
        'AMAZON.StopIntent': function () {
              var message = 'Auf wiederhören.';
            this.response.speak(message);
            this.emit(':responseReady');
        },
        'AMAZON.CancelIntent': function () {
              var message = 'Auf wiederhören.';
            this.response.speak(message);
            this.emit(':responseReady');
        },
        'SessionEndedRequest': function () {
            // No session ended logic
        },
        'Unhandled': function () {
              var message = 'Entschuldigung, Ich habe dich leider nicht verstanden.';
            this.response.speak(message).listen(message);
            this.emit(':responseReady');
        }
    }),
    playModeIntentHandlers: Alexa.CreateStateHandler(constants.states.PLAY_MODE, {
        /*
             *  All Intent Handlers for state : PLAY_MODE
             */
        'LaunchRequest': function () {
            /*
                   *  Session resumed in PLAY_MODE STATE.
                   *  If playback had finished during last session :
                   *      Give welcome message.
                   *      Change state to START_STATE to restrict user inputs.
                   *  Else :
                   *      Ask user if he/she wants to resume from last position.
                   *      Change state to RESUME_DECISION_MODE
                   */
            if (this.attributes['playbackFinished']) {
                this.handler.state = constants.states.START_MODE;

                var speech = 'Entschuldigung, Ich habe dich nicht verstanden.';
                var reprompt = 'Du kannst sagen Anfangen, um zu starten.';

                this.response.speak(speech).listen(reprompt);
                this.emit(':responseReady');
            } else {
                this.handler.state = constants.states.RESUME_DECISION_MODE;
                var lastPodcast = searchArrayWithId(this.attributes['hierarchy'], this.attributes['index']);

                if (lastPodcast.content != null) {
                    var speech = 'Du hörst gerade ' + lastPodcast.title + ' möchtest du es weiter hören?';
                    var reprompt = 'Du kannst ja sagen um es weiter zuhören oder nein <break time="100ms"/> um neu zu starten.';

                    this.response.speak(speech).listen(reprompt);
                    this.emit(':responseReady');
                } else {
                    this.handler.state = constants.states.START_MODE;
                    this.emitWithState('MenuIntent');
                }
            }
        },
        'PlayAudio': function () {
            controller.play.call(this)
        },
        'MenuIntent': function () {

            this.handler.state = constants.states.START_MODE;

            var message = 'Folgende Kategorien stehen dir zur Auswahl: ';
            var reprompt = 'Was möchtest du jetzt hören? Sage ';
            for (var i = 0; i < this.attributes['hierarchy'].length; i++) {
                message += this.attributes['hierarchy'][i].title + (this.attributes['hierarchy'].length - 2 == i ? ' oder ' : (this.attributes['hierarchy'].length - 1 == i ? '. ' : ', '));
                reprompt += this.attributes['hierarchy'][i].intent + (this.attributes['hierarchy'].length - 2 == i ? ' oder ' : (this.attributes['hierarchy'].length - 1 == i ? '. ' : ', '));
            }
            message += reprompt;

            this.response.audioPlayerStop();
            this.response.speak(message).listen(reprompt);
            this.emit(':responseReady');
        },
        /*'OtherAudio': function () {
            var message = 'Du hörst gerade ' + searchArrayWithId(this.attributes['hierarchy'], this.attributes['index']).title + '. Welchen Beitrag möchtest du hören?';
            var reprompt = 'Du findest alle verfügbaren Blogbeiträge in deiner Alexa-App.';
            this.response.speak(message).listen(reprompt);
        },*/
        'AMAZON.NextIntent': function () {
            controller.playNext.call(this)
        },
        'AMAZON.PreviousIntent': function () {
            controller.playPrevious.call(this)
        },
        'AMAZON.PauseIntent': function () {
            controller.stop.call(this)
        },
        'AMAZON.StopIntent': function () {
            controller.stop.call(this)
        },
        'AMAZON.CancelIntent': function () {
            controller.stop.call(this)
        },
        'AMAZON.ResumeIntent': function () {
            controller.play.call(this)
        },
        /*'AMAZON.LoopOnIntent': function () {
            controller.loopOn.call(this)
        },
        'AMAZON.LoopOffIntent': function () {
            controller.loopOff.call(this)
        },
        'AMAZON.ShuffleOnIntent': function () {
            controller.shuffleOn.call(this)
        },
        'AMAZON.ShuffleOffIntent': function () {
            controller.shuffleOff.call(this)
        },*/
        'AMAZON.StartOverIntent': function () {
            controller.startOver.call(this)
        },
        'AMAZON.HelpIntent': function () {
            var message = 'Mit „Alexa Pause“ pausierst du deinen Beitrag. <break time="100ms"/> Mit "Alexa fortsetzen" spielst du ihn weiter ab <break time="100ms"/> ' +
                'und mit „Alexa beenden“ beendest du ihn. <break time="100ms"/> Du kannst in der Blogliste mit "Alexa weiter" oder "Alexa zurück" die Beiträge wechseln. ' +
                '<break time="100ms"/> Sage Menue um in das Hauptmenü zu gelangen.';
            var reprompt = 'Sage Menue oder einen Beitrag deiner Wahl, um zu starten. ';
            this.response.speak(message).listen(reprompt);
            this.emit(':responseReady');
        },
        'SessionEndedRequest': function () {
            // No session ended logic
        },
        'Unhandled': function () {
            var message = 'Entschuldigung <break time="100ms"/> ich habe dich nicht verstanden. Du kannst weiter oder zurück sagen um in der Blogliste zu wechseln. ';
            this.response.speak(message).listen(message);
            this.emit(':responseReady');
        }
    }),
    remoteControllerHandlers: Alexa.CreateStateHandler(constants.states.PLAY_MODE, {
        /*
             *  All Requests are received using a Remote Control. Calling corresponding handlers for each of them.
             */
        'PlayCommandIssued': function () {
            controller.play.call(this)
        },
        'PauseCommandIssued': function () {
            controller.stop.call(this)
        },
        'NextCommandIssued': function () {
            controller.playNext.call(this)
        },
        'PreviousCommandIssued': function () {
            controller.playPrevious.call(this)
        }
    }),
    resumeDecisionModeIntentHandlers: Alexa.CreateStateHandler(constants.states.RESUME_DECISION_MODE, {
        /*
             *  All Intent Handlers for state : RESUME_DECISION_MODE
             */
        'LaunchRequest': function () {
            this.handler.state = constants.states.RESUME_DECISION_MODE;
            var lastPodcast = searchArrayWithId(this.attributes['hierarchy'], this.attributes['index']);

            if (lastPodcast.content != null) {
                var message = 'Du hörst gerade ' + lastPodcast.title + ' möchtest du den Beitrag weiter hören? ';
                var reprompt = 'Du kannst ja sagen um es weiter zuhören oder nein um neu zu starten. ';
                this.response.speak(message).listen(reprompt);
                this.emit(':responseReady');
            } else {
                this.handler.state = constants.states.START_MODE;
                this.emitWithState('LaunchRequest');
            }
        },
        'PlayAudio': function () {
            controller.play.call(this)
        },
        'MenuIntent': function () {

            this.handler.state = constants.states.START_MODE;

            var message = 'Folgende Kategorien stehen dir zur Auswahl: ';
            var reprompt = 'Was möchtest du jetzt hören? Sage ';
            for (var i = 0; i < this.attributes['hierarchy'].length; i++) {
                message += this.attributes['hierarchy'][i].title + (this.attributes['hierarchy'].length - 2 == i ? ' oder ' : (this.attributes['hierarchy'].length - 1 == i ? '. ' : ', '));
                reprompt += this.attributes['hierarchy'][i].intent + (this.attributes['hierarchy'].length - 2 == i ? ' oder ' : (this.attributes['hierarchy'].length - 1 == i ? '. ' : ', '));
            }
            message += reprompt;

            this.response.audioPlayerStop();
            this.response.speak(message).listen(reprompt);
            this.emit(':responseReady');
        },
        'AMAZON.YesIntent': function () {
            controller.play.call(this)
        },
        'AMAZON.NoIntent': function () {
            //controller.reset.call(this)
            this.attributes['index'] = -1;
            this.attributes['offsetInMilliseconds'] = 0;
            this.handler.state = constants.states.START_MODE;
            this.emitWithState('MenuIntent');
        },
        'AMAZON.HelpIntent': function () {

            var message = 'Mit „Alexa Pause“ pausierst du deinen Beitrag. <break time="100ms"/> Mit "Alexa fortsetzen" spielst du ihn weiter ab <break time="100ms"/> ' +
                'und mit „Alexa beenden“ beendest du ihn. <break time="100ms"/> Du kannst in der Blogliste mit "Alexa weiter" oder "Alexa zurück" die Beiträge wechseln. ' +
                '<break time="100ms"/> Sage Menue um in das Hauptmenü zu gelangen.';
            var reprompt = 'Sage Menue oder einen Beitrag deiner Wahl <break time="100ms"/> um zu starten.';

            this.response.speak(message).listen(reprompt);
            this.emit(':responseReady');
        },
        'AMAZON.StopIntent': function () {
            var message = 'Auf wiederhören. Bis zum nächsten mal. ';
            this.response.speak(message);
            this.emit(':responseReady');
        },
        'AMAZON.CancelIntent': function () {
            var message = 'Auf wiederhören. Bis zum nächsten mal. ';
            this.response.speak(message);
            this.emit(':responseReady');
        },
        'SessionEndedRequest': function () {
            // No session ended logic
        },
        'Unhandled': function () {
            var message = 'Entschuldigung, das ist kein gültiger Befehl. Bitte sage Hilfe <break time="100ms"/> um zu hören was du sagen kannst. ';
            this.response.speak(message).listen(message);
            this.emit(':responseReady');
        }
    })
};

module.exports = stateHandlers;

var controller = function () {
    return {
        play: function () {

            /*
                    *  Using the function to begin playing audio when:
                    *      Play Audio intent invoked.
                    *      Resuming audio when stopped/paused.
                    *      Next/Previous commands issued.
                    */
            if (this.attributes['playbackFinished']) {
                // Reset to top of the playlist when reached end.
                this.attributes['index'] = -1;
                this.attributes['offsetInMilliseconds'] = 0;
                this.attributes['playbackIndexChanged'] = true;
                this.attributes['playbackFinished'] = false;
            }

            var playBehavior = 'REPLACE_ALL';
            var podcast = null;
            var lastPodcast = null;

            if (typeof this.event.request.intent !== 'undefined')
                var slots = this.event.request.intent.slots;

            if (slots && slots.Sound && slots.Sound.value) {
                var soundName = slots.Sound.value

                if (soundName === "America" || soundName === "america")
                    soundName = "amerika";

                lastPodcast = searchArrayWithId(this.attributes['hierarchy'], this.attributes['index']);
                if(lastPodcast != false)
                    podcast = searchArrayWithIntent(lastPodcast, soundName);
                else
                    podcast = searchArrayWithIntent(this.attributes['hierarchy'], soundName);
                
                if (podcast) {
                    if (podcast.content != null) { // Eintrag mit Sound-File abspielen

                        this.handler.state = constants.states.PLAY_MODE;

                        var message = podcast.title + ' wird abgespielt. ';

                        var offsetInMilliseconds = this.attributes['offsetInMilliseconds'];
                        // Since play behavior is REPLACE_ALL, enqueuedToken attribute need to be set to null.
                        this.attributes['enqueuedToken'] = null;

                        this.attributes['index'] = podcast.id;

                        if (canThrowCard.call(this)) {
                            var cardTitle = 'Blogbeitrag: ' + podcast.title;
                            var cardContent = 'Blogbeitrag: ' + podcast.title;
                            this.response.cardRenderer(cardTitle, cardContent, null);
                        }

                        this.response.speak(message);

                        this.response.audioPlayerPlay(playBehavior, podcast.content, /* token */podcast.id, null, offsetInMilliseconds);
                        this.emit(':responseReady');

                    } else { // Eintrag ohne Sound-File
                        this.handler.state = constants.states.START_MODE;

                        if (!this.attributes['breadcrumbs'])
                            this.attributes['breadcrumbs'] = [];

                        this.attributes['breadcrumbs'].push(podcast.id);

                        if (podcast.children.length > 0) { // Menüpunkte auflisten

                            var message = '';
                            var reprompt = 'Was möchtest du jetzt hören? Sage ';
                            var appendReprompt = false;

                            for (var i = 0; i < podcast.children.length; i++) {
                                if (podcast.children[i].title.toLowerCase() !== podcast.children[i].intent.toLowerCase())
                                    appendReprompt = true;

                                message += podcast.children[i].title + (podcast.children.length - 2 == i ? ' oder ' : (podcast.children.length - 1 == i ? '. ' : ', '));
                                reprompt += podcast.children[i].intent + (podcast.children.length - 2 == i ? ' oder ' : (podcast.children.length - 1 == i ? '. ' : ', '));
                            }
                            if (appendReprompt)
                                message += reprompt;

                            this.response.speak(message).listen(reprompt + " Wenn du zurück möchtest sage Zurück oder Menü. ");
                            this.emit(':responseReady');
                        } else {
                            var message = 'In dieser Rubrik gibt es derzeit keinen Beitrag. Um einen Schritt zurück zu kommen sage zurück oder sage Menü. ';
                            this.response.speak(message).listen(message);
                            this.emit(':responseReady');
                        }
                    }
                } else {
                    this.emitWithState('Unhandled');
                }
            } else {
                lastPodcast = searchArrayWithId(this.attributes['hierarchy'], this.attributes['index']);
                if (lastPodcast != false) {
                    if (lastPodcast.content != null) {
                        this.handler.state = constants.states.PLAY_MODE;

                        var playBehavior = 'REPLACE_ALL';
                        var offsetInMilliseconds = this.attributes['offsetInMilliseconds'];

                        this.response.audioPlayerPlay(playBehavior, lastPodcast.content, /* token */lastPodcast.id, null, offsetInMilliseconds);
                        this.emit(':responseReady');
                    } else {
                        this.handler.state = constants.states.START_MODE;
                        this.emitWithState('LaunchRequest');
                    }
                } else {
                    this.emitWithState('Unhandled');
                }
            }
        },
        stop: function () {
            /*
                   *  Issuing AudioPlayer.Stop directive to stop the audio.
                   *  Attributes already stored when AudioPlayer.Stopped request received.
                   */
            this.response.audioPlayerStop();
            this.emit(':responseReady');
        },
        playNext: function () {
            /*
                   *  Called when AMAZON.NextIntent or PlaybackController.NextCommandIssued is invoked.
                   *  Index is computed using token stored when AudioPlayer.PlaybackStopped command is received.
                   *  If reached at the end of the playlist, choose behavior based on "loop" flag.
                   */
            var index = this.attributes['index'];
            var arr = searchArrayNextOrPrevious(this.attributes['hierarchy'], index, false);

            // Check for last audio file.
            if (arr) {
                // Set values to attributes.
                this.attributes['index'] = arr.id;
                this.attributes['offsetInMilliseconds'] = 0;
                this.attributes['playbackIndexChanged'] = true;

                controller.play.call(this);
            } else {
                // Reached at the end. Thus reset state to start mode and stop playing.
                this.handler.state = constants.states.START_MODE;
                var message = 'Du hast das Ende des Blogs erreicht.';
                this.response.speak(message).audioPlayerStop();
                return this.emit(':responseReady');
            }       
        },
        playPrevious: function () {
            /*
                   *  Called when AMAZON.PreviousIntent or PlaybackController.PreviousCommandIssued is invoked.
                   *  Index is computed using token stored when AudioPlayer.PlaybackStopped command is received.
                   *  If reached at the end of the playlist, choose behavior based on "loop" flag.
                   */
            var index = this.attributes['index'];
            var arr = searchArrayNextOrPrevious(this.attributes['hierarchy'], index, true);

            // Check for last audio file.
            if (arr) {
                // Set values to attributes.
                this.attributes['index'] = arr.id;
                this.attributes['offsetInMilliseconds'] = 0;
                this.attributes['playbackIndexChanged'] = true;

                controller.play.call(this);
            } else {
                // Reached at the end. Thus reset state to start mode and stop playing.
                this.handler.state = constants.states.START_MODE;
                var message = 'Du hast den Anfang des Blogs erreicht.';
                this.response.speak(message).audioPlayerStop();
                return this.emit(':responseReady');
            }           
        },
        /*loopOn: function () {
            // Turn on loop play.
            this.attributes['loop'] = true;
            var message = 'Endlosschleife eingeschaltet.';
            this.response.speak(message);
            this.emit(':responseReady');
        },
        loopOff: function () {
            // Turn off looping
            this.attributes['loop'] = false;
            var message = 'Endlosschleife ausgeschaltet.';
            this.response.speak(message);
            this.emit(':responseReady');
        },
        shuffleOn: function () {
            // Turn on shuffle play.
            this.attributes['shuffle'] = true;
            //shuffleOrder((newOrder) => {
                // Play order have been shuffled. Re-initializing indices and playing first song in shuffled order.
                //this.attributes['playOrder'] = newOrder;
                //this.attributes['index'] = 0;
                //this.attributes['offsetInMilliseconds'] = 0;
                //this.attributes['playbackIndexChanged'] = true
                controller.play.call(this);
            //}, this.event.request.locale);
        },
        shuffleOff: function () {
            // Turn off shuffle play.
            if (this.attributes['shuffle']) {
                this.attributes['shuffle'] = false;
                // Although changing index, no change in audio file being played as the change is to account for reordering playOrder
                //this.attributes['index'] = this.attributes['playOrder'][this.attributes['index']];
                //this.attributes['playOrder'] = Array.apply(null, { length: audioDataDE.length }).map(Number.call, Number);
            }
            controller.play.call(this);
        },*/
        startOver: function () {
            // Start over the current audio file.
            this.attributes['offsetInMilliseconds'] = 0;
            controller.play.call(this);
        },
        reset: function () {
            // Reset to top of the playlist.
            this.attributes['index'] = -1;
            this.attributes['offsetInMilliseconds'] = 0;
            this.attributes['playbackIndexChanged'] = true;
            controller.play.call(this);
        }
    }
}();


function searchArrayWithId(arr, id) {
    var currentChild, result;

    if (!Array.isArray(arr) && id == arr.id) {
        return arr;
    } else {
        for (var i = 0; i < (Array.isArray(arr) ? arr.length : arr.children.length); i++) {
            currentChild = (Array.isArray(arr) ? arr[i] : arr.children[i]);

            result = searchArrayWithId(currentChild, id);

            if (result !== false) {
                return result;
            }
        }
        return false;
    }
}

function searchArrayWithIntent(arr, intent) {
    var currentChild, result;

    if (!Array.isArray(arr) && intent.toLowerCase() == arr.intent.toLowerCase()) {
        return arr;
    } else {
        for (var i = 0; i < (Array.isArray(arr) ? arr.length : arr.children.length); i++) {
            currentChild = (Array.isArray(arr) ? arr[i] : arr.children[i]);

            result = searchArrayWithIntent(currentChild, intent);

            if (result !== false) {
                return result;
            }
        }
        return false;
    }
}

function searchArrayNextOrPrevious(arr, id, previous) {
    var currentChild, result;
    if (previous)
        id = id - 1;
    else
        id = id + 1;

    if (!Array.isArray(arr) && id == arr.id && arr.content != null) {
        return arr;
    } else {
        for (var i = 0; i < (Array.isArray(arr) ? arr.length : arr.children.length); i++) {
            currentChild = (Array.isArray(arr) ? arr[i] : arr.children[i]);

            result = searchArrayWithId(currentChild, id);

            if (result !== false) {
                return result;
            }
        }
        return false;
    }
}


function canThrowCard() {
    /*
       * To determine when can a card should be inserted in the response.
       * In response to a PlaybackController Request (remote control events) we cannot issue a card,
       * Thus adding restriction of request type being "IntentRequest".
       */
    if (this.event.request.type === 'IntentRequest' && this.attributes['playbackIndexChanged']) {
        this.attributes['playbackIndexChanged'] = false;
        return true;
    } else {
        return false;
    }
}

/*function shuffleOrder(callback, locale) {
    // Algorithm : Fisher-Yates shuffle
    var array = Array.apply(null, { length: audioDataDE.length }).map(Number.call, Number);
    var currentIndex = array.length;
    var temp,
        randomIndex;

    while (currentIndex >= 1) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temp = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temp;
    }
    callback(array);
}*/

function removeEmptyStringElements(obj) {
    for (var prop in obj) {
        if (typeof obj[prop] === 'object') {// dive deeper in
            removeEmptyStringElements(obj[prop]);
        } else if (obj[prop] === '') {// delete elements that are empty strings
            obj[prop] = null;
        }
    }
    return obj;
}