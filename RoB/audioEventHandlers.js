'use strict';

var Alexa = require('alexa-sdk');
var constants = require('./constants');

// Binding audio handlers to PLAY_MODE State since they are expected only in this mode.
var audioEventHandlers = Alexa.CreateStateHandler(constants.states.PLAY_MODE, {
    'PlaybackStarted': function () {
        /*
             * AudioPlayer.PlaybackStarted Directive received.
             * Confirming that requested audio file began playing.
             * Storing details in dynamoDB using attributes.
             */
        this.attributes['token'] = getToken.call(this);
        //this.attributes['index'] = getIndex.call(this);
        this.attributes['playbackFinished'] = false;
        this.emit(':saveState', true);
    },
    'PlaybackFinished': function () {
        /*
             * AudioPlayer.PlaybackFinished Directive received.
             * Confirming that audio file completed playing.
             * Storing details in dynamoDB using attributes.
             */
        this.attributes['playbackFinished'] = true;
        this.attributes['enqueuedToken'] = false;
        this.emit(':saveState', true);
    },
    'PlaybackStopped': function () {
        /*
             * AudioPlayer.PlaybackStopped Directive received.
             * Confirming that audio file stopped playing.
             * Storing details in dynamoDB using attributes.
             */
        this.attributes['token'] = getToken.call(this);
        //this.attributes['index'] = getIndex.call(this);
        this.attributes['offsetInMilliseconds'] = getOffsetInMilliseconds.call(this);
        this.emit(':saveState', true);
    },
    'PlaybackNearlyFinished': function () {
        /*
             * AudioPlayer.PlaybackNearlyFinished Directive received.
             * Using this opportunity to enqueue the next audio
             * Storing details in dynamoDB using attributes.
             * Enqueuing the next audio file.
             */
        if (this.attributes['enqueuedToken']) {
            /*
                   * Since AudioPlayer.PlaybackNearlyFinished Directive are prone to be delivered multiple times during the
                   * same audio being played.
                   * If an audio file is already enqueued, exit without enqueuing again.
                   */
            return this.context.succeed(true);
        }

        var arr = searchArrayNextOrPrevious(this.attributes['hierarchy'], this.attributes['index'], false);
        var enqueueIndex = 0;
        // Checking if  there are any items to be enqueued.
        if (arr) {
            enqueueIndex = arr.id;
        } else {
            if (this.attributes['loop']) {
                // Enqueueing the first item since looping is enabled.
                enqueueIndex = searchArrayNextOrPrevious(this.attributes['hierarchy'], 0, false);
            } else {
                // Nothing to enqueue since reached end of the list and looping is disabled.
                return this.context.succeed(true);
            }
        }
        // Setting attributes to indicate item is enqueued.
        this.attributes['enqueuedToken'] = 0;//String(this.attributes['playOrder'][enqueueIndex]);

        var enqueueToken = this.attributes['enqueuedToken'];
        var playBehavior = 'ENQUEUE';

        var podcast = searchArrayWithId(this.attributes['hierarchy'], enqueueIndex);
        var expectedPreviousToken = this.attributes['token'];
        var offsetInMilliseconds = 0;

        this.response.audioPlayerPlay(playBehavior, podcast.url, enqueueToken, expectedPreviousToken, offsetInMilliseconds);
        this.emit(':responseReady');
    },
    'PlaybackFailed': function () {
        //  AudioPlayer.PlaybackNearlyFinished Directive received. Logging the error.
        console.log("Playback Failed : %j", this.event.request.error);
        this.context.succeed(true);
    }
});

module.exports = audioEventHandlers;

function getToken() {
    // Extracting token received in the request.
    return this.event.request.token;
}

function getIndex() {
    // Extracting index from the token received in the request.
    var tokenValue = parseInt(this.event.request.token);
    return this.attributes['playOrder'].indexOf(tokenValue);
}

function getOffsetInMilliseconds() {
    // Extracting offsetInMilliseconds received in the request.
    return this.event.request.offsetInMilliseconds;
}

function searchArrayWithId(arr, id) {
    var currentChild, result;

    if (!Array.isArray(arr) && id == arr.id) {
        return arr;
    } else {
        for (var i = 0; i < (Array.isArray(arr) ? arr.length : arr.children.length); i++) {
            currentChild = (Array.isArray(arr) ? arr[i] : arr.children[i]);

            result = searchArrayWithId(currentChild, id);

            if (result !== false) {
                return result;
            }
        }
        return false;
    }
}

function searchArrayNextOrPrevious(arr, id, previous) {
    var currentChild, result;
    if (previous)
        id = id - 1;
    else
        id = id + 1;

    if (!Array.isArray(arr) && id == arr.id && arr.content != null) {
        return arr;
    } else {
        for (var i = 0; i < (Array.isArray(arr) ? arr.length : arr.children.length); i++) {
            currentChild = (Array.isArray(arr) ? arr[i] : arr.children[i]);

            result = searchArrayWithId(currentChild, id);

            if (result !== false) {
                return result;
            }
        }
        return false;
    }
}