﻿var Alexa = require('alexa-sdk');
var dynamo = require('./dynamoDB');

var hints = require('./hints.json');

exports.handler = function (event, context, callback) {
    var alexa = Alexa.handler(event, context);

    alexa.registerHandlers(handlers);
    alexa.execute();
};

var handlers = {

    'NewSession': function () { // Session wird erstellt oder wenn vorhanden aus Datenbank geladen

        console.log("Session init");

        var self = this;
        dynamo.getUserState(self.event.session, function (data) {

            console.log(data);

            if (data.item && self.attributes["gameState"] !== "Reset") {
                Object.assign(self.event.session.attributes, data.item);

                self.attributes["gameState"] = "Main";
            } else {
                self.attributes["gameState"] = "Reset";
            }
            
            if (self.attributes["gameState"] === "Reset") {

                self.attributes['currentWord'] = 0;
                self.attributes['hint'] = 0;
                self.attributes['usedWords'] = [];

                self.attributes['name'] = "";
                self.attributes['score'] = 0;

                self.attributes["gameState"] = "Main";
            }

            self.emit('LaunchRequest');
        });
    },
    'LaunchRequest': function () {

        if (this.attributes['name'] === "" && this.attributes["gameState"] === "Main") {
            this.emit(':ask', '<audio src="https://nordicskills.de/audio/WerBinIch/start_sound.mp3" /> Hallo <break time="200ms"/> ich freue mich das du mit mir spielen willst. Damit wir beide gleich spielen können, möchte ich gerne deinen Namen wissen. <break time="200ms"/> Wie heißt du? ', 'Also, wie ist dein Name?');
        } else if (this.attributes['name'] === "" && this.attributes["gameState"] === "RequestNameChange") {
            var speech = 'Entschuldigung das ich dich falsch verstanden habe. Dann versuchen wir es noch einmal. Wie ist dein Name? ';
            this.emit(':ask', speech, speech);
        } else {
            var speech = "Hallo " + this.attributes['name'] + ', ich freue mich das du da bist <break time="10ms"/> und mit mir spielen möchtest. <break time="100ms"/> Wenn du soweit bist, sage start <break time="100ms"/> oder anfangen.';
            this.emit(':ask', speech, speech);
        }
    },
    'YesIntent': function () {

        if (this.attributes["gameState"] === "RequestNewWord") {
            this.emit('NextWordIntent');
        } else if (this.attributes["gameState"] === "RequestNextHint") {
            this.attributes["gameState"] = "Main";
            this.emit('NoIntent');
        } else if (this.attributes["gameState"] === "RequestNameChange") {
            this.attributes["gameState"] = "Main";
            this.emit('LaunchRequest');
        } else { // Spieler meint die Lösung zu kennen
            this.emit(':ask', '<say-as interpret-as="interjection">hip hip hurra</say-as>. Jetzt bin ich aber gespannt. <break time="200ms"/> <say-as interpret-as="interjection">na und?</say-as> ' + this.attributes['name'] + 'wie lautet deine Antwort. ', + this.attributes['name'] + "? <break time='200ms'/> Wenn du die Antwort kennst, sage sie mir bitte!");
        }
    },
    'NoIntent': function () {

        if (this.attributes["gameState"] === "RequestNewWord" || this.attributes["gameState"] === "RequestNextHint") {
            this.emit('SessionEndedRequest');
        } else if (this.attributes["gameState"] === "RequestNameChange") {
            this.attributes['name'] = "";
            this.emit('LaunchRequest');
        } else { // Nächster Tipp

            if (this.attributes['hint'] < hints[this.attributes['currentWord']]["hints"].length - 2) {

                this.attributes['hint'] = this.attributes['hint'] + 1;
                var speech = hints[this.attributes['currentWord']]["hints"][this.attributes['hint']];
                speech += hintEnding[Math.floor(Math.random() * hintEnding.length)];
            } else if (this.attributes['hint'] < hints[this.attributes['currentWord']]["hints"].length - 1) {

                this.attributes['hint'] = this.attributes['hint'] + 1;
                var speech = hints[this.attributes['currentWord']]["hints"][this.attributes['hint']];
                speech += this.attributes['name'] + ' das war der letzte <phoneme alphabet=\"ipa\" ph=\"tɪp\">Tipp</phoneme> von mir. Mehr habe ich nicht für dich. Ist aber nicht <say-as interpret-as="interjection">schlimm</say-as>. Ich löse das Rätsel jetzt für dich. ';
                speech += hintEnding[Math.floor(Math.random() * hintEnding.length)];
            } else {

                var speech = "Die richtige Antwort lautet: " + hints[this.attributes['currentWord']]["answers"][0] + ". Möchtest du ein weiteres Wort erraten? ";
                this.attributes["gameState"] = "RequestNewWord";
            }

            this.emit(':ask', speech, speech);
        }
    },
    'AnswerIntent': function () { // Gegebene Antwort wird verarbeitet.

        if (this.attributes["name"] === "") {

            this.attributes["gameState"] = "RequestNameChange";
            this.attributes['name'] = this.event.request.intent.slots.ANSWER.value;
            this.emit(':ask', 'Danke, dass du mir deinen Namen verraten hast. <break time="200ms"/> Dann lass uns mal sehen, ob ich dich richtig verstanden habe. Ich habe ' + this.attributes['name'] + " verstanden. Ist das richtig? ", + this.attributes['name'] + "? habe ich deinen Namen richtig verstanden? ");
        } else {

            var answer = this.event.request.intent.slots.ANSWER.value;
            var correctAnswer = false;

            for (var possibleAnswer in hints[this.attributes['currentWord']]["answers"]) {
                if (hints[this.attributes['currentWord']]["answers"][possibleAnswer] === answer) {
                    correctAnswer = true;
                }
            }
            console.log("corr: " + correctAnswer);

            if (correctAnswer) {
                this.attributes['score'] = this.attributes['score'] + 1;

                var speech = '<say-as interpret-as="interjection">juhu</say-as>. Das ist richtig! Es war ' + hints[this.attributes['currentWord']]["answers"][0] + ". Das hast du klasse gemacht. " + '<audio src="https://nordicskills.de/audio/WerBinIch/klatschen.mp3" />' + "Möchtest du ein weiteres Wort erraten? ";
                this.attributes["gameState"] = "RequestNewWord";
            } else {

                if (this.attributes['hint'] === hints[this.attributes['currentWord']]["hints"].length - 1) {
                    var speech = "Das war leider nicht richtig " + this.attributes['name'] + ". Aber ich helfe dir. Die richtige Antwort lautet: " + hints[this.attributes['currentWord']]["answers"][0] + ". Möchtest du ein weiteres Wer bin ich? Wort erraten? ";
                    this.attributes["gameState"] = "RequestNewWord";
                } else {
                    var speech = "Das war leider nicht richtig " + this.attributes['name'] + ". Möchtest du noch einen <phoneme alphabet=\"ipa\" ph=\"tɪp\">Tipp</phoneme> von mir? ";
                    this.attributes["gameState"] = "RequestNextHint";
                }
            }
            this.emit(':ask', speech, speech);
        }
    },
    'NextWordIntent': function () { // Nächstes Wort

        this.attributes["gameState"] = "Main";

        var available = [];
        for (var word in hints) {
            if (this.attributes['usedWords'].indexOf(word) < 0) {
                available.push(word);
            }
        }
        if (!available.length) {
            this.emit(':ask', '<audio src="https://nordicskills.de/audio/WerBinIch/applaus.mp3" /> <say-as interpret-as="interjection">super</say-as> ' + this.attributes['name'] + '. Das waren alle Wer bin ich Rätsel. Mehr habe ich für dich nicht. <break time="250ms"/> Wenn du noch einmal spielen möchtest, Sage nochmal spielen. Wenn du aufhören willst, sage Ende oder Beenden. <audio src="https://nordicskills.de/audio/WerBinIch/schluss.mp3" />', "");
        } else {
            var rand = Math.floor(Math.random() * available.length);

            this.attributes['usedWords'].push(available[rand]);
            this.attributes['currentWord'] = available[rand];
            this.attributes['hint'] = 0;

            var speech = hints[this.attributes['currentWord']]["hints"][this.attributes['hint']]; // Hier wird der Hinweis ausgegeben
            speech += hintEnding[Math.floor(Math.random() * hintEnding.length)];

            this.emit(':ask', speech, speech);
        }
    },
    'ResetIntent': function () { // Spielstand zurücksetzen
        this.attributes["gameState"] = "Reset";
        this.emit('NewSession');
    },
    'AMAZON.HelpIntent': function () {
        this.emit(':ask', '<audio src="https://nordicskills.de/audio/WerBinIch/hilfe.mp3" /> Und jetzt wünsche ich dir viel Spaß. Wenn du soweit bist, sage start <break time="100ms"/> oder anfangen.', 'Wenn du soweit bist, sage start <break time="100ms"/> oder anfangen.');
    },
    'AMAZON.CancelIntent': function () {
        this.emit('SessionEndedRequest');
    },
    'AMAZON.StopIntent': function () {
        this.emit('SessionEndedRequest');
    },
    'SessionEndedRequest': function () { // Session beenden und in die Datenbank speichern

        console.log("should end......");

        var self = this;
        dynamo.putUserState(self.event.session, function (data) {
            console.log(data.message);

            var now = new Date();

            /*if (now.getHours() >= 6 && now.getHours() < 11) { // Morgen
                var speech = "Okay, schönen Tag.";
            } else if (now.getHours() >= 11 && now.getHours() < 18) { // Tag
                var speech = "Okay, schönen Tag noch.";
            } else { // Nacht
                var speech = "Okay, schönen Abend.";
            }*/

            var speech = "Gut, Danke das du mit mir gespielt hast. " + (self.attributes['name'] === "" ? "" : self.attributes['name']) + " du hast " + (self.attributes['score'] === 1 ? "einen" : self.attributes['score']) + " von " + (hints.length + 1) + " Begriffen richtig erraten. ";

            self.emit(':tell', speech);
        });
    }
};

// Tipp-Enden
var hintEnding = [
    '<break time="200ms"/> Kennst du die Lösung?',
    '<break time="200ms"/> Weißt du was oder wen ich meine?',
    '<break time="200ms"/> <say-as interpret-as="interjection">na?</say-as> kennst du die Lösung?',
    '<break time="200ms"/> Kennst du die Antwort?'
];