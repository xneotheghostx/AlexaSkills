/**
 * App ID for the skill
 */
var APP_ID = "amzn1.ask.skill.09fc96aa-8a5f-4d66-823a-15cb6f352c1a"


// endpoints urls
const AQ_BASE_URL = 'https://api.breezometer.com/baqi/'
const MAPS_BASE_URL = 'https://maps.googleapis.com/maps/api/geocode/'

// config für die abfrage
var config = {
    aqi_key: 'de4fef0f7fb349f29f3f21c275018069',
    google_key: 'AIzaSyAbLBsugCTI5VCgHo2jLcrZ3gq7uYQH94o',
	field_format: 'breezometer_aqi,breezometer_description,pollutants,dominant_pollutant_description,breezometer_color,data_valid',
    data_format: 'json'
}

/**
 * AlexaSkill prototype und hilfe funktion  154b6c88100047e9a819e366005b3789  
 */
var AlexaSkill = require('./AlexaSkill')
var https = require('https')

/**
 * LuftQualitaetSkill ist ein subclass für AlexaSkill.
 */
var LuftQualitaetSkill = function () {
    AlexaSkill.call(this, APP_ID)
};

// Extend AlexaSkill
LuftQualitaetSkill.prototype = Object.create(AlexaSkill.prototype);
LuftQualitaetSkill.prototype.constructor = LuftQualitaetSkill;

// ----------------------- Override AlexaSkill request and intent handlers -----------------------

LuftQualitaetSkill.prototype.eventHandlers.onSessionStarted = function (sessionStartedRequest, session) {
    console.log("onSessionStarted requestId: " + sessionStartedRequest.requestId
        + ", sessionId: " + session.sessionId);
    // hier wird Initialisiert
};

LuftQualitaetSkill.prototype.eventHandlers.onLaunch = function (launchRequest, session, response) {
    console.log("onLaunch requestId: " + launchRequest.requestId + ", sessionId: " + session.sessionId);
    handleWelcomeRequest(response);
};

LuftQualitaetSkill.prototype.eventHandlers.onSessionEnded = function (sessionEndedRequest, session) {
    console.log("onSessionEnded requestId: " + sessionEndedRequest.requestId
        + ", sessionId: " + session.sessionId);
    // alles bereinigen
};

/**
 * überschreibe intentHandlers um intent handling funktion abzubilden
 */
LuftQualitaetSkill.prototype.intentHandlers = {
    "HoleLuftQualitaet": function (intent, session, response) {
        var locationSlot = intent.slots.location
        var repromptText, speechOutput
        // intent endet aber location wurde nicht erkannt
        if(!locationSlot.value){
            repromptText = "Bitte versuche es noch einmal, sage einen Standort, z. B. Tiergarten, Berlin. "
                + "Für welchen Standort möchtest du Informationen zur Luftqualität ?";
            speechOutput = "Es tut mir leid, ich kann diesen Standort nicht finden. " + repromptText

            response.ask(speechOutput, repromptText)
            return
        }
        // location wurde gefunden
        var location = locationSlot.value

        handleRequest(location, response, session)
    },
	"MoreInfoIntent": function (intent, session, response) {
		handleMoreInfoRequest(response, session);
	},
		
    "AMAZON.HelpIntent": function (intent, session, response) {
        handleHelpRequest(response);
    },

    "AMAZON.StopIntent": function (intent, session, response) {
        var speechOutput = "Danke, das ich helfen durfte. Aufwiederhören.";
        response.tell(speechOutput);
    },

    "AMAZON.CancelIntent": function (intent, session, response) {
        var speechOutput = "Danke, das ich helfen durfte. Aufwiederhören.";
        response.tell(speechOutput);
    }
};

// -------------------------- LuftIndex skill --------------------------

// Benutzer begrüßen wenn er das skill startet
function handleWelcomeRequest(response) {
    var repromptText = "Für welchen Standort möchtest du Informationen zur Luftqualität ?",
    speechOutput = {
            speech: "<speak>Willkommen beim Luftindex. "
                + repromptText
                + "</speak>",
            type: AlexaSkill.speechOutputType.SSML
        },
        repromptOutput = {
            speech: "Ich kann dir Informationen über die "
                + "Luftqualität einer bestimmten Stadt einem Ort oder Stadtteil geben. Die Informationen sind der Luftindex, die Schadstoffe im einzelnen, sowie der Warnhinweis für den am stärksten gemessenen Schadstoff und deren Auswirkungen für den Menschen."
                + repromptText,
            type: AlexaSkill.speechOutputType.PLAIN_TEXT
        };

    response.ask(speechOutput, repromptOutput);
}

// handles request wenn der benutzer hilfe anfragt
function handleHelpRequest(response) {
    var repromptText = "Für welchen Standort möchtest du Informationen zur Luftqualität ? ";
    var speechOutput = "Ich kann dir Informationen über die Luftqualität einer bestimmten Stadt einem Ort oder Stadtteil geben. Die Informationen sind der Luftindex, die Schadstoffe im einzelnen, sowie der Warnhinweis für den am stärksten gemessenen Schadstoff und deren Auswirkungen für den Menschen. "
        + repromptText;

    response.ask(speechOutput, repromptText);
}
// Ausgabe der Werte 
function handleMoreInfoRequest(response, session) {
    
	if(session.attributes.nextText != undefined) {
		var repromptText = ""
		var speechOutput = {
			speech: "<speak>Hier kommen die Werte: " + session.attributes.nextText + "</speak>",
			type: AlexaSkill.speechOutputType.SSML
		}

		response.tellWithCard(speechOutput, repromptText);
	} else {
		response.tellWithCard("Entschuldigung, ich habe dich nicht verstanden.");
	}
	
}

function handleRequest(location, response, session){
    getAirQuality(location, session, function(err, data) {
        if(err){
            // Eine oder beide der api´s geben Fehler aus
            if(err.message) response.tellWithCard(err.message)
            else response.tellWithCard("Entschuldigung, beim Dienst Luftindex ist ein Problem aufgetreten. Bitte versuche es später erneut.")
        } else {
            // alles gut gelaufen ausgabe in Sprache und Text auf App
			
			speechOutput = {
				speech: "<speak>" + data + " Möchtest du weitere Informationen und Werte ?</speak>",
				type: AlexaSkill.speechOutputType.SSML
			},
			repromptOutput = {
				speech: "Entschuldigung! Ich habe dich nicht verstanden.",
				type: AlexaSkill.speechOutputType.PLAIN_TEXT
			};

			response.ask(speechOutput, repromptOutput);
        }
    })
}

function getAirQuality(location, session, callback){
    config.givenLocation = location
    var google_url = MAPS_BASE_URL + getMapsParameters()

    https.get(google_url, function (res) {
        console.log(google_url)
        var response = '';

        if (res.statusCode != 200) {
            callback(new Error())
        }

        res.on('data', function (data) {
            response += data;
        });

        res.on('end', function () {
            var obj = JSON.parse(response);

            if (obj.status == "ZERO_RESULTS") {
                callback(new Error("Entschuldigung! Google hat diesen Standort nicht erkannt. Bitte versuche es erneut!"))
            } else if (obj.status == "REQUEST_DENIED"){
                callback(new Error("Entschuldigung! Es gibt ein Problem mit Google Maps API."))
            } else if(obj.status == "INVALID_REQUEST"){
                callback(new Error("Die Adressangaben fehlen. Bitte versuche es erneut!"))
            } else {
                config.resolvedLocation = obj.results[0].formatted_address
                config.latitude = obj.results[0].geometry.location.lat
                config.longitude = obj.results[0].geometry.location.lng

                var airq_url = AQ_BASE_URL + getAQIParameters()
                https.get(airq_url, function(res){
                    console.log(airq_url)
                    var response = ''

                    if (res.statusCode != 200) {
                        callback(new Error())
                    }

                    res.on('data', function (data) {
                        response += data
                    });
                    res.on('end', function () {
                        var obj = JSON.parse(response);
                        if (obj.error) {
                            callback(new Error("Error: " + obj.error.message))
                        } else {
                            var text = 'Ich habe für ' + config.resolvedLocation +' eine '
                            var luftq_beschreibung = obj.breezometer_description
                            var luft_index = obj.breezometer_aqi
							if (luftq_beschreibung == "Excellent air quality") {
								luftq_beschreibung_n = ("Ausgezeichnete Luftqualität")
							} else if (luftq_beschreibung == "Fair air quality") {
								luftq_beschreibung_n = ("Gute Luftqualität")
							} else if (luftq_beschreibung == "Moderate air quality") {
								luftq_beschreibung_n = ("Mäßige Luftqualität")	
							} else if (luftq_beschreibung == "Low air quality") {
								luftq_beschreibung_n = ("Niedrige Luftqualität")
							} else if (luftq_beschreibung == "Poor air quality") {
								luftq_beschreibung_n = ("Schlechte Luftqualität")
							} else if (luftq_beschreibung == "Good air quality") {
								luftq_beschreibung_n = ("Gute Luftqualität")
							}
							// Vergabe für den Text nach wert der Luft
							if (luft_index >= 90) {
								luftq_index_text = ("Diese Luftqualität eignet sich hervorragend für körperliche Aktivitäten im Freien")
							} else if (luft_index <= 89) {
								luftq_index_text = ("Öffne die Fenster, lass die frische Luft hereinkommen und geh nach draußen")
							} else if (luft_index <= 79) {
								luftq_index_text = ("Die Menge an Schadstoffen in der Luft ist spürbar, aber es gibt keine Gefahr für die Gesundheit")	
							} else if (luft_index <= 70) {
								luftq_index_text = ("Die Menge an Schadstoffen in der Luft ist spürbar, aber es gibt keine Gefahr für Ihre Gesundheit - Es wird empfohlen die kommenden Stunden zu beobachten")
							} else if (luft_index <= 60) {
								luftq_index_text = ("Du musst nicht drinnen bleiben, aber es wird empfohlen, dass du die Luftqualität verfolgst")
							} else if (luft_index <= 40) {
								luftq_index_text = ("Es ist eine gute Idee, in geschlossenen und klimatisierten Räumen zu bleiben")
							} else if (luft_index <= 20) {
								luftq_index_text = ("Halte die Fenster geschlossen. Bleibe im Haus, denn du willst die Luft draußen nicht einatmen")	
							}
							
							text += luftq_beschreibung_n + ' gemessen. Das entspricht einem Luft Qualitätsindex von <say-as interpret-as="number">' + luft_index + '</say-as>. ' + luftq_index_text + '. '
							// Nach abfrage gehts weiter oder mit nein zum beenden
							var nextText = 'Die Gemessenen Schadstoffe sind: '
                            for (var prop in obj.pollutants) {
                                if (prop == "co") {
									nextText += 'Kolenmonoxid <say-as interpret-as="number">' + obj.pollutants[prop]["concentration"] + '</say-as> ' + obj.pollutants[prop]["units"] + '. ' 
								} else if (prop == "no2") {
									nextText += 'Stickstoffdioxid <say-as interpret-as="number">' + obj.pollutants[prop]["concentration"] + '</say-as> ' + obj.pollutants[prop]["units"] + '. '
								} else if (prop == "o3") {
									nextText += 'Ozon <say-as interpret-as="number">' + obj.pollutants[prop]["concentration"] + '</say-as> ' + obj.pollutants[prop]["units"] + '. '
								} else if (prop == "pm10") {
									nextText += 'Feinstaub <say-as interpret-as="number">' + obj.pollutants[prop]["concentration"] + '</say-as><say-as interpret-as="unit">' + obj.pollutants[prop]["units"] + '</say-as>. '	
								} else if (prop == "pm25") {
									nextText += 'Inhalierbare Partikel <say-as interpret-as="number">' + obj.pollutants[prop]["concentration"] + '</say-as><say-as interpret-as="unit">' + obj.pollutants[prop]["units"] + '</say-as>. '
								} else if (prop == "so2") {
									nextText += 'Schwefeldioxid <say-as interpret-as="number">' + obj.pollutants[prop]["concentration"] + '</say-as> ' + obj.pollutants[prop]["units"] + '. '	
								}
                            }
							if (obj.dominant_pollutant_description == "Fine particulate matter (<2.5\u00b5m)") {
								dom_schadstoff_bezeichnung = ("Feinstaub")
								luftq_beachten = ("Kleinstpartikel können über die Lunge aufgenommen werden. Dort verursachen sie lokale Entzündungen in den Atmungsorganen und dem Herz, so dass Herz-Kreislauf-und Atemwegserkrankungen wie Asthma und Bronchitis ausgelöst werden können.")
							} else if (obj.dominant_pollutant_description == "Inhalable particulate matter (<10\u00b5m)") {
								dom_schadstoff_bezeichnung = ("Inhalierbare Partikel")
								luftq_beachten = ("Staubpartikel können über die Lunge aufgenommen und in der Luftröhre den Bronchien und den Nasen-Rachenraum abgelagert werden. Dort verursachen sie lokale Entzündungen in den Atmungsorganen sodas Atemwegserkrankungen wie Asthma und Bronchitis ausgelöst werden können.")
							} else if (obj.dominant_pollutant_description == "Carbon monoxide") {
								dom_schadstoff_bezeichnung = ("Kohlenmonoxid")
								luftq_beachten = ("Längere Belastungen durch Kohlenmonoxid in geringeren Konzentrationen betreffen überwiegend die Funktionen des Zentralnervensystems und des Herz-Kreislauf-Systems. Daher sind neben Kindern, Schwangeren und älteren Menschen in erster Linie Herzkranke mit Verengung der Herzkranzgefäße betroffen.")
							} else if (obj.dominant_pollutant_description == "Nitrogen dioxide") {
								dom_schadstoff_bezeichnung = ("Stickstoffdioxid")
								luftq_beachten = ("Stickoxide reizen und schädigen die Atmungsorgane. Erhöhte Konzentrationen in der Atemluft haben einen negativen Effekt auf die Lungenfunktion von Kindern und Erwachsenen. Besonders bei Patienten mit Asthma und mit COPD ist mit einer einschränkung der Lungenfunktion und ein erhöhtes Risiko für Atemwegserkrankungen zurechnen.")
							} else if (obj.dominant_pollutant_description == "Ozone") {
								dom_schadstoff_bezeichnung = ("Ozon")
								luftq_beachten = ("Ozon kann die Atemwege reizen, was zu Husten, heiserkeit, keuchen und kurzatmigkeit führt. Kinder und Erwachsene mit Atemwegserkrankungen, Lungen- und Herzkrankheiten sowie ältere Menschen und Personen die im Freien trainieren, sind besonders anfällig für zu hohe Ozonwerte.")
							} else if (obj.dominant_pollutant_description == "Sulfur dioxide") {
								dom_schadstoff_bezeichnung = ("Schwefeldioxid")
								luftq_beachten = ("Schwefeldioxid reizt Schleimhäute und die Atemwege dabei kann es zu Gewebsveränderungen im oberen Atemtrakt, einer Zunahme des Atemwiderstands und einer höheren Infektanfälligkeit führen.")
							}	
                            nextText += 'Der Schadstoff mit dem höchsten wert ist ' + dom_schadstoff_bezeichnung
                            nextText += '. Die Auswirkungen sind: ' + luftq_beachten
//                          nextText += '. Die Ursachen sind: ' + obj.dominant_pollutant_text.causes
							session.attributes.nextText = nextText

                            callback(null, text)
                        }
                    }).on('error', function (e) {
                        console.log("Error: " + e.message);
                        callback(new Error(e.message));
                    })
                    
                });
            }
            
        }).on('error', function (e) {
            console.log("Error: " + e.message);
            callback(new Error(e.message));
        })
    })
}

// verbindung zu google maps api parameter
function getMapsParameters(){
    var params = config.data_format
    params += '?address=' + encodeURIComponent(config.givenLocation)
    params += '&key=' + config.google_key
	params += '&sensor=false&language=de'
	return params
}

// verbinde alle parameter bevor aufbau zum endpunkt
function getAQIParameters() {
  var params = '?'
  params += 'lat=' + encodeURIComponent(config.latitude)
  params += '&lon=' + encodeURIComponent(config.longitude)
  params += '&fields=' + config.field_format
  params += '&key=' + config.aqi_key
  return params;
}

// erstelle den handler, der auf den AlexaRequest reagiert.
exports.handler = function (event, context) {
    var skill = new LuftQualitaetSkill();
    skill.execute(event, context);
};
