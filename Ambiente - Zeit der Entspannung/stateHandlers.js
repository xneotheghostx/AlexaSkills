'use strict';

var Alexa = require('alexa-sdk');
var audioDataDE = require('./audioAssetsDE');
var audioDataEN = require('./audioAssetsEN');
var constants = require('./constants');

var stateHandlers = {
    startModeIntentHandlers: Alexa.CreateStateHandler(constants.states.START_MODE, {
        /*
             *  All Intent Handlers for state : START_MODE
             */
        'LaunchRequest': function () {
            // Initialize Attributes
            /*this.attributes['playOrder'] = Array.apply(null, {length: audioData.length}).map(Number.call, Number);
            this.attributes['index'] = 0;
            this.attributes['offsetInMilliseconds'] = 0;
            this.attributes['loop'] = true;
            this.attributes['shuffle'] = false;
            this.attributes['playbackIndexChanged'] = true;*/
            //  Change state to START_MODE
            this.handler.state = constants.states.START_MODE;
            if (this.event.request.locale == "de-DE") {

                var message = 'Willkommen zu deiner "Zeit der Entspannung". Sage "Ambiente" oder einen Titel deiner Wahl, um zu starten.';
                var reprompt = 'Hier ist eine kleine Auswahl für dich: '
                    + buildRandomIndex(audioDataDE, 5) + " Du findest alle verfügbaren Titel in deiner Alexa-App. "
                    + "Sage 'Ambiente' oder einen Titel deiner Wahl, um zu starten.";
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN"

                var message = 'Welcome to "relaxing time". Say "Ambience" or a title of your choice to start.';
                var reprompt = 'Here is a small selection for you: '
                    + buildRandomIndex(audioDataEN, 5) + " You can find all available titles in your Alexa app. "
                    + 'Say "Ambience" or a title of your choice to start.';
            }

            this.response.speak(message).listen(reprompt);

            if (this.event.request.locale == "de-DE") {
                var cardContent = buildIndexCard(audioDataDE);
                this.response.cardRenderer("Zeit der Entspannung", cardContent);
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                var cardContent = buildIndexCard(audioDataEN);
                this.response.cardRenderer("relaxing time", cardContent);
            }

            this.emit(':responseReady');
        },
        'PlayAudio': function () {
            if (!this.attributes['playOrder']) {
                // Initialize Attributes if undefined.
                if (this.event.request.locale == "de-DE") {
                    this.attributes['playOrder'] = Array.apply(null, { length: audioDataDE.length }).map(Number.call, Number);
                } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                    this.attributes['playOrder'] = Array.apply(null, { length: audioDataEN.length }).map(Number.call, Number);
                }
                this.attributes['index'] = 0;
                this.attributes['offsetInMilliseconds'] = 0;
                this.attributes['loop'] = true;
                this.attributes['shuffle'] = false;
                this.attributes['playbackIndexChanged'] = true;
                //  Change state to START_MODE
                this.handler.state = constants.states.START_MODE;
            }
            controller.play.call(this);
        },
        'AMAZON.HelpIntent': function () {
            if (this.event.request.locale == "de-DE") {
                var message = 'Mit zum beispiel „Alexa, Stopp in 60 Minuten“ kannst du das Abspielen automatisch nach 60 Minuten beenden. ' +
                    'Mit „Alexa, Pause“ pausierst du deinen Titel. Mit "Alexa, fortsetzen" spielst du ihn weiter ab und mit „Alexa, beenden“ beendest du ihn. ' +
                    'Du kannst in der Playliste mit "Alexa, weiter" oder "Alexa, zurück" die Titel wechseln. ' +
                    'Mit "Alexa, Endlosschleife an oder aus" kannst du eine Endlosschleife ein oder ausschalten. Ich wünsche dir eine Zeit der Entspannung. ' +
                    'Sage "Ambiente" oder einen Titel deiner Wahl, um zu starten.';
                var reprompt = 'Sage "Ambiente" oder einen Titel deiner Wahl, um zu starten.';
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA"  
                var message = 'For example, with "Alexa, Stop in 60 Minutes" you can automatically stop playback after 60 minutes. ' +
                    'With "Alexa, Pause" you pause your title. With "Alexa, continue" you continue playing and with "Alexa, Stop" you can stop the playback. ' +
                    'You can shift tracks in the playlist with "Alexa Next" or "Alexa Back". ' +
                    'With "Alexa, Loop On" or "Alexa, Loop Off" you can turn an loop on or off. I wish you a relaxing time. ' +
                    'Say "Ambience" or a title of your choice to start.';
                var reprompt = 'Say "Ambience" or a title of your choice to start.';
            }
            this.response.speak(message).listen(reprompt);
            this.emit(':responseReady');
        },
        'AMAZON.StopIntent': function () {
            if (this.event.request.locale == "de-DE") {
                var message = 'Auf wiederhören.';
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                var message = 'Speak to you soon.';
            }
            this.response.speak(message);
            this.emit(':responseReady');
        },
        'AMAZON.CancelIntent': function () {
            if (this.event.request.locale == "de-DE") {
                var message = 'Auf wiederhören.';
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                var message = 'Speak to you soon.';
            }
            this.response.speak(message);
            this.emit(':responseReady');
        },
        'SessionEndedRequest': function () {
            // No session ended logic
        },
        'Unhandled': function () {
            if (this.event.request.locale == "de-DE") {
                var message = 'Entschuldigung, Ich habe dich nicht verstanden.';
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                var message = 'Sorry, I did not understand you.';
            }
            this.response.speak(message).listen(message);
            this.emit(':responseReady');
        }
    }),
    playModeIntentHandlers: Alexa.CreateStateHandler(constants.states.PLAY_MODE, {
        /*
             *  All Intent Handlers for state : PLAY_MODE
             */
        'LaunchRequest': function () {
            /*
                   *  Session resumed in PLAY_MODE STATE.
                   *  If playback had finished during last session :
                   *      Give welcome message.
                   *      Change state to START_STATE to restrict user inputs.
                   *  Else :
                   *      Ask user if he/she wants to resume from last position.
                   *      Change state to RESUME_DECISION_MODE
                   */
            var message;
            var reprompt;
            if (this.attributes['playbackFinished']) {
                this.handler.state = constants.states.START_MODE;

                if (this.event.request.locale == "de-DE") {
                    message = 'Entschuldigung, Ich habe dich nicht verstanden.';
                    reprompt = 'Du kannst sagen spiele Musik, um zu starten.';
                } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                    message = 'Sorry, I did not understand you.';
                    reprompt = 'You can say play music to start.';
                }
            } else {
                this.handler.state = constants.states.RESUME_DECISION_MODE;

                if (this.event.request.locale == "de-DE") {
                    message = 'Du hörst gerade ' + audioDataDE[this.attributes['playOrder'][this.attributes['index']]].title + ' möchtest du es weiter hören?';
                    reprompt = 'Du kannst ja sagen um es weiter zuhören oder nein um neu zu starten.';
                } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                    message = 'You are listening to ' + audioDataEN[this.attributes['playOrder'][this.attributes['index']]].title + '. Do you want to continue listening to it?';
                    reprompt = 'You can say yes to keep listening or no to restart.';
                }
            }

            this.response.speak(message).listen(reprompt);
            this.emit(':responseReady');
        },
        'PlayAudio': function () {
            controller.play.call(this)
        },
        'OtherAudio': function () {
            if (this.event.request.locale == "de-DE") {
                var message = 'Du hörst gerade ' + audioDataDE[this.attributes['playOrder'][this.attributes['index']]].title + '. Welchen Song möchtest du hören?';
                var reprompt = 'Du findest alle verfügbaren Titel in deiner Alexa-App.';
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                var message = 'You are listening to ' + audioDataEN[this.attributes['playOrder'][this.attributes['index']]].title + '. Which song do you want to hear?';
                var reprompt = 'You can find all available titles in your Alexa app.';
            }
            this.response.speak(message).listen(reprompt);
        },
        'AMAZON.NextIntent': function () {
            controller.playNext.call(this)
        },
        'AMAZON.PreviousIntent': function () {
            controller.playPrevious.call(this)
        },
        'AMAZON.PauseIntent': function () {
            controller.stop.call(this)
        },
        'AMAZON.StopIntent': function () {
            controller.stop.call(this)
        },
        'AMAZON.CancelIntent': function () {
            controller.stop.call(this)
        },
        'AMAZON.ResumeIntent': function () {
            controller.play.call(this)
        },
        'AMAZON.LoopOnIntent': function () {
            controller.loopOn.call(this)
        },
        'AMAZON.LoopOffIntent': function () {
            controller.loopOff.call(this)
        },
        'AMAZON.ShuffleOnIntent': function () {
            controller.shuffleOn.call(this)
        },
        'AMAZON.ShuffleOffIntent': function () {
            controller.shuffleOff.call(this)
        },
        'AMAZON.StartOverIntent': function () {
            controller.startOver.call(this)
        },
        'AMAZON.HelpIntent': function () {
            if (this.event.request.locale == "de-DE") {
                var message = 'Mit zum beispiel „Alexa, Stopp in 60 Minuten“ kannst du das Abspielen automatisch nach 60 Minuten beenden. ' +
                    'Mit „Alexa, Pause“ pausierst du deinen Titel. Mit "Alexa, fortsetzen" spielst du ihn weiter ab und mit „Alexa, beenden“ beendest du ihn. ' +
                    'Du kannst in der Playliste mit "Alexa, weiter" oder "Alexa, zurück" die Titel wechseln. ' +
                    'Mit "Alexa, Endlosschleife an oder aus" kannst du eine Endlosschleife ein oder ausschalten. Ich wünsche dir eine Zeit der Entspannung. ' +
                    'Sage "Ambiente" oder einen Titel deiner Wahl, um zu starten.';
                var reprompt = 'Sage "Ambiente" oder einen Titel deiner Wahl, um zu starten.';
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                var message = 'For example, with "Alexa, Stop in 60 Minutes" you can automatically stop playback after 60 minutes. ' +
                    'With "Alexa, Pause" you pause your title. With "Alexa, continue" you continue playing and with "Alexa, Stop" you can stop the playback. ' +
                    'You can shift tracks in the playlist with "Alexa Next" or "Alexa Back". ' +
                    'With "Alexa, Loop On" or "Alexa, Loop Off" you can turn an loop on or off. I wish you a relaxing time. ' +
                    'Say "Ambience" or a title of your choice to start.';
                var reprompt = 'Say "Ambience" or a title of your choice to start.';
            }
            this.response.speak(message).listen(reprompt);
            this.emit(':responseReady');
        },
        'SessionEndedRequest': function () {
            // No session ended logic
        },
        'Unhandled': function () {
            if (this.event.request.locale == "de-DE") {
                var message = 'Entschuldigung, Ich habe dich nicht verstanden. Du kannst weiter oder zurück sagen, um in der Playliste zu wechseln.';
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                var message = 'Sorry, I did not understand you. You can say next or back to shift in the playlist.';
            }
            this.response.speak(message).listen(message);
            this.emit(':responseReady');
        }
    }),
    remoteControllerHandlers: Alexa.CreateStateHandler(constants.states.PLAY_MODE, {
        /*
             *  All Requests are received using a Remote Control. Calling corresponding handlers for each of them.
             */
        'PlayCommandIssued': function () {
            controller.play.call(this)
        },
        'PauseCommandIssued': function () {
            controller.stop.call(this)
        },
        'NextCommandIssued': function () {
            controller.playNext.call(this)
        },
        'PreviousCommandIssued': function () {
            controller.playPrevious.call(this)
        }
    }),
    resumeDecisionModeIntentHandlers: Alexa.CreateStateHandler(constants.states.RESUME_DECISION_MODE, {
        /*
             *  All Intent Handlers for state : RESUME_DECISION_MODE
             */
        'LaunchRequest': function () {
            if (this.event.request.locale == "de-DE") {
                var message = 'Du hörst gerade ' + audioDataDE[this.attributes['playOrder'][this.attributes['index']]].title + ' möchtest du es weiter hören?';
                var reprompt = 'Du kannst ja sagen um es weiter zuhören oder nein um neu zu starten.';
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                var message = 'You are listening to ' + audioDataEN[this.attributes['playOrder'][this.attributes['index']]].title + '. Do you want to continue listening to it?';
                var reprompt = 'You can say yes to keep listening or no to restart.';
            }
            this.response.speak(message).listen(reprompt);
            this.emit(':responseReady');
        },
        'PlayAudio': function () {
            controller.play.call(this)
        },
        'AMAZON.YesIntent': function () {
            controller.play.call(this)
        },
        'AMAZON.NoIntent': function () {
            //controller.reset.call(this)
            this.handler.state = '';
            this.attributes['STATE'] = '';
            this.emitWithState('LaunchRequest');
        },
        'AMAZON.HelpIntent': function () {
            if (this.event.request.locale == "de-DE") {
                var message = 'Du hörst gerade ' + audioDataDE[this.attributes['playOrder'][this.attributes['index']]].title + ' möchtest du es weiter hören?';
                var reprompt = 'Du kannst ja sagen um es weiter zuhören oder nein um neu zu starten.';
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                var message = 'You are listening to ' + audioDataEN[this.attributes['playOrder'][this.attributes['index']]].title + '. Do you want to continue listening to it?';
                var reprompt = 'You can say yes to keep listening or no to restart.';
            }
            this.response.speak(message).listen(reprompt);
            this.emit(':responseReady');
        },
        'AMAZON.StopIntent': function () {
            if (this.event.request.locale == "de-DE") {
                var message = 'Auf wiederhören.';
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                var message = 'Speak to you soon.';
            }
            this.response.speak(message);
            this.emit(':responseReady');
        },
        'AMAZON.CancelIntent': function () {
            if (this.event.request.locale == "de-DE") {
                var message = 'Auf wiederhören.';
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                var message = 'Speak to you soon.';
            }
            this.response.speak(message);
            this.emit(':responseReady');
        },
        'SessionEndedRequest': function () {
            // No session ended logic
        },
        'Unhandled': function () {
            if (this.event.request.locale == "de-DE") {
                var message = 'Entschuldigung, das ist kein gültiger Befehl. Bitte sage Hilfe, um zu hören was du sagen kannst.';
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                var message = "Sorry, that's not a valid command. Please say help to hear what you can say.";
            }
            this.response.speak(message).listen(message);
            this.emit(':responseReady');
        }
    })
};

module.exports = stateHandlers;

var controller = function () {
    return {
        play: function () {
            /*
                   *  Using the function to begin playing audio when:
                   *      Play Audio intent invoked.
                   *      Resuming audio when stopped/paused.
                   *      Next/Previous commands issued.
                   */
            this.handler.state = constants.states.PLAY_MODE;

            if (this.attributes['playbackFinished']) {
                // Reset to top of the playlist when reached end.
                this.attributes['index'] = 0;
                this.attributes['offsetInMilliseconds'] = 0;
                this.attributes['playbackIndexChanged'] = true;
                this.attributes['playbackFinished'] = false;
            }

            var token;
            var playBehavior = 'REPLACE_ALL';
            var podcast = null;

            if (typeof this.event.request.intent !== 'undefined')
                var slots = this.event.request.intent.slots;

            if (slots && slots.Sound && slots.Sound.value) {
                var soundName = slots.Sound.value

                if (this.event.request.locale == "de-DE") {
                    podcast = audioDataDE.find(function (data, idx) {
                        token = idx
                        return data.title.toLowerCase() === soundName.toLowerCase()
                    });
                } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                    podcast = audioDataEN.find(function (data, idx) {
                        token = idx
                        return data.title.toLowerCase() === soundName.toLowerCase()
                    });
                }
            }

            if (!podcast) {
                if (this.event.request.locale == "de-DE") {
                    podcast = audioDataDE[this.attributes['playOrder'][this.attributes['index']]];
                } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                    podcast = audioDataEN[this.attributes['playOrder'][this.attributes['index']]];
                }
                token = String(this.attributes['playOrder'][this.attributes['index']]);
            }

            var offsetInMilliseconds = this.attributes['offsetInMilliseconds'];
            // Since play behavior is REPLACE_ALL, enqueuedToken attribute need to be set to null.
            this.attributes['enqueuedToken'] = null;

            if (canThrowCard.call(this)) {
                if (this.event.request.locale == "de-DE") {
                    var cardTitle = 'Spiele ' + podcast.title;
                    var cardContent = 'Spiele ' + podcast.title;
                } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                    var cardTitle = 'Playing ' + podcast.title;
                    var cardContent = 'Playing ' + podcast.title;
                }
                this.response.cardRenderer(cardTitle, cardContent, null);
            }

            this.response.audioPlayerPlay(playBehavior, podcast.url, token, null, offsetInMilliseconds);
            this.emit(':responseReady');
        },
        stop: function () {
            /*
                   *  Issuing AudioPlayer.Stop directive to stop the audio.
                   *  Attributes already stored when AudioPlayer.Stopped request received.
                   */
            this.response.audioPlayerStop();
            this.emit(':responseReady');
        },
        playNext: function () {
            /*
                   *  Called when AMAZON.NextIntent or PlaybackController.NextCommandIssued is invoked.
                   *  Index is computed using token stored when AudioPlayer.PlaybackStopped command is received.
                   *  If reached at the end of the playlist, choose behavior based on "loop" flag.
                   */
            var index = this.attributes['index'];
            index += 1;
            // Check for last audio file.
            if (index === (this.event.request.locale == "de-DE" ? audioDataDE.length : audioDataEN.length)) {
                if (this.attributes['loop']) {
                    index = 0;
                } else {
                    // Reached at the end. Thus reset state to start mode and stop playing.
                    this.handler.state = constants.states.START_MODE;

                    if (this.event.request.locale == "de-DE") {
                        var message = 'Du hast das Ende der Playliste erreicht.';
                    } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                        var message = "You've reached the end of the playlist.";
                    }
                    this.response.speak(message).audioPlayerStop();
                    return this.emit(':responseReady');
                }
            }
            // Set values to attributes.
            this.attributes['index'] = index;
            this.attributes['offsetInMilliseconds'] = 0;
            this.attributes['playbackIndexChanged'] = true;

            controller.play.call(this);
        },
        playPrevious: function () {
            /*
                   *  Called when AMAZON.PreviousIntent or PlaybackController.PreviousCommandIssued is invoked.
                   *  Index is computed using token stored when AudioPlayer.PlaybackStopped command is received.
                   *  If reached at the end of the playlist, choose behavior based on "loop" flag.
                   */
            var index = this.attributes['index'];
            index -= 1;
            // Check for last audio file.
            if (index === -1) {
                if (this.attributes['loop']) {
                    if (this.event.request.locale == "de-DE") {
                        index = audioDataDE.length - 1;
                    } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                        index = audioDataEN.length - 1;
                    }
                } else {
                    // Reached at the end. Thus reset state to start mode and stop playing.
                    this.handler.state = constants.states.START_MODE;

                    if (this.event.request.locale == "de-DE") {
                        var message = 'Du hast den Anfang der Playliste erreicht.';
                    } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                        var message = "You've reached the top of the playlist.";
                    }
                    this.response.speak(message).audioPlayerStop();
                    return this.emit(':responseReady');
                }
            }
            // Set values to attributes.
            this.attributes['index'] = index;
            this.attributes['offsetInMilliseconds'] = 0;
            this.attributes['playbackIndexChanged'] = true;

            controller.play.call(this);
        },
        loopOn: function () {
            // Turn on loop play.
            this.attributes['loop'] = true;
            if (this.event.request.locale == "de-DE") {
                var message = 'Endlosschleife eingeschaltet.';
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                var message = 'Loop switched on.';
            }
            this.response.speak(message);
            this.emit(':responseReady');
        },
        loopOff: function () {
            // Turn off looping
            this.attributes['loop'] = false;
            if (this.event.request.locale == "de-DE") {
                var message = 'Endlosschleife ausgeschaltet.';
            } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                var message = 'Loop switched off.';
            }
            this.response.speak(message);
            this.emit(':responseReady');
        },
        shuffleOn: function () {
            // Turn on shuffle play.
            this.attributes['shuffle'] = true;
            shuffleOrder((newOrder) => {
                // Play order have been shuffled. Re-initializing indices and playing first song in shuffled order.
                this.attributes['playOrder'] = newOrder;
                this.attributes['index'] = 0;
                this.attributes['offsetInMilliseconds'] = 0;
                this.attributes['playbackIndexChanged'] = true;
                controller.play.call(this);
            }, this.event.request.locale);
        },
        shuffleOff: function () {
            // Turn off shuffle play.
            if (this.attributes['shuffle']) {
                this.attributes['shuffle'] = false;
                // Although changing index, no change in audio file being played as the change is to account for reordering playOrder
                this.attributes['index'] = this.attributes['playOrder'][this.attributes['index']];
                if (this.event.request.locale == "de-DE") {
                    this.attributes['playOrder'] = Array.apply(null, { length: audioDataDE.length }).map(Number.call, Number);
                } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
                    this.attributes['playOrder'] = Array.apply(null, { length: audioDataEN.length }).map(Number.call, Number);
                }
            }
            controller.play.call(this);
        },
        startOver: function () {
            // Start over the current audio file.
            this.attributes['offsetInMilliseconds'] = 0;
            controller.play.call(this);
        },
        reset: function () {
            // Reset to top of the playlist.
            this.attributes['index'] = 0;
            this.attributes['offsetInMilliseconds'] = 0;
            this.attributes['playbackIndexChanged'] = true;
            controller.play.call(this);
        }
    }
}();

function canThrowCard() {
    /*
       * To determine when can a card should be inserted in the response.
       * In response to a PlaybackController Request (remote control events) we cannot issue a card,
       * Thus adding restriction of request type being "IntentRequest".
       */
    if (this.event.request.type === 'IntentRequest' && this.attributes['playbackIndexChanged']) {
        this.attributes['playbackIndexChanged'] = false;
        return true;
    } else {
        return false;
    }
}

function shuffleOrder(callback, locale) {
    // Algorithm : Fisher-Yates shuffle
    if (locale == "de-DE") {
        var array = Array.apply(null, { length: audioDataDE.length }).map(Number.call, Number);
    } else { // this.event.request.locale == "en-US" || "en-GB" || "en-IN" || "en-CA" 
        var array = Array.apply(null, { length: audioDataEN.length }).map(Number.call, Number);
    }
    var currentIndex = array.length;
    var temp,
        randomIndex;

    while (currentIndex >= 1) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temp = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temp;
    }
    callback(array);
}

function buildRandomIndex(data, count) {

    var arr = []
    while (arr.length < count) {
        var randomnumber = Math.ceil(Math.random() * data.length - 1)
        if (arr.indexOf(randomnumber) > -1) continue;
        arr[arr.length] = randomnumber;
    }
    console.log("arr" + arr);
    var speech = "";
    for (var i = 0; i < arr.length; i++) {
        speech += data[arr[i]].title + ", <break time='1s' />";
    }

    return speech;
}

function buildIndexCard(data) {

    var content = "";
    for (var i = 0; i < data.length; i++) {
        content += data[i].title + "\n";
    }

    return content;
}