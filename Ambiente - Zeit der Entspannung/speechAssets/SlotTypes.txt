LIST_OF_SOUNDS

de_DE
Ambiente
Ave Maria
Ein Lied für Mama
Ein neuer Anfang
Ein langer Tag
Folge deinem Herzen
Geh mit mir
Gemeinsam mit dir
Glückliche Zeiten
Graswiese
Im Hintergrund
Klareswasser
Lass dich fallen
Milchstrasse
Öffne deine Augen
Romanze am Klavier
Schlaf schön
Smaragdfluss
Stimmungsvoll
Traumland
Warme Herzen
Windige Tage
Zuhause

en_US
Ambiance
Ave Maria
A song for Mama
A new beginning
A long day
Follow your heart
Walk with me
Together with you
Happy times
Grass meadow
In the background
Clear water
Let it go
Milky Way
Open your eyes
Romance at the piano
Sleep well
Emerald river
Atmospheric
Dreamland
Warm heart
Windy days
At home