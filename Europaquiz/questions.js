﻿'use strict';

module.exports = {
    /**
     * Auf ? und Zeichen achten.
     * Erste Frage ist immer die Richtige.
     */
    "QUESTIONS_DE_DE" : [
		{
			"<break time='50ms'/><s>Thema Bevölkerung</s> Wie viele Mitglieder hatte die Europäische Union EU Anfang 2016 ?": [
				"28",
				"25",
				"47",
				"5"
			]
        },
        {
            "<break time='50ms'/><s>Thema Bevölkerung</s> Wie viele Menschen leben ungefähr seit 2016 in der EU ?": [
                "493 Millionen",
                "450 Millionen",
                "550 Millionen",
                "800 Millionen"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bevölkerung</s> In der Europäischen Union leben mehr Menschen als ?": [
                "in den USA",
                "in China",
                "in Indien",
                "in Cuba"
            ]
        },
		{
            "<break time='50ms'/><s>Thema Bevölkerung</s> <s>In nur einer Weltregion wird sich die Bevölkerungszahl bis 2060 reduzieren</s> In welcher ?": [
				"Europa",
				"Asien",
				"Nordamerika",
				"Afrika"
			]
		},
		{
            "<break time='50ms'/><s>Thema Bevölkerung</s> Welche Aussage ist bezogen auf die Fläche der Staaten richtig ?": [
				"Frankreich ist das größte Land der EU.",
				"Deutschland ist das größte Land der EU.",
				"Belgien ist größer als Finnland.",
				"Spanien ist kleiner als Zypern."
			]
		},
		{
			"<break time='50ms'/><s>Thema Bevölkerung</s> <s>Im Jahr neunzehnhundertfünfzieg waren in Europa rund 8000 Personen 100 Jahre oder älter</s> Wie viele werden es im Jahr 2050 sein ?": [
				"mehr als 750.000",
				"rund 110.000",
				"16.000",
				"4.000"
			]
		},
		{
			"<break time='50ms'/><s>Thema Bevölkerung</s> <s>In der EU werden jährlich mehr als 2 Millionen Ehen geschlossen</s> Wie viele werden in etwa jährlich geschieden ?": [
				"1.000.000",
				"1.000.000",
				"100.000",
				"10.000"
			]
		},
        {
            "<break time='50ms'/><s>Thema Bevölkerung</s> Von den weltweit 214 Millionen Migranten lebten mit 70 Millionen die meisten in ? ": [
                "Europa",
                "Asien",
                "Nordamerika",
                "Afrika"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bevölkerung</s> Wie viele Personen erwarben im Jahr 2009 die Staatsbürgerschaft eines EU-Staates ?": [
                "776.000",
                "776",
                "7.776",
                "77.600"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bevölkerung</s> <s>Im Jahr 2011 stellten gut 300000 Personen einen Asylantrag in der EU</s> Die meisten davon stammten aus ?": [
                "Afghanistan",
                "den USA",
                "Russland",
                "dem Irak"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bevölkerung</s> In keiner Region der Welt sind im Zeitraum 2000 bis 2015 so viele Menschen eingewandert wie nach ?": [
                "Europa",
                "Afrika",
                "Asien",
                "Nordamerika"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bevölkerung</s> Die meisten Einwohner der Europäischen Union sind ?": [
                "Katholiken",
                "Protestanten",
                "Muslime",
                "Hindus"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Wirtschaft und Finanzen</s> <break time='50ms'/> Welche Aussage ist falsch ?": [
                "Das BIP Afrikas war im Jahr 2009 höher als das Spaniens.",
                "Das BIP Großbritanniens war im Jahr 2009 höher als das Russlands.",
                "Das BIP Frankreichs war im Jahr 2009 höher als das des Nahen und Mittleren Ostens.",
                "Das BIP Deutschlands war im Jahr 2009 höher als das Südamerikas."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Wirtschaft und Finanzen</s> <break time='50ms'/> In welchem Staat Europas ist die Wirtschaftskraft pro Kopf am höchsten ?": [
                "Luxemburg",
                "Deutschland",
                "Rumänien",
                "Schweden"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Wirtschaft und Finanzen</s> <break time='50ms'/> Wie hoch ist der Anteil des Warenhandels, den die EU innerhalb der eigenen Grenzen abwickelt ?": [
                "knapp zwei Drittel",
                "knapp die Hälfte",
                "knapp ein Viertel",
                "knapp ein Zehntel"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Wirtschaft und Finanzen</s> <break time='50ms'/>  Die beiden wichtigsten Handelspartner der EU sind ?": [
                "die USA und China",
                "Norwegen und die Schweiz",
                "Angola und Kenia",
                "Russland und Saudi-Arabien"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Wirtschaft und Finanzen</s> <break time='50ms'/> Europaweit hat sich der durchschnittliche Unternehmenssteuersatz seit Mitte der neunzehnhundertneunzieger Jahre ?": [
                "reduziert",
                "verdoppelt",
                "erhöht",
                "knapp verdreifacht"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Wirtschaft und Finanzen</s> <break time='50ms'/> Zu den wichtigsten Einnahmequellen der öffentlichen Haushalte gehören ?": [
                "Steuern und Sozialabgaben",
                "Gebühren",
                "Bußgelder",
                "Spenden"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Wirtschaft und Finanzen</s> <break time='50ms'/> In welchem EU-Mitgliedstaat war bezogen auf das BIP der öffentliche Schuldenstand im Jahr 2015 am höchsten ?": [
                "Griechenland",
                "Italien",
                "Luxemburg",
                "Deutschland"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Wirtschaft und Finanzen</s> <break time='50ms'/> Welche Aussage ist falsch ?": [
                "Auch in den Jahren 2010 bis 2015 ist das jährliche öffentliche Defizit der EU-28 nicht gesunken.",
                "Zwischen 2003 und 2007 verringerte sich das jährliche öffentliche Defizit der EU-28 viermal in Folge.",
                "Die absoluten Defizite der EU-28 in den Jahren 2009 und 2010 waren die höchsten in der Geschichte der EU.",
                "Im Jahr 2009 hatte kein einziges Land einen positiven Finanzierungssaldo."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Wirtschaft und Finanzen</s> <break time='50ms'/> Bezogen auf das Bruttonationaleinkommen haben sich die Haushaltseinnahmen der EU zwischen 2000 und 2010 ?": [
                "kaum verändert.",
                "mehr als verdoppelt.",
                "etwa halbiert.",
                "sehr stark erhöht."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Wirtschaft und Finanzen</s> <break time='50ms'/> Welche Aussage ist falsch ?": [
                "Bezogen auf die absoluten Zahlen war Griechenland im Jahr 2015 erneut der größte Nettoempfänger der EU.",
                "Bezogen auf die absoluten Zahlen war Deutschland im Jahr 2015 erneut der größte Nettozahler der EU.",
                "Bezogen auf den jeweiligen Anteil am Bruttoinlandsprodukt (BIP) waren im Jahr 2015 die Niederlande der größte Nettozahler der EU.",
                "Pro Kopf zahlte 2015 niemand so viel an die EU wie die Bürgerinnen und Bürger Schwedens. "
            ]
        },
        {
            "<break time='50ms'/><s>Thema Erwerbstätigkeit und Arbeitslosigkeit</s> Auf EU-Ebene wurde das Ziel vereinbart, eine Beschäftigungsquote von ?": [
                "70 Prozent zu erreichen.",
                "100 Prozent zu erreichen.",
                "200 Prozent zu erreichen.",
                "50 Prozent zu erreichen." 
            ]
        },
        {
            "<break time='50ms'/><s>Thema Erwerbstätigkeit und Arbeitslosigkeit</s>Die Aussage <s>Je höher der Bildungsstand, desto höher die Erwerbstätigenquote</s> gilt ?": [
                "für die EU und alle Mitgliedstaaten, für Männer und für Frauen.",
                "nicht für die EU.",
                "nicht für alle Mitgliedstaaten der EU.",
                "nur für Männer, aber nicht für Frauen."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Erwerbstätigkeit und Arbeitslosigkeit</s> In welchem dieser Staaten war der Unterschied zwischen der Erwerbstätigenquote der Männer und der der Frauen am größten ?": [
                "Türkei ",
                "Deutschland",
                "Italien",
                "Griechenland"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Erwerbstätigkeit und Arbeitslosigkeit</s> In der EU gilt, dass sich die Erwerbstätigenquote der Frauen mit steigender Kinderzahl ?": [
                "verringert.",
                "verdoppelt.",
                "knapp verdreifacht.",
                "erhöht."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Erwerbstätigkeit und Arbeitslosigkeit</s> Die Aussage <s>Frauen gehen häufiger einer Teilzeitbeschäftigung nach als Männer</s> gilt ?": [
                "in allen EU-Staaten.",
                "nicht in allen EU-Staaten.",
                "nicht in der EU.",
                "nicht in Deutschland."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Erwerbstätigkeit und Arbeitslosigkeit</s> Welche EU-Staaten hatten im Jahr 2015 die höchsten Arbeitslosenquoten ?": [
                "Griechenland und Spanien",
                "Luxemburg und Deutschland",
                "Österreich und die Niederlande",
                "Lettland und Litauen"
            ]
        },
        
        {
            "<break time='50ms'/><s>Thema Erwerbstätigkeit und Arbeitslosigkeit</s> Die globale Finanz und Wirtschaftskrise ließ die Arbeitslosigkeit zwischen 2008 und 2009 ?": [
                "in 27 EU-Staaten steigen.",
                "in 22 EU-Staaten steigen.",
                "in 17 EU-Staaten steigen.",
                "in 12 EU-Staaten steigen."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Erwerbstätigkeit und Arbeitslosigkeit</s> Nach einer Erhebung im Jahr 2010 waren in der EU ?": [
                "40 Prozent der Arbeitslosen ein Jahr oder länger arbeitslos.",
                "20 Prozent der Arbeitslosen ein Jahr oder länger arbeitslos.",
                "0 Prozent der Arbeitslosen ein Jahr oder länger arbeitslos.",
                "10 Prozent der Arbeitslosen ein Jahr oder länger arbeitslos."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Erwerbstätigkeit und Arbeitslosigkeit</s> Welche Aussage gilt nicht für die EU ?": [
                "Der Bildungsstand hat keinen Einfluss auf die Arbeitslosenquote.",
                "Der Bildungsstand hat großen Einfluss auf die Arbeitslosenquote.",
                "Je höher der Bildungsstand, desto niedriger die Arbeitslosenquote.",
                "Je niedriger der Bildungsstand, desto höher die Arbeitslosenquote."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Erwerbstätigkeit und Arbeitslosigkeit</s> In welchem dieser Staaten war die Arbeitslosigkeit der Männer niedriger als die der Frauen ?": [
                "Griechenland",
                "Großbritannien",
                "Irland",
                "Deutschland"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Armut, Einkommen und sozialer Schutz</s> Wie viele Personen waren in der EU im Jahr 2008 armutsgefährdet ?": [
                "16 Prozent",
                "Es gibt keine Armut in der EU.",
                "8 Prozent",
                "4 Prozent"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Armut, Einkommen und sozialer Schutz</s> Welche Aussage ist falsch ?": [
                "Kinder verringern das Armutsrisiko.",
                "Je höher der Bildungsgrad, desto niedriger das Armutsrisiko.",
                "Erwerbslose sind eher von Armut betroffen als Erwerbstätige.",
                "Frauen sind eher von Armut betroffen als Männer."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Armut, Einkommen und sozialer Schutz</s> In welchem dieser Länder war das Durchschnittseinkommen im Jahr 2008 am höchsten ?": [
                "Luxemburg",
                "Deutschland",
                "Schweiz",
                "Slowenien"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Armut, Einkommen und sozialer Schutz</s> Am größten war die Einkommensungleichheit 2008 in ?": [
                "Lettland und Rumänien",
                "Polen und Portugal",
                "Deutschland und Österreich",
                "Schweden und Finnland"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Armut, Einkommen und sozialer Schutz</s> Mehr als zwei Drittel der Sozialleistungsausgaben der EU entfallen auf die Bereiche ?": [
                "Alter und Gesundheit",
                "Familie und Arbeitslosigkeit",
                "Invalidität und Hinterbliebene",
                "Wohnung und soziale Ausgrenzung"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bildung, Forschung und Entwicklung</s> Im Verhältnis zum Bruttoinlandsprodukt haben sich die Bildungsausgaben der EU seit neunzehnhundertneunundneunzieg ?": [
                "nur leicht verändert.",
                "verdoppelt.",
                "nahezu verdreifacht.",
                "halbiert."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bildung, Forschung und Entwicklung</s> In welchen Staaten ist die Lesekompetenz nach den Ergebnissen der PISA-Studie 2009 am höchsten ?": [
                "Finnland und Niederlande",
                "Aserbaidschan und Albanien",
                "Rumänien und Bulgarien",
                "Dänemark und Deutschland"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bildung, Forschung und Entwicklung</s> Welche Aussage ist bezogen auf die PISA-Studie 2009 falsch ?": [
                "Im Bereich Mathematik erzielten die Mädchen in 23 der 36 europäischen Staaten eine höhere Punktzahl als die Jungen.",
                "Bei den Naturwissenschaften erzielten die Mädchen in 23 der 36 europäischen Staaten eine höhere Punktzahl als die Jungen.",
                "Im Bereich Mathematik lag Deutschland auf Rang 6 von 36 europäischen Staaten.",
                "Im Bereich Naturwissenschaften lag Deutschland auf Rang 4 von 36 europäischen Staaten."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bildung, Forschung und Entwicklung</s> Welche Fremdsprache lernen die Schüler EU-weit am häufigsten ?": [
                "Englisch",
                "Französisch",
                "Deutsch",
                "Spanisch"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bildung, Forschung und Entwicklung</s> Der Anteil der Personen, die nie das Internet nutzen, lag EU-weit im Jahr 2010 bei ?": [
                "rund einem Viertel.",
                "mehr als der Hälfte.",
                "weniger als 2 Prozent.",
                "rund 90 Prozent."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bildung, Forschung und Entwicklung</s> Am häufigsten nutzen die EU-Bürger das Internet ?": [
                "um E-Mails zu senden/empfangen.",
                "für die Lektüre/das Herunterladen von Online-Zeitungen und Nachrichtenmagazinen.",
                "für Bankgeschäfte.",
                "um mit Behörden zu interagieren."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bildung, Forschung und Entwicklung</s> Von den 1000 Unternehmen mit den höchsten Ausgaben für Forschung und Entwicklung, investierten die am meisten mit Sitz in ?": [
                "Deutschland",
                "Frankreich",
                "Zypern",
                "Ungarn"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bildung, Forschung und Entwicklung</s> Die 1000 Unternehmen mit Firmensitz in Europa, die am meisten für Forschung und Entwicklung ausgeben, investierten im Jahr 2010 am meisten in den Bereichen ?": [
                "Pharmaindustrie und Automobilbau",
                "Software und Telekommunikation",
                "Elektroteile und geräte ",
                "Medien und Freizeitwaren"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bildung, Forschung und Entwicklung</s> Welches Unternehmen gab 2010 am meisten für Forschung und Entwicklung aus ?": [
                "Roche für Pharma",
                "Volkswagen für Automobil",
                "EADS für Luft und Raumfahrt",
                "Nokia für Telekommunikation"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Bildung, Forschung und Entwicklung</s> Bei welchem Sektor war 2010 die Forschungsintensität am höchsten ?": [
                "Biotechnologie",
                "Software",
                "Nutzfahrzeuge",
                "Industriemaschinen"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Energie</s> Wo wird am meisten Energie pro Kopf verbraucht ?": [
                "Island",
                "Estland",
                "Deutschland",
                "Bulgarien"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Energie</s> Den größten Anteil an der Primärenergie-Versorgung der EU hat ?": [
                "Öl",
                "Gas",
                "Kernenergie",
                "Wasserkraft"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Energie</s> EU-weit lag der Anteil der erneuerbaren Energien an der Versorgung mit Primärenergie im Jahr 2015 bei ?": [
                "20 Prozent",
                "10 Prozent",
                "50 Prozent",
                "70 Prozent "
            ]
        },
        {
            "<break time='50ms'/><s>Thema Energie</s> Welche Staaten müssen relativ zu ihrem Verbrauch am meisten Energie importieren ?": [
                "Zypern und Malta",
                "Luxemburg und Deutschland",
                "Estland und Rumänien",
                "Tschechische Republik und Großbritannien"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Energie</s> Der wichtigste Energie-Zulieferer der EU ist ?": [
                "Russland",
                "Saudi-Arabien",
                "Norwegen",
                "Trinidad und Tobago"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Energie</s> Wieviel EU Bürger sprechen sich 2016 für einen gemeinsamen Energiemarkt aus ?": [
                "70 Prozent",
                "25 Prozent",
                "40 Prozent",
                "25 Prozent"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Bürger der EU</s> Als EU-Bürger fühlen sich laut Eurobarometer-Umfrage 2016 ?": [
                "66 Prozent",
                "55 Prozent",
                "30 Prozent",
                "75 Prozent"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Bürger der EU</s> An einen Gott glauben laut Eurobarometer Umfrage 2010 in der EU ?": [
                "52 Prozent der Bevölkerung.",
                "32 Prozent der Bevölkerung.",
                "22 Prozent der Bevölkerung.",
                "82 Prozent der Bevölkerung."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Bürger der EU</s> Welche Werte repräsentieren laut Eurobarometer-Umfrage 2010 am besten die EU ?": [
                "Menschenrechte und Demokratie",
                "Gleichheit und Solidarität",
                "Narzissmus und Egoismus",
                "Frieden und Religion"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Bürger der EU</s> Welche Aussage ist laut Eurobarometer-Umfrage 2015 falsch?": [
                "Die meisten EU-Bürger vertrauen der EU.",
                "Die meisten EU-Bürger vertrauen der EU nicht.",
                "In 3 EU Staaten <s>darunter Deutschland</s> vertrauen mehr als die Hälfte der Bürger der EU nicht.",
                "Bei den EU-Beitrittskandidaten überwiegt das Vertrauen in die EU."
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Bürger der EU</s> In wie vielen EU Staaten ist die Mehrheit der Bevölkerung der Meinung, dass die Globalisierung weltweit gültige Regeln erfordert ?": [
                "In allen",
                "25 von 28",
                "12 von 28",
                "In keinem"
            ]
        },
        {
            "<break time='50ms'/><s>Thema EU,  USA, China</s> Wo leben die meisten Menschen ?": [
                "China",
                "USA",
                "Deutschland",
                "EU"
            ]
        },
        {
            "<break time='50ms'/><s>Thema EU,  USA, China</s> Wo ist das Bruttoinlandsprodukt (BIP) am höchsten ?": [
                "EU",
                "USA",
                "China",
                "Deutschland"
            ]
        },
        {
            "<break time='50ms'/><s>Thema EU,  USA, China</s> Wo wurde 2009 am meisten Energie verbraucht ?": [
                "China",
                "USA",
                "EU",
                "Deutschland"
            ]
        },
        {
            "<break time='50ms'/><s>Thema EU,  USA, China</s> Wer war 2016 Exportweltmeister ?": [
                "Deutschland",
                "China",
                "EU",
                "USA"
            ]
        },
        {
            "<break time='50ms'/><s>Thema EU,  USA, China</s> Von den 1400 Unternehmen, die am meisten für Forschung und Entwicklung ausgeben, hatten im Jahr 2016 die meisten ihren Firmensitz ?": [
                "in den USA",
                "in China",
                "in den 28 Mitgliedstaaten der EU",
                "in Deutschland"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Gemischtes</s> Was bedeuten die zwölf Sterne auf der EU-Flagge ?": [
                "Sie stehen für Vollkommenheit, Vollständigkeit und Einheit",
                "Sie stellen die Gründungsmitglieder der Europäischen Union dar",
                "Sie stehen für zwölf Leitsätze der Europäischen Union",
                "Sie stehen für die 12 Länder der EU"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Gemischtes</s> Wie viele Abgeordnete sitzen 2017 im Europäischen Parlament ?": [
                "750",
                "685",
                "585",
                "850"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Gemischtes</s> Welcher dieser Staaten ist 2017 nicht Mitglied in der Europäischen Union ?": [
                "Norwegen",
                "Finnland",
                "Schweden",
                "Dänemark"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Gemischtes</s> Wer hat und hatte 2017 die EU Ratspräsidentschaft inne ?": [
                "Malta und Estland",
                "Niederlande und Slowakei",
                "Bulgarien und Österreich",
                "Luxemburg"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Gemischtes</s> Wann finden die nächsten Wahlen zum Europäischen Parlament statt ?": [
                "2019",
                "2018",
                "2020",
                "2025"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Gemischtes</s> Wie viel Geld hat die Europäische Union 2016 ausgegeben ?": [
                "144 Milliarden Euro",
                "120 Milliarden Euro",
                "135 Milliarden Euro",
                "95 Milliarden Euro"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Gemischtes</s> Wann wurde die Europäische Gemeinschaft zur EU ?": [
                "Neunzehnhundertzweiundneunzig",
                "Neunzehnhundertneunzig",
                "Neunzehnhundertsechzieg",
                "Neunzehnhundertfünfundachtzieg"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Gemischtes</s> Wie hieß der neunzenhundertzweiundneunzieg geschlossene Vertrag, als die Europäische Gemeinschaft zur EU wurde ?": [
                "Vertrag von Maastrich",
                "Schengener Abkommen",
                "Vertrag von Europa",
                "Vertrag von Helsich"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Gemischtes</s> In wie vielen Ländern der EU wird mit Euro gezahlt ?": [
                "19",
                "28",
                "12",
                "21"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Geschichte der EU</s> Wie hieß der französische Außenminister, der neunzehnhunderfünfzieg die Montanunion ins Leben rief ?": [
                "Robert Schuman",
                "Georges Bidault",
                "Maurice Schumann",
                "Edgar Faure"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Geschichte der EU</s> <s>Am Achzenten April neunzehnhundereinundfünfzieg wurde die Montanunion gegründet</s> Wie viele Staaten waren Gründungsmitglieder ?": [
                "6",
                "3",
                "10",
                "5"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Geschichte der EU</s> <s>Neunzehnhundersiebenundfünfzieg wurde die Zusammenarbeit auf die gesamte Wirtschaft ausgeweitet</s> In welcher Stadt wurden die Europäische Wirtschaftsgemeinschaft und die Europäische Atomgemeinschaft gegründet?": [
                "Rom",
                "Paris",
                "Brüssel",
                "Luxemburg"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Geschichte der EU</s> In welchem Jahr fand die sogenannte Westerweiterung statt, die erste Vergrößerung der Europäischen Gemeinschaft ?": [
                "Neunzehnhundertdreiundsiebzieg",
                "Neunzehnhundertsiebzig",
                "Neunzehnhundertneunundsechzieg",
                "Neunzehnhundertsechsundsiebzieg"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Geschichte der EU</s> In welchem Politikfeld wurde zuerst eine gemeinschaftliche Politik der Mitgliedsstaaten entwickelt ?": [
                "Agrarpolitik",
                "Verteidigungspolitik",
                "Wirtschaftspolitik",
                "Sozialpolitik"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Geschichte der EU</s> Wann trat der Maastrichter Vertrag in Kraft ?": [
                "Neunzehnhundertdreiundneunzig",
                "Neunzehnhundertneunzig",
                "Neunzehnhunderteinundneunzig",
                "Neunzehnhundertsechsundneunzig"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Geschichte der EU</s> Nach welcher Stadt sind die Kriterien benannt, die ein Land erfüllen muss, wenn es der EU beitreten will ?": [
                "Kopenhagen",
                "Brüssel",
                "Straßburg",
                "Lissabon"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Geschichte der EU</s> In welchem Jahr wurden Polen, Estland und Zypern in die EU aufgenommen ?": [
                "2004",
                "2001",
                "2005",
                "2007"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Geschichte der EU</s> Wie viele Mitgliedsstaaten umfasst die Europäische Union zurzeit ?": [
                "28",
                "27",
                "24",
                "19"
            ]
        },
        {
            "<break time='50ms'/><s>Thema Die Geschichte der EU</s> <s>Im Jahr 2005 scheiterte der Versuch einer Europäischen Verfassung, da sich bei Volksabstimmungen in zwei Ländern die Menschen dagegen aussprachen</s> Welche Länder waren das?": [
                "Frankreich und Niederlande",
                "Deutschland und Polen",
                "Irland und Großbritannien",
                "Spanien und Luxemburg"
            ]
        },
	],
	    	
};