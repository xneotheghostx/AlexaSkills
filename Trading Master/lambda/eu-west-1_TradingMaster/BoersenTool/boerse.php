<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" style="padding: 10px 10px;" href="#"><span><img src="favicon.png"></span><span style="margin-left: 10px;">Börsen-Tool</span></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
        <li class="active"><a href="?page=aktien">Aktien und Events</a></li>
        <li><a href="?page=users">Users</a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Utils <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="https://nordicskills.de/BoersenTool/ajax/updateAktien.php">Manuelles Update</a></li>
                <li><a href="https://nordicskills.de/BoersenTool/ajax/importIndex.php">Import Index</a></li>
                <!--<li><a data-toggle="modal" data-target="#newShow">Import Index</a></li>
        <li><a data-toggle="modal" data-target="#newMovie">Manuelles Update</a></li>-->
                <!--<li><a href="#">Something else here</a></li>
        <li role="separator" class="divider"></li>
        <li class="dropdown-header">Nav header</li>
        <li><a href="#">Separated link</a></li>
        <li><a href="#">One more separated link</a></li>-->
            </ul>
        </li>
        <li><a id="saveLink" href="#" onclick="saveChanges()">Speichern</a></li>
        </ul>
    </div><!--/.nav-collapse -->
    </div>
</nav>

<!--<div id="newMovie" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Neuen Film hinzufügen</h4>
      </div>
      <div class="modal-body">
        <form id="form-movies">
            <input type="text" id="movie-title" class="form-control" placeholder="Titel" name="movie-title" required autofocus>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="addMovieButton" data-dismiss="modal">Hinzufügen</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>

<div id="newShow" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Neue Serie hinzufügen</h4>
      </div>
      <div class="modal-body">
        <form id="form-shows">
            <input type="text" id="show-title" class="form-control" placeholder="Titel" name="show-title" required autofocus>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="addShowButton" data-dismiss="modal">Hinzufügen</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>-->

<div class="container theme-showcase" role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <!--<div class="jumbotron">
    <h1>Theme example</h1>
    <p>This is a template showcasing the optional theme stylesheet included in Bootstrap. Use it as a starting point to create something more unique by building on or modifying it.</p>
    </div>-->
	<style>
		.form-group {
			padding-left: 5px;
			padding-right: 5px;
		}
		.form-row .col-md-1 {
			padding-left: 5px;
			padding-right: 5px;
		}
		.form-row .col-md-2 {
			padding-left: 5px;
			padding-right: 5px;
		}
		.form-row .col-md-3 {
			padding-left: 5px;
			padding-right: 5px;
		}
		.form-row .col-md-7 {
			padding-left: 5px;
			padding-right: 5px;
		}
	</style>

    <h2>Events</h2>
    <div class="row thumbnail-sortable-movies" style="margin-left: 0px;">
        <div class="form-row">
            <div class="col-md-2">
                <label>Aktien-ID</label>
            </div>
            <div class="col-md-6">
                <label>Speech (SSML)</label>
            </div>
            <div class="col-md-2">
                <label>Start (Zeitpunkt)</label>
            </div>
            <div class="col-md-1">
                <label>Offset (€)</label>
            </div>
        </div>
        <form id="events"></form>
        <br><br>
        <button id="addEventButton" class="btn btn-primary" style="width: 3%; margin-left: 6px; padding: 6px;">+</button>
    </div>

    <h2>Aktien</h2>
    <div class="row thumbnail-sortable-shows" style="margin-left: 0px;">
	    <div class="form-row">
            <div class="col-md-2">
                <label>ID</label>
            </div>
            <div class="col-md-2">
                <label>Name</label>
            </div>
            <div class="col-md-2">
	            <label>Alias</label>
            </div>
            <div class="col-md-1">
	            <label>Kurs</label>
            </div>
            <div class="col-md-1">
                <label>Änderung</label>
            </div>
            <div class="col-md-1">
                <label>Volumen</label>
            </div>
            <div class="col-md-3">
                <label>Daten von</label>
            </div>
	    </div>
		<form id="aktien">
			
		</form>
		<br><br>
		<!--<button id="addAktieButton" class="btn btn-primary" style="width: 3%; margin-left: 6px; padding: 6px;">+</button>-->
    </div>

    <hr>
    <footer>
        <p>© 2017 Nordic-Skills</p>
    </footer>

    <script>
		$(function() {

            /*$( "#addAktieButton" ).click(function() {
                addAktie();
            });*/

			$( "#addEventButton" ).click(function() {
                addEvent();
            });

            /*$("#form-movies").submit(function(e){
                addMovie($("#movie-title").val());
                e.preventDefault(e);
            });

            $("#form-shows").submit(function(e){
                addShow($("#show-title").val());
                e.preventDefault(e);
            });*/

            $.ajax({
                url: "ajax/getAktien.php",
                type: "post",
                /*data: { type: "movie" },*/
                dataType: "json"
            }).done(function(data) {

				for(var i = 0; i < data.length; i++) {
					$("#aktien").append(formatAktienDiv(data[i]['id']['S']));
					$("#" + data[i]['id']['S'] + "_ID").val(data[i]["id"]['S']);
                    $("#" + data[i]['id']['S'] + "_Name").val(data[i]["name"]['S']);
                    if(typeof(data[i]["alias"]) !== "undefined")
                        $("#" + data[i]['id']['S'] + "_Alias").val(data[i]["alias"]['S']);
                    $("#" + data[i]['id']['S'] + "_Kurs").val(data[i]["current"]['S']);
                    $("#" + data[i]['id']['S'] + "_Change").val(data[i]["change"]['S']);
                    $("#" + data[i]['id']['S'] + "_Volumen").val(data[i]["volume"]['S']);
                    var date = new Date(Date.parse(data[i]["timestamp"]['S']));
                    date.setMinutes(date.getMinutes() + 15);
                    //date.setHours(date.getHours() - 1);
                    $("#" + data[i]['id']['S'] + "_Timestamp").val(date.toLocaleDateString() + " " + date.toLocaleTimeString());
				}
            });

			$.ajax({
                url: "ajax/getEvents.php",
                type: "post",
                /*data: { type: "movie" },*/
                dataType: "json"
            }).done(function(data) {

				for(var i = 0; i < data.length; i++) {
					$("#events").append(formatEventDiv(data[i]['id']['S']));
					$("#" + data[i]['id']['S'] + "_ID").val(data[i]["aktienId"]['S']);
                    $("#" + data[i]['id']['S'] + "_Speech").val(data[i]["speech"]['S']);
                    $("#" + data[i]['id']['S'] + "_Start").val(data[i]["start"]['S']);
                    $("#" + data[i]['id']['S'] + "_Offset").val(data[i]["offset"]['S']);
				}
            });
		});

        /*function addAktie() {
            $("#aktien").append(formatAktienDiv(""));
			/*$.ajax({
                url: "https://api.themoviedb.org/3/search/movie?api_key=b3443ebe1ef5c3586f25ae5b3fc972d0&language=de-DE&query=" + title + "&page=1&include_adult=true",
                type: "post",
                dataType: "json"
            }).done(function(data) {

                if(data.total_results > 0) {
                    $("#movies").append(formatMovieDiv(data.results[0]));

                    $('.thumbnail-sortable-movies').sortable({
                        placeholderClass: 'col-sm-6 col-md-4'
                    });

                    saveLists();

                } else {
                    alert("Film nicht gefunden!");
                }
            });
        }*/

        function formatAktienDiv(id) {
            var div = '<div class="form-row">' +
                        '<div class="form-group col-md-2">' +
						  '<input type="text" class="form-control" id="' + id + '_ID" placeholder="' + 'ID' + '" readonly>' +
						'</div>' +
						'<div class="form-group col-md-2">' +
						  '<input type="text" class="form-control" id="' + id + '_Name" placeholder="' + 'Name' + '" readonly>' +
						'</div>' +
                        '<div class="form-group col-md-2">' +
						  '<input type="text" class="form-control" id="' + id + '_Alias" placeholder="' + 'Alias' + '">' +
						'</div>' +
						'<div class="form-group col-md-1">' +
						  '<input type="text" class="form-control" id="' + id + '_Kurs" placeholder="' + 'Kurs' + '" readonly>' +
						'</div>' +
                        '<div class="form-group col-md-1">' +
						  '<input type="text" class="form-control" id="' + id + '_Change" placeholder="' + 'Änderung' + '" readonly>' +
						'</div>' +
                        '<div class="form-group col-md-1">' +
						  '<input type="text" class="form-control" id="' + id + '_Volumen" placeholder="' + 'Volumen' + '" readonly>' +
						'</div>' +
                        '<div class="form-group col-md-2">' +
						  '<input type="text" class="form-control" id="' + id + '_Timestamp" placeholder="' + 'Timestamp' + '" readonly>' +
						'</div>' +
					  '</div>';
            return div;
        }

		function addEvent() {

            $.ajax({
                url: "ajax/getUniqid.php",
                type: "post",
                data: { },
                dataType: "html"
            }).done(function(data) {

				$("#events").append(formatEventDiv(data));
            });

			/*$.ajax({
                url: "https://api.themoviedb.org/3/search/movie?api_key=b3443ebe1ef5c3586f25ae5b3fc972d0&language=de-DE&query=" + title + "&page=1&include_adult=true",
                type: "post",
                dataType: "json"
            }).done(function(data) {

                if(data.total_results > 0) {
                    $("#movies").append(formatMovieDiv(data.results[0]));

                    $('.thumbnail-sortable-movies').sortable({
                        placeholderClass: 'col-sm-6 col-md-4'
                    });

                    saveLists();

                } else {
                    alert("Film nicht gefunden!");
                }
            });*/
        }

        function formatEventDiv(id) {
            var div = '<div class="form-row" id="' + id + '">' +
						'<div class="form-group col-md-2">' +
						  '<input type="text" class="form-control" id="' + id + '_ID" placeholder="' + 'Aktien-ID' + '">' +
						'</div>' +
						'<div class="form-group col-md-6">' +
						  '<input type="text" class="form-control" id="' + id + '_Speech" placeholder="' + 'Speech (SSML)' + '">' +
						'</div>' +
						'<div class="form-group col-md-2">' +
						  '<input type="text" class="form-control" id="' + id + '_Start" placeholder="' + 'z.B. 29.12.2017 12:45' + '">' +
						'</div>' +
						'<div class="form-group col-md-1">' +
						  '<input type="text" class="form-control" id="' + id + '_Offset" placeholder="' + 'z.B. -3' + '">' +
						'</div>' +
						'<div class="form-group col-md-1">' +
						  '<button class="btn btn-danger" style="width: 36%; padding: 6px;" onclick="confirm(\'Sicher?\') ? deleteEvent(\'' + id + '\') : alert(\'Abgebrochen!\')">X</button>' +
						'</div>' +
					  '</div>';
            return div;
        }

        function deleteEvent(id) {
			$.ajax({
                url: "ajax/deleteEvent.php",
                type: "post",
                data: { eventId: id },
                dataType: "html"
            }).done(function (data) {

                $("#" + id + "_ID").parent().parent().remove();
            });
        }

        function saveChanges() {

            $("#saveLink").html('<span class="glyphicon glyphicon-cloud"></span>');

            var eventsForm = {};
            $("#events .form-row").each(function (index) {
                
                var eventsFormChild = {};
                $(this).children().each(function (index2) {
                    if($(this).children().first().is("input"))
                        eventsFormChild[$(this).children().first().attr('id')] = $(this).children().first().val();
                });
                eventsForm[$(this).attr('id')] = eventsFormChild;
            });

            var aktienForm = {};
            $("#aktien :input").each(function (index) {
                aktienForm[$(this).attr('id')] = $(this).val();
            });

			$.ajax({
                url: "ajax/updateData.php",
                type: "post",
                data: { events: JSON.stringify(eventsForm), aktien: JSON.stringify(aktienForm) },
                dataType: "html"
            }).done(function(data) {

				$("#saveLink").html('Speichern');
            });
        }
	</script>
</div> <!-- /container -->