<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" style="padding: 10px 10px;" href="#"><span><img src="favicon.png"></span><span style="margin-left: 10px;">Börsen-Tool</span></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
        <li><a href="?page=aktien">Aktien und Events</a></li>
        <li class="active"><a href="?page=users">Users</a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Utils <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="https://nordicskills.de/BoersenTool/ajax/updateAktien.php">Manuelles Update</a></li>
                <li><a href="https://nordicskills.de/BoersenTool/ajax/importIndex.php">Import Index</a></li>
            </ul>
        </li>
        </ul>
    </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container theme-showcase" role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <!--<div class="jumbotron">
    <h1>Theme example</h1>
    <p>This is a template showcasing the optional theme stylesheet included in Bootstrap. Use it as a starting point to create something more unique by building on or modifying it.</p>
    </div>-->
	<style>
		.form-group {
			padding-left: 5px;
			padding-right: 5px;
		}
		.form-row .col-md-1 {
			padding-left: 5px;
			padding-right: 5px;
		}
		.form-row .col-md-2 {
			padding-left: 5px;
			padding-right: 5px;
		}
		.form-row .col-md-3 {
			padding-left: 5px;
			padding-right: 5px;
		}
		.form-row .col-md-7 {
			padding-left: 5px;
			padding-right: 5px;
		}
	</style>

    <h2>Users</h2>
    <div class="row thumbnail-sortable-movies" style="margin-left: 0px;">
        <div class="form-row">
            <div class="col-md-3">
                <label>UserID</label>
            </div>
            <div class="col-md-1">
                <label>Name</label>
            </div>
            <div class="col-md-1">
                <label>Vermögen</label>
            </div>
            <div class="col-md-1">
                <label>Kapital</label>
            </div>
            <div class="col-md-1">
                <label>Transaktionen</label>
            </div>
            <div class="col-md-5">
                <label>Aktien</label>
            </div>
        </div>
        <div id="users"></div>
    </div>

    <hr>
    <footer>
        <p>© 2017 Nordic-Skills</p>
    </footer>

    <script>
		$(function() {

            $.ajax({
                url: "ajax/getUsers.php",
                type: "post",
                /*data: { type: "movie" },*/
                dataType: "json"
            }).done(function(data) {

				for(var i = 0; i < data.length; i++) {
					$("#users").append(formatUserDiv(data[i]));
				}
            });
		});

        function formatUserDiv(data) {

            var aktien = "[";
            for(var i = 0; i < data['ownedStocks']['L'].length; i++) {
                if(typeof(data['ownedStocks']['L'][i]['M']['count']['S']) === 'undefined')
                    aktien += data['ownedStocks']['L'][i]['M']['count']['N'];
                else
                    aktien += data['ownedStocks']['L'][i]['M']['count']['S']
                aktien += "x " + data['ownedStocks']['L'][i]['M']['aktienId']['S'] + " | ";
            }
            aktien += "]";

            var div = '<div class="form-row">' +
                        '<div class="col-md-3" title="' + data['userId']['S'] + '" style="overflow: hidden;">' + data['userId']['S'] + '</div>' +
                        '<div class="col-md-1">' + data['name']['S'] + '</div>' +
                        '<div class="col-md-1">' + data['totalCapital']['N'] + '</div>' +
                        '<div class="col-md-1">' + data['capital']['N'] + '</div>' +
                        '<div class="col-md-1">' + data['transactions']['N'] + '</div>' +
                        '<div class="col-md-5" title="' + aktien + '" style="overflow: hidden;">' + aktien + '</div>' +
					  '</div>';
            return div;
        }
	</script>
</div> <!-- /container -->