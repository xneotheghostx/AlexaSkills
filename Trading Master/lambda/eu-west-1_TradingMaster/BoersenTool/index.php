<?php
ini_set('display_errors', 'On');
session_start();

if(isset($_POST["password"]) && $_POST["password"] == 'nordicpass')
    $_SESSION['user'] = 1;
?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico">

        <title>Börsen-Tool</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Bootstrap theme -->
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <!--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">-->

        <!-- Custom styles for this template -->
        <?php
        if(isset($_SESSION['user'])) {
            echo '<link href="css/theme.css" rel="stylesheet">';
        } else { 
            echo '<link href="css/signin.css" rel="stylesheet">';
        } 
        ?>

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <!--<script src="../../assets/js/ie-emulation-modes-warning.js"></script>-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.sortable.min.js"></script>
        <!--<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>-->
        <script src="js/bootstrap.min.js"></script>
        <!--<script src="../../assets/js/docs.min.js"></script>-->
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->

        <?php
        if(isset($_SESSION['user']) && (!isset($_GET['page']) || $_GET['page'] == "aktien")) {
            require_once 'boerse.php';
        } else if(isset($_SESSION['user']) && $_GET['page'] == "users") {
            require_once 'users.php';
        } else {
            require_once 'signin.php';
        }
        ?>
    </body>
</html>
