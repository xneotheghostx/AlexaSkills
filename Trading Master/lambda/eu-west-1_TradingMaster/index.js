﻿var Alexa = require('alexa-sdk');
var dynamo = require('./dynamoDB');
var constants = require('./constants');
var moment = require('./moment');
var aplListStocks = require('./apl_template_listStocks.json');
var aplPortfolio = require('./apl_template_listPortfolio.json');
var aplCard = require('./apl_template_card.json');

exports.handler = function (event, context, callback) {
    var alexa = Alexa.handler(event, context);

    alexa.appId = constants.appId;
    alexa.registerHandlers(handlers.setupModeIntentHandlers, handlers.mainModeIntentHandlers);
    alexa.execute();
};

var handlers = {

    setupModeIntentHandlers: Alexa.CreateStateHandler(constants.states.SETUP_MODE, {

        'LaunchRequest': function () {

            this.emitWithState('NewSession');
        },
        'NewSession': function () {

            console.log("Session init");

            // Init non-db Attributes
            this.attributes['fullMenuCount'] = 0;

            var self = this;
            dynamo.getUserState(self.event.session, function (data) {
                dynamo.getDAXStocks(self.event.session, function (stocks) {
                    if (stocks.item)
                        self.event.session.attributes['stocks'] = stocks.item;
                    else
                        console.log(stocks.message);

                    dynamo.getEvents(self.event.session, function (events) {
                        if (events.item)
                            self.event.session.attributes['events'] = events.item;
                        else
                            console.log(events.message);

                        if (data.item) {
                            Object.assign(self.event.session.attributes, data.item);

                            if (self.attributes["name"] === "InitalName") {
                                self.emit('NoFirstName');
                            } else {
                                self.emit('WelcomeBackIntent');
                            }
                        } else {
                            self.emit('ResetIntent');
                        }
                    });
                });
            });
        },
        'ResetIntent': function () {

            this.attributes["name"] = "InitalName";
            this.attributes["type"] = "Player";
            this.attributes["totalCapital"] = 25000;
            this.attributes["capital"] = 25000;
            this.attributes['ownedStocks'] = [];
            this.attributes['playedEvents'] = [];
            this.attributes['transactions'] = 0;
            this.handler.state = constants.states.SETUP_MODE;

            this.emitWithState('WelcomeIntent');
        },

        'WelcomeIntent': function () { // Erste Begrüßung.

            var speech = "Herzlich Willkommen zu deinem Börsenspiel. <break time='100ms'/> Das Erbe deiner Tante Sophie von 25.000 Euro, ist bereits auf deinem Konto eingegangen. "
                + "Da dies dein erster Besuch ist, werde ich dich über einige dinge die das Spiel betreffen Informieren. <break time='150ms'/> Mit diesem Startkapital kannst du Aktien kaufen. Jeder Handel, "
                + "ob Kauf oder Verkauf, kostet eine Börsengebühr von 5 Euro. Für den Handel stehen dir zur Zeit 30 Aktien aus dem DAX zur Verfügung. Nachdem du mir gleich einen Namen genannt hast, "
                + "kommst du zum Hauptmenü. Dort kannst du Aktien kaufen oder verkaufen, die aktuellen Börsennachrichten oder dein Portfolio anhören. Für Hilfe kannst du jederzeit <break time='100ms'/> "
                + "Alexa Hilfe sagen. Damit du in der Bestenliste auftauchen kannst, nenne mir bitte jetzt einen Namen <break time='100ms'/> oder sage Anonym. ";
            var reprompt = "Wenn du nicht namentlich in der Liste auftauchen möchtest, sage Anonym <break time='150ms'/> oder nenne jetzt einen Namen.";
            this.emit(':ask', speech, reprompt);
        },
        'WelcomeBackIntent': function () { // Rückkehrer, wenn Name festgelegt wurde.

            var speech = "Hallo " + this.attributes['name'] + "<break time='150ms'/> gut das du wieder da bist. <break time='150ms'/> Ich wünsche dir viel Erfolg beim Handeln. ";

            this.handler.state = constants.states.MAIN_MODE;
            this.emitWithState('EventsIntent', speech);
        },
        'NoFirstName': function () { // Rückkehrer, wenn noch kein Name festgelegt wurde.

            var speech = "Willkommen zum Börsenspiel. "
                + "Du hast dir noch keinen Namen gegeben. <break time='150ms'/> Wenn du in der Liste der besten Investoren auftauchen möchtest, nenne mir bitte einen Namen. ";
            var reprompt = "Wenn du nicht namentlich in der Liste auftauchen möchtest, "
                + "sage Anonym <break time='150ms'/> andernfalls nenne mir jetzt bitte einen Namen.";
            this.emit(':ask', speech, reprompt);
        },

        'NameIntent': function () {

            if (blacklist.indexOf(this.event.request.intent.slots.name.value.toLowerCase()) == -1) {

                this.attributes['setupMode'] = "VerifyName";
                this.attributes['name'] = this.event.request.intent.slots.name.value;

                var speech = "Dein Name lautet nun " + this.attributes['name'] + " . Ist das richtig? ";
                var reprompt = "Dein Name lautet nun " + this.attributes['name'] + " . Ist das richtig? ";
            } else {

                var speech = "Der genannte Name ist nicht erlaubt, bitte nenne einen anderen. ";
                var reprompt = "Nenne mir bitte einen Namen. ";
            }
            this.emit(':ask', speech, reprompt);
        },
        'AMAZON.YesIntent': function () {

            if (this.attributes['setupMode'] == "VerifyName") {
                this.attributes['setupMode'] = "";
                this.handler.state = constants.states.MAIN_MODE;
                this.emitWithState('MenuIntent');
            } else {
                this.emit('Unhandled');
            }
        },
        'AMAZON.NoIntent': function () {

            if (this.attributes['setupMode'] == "VerifyName") {
                var speech = "Okay, nenne mir jetzt deinen richtigen Namen. ";
                this.emit(':ask', speech, speech);
            } else {
                this.emit('Unhandled');
            }
        },

        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            console.log("should end......");

            var self = this;
            saveToDynamoDB(self, function () {

                var speech = "Danke fürs Handeln. Bis zum nächsten mal. ";
                var cardTitle = "Auf wiederhören und vielen Dank für´s Spielen.";
                var cardText = "Danke, dass du mit mir gespielt hast.\n\nWenn dir dieser Skill gefallen hat, freut sich das Team von \"Nordic Skills\" über eine positive Bewertung von dir!\nDies hilft ihnen zukünftig weitere Skills zu entwickeln. \nUnd so bewertest du einen Skill:\n\n1. Öffne die Alexa App auf deinem Smartphone, Laptop oder PC\n\n2. Gehe in den Bereich „Skills“\n\n3. Tippe auf „Ihre Skills“ in der rechten oberen Ecke\n\n4. Suche nach „Meine Börse“\n\n5. Scrolle bis nach unten und tippe auf „Schreiben Sie eine Rezension“\n\n6. Unterstütze das Team von \"Nordic Skills\" mit deiner 5-Sterne-Bewertung\n\nDanke!";
                var imageObj = { smallImageUrl: "https://nordicskills.de/bilder/DerZauberwaldKendell/Danke.jpg", largeImageUrl: "https://nordicskills.de/bilder/DerZauberwaldKendell/Danke.jpg" };
                self.emit(':tellWithCard', speech, cardTitle, cardText, imageObj);
            });
        },
        'Unhandled': function () {
            var speech = 'Entschuldigung, Ich habe dich nicht verstanden. ';
            this.emit(':ask', speech, speech);
        }
    }),
    mainModeIntentHandlers: Alexa.CreateStateHandler(constants.states.MAIN_MODE, {

        'MenuIntent': function (speech) {

            // Reset Transaction-Info
            this.attributes['request'] = "None";
            this.attributes['selectedStock'] = {};
            this.attributes['transactionCost'] = 0;
            this.attributes['transactionAmount'] = 0;

            

            if (speech === undefined)
                var speech = "";

            speech += "<break time='100ms'/> Hauptmenü. <break time='100ms'/> Was möchtest du jetzt tun? ";
            if (this.attributes['fullMenuCount'] < 1) {
                speech += "<break time='75ms'/> Du kannst Aktien kaufen <break time='100ms'/> oder verkaufen. <break time='100ms'/> Börsennachrichten oder dein Portfolio hören. Für eine genaue Erklärung der Menüpunkte, sage Hilfe. ";
                this.attributes['fullMenuCount'] = this.attributes['fullMenuCount'] + 1;
            }
            var reprompt = "Was möchtest du jetzt tun? <break time='75ms'/> Du kannst Aktien kaufen <break time='100ms'/> oder verkaufen. <break time='100ms'/> Börsennachrichten oder dein Portfolio hören. Für eine genaue Erklärung der Menüpunkte, sage Hilfe.";

            if (supportsDisplay.call(this)) {
                this.response._addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    version: '1.0',
                    document: aplPortfolio,
                    datasources: {
                        "listTemplate1Metadata": {
                            "type": "object",
                            "objectId": "lt1Metadata",
                            "backgroundImage": {
                                "contentDescription": null,
                                "smallSourceUrl": null,
                                "largeSourceUrl": null,
                                "sources": [
                                    {
                                        "url": "https://nordicskills.de/bilder/TradinMaster/Starseite.jpg",
                                        "size": "small",
                                        "widthPixels": 0,
                                        "heightPixels": 0
                                    },
                                    {
                                        "url": "https://nordicskills.de/bilder/TradinMaster/Startseite.jpg",
                                        "size": "large",
                                        "widthPixels": 0,
                                        "heightPixels": 0
                                    }
                                ]
                            },
                            "title": "Trading Master - Hauptmenü"
                        },
                        "listTemplate1ListData": {
                            "type": "list",
                            "listId": "lt1Sample",
                            "totalNumberOfItems": 4,
                            "listPage": {
                                "listItems": [
                                    {
                                        "listItemIdentifier": "AktienKaufen",
                                        "textContent": {
                                            "primaryText": {
                                                "type": "PlainText",
                                                "text": "Aktien kaufen"
                                            }
                                        },
                                        "token": "AktienKaufen"
                                    },
                                    {
                                        "listItemIdentifier": "AktienVerkaufen",
                                        "textContent": {
                                            "primaryText": {
                                                "type": "PlainText",
                                                "text": "Aktien verkaufen"
                                            }
                                        },
                                        "token": "AktienVerkaufen"
                                    },
                                    {
                                        "listItemIdentifier": "Boersennachrichten",
                                        "textContent": {
                                            "primaryText": {
                                                "type": "PlainText",
                                                "text": "Börsennachrichten"
                                            }
                                        },
                                        "token": "Boersennachrichten"
                                    },
                                    {
                                        "listItemIdentifier": "Portfolio",
                                        "textContent": {
                                            "primaryText": {
                                                "type": "PlainText",
                                                "text": "Portfolio"
                                            }
                                        },
                                        "token": "Portfolio"
                                    }
                                ]
                            }
                        }
                    }
                });
                this.response.speak(speech).listen(reprompt);
                this.emit(':responseReady');
            } else {
                this.emit(':ask', speech, reprompt);
            }
        },

        'BuyStockIntent': function () {

            this.attributes['request'] = "Buy";

            var speech = "Nenne mir den Aktienamen, welche du kaufen möchtest? ";
            this.emit(':ask', speech, speech);
        },
        'SellStockIntent': function () {

            this.attributes['request'] = "Sell";

            var speech = "Nenne mir den Aktienamen, welche du verkaufen möchtest? ";
            this.emit(':ask', speech, speech);
        },
        'EventsIntent': function (speech) {

            var invokedByWelcome = false;
            if (speech === undefined) {
                var speech = "";
            } else {
                invokedByWelcome = true;
            }

            if (invokedByWelcome) {

                for (var i = 0; i < this.attributes['events'].length; i++) {

                    var parsedDateTime = moment(this.attributes['events'][i].start, "DD/MM/YYYY HH:mm");
                    if (moment().add(1, 'hours').isSameOrAfter(parsedDateTime) && this.attributes['playedEvents'].indexOf(this.attributes['events'][i].id) == -1) {
                        speech += this.attributes['events'][i].speech + "<break time='1s'/>";
                        this.attributes['playedEvents'].push(this.attributes['events'][i].id);
                    }
                }
            } else {

                for (var i = 0; i < this.attributes['events'].length; i++) {

                    speech += this.attributes['events'][i].speech + "<break time='1s'/>";
                    this.attributes['playedEvents'].push(this.attributes['events'][i].id);

                    if (i > 3)
                        break;
                }
                if (speech != "") {
                    speech = "Dies sind die aktuellen Nachrichten. : <break time='100ms'/> " + speech;
                } else {
                    speech = "Es gibt derzeit keine neuen Nachrichten. ";
                }
            }
            // Clear older Events
            this.attributes['playedEvents'] = cleanPlayedEvents(this);

            this.emitWithState('MenuIntent', speech);
        },
        'PortfolioIntent': function () {

            if (this.attributes['ownedStocks'].length <= 0) {
                var speech = "Du besitzt derzeit keine Aktien. ";
                this.emitWithState('MenuIntent', speech);
            }

            this.attributes['request'] = "Portfolio";

            if (supportsDisplay.call(this)) {
                
                var speech = "Hier ist die Liste deiner aktuellen Aktien. <break time='150ms'/> Wenn du möchtest, dass ich sie dir jetzt vorlese, sage weiter oder sage Menü, um ins Hauptmenü zurückzukehren. ";

                var portfolioItemList = [];
                for (var i = 0; i < this.attributes['stocks'].length; i++) {
                    for (var j = 0; j < this.attributes['ownedStocks'].length; j++) {
                        if (this.attributes['stocks'][i].id == this.attributes['ownedStocks'][j].aktienId) {
                            var singleCost = parseFloat(this.attributes['stocks'][i].current) + parseFloat(getEventChange(this, this.attributes['stocks'][i].id));

                            portfolioItemList.push({
                                "listItemIdentifier": this.attributes['stocks'][i].id,
                                "textContent": {
                                    "primaryText": {
                                        "type": "PlainText",
                                        "text": this.attributes['stocks'][i].alias
                                    },
                                    "secondaryText": {
                                        "type": "PlainText",
                                        "text": "Anzahl: " + this.attributes['ownedStocks'][j].count
                                    },
                                    "tertiaryText": {
                                        "type": "PlainText",
                                        "text": (singleCost * this.attributes['ownedStocks'][j].count).toFixed(2) + "€"
                                    }
                                },
                                "token": this.attributes['stocks'][i].id
                            });
                        }
                    }
                }

                this.response._addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    version: '1.0',
                    document: aplPortfolio,
                    datasources: {
                        "listTemplate1Metadata": {
                            "type": "object",
                            "objectId": "lt1Metadata",
                            "backgroundImage": {
                                "contentDescription": null,
                                "smallSourceUrl": null,
                                "largeSourceUrl": null,
                                "sources": [
                                    {
                                        "url": "https://nordicskills.de/bilder/TradinMaster/TradingMasterBG.jpg",
                                        "size": "small",
                                        "widthPixels": 0,
                                        "heightPixels": 0
                                    },
                                    {
                                        "url": "https://nordicskills.de/bilder/TradinMaster/TradingMasterBG.jpg",
                                        "size": "large",
                                        "widthPixels": 0,
                                        "heightPixels": 0
                                    }
                                ]
                            },
                            "title": "Dein Portfolio"
                        },
                        "listTemplate1ListData": {
                            "type": "list",
                            "listId": "lt1Sample",
                            "totalNumberOfItems": portfolioItemList.length,
                            "listPage": {
                                "listItems": portfolioItemList
                            }
                        }
                    }
                });
                this.response.speak(speech).listen(speech);
                this.emit(':responseReady');
            } else {
                var speech = "Du findest eine Liste deiner aktuellen Aktien in der Alexa App. <break time='150ms'/> Wenn du möchtest, dass ich sie dir jetzt vorlese, sage weiter oder sage Menü, um ins Hauptmenü zurückzukehren. ";

                var content = "";
                for (var i = 0; i < this.attributes['stocks'].length; i++) {
                    for (var j = 0; j < this.attributes['ownedStocks'].length; j++) {
                        if (this.attributes['stocks'][i].id == this.attributes['ownedStocks'][j].aktienId) {
                            var singleCost = parseFloat(this.attributes['stocks'][i].current) + parseFloat(getEventChange(this, this.attributes['stocks'][i].id));
                            content += this.attributes['stocks'][i].alias + "\n- Anzahl: " + this.attributes['ownedStocks'][j].count + " | Gesamtwert: " + (singleCost * this.attributes['ownedStocks'][j].count).toFixed(2) + "€ \n\u{00A0}\n";
                        }
                    }
                }
                content = content.replace(/und/g, "&");

                this.emit(':askWithCard', speech, speech, "Dein Portfolio", content, null);
            }
        },

        'ListStocksIntent': function (speech) {

            if (speech === undefined) {
                var speech = "";
            }

            this.attributes['request'] = "DAX";
            
            if (supportsDisplay.call(this)) {
                speech  += "Hier ist die Liste der Aktien mit den aktuellen Kursen. Wenn du möchtest das ich sie dir vorlese, sage weiter oder sage Menü, um ins Hauptmenü zurückzukehren. ";
                var stockItemList = [];
                for (var i = 0; i < this.attributes['stocks'].length; i++) {
                    stockItemList.push({
                        "listItemIdentifier": this.attributes['stocks'][i].id,
                        "textContent": {
                            "primaryText": {
                                "type": "PlainText",
                                "text": this.attributes['stocks'][i].alias
                            },
                            "tertiaryText": {
                                "type": "PlainText",
                                "text": this.attributes['stocks'][i].current + "€"
                            }
                        },
                        "token": this.attributes['stocks'][i].id
                    });
                }

                this.response._addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    version: '1.0',
                    document: aplListStocks,
                    datasources: {
                        "listTemplate1Metadata": {
                            "type": "object",
                            "objectId": "lt1Metadata",
                            "backgroundImage": {
                                "contentDescription": null,
                                "smallSourceUrl": null,
                                "largeSourceUrl": null,
                                "sources": [
                                    {
                                        "url": "https://nordicskills.de/bilder/TradinMaster/Aktienliste.jpg",
                                        "size": "small",
                                        "widthPixels": 0,
                                        "heightPixels": 0
                                    },
                                    {
                                        "url": "https://nordicskills.de/bilder/TradinMaster/Aktienliste.jpg",
                                        "size": "large",
                                        "widthPixels": 0,
                                        "heightPixels": 0
                                    }
                                ]
                            },
                            "title": "Aktuelle Aktienkurse"
                        },
                        "listTemplate1ListData": {
                            "type": "list",
                            "listId": "lt1Sample",
                            "totalNumberOfItems": stockItemList.length,
                            "listPage": {
                                "listItems": stockItemList
                            }
                        }
                    }
                });
                this.response.speak(speech).listen(speech);
                this.emit(':responseReady');
            } else {
                speech  += "Du findest eine Liste der Aktien, in deiner Alexa-App. Wenn du möchtest das ich sie dir vorlese, sage weiter oder sage Menü, um ins Hauptmenü zurückzukehren. ";

                var content = "";
                for (var i = 0; i < this.attributes['stocks'].length; i++) {
                    content += this.attributes['stocks'][i].alias + " - " + this.attributes['stocks'][i].current + "€ \n";
                }
                content = content.replace(/und/g, "&");

                this.emit(':askWithCard', speech, speech, "Aktuelle Aktienkurse", content, null);
            }
        },

        'ContinueIntent': function () {

            if (this.attributes['request'] == "Portfolio") {

                var speech = "Du besitzt derzeit folgende Aktien. ";
                for (var i = 0; i < this.attributes['stocks'].length; i++) {
                    for (var j = 0; j < this.attributes['ownedStocks'].length; j++) {
                        if (this.attributes['stocks'][i].id == this.attributes['ownedStocks'][j].aktienId) {
                            var singleCost = parseFloat(this.attributes['stocks'][i].current) + parseFloat(getEventChange(this, this.attributes['stocks'][i].id));
                            speech += this.attributes['ownedStocks'][j].count + " " + this.attributes['stocks'][i].alias + " Aktien im Wert von insgesamt " + singleCost * this.attributes['ownedStocks'][j].count + " Euro. <break time='200ms'/>"
                        }
                    }
                }
                this.emitWithState('MenuIntent', speech);
            } else if (this.attributes['request'] == "DAX") {

                var speech = "Im DAX sind derzeit folgende Unternehmen gelistet: ";
                for (var i = 0; i < this.attributes['stocks'].length; i++) {
                    var singleCost = parseFloat(this.attributes['stocks'][i].current) + parseFloat(getEventChange(this, this.attributes['stocks'][i].id));
                    speech += this.attributes['stocks'][i].alias + " mit einem aktuellen Kurs von " + singleCost + " Euro. <break time='200ms'/>"
                }
                this.emitWithState('MenuIntent', speech);
            } else {
                this.emit('Unhandled');
            }
        },


        'CurrentPriceIntent': function () {

            this.attributes['request'] = "Price";

            var speech = "Nenne mir eine Aktie, um ihren aktuellen Kurs zu erfahren. ";
            this.emit(':ask', speech, speech);
        },

        'HundertIntent': function () {

            var hundert = this.event.request.intent.slots.hundert.value;

            switch (hundert) {
                case "hundert":
                    this.emitWithState('NumberIntent', 100);
                    break;
                case "ein hundert":
                    this.emitWithState('NumberIntent', 100);
                    break;
                case "zwei hundert":
                    this.emitWithState('NumberIntent', 200);
                    break;
                case "drei hundert":
                    this.emitWithState('NumberIntent', 300);
                    break;
                case "vier hundert":
                    this.emitWithState('NumberIntent', 400);
                    break;
                case "fünf hundert":
                    this.emitWithState('NumberIntent', 500);
                    break;
                case "sechs hundert":
                    this.emitWithState('NumberIntent', 600);
                    break;
                case "sieben hundert":
                    this.emitWithState('NumberIntent', 700);
                    break;
                case "acht hundert":
                    this.emitWithState('NumberIntent', 800);
                    break;
                case "neun hundert":
                    this.emitWithState('NumberIntent', 900);
                    break;
            }
        },
        
        'NameIntent': function () {
            this.emitWithState("DAXIntent", this.event.request.intent.slots.name.value);
        },
        'DAXIntent': function (stockName) {

            if (stockName === undefined)
                var stockName = this.event.request.intent.slots.dax.value;

            if (!(getStockByName(this, stockName) === undefined))
                this.attributes['selectedStock'] = getStockByName(this, stockName);

            if (this.attributes['selectedStock'] === undefined || this.attributes['selectedStock'].current === undefined)
                this.emitWithState('Unhandled');

            var singleCost = parseFloat(this.attributes['selectedStock'].current) + parseFloat(getEventChange(this, this.attributes['selectedStock'].id));
            if (this.attributes['request'] == "Price") {

                this.attributes['request'] = "None";

                var speech = this.attributes['selectedStock'].alias + " hat einen aktuellen Wert von " + singleCost.toString() + " Euro mit einer Schwankung von " + singleCost.toString().replace("-", "minus ") + " Euro, seit der letzten Börseneröffnung.";
                
                if (supportsDisplay.call(this)) {
                    this.response._addDirective({
                        type: 'Alexa.Presentation.APL.RenderDocument',
                        version: '1.0',
                        document: aplCard,
                        datasources: {
                            "bodyTemplate1Data": {
                                "type": "object",
                                "objectId": "bt1Sample",
                                "backgroundImage": {
                                    "contentDescription": null,
                                    "smallSourceUrl": null,
                                    "largeSourceUrl": null,
                                    "sources": [
                                        {
                                            "url": "https://nordicskills.de/bilder/TradinMaster/KaufVerkaufN.jpg",
                                            "size": "small",
                                            "widthPixels": 0,
                                            "heightPixels": 0
                                        },
                                        {
                                            "url": "https://nordicskills.de/bilder/TradinMaster/KaufVerkaufN.jpg",
                                            "size": "large",
                                            "widthPixels": 0,
                                            "heightPixels": 0
                                        }
                                    ]
                                },
                                "title": "Aktien-Preis",
                                "textContent": {
                                    "primaryText": {
                                        "type": "PlainText",
                                        "text": speech
                                    }
                                }
                            }
                        }
                    });
                    this.response.speak(speech).listen(speech);
                    this.emit(':responseReady');
                } else {
                    this.emit(':askWithCard', speech, speech, "Aktien-Preis", speech);
                }
            } else if (this.attributes['request'] == "Buy") {

                if (this.attributes['capital'] < singleCost) {
                    var speech = "Du hast leider nicht genug Kapital für eine " + this.attributes['selectedStock'].alias + " Aktie. Nenne mir eine andere Aktie oder sage Menü.";
                } else {
                    this.attributes['request'] = "BuyCount";
                    var speech = "Wie viele " + this.attributes['selectedStock'].alias + " Aktien möchtest du kaufen? ";
                }
                this.emit(':ask', speech, speech);
            } else if (this.attributes['request'] == "Sell") {

                for (var i = 0; i < this.attributes['ownedStocks'].length; i++) {

                    if (this.attributes['ownedStocks'][i].aktienId == this.attributes['selectedStock'].id) {
                        
                        this.attributes['request'] = "SellCount";

                        var priceDiff = singleCost - parseFloat(this.attributes['ownedStocks'][i].purchasePrice);
                        priceDiff = parseFloat(priceDiff.toFixed(2));
                        var speech = "Du besitzt " + this.attributes['ownedStocks'][i].count + " " + this.attributes['selectedStock'].alias + " Aktien. Der Wert ist pro Aktie, seit dem Kauf " + (priceDiff == 0 ? "unverändert geblieben" : (priceDiff > 0 ? "um " + priceDiff.toFixed(2) + " Euro gestiegen" : "um " + priceDiff.toFixed(2).substring(1) + " Euro gefallen. ")) + ". Wie viele möchtest du verkaufen? ";
                        
                        if (supportsDisplay.call(this)) {
                            this.response._addDirective({
                                type: 'Alexa.Presentation.APL.RenderDocument',
                                version: '1.0',
                                document: aplCard,
                                datasources: {
                                    "bodyTemplate1Data": {
                                        "type": "object",
                                        "objectId": "bt1Sample",
                                        "backgroundImage": {
                                            "contentDescription": null,
                                            "smallSourceUrl": null,
                                            "largeSourceUrl": null,
                                            "sources": [
                                                {
                                                    "url": "https://nordicskills.de/bilder/TradinMaster/KaufVerkaufN.jpg",
                                                    "size": "small",
                                                    "widthPixels": 0,
                                                    "heightPixels": 0
                                                },
                                                {
                                                    "url": "https://nordicskills.de/bilder/TradinMaster/KaufVerkaufN.jpg",
                                                    "size": "large",
                                                    "widthPixels": 0,
                                                    "heightPixels": 0
                                                }
                                            ]
                                        },
                                        "title": "Aktien verkaufen",
                                        "textContent": {
                                            "primaryText": {
                                                "type": "PlainText",
                                                "text": speech
                                            }
                                        }
                                    }
                                }
                            });
                            this.response.speak(speech).listen(speech);
                            this.emit(':responseReady');
                        } else {
                            this.emit(':askWithCard', speech, speech, "Aktien verkaufen", speech);
                        }
                    }
                }
                var speech = "Du besitzt keine " + this.attributes['selectedStock'].alias + " Aktien. ";
                this.emitWithState('MenuIntent', speech);
            } else {
                this.emit('Unhandled');
            }
        },
        'NumberIntent': function (parameterNumber) {

            if (parameterNumber === undefined) {
                var number = this.event.request.intent.slots.number.value;
            } else {
                var number = parameterNumber;
            }

            if (this.attributes['request'] == "BuyCount") {

                this.attributes['transactionSingleCost'] = parseFloat(this.attributes['selectedStock'].current) + parseFloat(getEventChange(this, this.attributes['selectedStock'].id));
                this.attributes['transactionCost'] = (this.attributes['transactionSingleCost'] * number) + 5;
                this.attributes['transactionCost'] = parseFloat(this.attributes['transactionCost'].toFixed(2));
                if (this.attributes['capital'] >= this.attributes['transactionCost']) {
                    this.attributes['transactionAmount'] = number;
                    this.attributes['request'] = "BuyConfirm";
                    var speech = "Du möchtest also " + number + " " + this.attributes['selectedStock'].alias + " Aktien für einen Preis von " + (this.attributes['transactionCost'] - 5).toFixed(2) + " Euro kaufen? ";
                } else {
                    while (this.attributes['capital'] < this.attributes['transactionCost']) {
                        number--;
                        this.attributes['transactionCost'] = (this.attributes['transactionSingleCost'] * number) + 5;
                        this.attributes['transactionCost'] = parseFloat(this.attributes['transactionCost'].toFixed(2));
                    }
                    this.attributes['transactionAmount'] = number;
                    this.attributes['request'] = "BuyConfirm";
                    var speech = "Dein Kapital reicht nicht für diese Transaktion. Möchtest du stattdessen " + number + " für einen Preis von " + (this.attributes['transactionCost'] - 5).toFixed(2) + " kaufen? ";
                }
                if (supportsDisplay.call(this)) {
                    this.response._addDirective({
                        type: 'Alexa.Presentation.APL.RenderDocument',
                        version: '1.0',
                        document: aplCard,
                        datasources: {
                            "bodyTemplate1Data": {
                                "type": "object",
                                "objectId": "bt1Sample",
                                "backgroundImage": {
                                    "contentDescription": null,
                                    "smallSourceUrl": null,
                                    "largeSourceUrl": null,
                                    "sources": [
                                        {
                                            "url": "https://nordicskills.de/bilder/TradinMaster/KaufVerkaufN.jpg",
                                            "size": "small",
                                            "widthPixels": 0,
                                            "heightPixels": 0
                                        },
                                        {
                                            "url": "https://nordicskills.de/bilder/TradinMaster/KaufVerkaufN.jpg",
                                            "size": "large",
                                            "widthPixels": 0,
                                            "heightPixels": 0
                                        }
                                    ]
                                },
                                "title": "Aktien kaufen",
                                "textContent": {
                                    "primaryText": {
                                        "type": "PlainText",
                                        "text": speech
                                    }
                                }
                            }
                        }
                    });
                    this.response.speak(speech).listen(speech);
                    this.emit(':responseReady');
                } else {
                    this.emit(':askWithCard', speech, speech, "Aktien kaufen", speech);
                }
            } else if (this.attributes['request'] == "SellCount") {

                this.attributes['transactionSingleCost'] = parseFloat(this.attributes['selectedStock'].current) + parseFloat(getEventChange(this, this.attributes['selectedStock'].id));
                this.attributes['transactionCost'] = (this.attributes['transactionSingleCost'] * number) - 5;
                this.attributes['transactionCost'] = parseFloat(this.attributes['transactionCost'].toFixed(2));
                this.attributes['transactionAmount'] = number;

                for (var i = 0; i < this.attributes['ownedStocks'].length; i++) {

                    if (this.attributes['ownedStocks'][i].aktienId == this.attributes['selectedStock'].id) {

                        if (this.attributes['transactionAmount'] <= this.attributes['ownedStocks'][i].count) {

                            var speech = "Du möchtest " + this.attributes['transactionAmount'] + " " + this.attributes['selectedStock'].alias + " Aktien für einen Preis von " + (this.attributes['transactionCost'] + 5).toFixed(2) + " Euro verkaufen? ";
                        } else {

                            this.attributes['transactionAmount'] = this.attributes['ownedStocks'][i].count;
                            this.attributes['transactionCost'] = (this.attributes['transactionSingleCost'] * this.attributes['transactionAmount']) - 5;
                            this.attributes['transactionCost'] = parseFloat(this.attributes['transactionCost'].toFixed(2));

                            var speech = "Du besitzt nur " + this.attributes['transactionAmount'] + " " + this.attributes['selectedStock'].alias + " Aktien. Möchtest du sie für einen Preis von " + (this.attributes['transactionCost'] + 5).toFixed(2) + " Euro verkaufen?";
                        }
                        this.attributes['request'] = "SellConfirm";

                        if (supportsDisplay.call(this)) {
                            this.response._addDirective({
                                type: 'Alexa.Presentation.APL.RenderDocument',
                                version: '1.0',
                                document: aplCard,
                                datasources: {
                                    "bodyTemplate1Data": {
                                        "type": "object",
                                        "objectId": "bt1Sample",
                                        "backgroundImage": {
                                            "contentDescription": null,
                                            "smallSourceUrl": null,
                                            "largeSourceUrl": null,
                                            "sources": [
                                                {
                                                    "url": "https://nordicskills.de/bilder/TradinMaster/KaufVerkaufN.jpg",
                                                    "size": "small",
                                                    "widthPixels": 0,
                                                    "heightPixels": 0
                                                },
                                                {
                                                    "url": "https://nordicskills.de/bilder/TradinMaster/KaufVerkaufN.jpg",
                                                    "size": "large",
                                                    "widthPixels": 0,
                                                    "heightPixels": 0
                                                }
                                            ]
                                        },
                                        "title": "Aktien verkaufen",
                                        "textContent": {
                                            "primaryText": {
                                                "type": "PlainText",
                                                "text": speech
                                            }
                                        }
                                    }
                                }
                            });
                            this.response.speak(speech).listen(speech);
                            this.emit(':responseReady');
                        } else {
                            this.emit(':askWithCard', speech, speech, "Aktien verkaufen", speech);
                        }
                    }
                }
            } else {
                this.emit('Unhandled');
            }
        },
        'AMAZON.YesIntent': function () {

            if (this.attributes['request'] == "BuyConfirm") {

                var speech = "Okay, Die Transaktion wurde abgeschlossen. ";
                this.attributes['transactions']++;
                this.attributes['capital'] -= this.attributes['transactionCost'];

                console.log("Buy: " + this.attributes['name'] + ": " + " AktienID: " + this.attributes['selectedStock'].id + " TransaktionCost: " + this.attributes['transactionCost']);
                for (var i = 0; i < this.attributes['ownedStocks'].length; i++) {

                    if (this.attributes['ownedStocks'][i].aktienId == this.attributes['selectedStock'].id) {
                        this.attributes['ownedStocks'][i].count = parseInt(this.attributes['ownedStocks'][i].count) + parseInt(this.attributes['transactionAmount']);
                        this.attributes['ownedStocks'][i].purchasePrice = this.attributes['transactionSingleCost'];
                        this.emitWithState('MenuIntent', speech);
                    }
                }
                var stock = { "aktienId": this.attributes['selectedStock'].id, "count": parseInt(this.attributes['transactionAmount']), "purchasePrice": this.attributes['transactionSingleCost'] }
                this.attributes['ownedStocks'].push(stock);

                this.emitWithState('MenuIntent', speech);
            } else if (this.attributes['request'] == "SellConfirm") {

                var speech = "Okay, Die Transaktion wurde abgeschlossen. ";
                this.attributes['transactions']++;
                this.attributes['capital'] += this.attributes['transactionCost'];

                console.log("Sell: " + this.attributes['name'] + ": " + " AktienID: " + this.attributes['selectedStock'].id + " TransaktionCost: " + this.attributes['transactionCost']);
                for (var i = 0; i < this.attributes['ownedStocks'].length; i++) {
                    if (this.attributes['ownedStocks'][i].aktienId == this.attributes['selectedStock'].id) {
                        this.attributes['ownedStocks'][i].count = parseInt(this.attributes['ownedStocks'][i].count) - parseInt(this.attributes['transactionAmount']);
                        if (this.attributes['ownedStocks'][i].count <= 0)
                            this.attributes['ownedStocks'].splice(i, 1);

                        this.emitWithState('MenuIntent', speech);
                    }
                }
            } else {
                this.emit('Unhandled');
            }
        },
        'AMAZON.NoIntent': function () {

            if (this.attributes['request'] == "BuyConfirm" || this.attributes['request'] == "SellConfirm") {
                this.emitWithState('CancelPurchaseIntent');
            } else {
                this.emit('Unhandled');
            }
        },
        'EveryIntent': function () {

            if (this.attributes['request'] == "SellCount") {

                for (var i = 0; i < this.attributes['ownedStocks'].length; i++) {
                    if (this.attributes['ownedStocks'][i].aktienId == this.attributes['selectedStock'].id) {
                        this.emitWithState('NumberIntent', this.attributes['ownedStocks'][i].count);
                    }
                }
            } else {
                this.emit('Unhandled');
            }
        },

        'CancelPurchaseIntent': function () {

            var speech = "Okay, Die Transaktion wurde abgebrochen. ";
            this.emitWithState('MenuIntent', speech);
        },

        'TopInvestorsIntent': function () {
        
            var self = this;
            saveToDynamoDB(self, function () {
                dynamo.getTopInvestors(self.event.session, function (data) {

                    var outp = "";
                    var j = 0;
                    for (var i = 0; i < data.item.length; i++) {
                        if (data.item[i].name !== "InitalName") {
                            j++;
                            outp += " Platz " + j + ", " + data.item[i].name + "<break time='75ms'/> mit einem Kapital von " + data.item[i].totalCapital + " Euro. ";
                        }
                    }
                    self.emitWithState('MenuIntent', outp);
                });
            });
        },

        'CurrentCapitalIntent': function () {
            calulateTotalCapital(this);
            var speech = "Dein aktuelles Vermögen beträgt " + this.attributes["capital"] + " Euro. <break time='100ms' /> Dein Kapital, inklusive der aktuellen Aktienwerte, beträgt derzeit : " + this.attributes["totalCapital"] + " Euro. <break time='100ms'/> ";
            this.emitWithState('MenuIntent', speech); // Weiterleiten ins Menü
        },

        'AMAZON.HelpIntent': function () {
            var speech = "Hilfe für das Börsenspiel. Du kannst folgendes sagen. <break time='100ms'/> Um Aktien zu kaufen oder zu verkaufen, sagst du <break time='100ms'/> Aktienkaufen oder Aktienverkaufen. "
                + "<break time='200ms' /> Für die Aktuellen Börsenkurse, sagst du Aktienpreise. Um die Aktuellen Nachrichten zu Hören, sagts du Nachrichten und um dein Portfolio zu hören, Portfolio. "
                + "Für die höhe deines aktuellen Kapitals, sagst du Kontostand. Um die Liste der Aktien zu hören, sagst du <break time='100ms'/> Aktienliste. "
                + "Für die Liste der besten Investoren, sagst du Top investor <break time='100ms'/> und mit Menü oder Hauptmenü <break time='100ms' /> kommst du zurück in das Hauptmenü. ";
            var repromt = "";
            this.emit(':ask', speech, repromt);
        },
        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            console.log("should end......");

            var self = this;
            saveToDynamoDB(self, function () {

                var speech = "Danke fürs Handeln. Bis zum nächsten mal. ";
                var cardTitle = "Auf wiederhören und vielen Dank für´s Spielen.";
                var cardText = "Danke, dass du mit mir gespielt hast.\n\nWenn dir dieser Skill gefallen hat, freut sich das Team von \"Nordic Skills\" über eine positive Bewertung von dir!\nDies hilft ihnen zukünftig weitere Skills zu entwickeln. \nUnd so bewertest du einen Skill:\n\n1. Öffne die Alexa App auf deinem Smartphone, Laptop oder PC\n\n2. Gehe in den Bereich „Skills“\n\n3. Tippe auf „Ihre Skills“ in der rechten oberen Ecke\n\n4. Suche nach „Meine Börse“\n\n5. Scrolle bis nach unten und tippe auf „Schreiben Sie eine Rezension“\n\n6. Unterstütze das Team von \"Nordic Skills\" mit deiner 5-Sterne-Bewertung\n\nDanke!";
                var imageObj = { smallImageUrl: "https://nordicskills.de/bilder/DerZauberwaldKendell/Danke.jpg", largeImageUrl: "https://nordicskills.de/bilder/DerZauberwaldKendell/Danke.jpg" };
                self.emit(':tellWithCard', speech, cardTitle, cardText, imageObj);
            });
        },
        'Unhandled': function () {

            if (this.attributes['request'] == "Price" || this.attributes['request'] == "Buy" || this.attributes['request'] == "Sell") {

                var speech = "Ich konnte diese Aktie leider nicht finden. Versuche eine andere oder sage, Aktien-Liste. ";
            } else {
                var speech = 'Entschuldigung, Ich habe dich nicht verstanden.';
            }
            
            this.emit(':ask', speech, speech);
        }
    })
};

function saveToDynamoDB(self, cb) {

    calulateTotalCapital(self);

    dynamo.putUserState(self.event.session, function (data) {
        console.log(data.message);

        cb();
    });
}

function calulateTotalCapital(self) {
    var totalCapital = self.attributes['capital'];
    for (var i = 0; i < self.attributes['ownedStocks'].length; i++) {
        totalCapital += (parseFloat(getStockByID(self, self.attributes['ownedStocks'][i].aktienId).current) + parseFloat(getEventChange(self, self.attributes['ownedStocks'][i].aktienId))) * parseInt(self.attributes['ownedStocks'][i].count);
    }
    self.attributes['totalCapital'] = parseFloat(totalCapital.toFixed(2));
}

function getStockByID(self, stockId) {
    for (var i = 0; i < self.attributes['stocks'].length; i++) {
        if (self.attributes['stocks'][i].id == stockId) {
            return self.attributes['stocks'][i];
        }
    }
}

function getStockByName(self, stockName) {
    for (var i = 0; i < self.attributes['stocks'].length; i++) {
        // Removing all Whitespaces, Dots, replacing "1" with "eins" and setting to lower case
        if (self.attributes['stocks'][i].alias.replace(/\./g,'').replace(/\s/g,'').replace("1", "eins").replace("7", "sieben").toLowerCase() == stockName.replace(/\./g,'').replace(/\s/g,'').replace("1", "eins").replace("7", "sieben").toLowerCase()) {
            return self.attributes['stocks'][i];
        }
    }
}

function getEventChange(self, stockId) {
    var change = 0;
    for (var i = 0; i < self.attributes['events'].length; i++) {
        if (stockId == self.attributes['events'][i].aktienId) {

            var parsedDateTime = moment(self.attributes['events'][i].start, "DD/MM/YYYY HH:mm");
            if (moment().add(1, 'hours').isSameOrAfter(parsedDateTime))
                change += parseFloat(self.attributes['events'][i].offset);
        }
    }
    return change;
}

function cleanPlayedEvents(self) {

    for (var i = 0; i < self.attributes['playedEvents'].length; i++) {
        var found = false;
        for (var j = 0; j < self.attributes['events'].length; j++) {
            if (self.attributes['playedEvents'][i] === self.attributes['events'][j].id)
                found = true;
        }
        if (!found)
            delete self.attributes['playedEvents'][i];
    }

    var arr = [];
    for (var i = 0; i < self.attributes['playedEvents'].length; i++) {
        if (self.attributes['playedEvents'][i])
            arr.push(self.attributes['playedEvents'][i]);
    }
    return arr;
}

function supportsDisplay() { 
    var hasDisplay = 
    this.event.context && 
    this.event.context.System && 
    this.event.context.System.device && 
    this.event.context.System.device.supportedInterfaces && 
    this.event.context.System.device.supportedInterfaces['Alexa.Presentation.APL']
 
  return hasDisplay; 
} 

var blacklist =
[
    "bank",
    "bänker",
    "hitler",
    "nazi",
    "neonazi",
    "hure",
    "porno",
    "pornostar",
    "kinderporno",
    "kindersex",
    "kinderfick",
    "kinderficker",
    "kinderschänder",
    "kindergewalt",
    "vergewaltigung",
    "vergewaltiger",
    "start",
    "jackpot",
    "bitch",
    "whore",
    "fuck",
    "porn",
    "rapist"
];
