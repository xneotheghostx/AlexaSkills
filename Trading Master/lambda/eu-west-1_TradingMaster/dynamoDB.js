'use strict'

var fs = require('fs');
var AWS = require("aws-sdk")
var skill = require('./index').skill
var config = require('./config.json')

var LOCAL_CONTEXT = true

try {
  fs.accessSync( process.env[(process.platform === 'win32') ? 'USERPROFILE' : 'HOME'] + '/.aws')
} catch (e) {
  LOCAL_CONTEXT = false
}

// if this is being tested locally, load appropriate creds
if ( LOCAL_CONTEXT && config.awsProfileName ) {
  AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: config.awsProfileName })
}

var docClient = new AWS.DynamoDB.DocumentClient({ region: config.dynamoRegion || 'us-east-1' })

function putUserState ( session, cb ) {
    if (!config.dynamoUserTableName ) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

  var params = {
    "TableName": config.dynamoUserTableName,
    "Item": {
      "userId": session.user.userId,
      "name": session.attributes.name,
      "type": session.attributes.type,
      "capital": session.attributes.capital,
      "totalCapital": session.attributes.totalCapital,
      "ownedStocks": session.attributes.ownedStocks,
      "playedEvents": session.attributes.playedEvents,
      "transactions": session.attributes.transactions
    }
  }

  docClient.put( params, handler( cb ) )

}

function getUserState ( session, cb ) {
    if (!config.dynamoUserTableName ) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

  var params = {
    "TableName": config.dynamoUserTableName,
    "Key": {
      "userId": session.user.userId
    },
    "AttributesToGet": [
      "name",
      "type",
      "capital",
      "totalCapital",
      "ownedStocks",
      "playedEvents",
      "transactions"
    ],
  }

  docClient.get( params, handler( cb ) )

}

function getDAXStocks(session, cb) {
    if (!config.dynamoStockTableName) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

    var params = {
        TableName: config.dynamoStockTableName,
        IndexName: "index-name-index",
        KeyConditionExpression: "#n = :n",
        ConsistentRead: false,
        ExpressionAttributeNames: {
            "#n": "index"
        },
        ExpressionAttributeValues: {
            ":n": "DAX"
        },
        ScanIndexForward: true,
        Select: "ALL_ATTRIBUTES"
    }

    docClient.query(params, handler(cb))
}

function getEvents(session, cb) {
    if (!config.dynamoEventTableName) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

    var params = {
        TableName: config.dynamoEventTableName,
        IndexName: "type-index",
        KeyConditionExpression: "#n = :n",
        ConsistentRead: false,
        ExpressionAttributeNames: {
            "#n": "type"
        },
        ExpressionAttributeValues: {
            ":n": "Standard"
        },
        ScanIndexForward: false,
        Select: "ALL_ATTRIBUTES"
    }

    docClient.query(params, handler(cb))
}

function getEvents(session, cb) {
    if (!config.dynamoEventTableName) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

    var params = {
        TableName: config.dynamoEventTableName,
        IndexName: "type-index",
        KeyConditionExpression: "#n = :n",
        ConsistentRead: false,
        ExpressionAttributeNames: {
            "#n": "type"
        },
        ExpressionAttributeValues: {
            ":n": "Standard"
        },
        ScanIndexForward: false,
        Select: "ALL_ATTRIBUTES"
    }

    docClient.query(params, handler(cb))
}

function getTopInvestors(session, cb) {
    if (!config.dynamoUserTableName) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

    var params = {
        TableName: config.dynamoUserTableName,
        IndexName: "type-capital-index",
        KeyConditionExpression: "#n = :n",
        ConsistentRead: false,
        ExpressionAttributeNames: {
            "#n": "type"
        },
        ExpressionAttributeValues: {
            ":n": "Player"
        },
        ScanIndexForward: true,
        Limit: 10,
        Select: "ALL_ATTRIBUTES"
    }

    docClient.query(params, handler(cb))
}

function handler(cb) {
    return function (err, data) {
        if (err) {
            cb({
                message: "ERROR: Dynamo DB - " + JSON.stringify(err, null, 2),
                item: null
            })
        } else {
            if (typeof data.Items === "undefined") {
                cb({
                    message: "SUCCESS: Dynamo DB",
                    item: data.Item
                })
            } else {
                cb({
                    message: "SUCCESS: Dynamo DB",
                    item: data.Items
                })
            }
        }
    }
}

module.exports = {
  putUserState: putUserState,
  getUserState: getUserState,
  getDAXStocks: getDAXStocks,
  getEvents: getEvents,
  getTopInvestors: getTopInvestors
}