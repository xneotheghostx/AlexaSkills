﻿var Alexa = require('alexa-sdk');
var dynamo = require('./dynamoDB');
var constants = require('./constants');

var words = require('./words.json');

exports.handler = function (event, context, callback) {
    var alexa = Alexa.handler(event, context);

    alexa.appId = constants.appId;
    alexa.registerHandlers(handlers.setupModeIntentHandlers, handlers.mainModeIntentHandlers);
    alexa.execute();
};

var handlers = {

    setupModeIntentHandlers: Alexa.CreateStateHandler(constants.states.SETUP_MODE, {
        'NewSession': function () {

            // Skip
            this.handler.state = constants.states.MAIN_MODE;
            this.emitWithState('NextWordIntent');


            console.log("Session init");
            this.handler.state = constants.states.SETUP_MODE;

            var self = this;
            dynamo.getUserState(self.event.session, function (data) {

                console.log(data);

                if (data.item) {
                    Object.assign(self.event.session.attributes, data.item);

                    if (self.attributes["players"].length > 0) {
                        self.emit('SaveFoundIntent');
                    } else {
                        self.emit('ResetIntent');
                    }
                } else {
                    self.emit('ResetIntent');
                }
            });
        },
        'LaunchRequest': function () {

            this.emitWithState('NewSession');
        },
        'ResetIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            this.attributes['players'] = [];

            this.attributes['used'] = '{}';

            this.attributes["minigames"] = false;
            this.attributes["round"] = 0;
            this.attributes["maxRounds"] = 20;

            this.attributes["currentPlayer"] = 0;

            this.emitWithState('NewGameIntent', speech);
        },
        'NewGameIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            speech += "Willkommen, ich bin Alexa und werde dich im Spiel begleiten. Möchtest du die Regeln hören?";
            this.attributes["setupState"] = "RequestRoles";
            this.emit(':ask', speech, speech);
        },
        'SaveFoundIntent': function () {

            var speech = "Willkommen zurück. Ich habe einen alten Spielstand von ";

            for (var i = 0; i < this.attributes["players"].length; i++) {

                if (this.attributes["players"].length == 1) {
                    speech += this.attributes["players"][i].name;
                } else if (i == this.attributes["players"].length - 1) {
                    speech += " und " + this.attributes["players"][i].name;
                } else {
                    speech += this.attributes["players"][i].name + ", ";
                }
            }

            speech += " gefunden. Wenn du von dort aus weiterspielen möchtest, sage weiter spielen. Wenn du eine neue Runde starten willst sage neues spiel. "

            this.emit(':ask', speech, speech);
        },
        'ContinueIntent': function () {

            this.handler.state = constants.states.MAIN_MODE;
            this.emitWithState('NextWordIntent');
        },
        'SetNameIntent': function () {

            if (this.attributes["setupState"] && this.attributes["setupState"] === "RequestRoles") {
                var speech = "Nenne den Namen des ersten Spielers.";
                this.attributes["setupState"] = "AssignName";
                this.emit(':ask', speech, speech);
            }
            this.emit('Unhandled');
        },

        'NameIntent': function () {

            var name = this.event.request.intent.slots.name.value;
            name = name.replace(/\W/g, '');
            name = name.toLowerCase();

            if (this.attributes["setupState"] && (this.attributes["setupState"] === "AssignName" || this.attributes["setupState"] === "ChangeName")) {

                if (this.attributes["setupState"] === "AssignName")
                    this.attributes['players'].push({ name: this.event.request.intent.slots.name.value, score: 0 });
                else if (this.attributes["setupState"] === "ChangeName")
                    this.attributes['players'][this.attributes['players'].length - 1].name = this.event.request.intent.slots.name.value;

                this.attributes["setupState"] = "IsNameCorrect";

                var speech = "Okay, Spieler " + this.attributes['players'].length + " heißt jetzt " + this.attributes['players'][this.attributes['players'].length - 1].name + ". Ist das richtig?";
                this.emit(':ask', speech, speech);
            }
            this.emit('Unhandled');
        },

        'NumberIntent': function () {

            var number = this.event.request.intent.slots.number.value;

            if (!isNaN(number) && this.attributes["setupState"] == "maxRounds") {
                if (number >= 5) {
                    this.attributes["maxRounds"] = number;
                    speech = "Ich habe " + number + " Runden verstanden. Ist das richtig? ";
                    this.emit(':ask', speech, speech);
                } else {
                    speech = "Es müssen mindestens 5 Runden gespielt werden. ";
                    this.emit(':ask', speech, speech);
                }
            }
            this.emit('Unhandled');
        },

        'ReadyIntent': function () {

            if (this.attributes["setupState"] && this.attributes["setupState"] === "PlayerReady") {

                this.handler.state = constants.states.QUIZ_MODE;
                this.emitWithState('NextRoundIntent');
            }
            this.emit('Unhandled');
        },

        'AMAZON.YesIntent': function () {

            if (this.attributes["setupState"]) {
                if (this.attributes["setupState"] === "RequestRoles") {

                    this.emit('AMAZON.HelpIntent');
                } else if (this.attributes["setupState"] === "IsNameCorrect") {

                    if (this.attributes["players"].length >= 10) {

                        this.attributes["setupState"] = "maxRounds";
                        var speech = "Du hast die maximale Spielerzahl erreicht. Wie viele Runden möchtest du spielen. 10 Runden dauern, bei einer Gruppen-Größe von 2 Spielern, 15 Minuten. ";
                        this.emit(':ask', speech, speech);
                    }
                    this.attributes["setupState"] = "AssignName";

                    var speech = "Okay, möchtest du einen weiteren Spieler hinzufügen.";
                    this.emit(':ask', speech, speech);
                } else if (this.attributes["setupState"] === "AssignName") {

                    var speech = "Nenne mir seinen Namen.";
                    this.emit(':ask', speech, speech);
                } else if (this.attributes["setupState"] === "maxRounds") {

                    if (this.attributes['players'].length == 1) {

                        this.attributes["setupState"] = "PlayerReady";

                        var speech = "Okay. ";
                        for (var i = 0; i < this.attributes["players"].length; i++) {
                            speech += this.attributes["players"][i].name + ", ";
                        }
                        speech += " wenn du bereit bist, sag bereit.";
                        this.emit(':ask', speech, speech);
                    }

                    this.emit('MinigamesIntent');
                } else if (this.attributes["setupState"] === "RequestMinigames") {

                    this.attributes["minigames"] = true;
                    this.attributes["setupState"] = "PlayerReady";

                    var speech = "Okay. ";
                    for (var i = 0; i < this.attributes["players"].length; i++) {
                        speech += this.attributes["players"][i].name + ", ";
                    }
                    speech += " wenn ihr bereit seid, sag bereit.";
                    this.emit(':ask', speech, speech);
                }
            }
            this.emit('Unhandled');
        },
        'AMAZON.NoIntent': function () {

            if (this.attributes["setupState"]) {
                if (this.attributes["setupState"] === "RequestRoles") {

                    this.emit('SetNameIntent');
                } else if (this.attributes["setupState"] === "IsNameCorrect") {

                    this.attributes["setupState"] = "ChangeName";

                    var speech = "Nenne mir seinen Namen.";
                    this.emit(':ask', speech, speech);
                } else if (this.attributes["setupState"] === "AssignName") {

                    this.attributes["setupState"] = "maxRounds";
                    var speech = "Wie viele Runden möchtest du spielen. 10 Runden dauern, bei einer Gruppen-Größe von 2 Spielern, 15 Minuten. ";
                    this.emit(':ask', speech, speech);
                    //this.emit('MinigamesIntent')
                } else if (this.attributes["setupState"] === "maxRounds") {

                    var speech = "Wie viele Runden soll gespielt werden. ";
                    this.emit(':ask', speech, speech);
                } else if (this.attributes["setupState"] === "RequestMinigames") {

                    this.attributes["minigames"] = false;
                    this.attributes["setupState"] = "PlayerReady";

                    var speech = "Okay. ";
                    for (var i = 0; i < this.attributes["players"].length; i++) {
                        speech += this.attributes["players"][i].name + ", ";
                    }
                    speech += " wenn ihr bereit seid, sag bereit.";
                    this.emit(':ask', speech, speech);
                }
            }
            this.emit('Unhandled');
        },

        'AMAZON.HelpIntent': function () {

            var speech = "";
            speech += "";
            speech += "";
            this.emit(':ask', speech, "Sage fortfahren.");
        },
        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            console.log("should end......");

            var self = this;
            dynamo.putUserState(self.event.session, function (data) {
                console.log(data.message);

                var speech = "Okay, auf wiederhören.";
                self.emit(':tell', speech);
            });
        },
        'Unhandled': function () {
            var speech = 'Entschuldigung, Ich habe dich nicht verstanden.';
            this.emit(':ask', speech, speech);
        }
    }),
    mainModeIntentHandlers: Alexa.CreateStateHandler(constants.states.MAIN_MODE, {

        'AnswerIntent': function () {

            var name = this.event.request.intent.slots.answer.value;
            name = name.replace(/\W/g, '');
            name = name.toLowerCase();

            if (name == "a" || name == "b" || name == "c") {
                this.attributes["currentAnswer"] = name;
                this.emitWithState('NextQuestionIntent');
            } else {
                this.emit('Unhandled');
            }
        },
        'NextRoundIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            if (this.attributes["round"] >= this.attributes["maxRounds"]) {
                this.emitWithState('EndIntent', speech);
            }

            this.attributes["round"] = this.attributes["round"] + 1;

            if (this.attributes["players"].length > 1)
                speech += "Runde <say-as interpret-as='number'>" + this.attributes["round"] + "</say-as>. <break time='500ms' />";

            this.emitWithState('NextQuestionIntent', speech);
        },

        'NextWordIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            speech += "Hier kommt das Wort: ";
            this.attributes['currentWord'] = words[Math.floor(Math.random() * words.length) + 1].word;
            var charArr = this.attributes['currentWord'].shuffle().split('');

            for (var i = 0; i < charArr.length; i++) {
                speech += charArr[i] + ". ";
            }
            speech += "<break time='1s'/> Und, hast du eine Idee? ";
            var reprompt = "Und, hast du eine Idee? ";
            this.emit(':ask', speech, reprompt);
        },

        'WordIntent': function (speech) {

            var name = this.event.request.intent.slots.word.value;
            name = name.toLowerCase();

            if (name == this.attributes['currentWord'].toLowerCase())
                var speech = "Richtig! ";
            else
                var speech = "Falsch! ";

            speech += "Die Lösung war " + this.attributes['currentWord'] + ". ";
            this.emitWithState('NextWordIntent', speech);
        },

        'NextQuestionIntent': function (speech) {
            console.log("Intent: NextQuestionIntent");

            if (typeof this.attributes["players"][this.attributes["currentPlayer"]] === 'undefined')
                this.emitWithState('Unhandled');

            if (typeof speech === 'undefined')
                var speech = "";

            var used = JSON.parse(this.attributes['used'] || '{}');
            var currentQuestionId = this.attributes["q"];
            //console.log('answer question=' + currentQuestionId + ' session=', session);

            // Wenn eine Antwort gegeben wurde. Erst das Richtig oder Falsch ausgeben.
            if (this.attributes["currentAnswer"] && this.attributes["currentAnswer"] != "") {

                var q = currentQuestionId ? quiz.getQuestion(currentQuestionId) : null;
                // found question in session; check answer
                if (q) {
                    var answer = this.attributes["currentAnswer"] || 'X';
                    answer = answer.slice(0, 1).toUpperCase();
                    if (q.validAnswers().indexOf(answer) < 0) {
                        answer = 'X';
                    }
                    console.log('answer normalized=' + answer);
                    if (q.isCorrect(answer)) {
                        speech += "<audio src='https://nordicskills.de/audio/MiniGame/correct.mp3' /> Die Antwort ist richtig. Du erhältst einen Punkt .";
                        this.attributes["players"][this.attributes["currentPlayer"]].score += 1;
                    } else {
                        speech += "<audio src='https://nordicskills.de/audio/MiniGame/incorrect.mp3' /> Das war falsch, die richtige Antwort wäre " + q.answerText() + ".";
                    }
                    //say.push(q.explanation());
                    // save question and answer to used questions
                    used[currentQuestionId] = answer;
                }
                this.attributes['used'] = JSON.stringify(used);
                console.log('questions=', Object.keys(used).length);
                this.attributes["currentAnswer"] = "";

                this.attributes["currentPlayer"] = this.attributes["currentPlayer"] + 1;
                if (this.attributes["currentPlayer"] == this.attributes["players"].length) {
                    this.attributes["currentPlayer"] = 0;

                    if (this.attributes["players"].length > 1) {
                        speech += "Runde " + this.attributes["round"] + " ist vorbei. <audio src='https://nordicskills.de/audio/MiniGame/short-transition.mp3' />";

                        if (this.attributes["minigames"] && this.attributes["round"] != 0 && this.attributes["round"] % 5 == 0) {
                            this.attributes = randomMinigame(this.attributes);

                            speech += "Mini Spiel-Runde! <audio src='https://nordicskills.de/audio/MiniGame/roulette.mp3' /> <audio src='https://nordicskills.de/audio/MiniGame/ding.mp3' /> " + minigames[this.attributes["currentMinigameID"]].desc + ". ";

                            var minigame = minigames[this.attributes['currentMinigameID']].name;

                            this.handler.state = "_MINIGAME_" + minigame.toUpperCase();
                            this.emitWithState(minigame + "StartIntent", speech);
                        }

                        if (this.attributes["round"] != 0 && this.attributes["round"] % 6 == 0) {
                            speech += scoreboard(this.attributes);
                        }
                    }
                    this.emitWithState("NextRoundIntent", speech);
                }
            }

            // get next question
            var next = quiz.getNextQuestion(Object.keys(used));
            if (next) {
                //say.push('<s>Question ' + (numQuestions + 1) + '. <break strength="x-strong" /></s>');
                if (this.attributes["players"].length > 1) {
                    speech += "Frage an " + this.attributes["players"][this.attributes["currentPlayer"]].name + ". <break time='500ms' />";
                }
                speech += next.questionAndAnswers();
                this.attributes["q"] = next.id;
                //response.shouldEndSession(false, 'What do you think? Is it ' + next.choices() + '?');
            } else {
                speech += "Das waren alle Fragen. Ab jetzt könnte es zu Wiederholungen kommen. ";
            }

            var reprompt = "Wenn ich die Frage wiederholen soll, sage wiederholen.";

            this.emit(':ask', speech, reprompt);
        },
        'ScoresIntent': function () {

            var speech = scoreboard(this.attributes);
            speech += "<break time='500ms' />";
            this.emitWithState("NextQuestionIntent", speech);
        },
        'RepeatIntent': function () {

            var q = quiz.getQuestion(this.attributes["q"]);
            if (q) {
                var speech = "Ich wiederhole. " + q.questionAndAnswers();
                this.emit(':ask', speech, speech);
            }
        },

        'EndIntent': function (speech) {

            if (typeof speech === 'undefined')
                var speech = "";

            speech += "<audio src='https://nordicskills.de/audio/MiniGame/short-transition.mp3' /> Endstand: <break time='500ms' />";

            var sortedScores = this.attributes["players"].concat().sort(function (a, b) { return (a.score > b.score) ? 1 : ((b.score < a.score) ? -1 : 0); });
            for (var i = 0; i < sortedScores.length; i++) {
                if (i == 0) {
                    speech += "Den letzten Platz belegt ";
                } else if (i == sortedScores.length - 1) {
                    speech += "Der Gewinner des Quiz-Roulettes ist damit ";
                } else {
                    speech += "Platz " + (sortedScores.length - i) + " belegt ";
                }

                speech += sortedScores[i].name + " mit " + (sortedScores[i].score == 1 ? "einem Punkt" : (sortedScores[i].score + " Punkten")) + ". <break time='500ms' />";
            }
            speech += "Glückwunsch! <break time='500ms' /> Warte kurz, um ein neues Spiel zu starten oder sage beenden, um das Quiz zu beenden. <break time='1s' />"

            this.handler.state = constants.states.SETUP_MODE;
            this.emitWithState("ResetIntent", speech);
        },

        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            console.log("should end......");

            var self = this;
            dynamo.putUserState(self.event.session, function (data) {
                console.log(data.message);

                var speech = "Okay, auf wiederhören.";
                self.emit(':tell', speech);
            });
        },
        'Unhandled': function () {
            var speech = 'Entschuldigung, Ich habe dich nicht verstanden.';
            this.emit(':ask', speech, speech);
        }
    })
};

function scoreboard(attributes) {

    var sortedScores = attributes["players"].concat().sort(function (a, b) { return (a.score < b.score) ? 1 : ((b.score > a.score) ? -1 : 0); });

    var speech = "";
    for (var i = 0; i < sortedScores.length; i++) {
        if (i == 0) {
            speech += "Momentan führt ";
        } else if (i == 1) {
            speech += "gefolgt von ";
        } else if (i == sortedScores.length - 1) {
            speech += "Schlusslicht ist ";
        }

        speech += sortedScores[i].name + " mit " + (sortedScores[i].score == 1 ? "einem Punkt" : (sortedScores[i].score + " Punkten")) + ", ";

        if (i > 1 && i == sortedScores.length - 1) {
            speech += "aber Kopf hoch es ist noch nicht vorbei. ";
        }
    }
    speech += "<break time='500ms' />"
    return speech;
}

String.prototype.shuffle = function () {
    var a = this.split(""),
        n = a.length;

    for (var i = n - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
    return a.join("");
}