/*
 * Copyright 2018 Amazon.com, Inc. and its affiliates. All Rights Reserved.
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * http://aws.amazon.com/asl/
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
'use strict';
var logger = require('../utils/logger.js');
var settings = require('../config/settings.js');
var stringSimilarity = require('string-similarity');
var moment = require('moment');

const gameHelper = {
  /*
   *  Given an answer string, attempt to remove unnecessary variance, such
   *  as casing, extra leading or trailing spacing, and convert single digit
   *  numbers to their word forms
   */
  normalizeAnswer: function (answer, locale) {
    let normalizedAnswer = ('' + (answer || '')).toLowerCase();

    // remove any leading/trailing spaces
    normalizedAnswer = normalizedAnswer.replace(/^\s+|\s+$/g, '');

    // remove leading articles, such as 'a', 'an', 'the'
    if (locale == "de-DE") {
      normalizedAnswer = normalizedAnswer.replace(/^(der|die|das)\s+/g, '');

      switch (normalizedAnswer) {
        case '1':
          normalizedAnswer = 'eins';
          break;
        case '2':
          normalizedAnswer = 'zwei';
          break;
        case '3':
          normalizedAnswer = 'drei';
          break;
        case '4':
          normalizedAnswer = 'vier';
          break;
        case '5':
          normalizedAnswer = 'fünf';
          break;
        case '6':
          normalizedAnswer = 'sechs';
          break;
        case '7':
          normalizedAnswer = 'sieben';
          break;
        case '8':
          normalizedAnswer = 'acht';
          break;
        case '9':
          normalizedAnswer = 'neun';
          break;
        case '0':
          normalizedAnswer = 'null';
          break;
      }
    } else {
      normalizedAnswer = normalizedAnswer.replace(/^(a|an|the)\s+/g, '');

      switch (normalizedAnswer) {
        case '1':
          normalizedAnswer = 'one';
          break;
        case '2':
          normalizedAnswer = 'two';
          break;
        case '3':
          normalizedAnswer = 'three';
          break;
        case '4':
          normalizedAnswer = 'four';
          break;
        case '5':
          normalizedAnswer = 'five';
          break;
        case '6':
          normalizedAnswer = 'six';
          break;
        case '7':
          normalizedAnswer = 'seven';
          break;
        case '8':
          normalizedAnswer = 'eight';
          break;
        case '9':
          normalizedAnswer = 'nine';
          break;
        case '0':
          normalizedAnswer = 'zero';
          break;
      }
    }

    return normalizedAnswer;
  },

  shuffleList: function (orderedList) {
    orderedList = orderedList.slice(0);
    for (let i = 0; i < orderedList.length - 1; i++) {
      let j = i + Math.floor(Math.random() * Math.floor(orderedList.length - i));
      let saveIndex = orderedList[i];
      orderedList[i] = orderedList[j];
      orderedList[j] = saveIndex;
    }
    return orderedList;
  }
};

/*
 * This file contains most of the game logic, while the actual intent & event request
 * handler can be found in the gamePlayHandlers.js file.
 *
 * The game loop is as follows
 *  1) Determine where in the game we are beginning, somewhere in middle. Add
 *      any accumulated speech to the response.
 *  2) Find the next question from the ../config/questions.js library
 *  3) Ask the question and return a GameEngine.StartInputHandler directive
 *      https://developer.amazon.com/docs/gadget-skills/receive-echo-button-events.html
 *  4) When a button event comes in, send a voice prompt using the answerQuestion method
 *  5) Process the input, collect the right/wrong verbal response
 *  6) Pass this accumulated speech to the loop and start at the beginning of loop
 */
const Game = {
  supportsDisplay: function (requestEnvelope) {
    var hasDisplay =
      requestEnvelope.context &&
      requestEnvelope.context.System &&
      requestEnvelope.context.System.device &&
      requestEnvelope.context.System.device.supportedInterfaces &&
      requestEnvelope.context.System.device.supportedInterfaces.Display

    return hasDisplay;
  },
  getItemNameById: function (id, locale) {

    for (var i = 0; i < settings.ITEMS.length; i++) {
      if (settings.ITEMS[i].id == id) {
        return locale == 'de-DE' ? settings.ITEMS[i].name_de : settings.ITEMS[i].name_en;
      }
    }
  },
  isInventoryEmpty: function (sessionAttributes) {

    if (sessionAttributes.INVENTORY.length == 0)
      return true;

    for (var i = 0; i < sessionAttributes.INVENTORY.length; i++) {
      if (sessionAttributes.INVENTORY[i].amount > 0 && (Game.getItemById(sessionAttributes.INVENTORY[i].id).type === 'Consumable' || Game.getItemById(sessionAttributes.INVENTORY[i].id).type === 'Multi')) {
        return false;
      }
    }
    return true;
  },
  hasItemInInventory: function (id, sessionAttributes) {

    for (var i = 0; i < sessionAttributes.INVENTORY.length; i++) {
      if (sessionAttributes.INVENTORY[i].id == id && sessionAttributes.INVENTORY[i].amount > 0) {
        logger.debug("Has Item: " + id + ". ");
        return true;
      }
    }
    return false;
  },
  addItemToInventory: function (itemId, itemAmount, sessionAttributes) {

    for (var i = 0; i < sessionAttributes.INVENTORY.length; i++) {
      if (sessionAttributes.INVENTORY[i].id == itemId) {
        sessionAttributes.INVENTORY[i].amount += itemAmount;
        logger.debug(itemAmount + "x Item: " + itemId + " added. ");
        return sessionAttributes;
      }
    }
    sessionAttributes.INVENTORY.push({ id: itemId, amount: itemAmount })
    logger.debug(itemAmount + "x Item: " + itemId + " added. ");
    return sessionAttributes;
  },
  removeItemToInventory: function (id, sessionAttributes) {

    for (var i = 0; i < sessionAttributes.INVENTORY.length; i++) {
      if (sessionAttributes.INVENTORY[i].id == id) {
        sessionAttributes.INVENTORY[i].amount -= 1;
        logger.debug("Item: " + id + " removed. ");
        return sessionAttributes;
      }
    }
    return sessionAttributes;
  },
  getItemById: function (id) {
    for (var i = 0; i < settings.ITEMS.length; i++) {
      if (settings.ITEMS[i].id == id) {
        return settings.ITEMS[i];
      }
    }
  },
  getItemByName: function (name, locale) {

    if (locale == "de-DE") {
      for (var i = 0; i < settings.ITEMS.length; i++) {
        if (settings.ITEMS[i].name_de.toLowerCase() == name.toLowerCase()) {
          return settings.ITEMS[i];
        }
      }
    } else {
      for (var i = 0; i < settings.ITEMS.length; i++) {
        if (settings.ITEMS[i].name_en.toLowerCase() == name.toLowerCase()) {
          return settings.ITEMS[i];
        }
      }
    }
  },
  getCurrentWeekday: function () {
    return moment().format("ddd");
  },
  switchEinsEin: function (number, requestEnvelope) {

    if (number != 1)
      return number;

    if (requestEnvelope.request.locale == 'de-DE') {
      return 'ein';
    } else {
      return number;
    }
  },
  switchEinsEine: function (number, requestEnvelope) {

    if (number != 1)
      return number;

    if (requestEnvelope.request.locale == 'de-DE') {
      return 'eine';
    } else {
      return number;
    }
  },
  switchEinsEineSleep: function (number, requestEnvelope) {

    if (requestEnvelope.request.locale == 'de-DE') {
      if (number != 1 && number != 0)
        return number + ' Stunden';

      if (number === 0)
        return 'weniger als eine Stunde'
      else
        return 'eine Stunde';
    } else {
      if (number != 1 && number != 0)
        return number + ' hours';

      if (number === 0)
        return 'less than one hour'
      else
        return "one hour";
    }
  },
  getHomePic: function (sessionAttributes) {

    if (sessionAttributes.SPECIES === 'katze') {

      if (sessionAttributes.STATE === settings.STATE.SLEEPING_STATE) {
        if (Game.hasItemInInventory(6, sessionAttributes) && Game.hasItemInInventory(7, sessionAttributes)) { // Beides
          return 'Wohnzimmer-Schalfkorb-Katze-mit-Items.jpg';
        } else if (Game.hasItemInInventory(6, sessionAttributes)) { // Wollknäuel
          return 'Wohnzimmer-Schalfkorb-Katze-mit-wollknäuel.jpg';
        } else if (Game.hasItemInInventory(7, sessionAttributes)) { // Kratz-Baum
          return 'Wohnzimmer-Schalfkorb-Katze-mit-kratzbaum.jpg';
        }
        return 'Wohnzimmer-Schalfkorb-Katze-einzeln.jpg';
      }


      if (sessionAttributes.STATS.age >= 1 && sessionAttributes.STATS.age < 30) { // Klein        
        if (Game.hasItemInInventory(6, sessionAttributes) && Game.hasItemInInventory(7, sessionAttributes)) { // Beides
          return 'Wohnzimmer-Katze-klein-einzeln-mit-Items.jpg';
        } else if (Game.hasItemInInventory(6, sessionAttributes)) { // Wollknäuel
          return 'Wohnzimmer-Katze-klein-einzeln-mit-Wolle.jpg';
        } else if (Game.hasItemInInventory(7, sessionAttributes)) { // Kratz-Baum
          return 'Wohnzimmer-Katze-klein-einzeln-mit-kratzbaum.jpg';
        }
        return 'Wohnzimmer-Katze-klein-einzeln.jpg';
      } else if (sessionAttributes.STATS.age >= 30 && sessionAttributes.STATS.age < 60) { // Mittel       
        if (Game.hasItemInInventory(6, sessionAttributes) && Game.hasItemInInventory(7, sessionAttributes)) { // Beides
          return 'Wohnzimmer-Katze-mittel-mit-Items.jpg';
        } else if (Game.hasItemInInventory(6, sessionAttributes)) { // Wollknäuel
          return 'Wohnzimmer-Katze-mittel-mit-Wolle.jpg';
        } else if (Game.hasItemInInventory(7, sessionAttributes)) { // Kratz-Baum
          return 'Wohnzimmer-Katze-mittel-mit-kratzbaum.jpg';
        }
        return 'Wohnzimmer-Katze-mittel-einzeln.jpg';
      } else { // Groß
        if (Game.hasItemInInventory(6, sessionAttributes) && Game.hasItemInInventory(7, sessionAttributes)) { // Beides
          return 'Wohnzimmer-Katze-groß-einzeln-mit-Items.jpg';
        } else if (Game.hasItemInInventory(6, sessionAttributes)) { // Wollknäuel
          return 'Wohnzimmer-Katze-groß-einzeln-Woll.jpg';
        } else if (Game.hasItemInInventory(7, sessionAttributes)) { // Kratz-Baum
          return 'Wohnzimmer-Katze-groß-einzeln-kratzbaum.jpg';
        }
        return 'Wohnzimmer-Katze-groß-einzeln.jpg';
      }
    } else {

      if (sessionAttributes.STATE === settings.STATE.SLEEPING_STATE) {
        if (Game.hasItemInInventory(4, sessionAttributes) && Game.hasItemInInventory(5, sessionAttributes)) { // Beides
          return 'Wohnzimmer-Schalfkorb-Hund-mit-Items.jpg';
        } else if (Game.hasItemInInventory(4, sessionAttributes)) { // Knochen
          return 'Wohnzimmer-Schalfkorb-Hund-mit-knochen.jpg';
        } else if (Game.hasItemInInventory(5, sessionAttributes)) { // Spielball
          return 'Wohnzimmer-Schalfkorb-Hund-mit-bälle.jpg';
        }
        return 'Wohnzimmer-Schalfkorb-Hund-einzeln.jpg';
      }

      if (sessionAttributes.STATS.age >= 1 && sessionAttributes.STATS.age < 30) { // Klein        
        if (Game.hasItemInInventory(4, sessionAttributes) && Game.hasItemInInventory(5, sessionAttributes)) { // Beides
          return 'Wohnzimmer-Hund-klein-einzeln-mit-Items.jpg';
        } else if (Game.hasItemInInventory(4, sessionAttributes)) { // Knochen
          return 'Wohnzimmer-Hund-klein-einzeln-mit-knochen.jpg';
        } else if (Game.hasItemInInventory(5, sessionAttributes)) { // Spielball
          return 'Wohnzimmer-Hund-klein-einzeln-mit-bälle.jpg';
        }
        return 'Wohnzimmer-Hund-klein-einzeln.jpg';
      } else if (sessionAttributes.STATS.age >= 30 && sessionAttributes.STATS.age < 60) { // Mittel
        if (Game.hasItemInInventory(4, sessionAttributes) && Game.hasItemInInventory(5, sessionAttributes)) { // Beides
          return 'Wohnzimmer-Hund-mittel-mit-Items.jpg';
        } else if (Game.hasItemInInventory(4, sessionAttributes)) { // Knochen
          return 'Wohnzimmer-Hund-mittel-mit-knochen.jpg';
        } else if (Game.hasItemInInventory(5, sessionAttributes)) { // Spielball
          return 'Wohnzimmer-Hund-mittel-mit-bälle.jpg';
        }
        return 'Wohnzimmer-Hund-mittel-einzeln.jpg';
      } else { // Groß
        if (Game.hasItemInInventory(4, sessionAttributes) && Game.hasItemInInventory(5, sessionAttributes)) { // Beides
          return 'Wohnzimmer-Hund-groß-einzeln-mit-Items.jpg';
        } else if (Game.hasItemInInventory(4, sessionAttributes)) { // Knochen
          return 'Wohnzimmer-Hund-groß-einzeln-mit-knochen.jpg';
        } else if (Game.hasItemInInventory(5, sessionAttributes)) { // Spielball
          return 'Wohnzimmer-Hund-groß-einzeln-mit-bälle.jpg';
        }
        return 'Wohnzimmer-Hund-groß-einzeln.jpg';
      }
    }
  },
  getCityPic: function (sessionAttributes, requestEnvelope) {

    var randomNr = Math.round(Math.random());

    if (sessionAttributes.SPECIES === "katze") {
      if (requestEnvelope.request.locale === 'de-DE') {
        if (randomNr == 0)
          return 'Stadtplan3D-Deutsch-Katze.jpg';
        else
          return 'Stadtplan-deutsch.jpg';
      } else {
        if (randomNr == 0)
          return 'Stadtplan3D-Englisch-Katze.jpg';
        else
          return 'Stadtplan-englisch.jpg';
      }
    } else {
      if (requestEnvelope.request.locale === 'de-DE') {
        if (randomNr == 0)
          return 'Stadtplan3D-Deutsch-Hund.jpg';
        else
          return 'Stadtplan-deutsch.jpg';
      } else {
        if (randomNr == 0)
          return 'Stadtplan3D-Englisch-Hund.jpg';
        else
          return 'Stadtplan-englisch.jpg';
      }
    }
  },
  randomPetSound: function (sessionAttributes) {
    if (sessionAttributes.SPECIES == 'katze') {
      return 'Meow/' + settings.MEOWS[Math.floor(Math.random() * settings.MEOWS.length)];
    } else {
      return 'Bark/' + settings.BARKS[Math.floor(Math.random() * settings.BARKS.length)];
    }
  },
  buildBuyPrompt: function (amount, id, requestEnvelope) {

    if (requestEnvelope.request.locale == 'de-DE') {
      if (amount >= settings.ITEMS[id].prompts_de.length) {
        return settings.ITEMS[id].prompts_de[settings.ITEMS[id].prompts_de.length - 1].replace('{0}', amount);
      } else {
        return settings.ITEMS[id].prompts_de[amount - 1];
      }
    } else {
      if (amount >= settings.ITEMS[id].prompts_en.length) {
        return settings.ITEMS[id].prompts_en[settings.ITEMS[id].prompts_en.length - 1].replace('{0}', amount);
      } else {
        return settings.ITEMS[id].prompts_en[amount - 1];
      }
    }
  },
  updateAttributes: function (sessionAttributes) {

    let current = moment(moment().format("YYYY-MM-DD"));
    let saved = moment(moment().format("YYYY-MM-DD"));

    if (sessionAttributes.DATE)
      saved = moment(sessionAttributes.DATE);

    let dayDiff = current.diff(moment(saved.format("YYYY-MM-DD")), 'days');

    current = moment();

    let hourDiff = current.diff(saved, 'hours');
    sessionAttributes.DATE = moment().format();

    if (dayDiff < 0)
      dayDiff = 0;
    if (hourDiff < 0)
      hourDiff = 0;

    return Game.calculateStats(sessionAttributes, dayDiff, hourDiff);
  },
  calculateStats: function (sessionAttributes, dayDiff, hourDiff) {

    if (dayDiff >= 1 || hourDiff >= 24)
      sessionAttributes.COINS += settings.MULTIPLIERS.coinsPerDay;

    sessionAttributes.STATS.age += parseInt(dayDiff);

    // Illness Calulation
    sessionAttributes.STATS.illness -= Math.round(hourDiff * settings.MULTIPLIERS.healingMultiplier);
    sessionAttributes.STATS.illness = Math.max(0, sessionAttributes.STATS.illness);

    if (sessionAttributes.STATS.hunger >= settings.MULTIPLIERS.hungerThreshold) {
      sessionAttributes.STATS.illness += Math.round(hourDiff * settings.MULTIPLIERS.hungerIllnessMultiplier);
      sessionAttributes.STATS.illness = Math.min(sessionAttributes.STATS.illness, 100);
    }
    if (sessionAttributes.STATS.thirst >= settings.MULTIPLIERS.thirstThreshold) {
      sessionAttributes.STATS.illness += Math.round(hourDiff * settings.MULTIPLIERS.thirstIllnessMultiplier);
      sessionAttributes.STATS.illness = Math.min(sessionAttributes.STATS.illness, 100);
    }
    if (sessionAttributes.STATS.fatigue >= settings.MULTIPLIERS.fatigueThreshold) {
      sessionAttributes.STATS.illness += Math.round(hourDiff * settings.MULTIPLIERS.fatigueIllnessMultiplier);
      sessionAttributes.STATS.illness = Math.min(sessionAttributes.STATS.illness, 100);
    }

    if (this.hasItemInInventory(5, sessionAttributes) || this.hasItemInInventory(7, sessionAttributes)) {
      sessionAttributes.STATS.boredom -= Math.round(hourDiff * settings.MULTIPLIERS.toyAtHomeMultiplier);
      sessionAttributes.STATS.boredom = Math.max(0, sessionAttributes.STATS.boredom);
    }
    if (sessionAttributes.STATS.boredom >= settings.MULTIPLIERS.boredomThreshold) {
      sessionAttributes.STATS.attachment -= Math.round(hourDiff * settings.MULTIPLIERS.attachmentMultiplier);
      sessionAttributes.STATS.attachment = Math.max(0, sessionAttributes.STATS.attachment);
    }

    sessionAttributes.STATS.hunger += Math.round(hourDiff * settings.MULTIPLIERS.hungerMultiplier);
    sessionAttributes.STATS.hunger = Math.min(sessionAttributes.STATS.hunger, 100);
    sessionAttributes.STATS.thirst += Math.round(hourDiff * settings.MULTIPLIERS.thirstMultiplier);
    sessionAttributes.STATS.thirst = Math.min(sessionAttributes.STATS.thirst, 100);
    if(sessionAttributes.PLACE !== "HOME") {
      sessionAttributes.STATS.fatigue += Math.round(hourDiff * settings.MULTIPLIERS.fatigueMultiplier);
      sessionAttributes.STATS.fatigue = Math.min(sessionAttributes.STATS.fatigue, 100);
    }
    sessionAttributes.STATS.boredom += Math.round(hourDiff * settings.MULTIPLIERS.boredomMultiplier);
    sessionAttributes.STATS.boredom = Math.min(sessionAttributes.STATS.boredom, 100);

    return sessionAttributes;
  },
  saveSleepStart: function (sessionAttributes) {

    sessionAttributes.SLEEP_START = moment().format();
    return sessionAttributes;
  },
  getSleepDiff: function (sessionAttributes) {

    return moment().diff(moment(sessionAttributes.SLEEP_START), 'hours');
  },
  addMinigameReward: function (sessionAttributes) {
    sessionAttributes.STATS.hunger -= settings.MULTIPLIERS.foodUsageMinigame;
    sessionAttributes.STATS.hunger = Math.max(0, sessionAttributes.STATS.hunger);
    sessionAttributes.STATS.thirst -= settings.MULTIPLIERS.waterUsageMinigame;
    sessionAttributes.STATS.thirst = Math.max(0, sessionAttributes.STATS.thirst);
    sessionAttributes.STATS.boredom -= settings.MULTIPLIERS.boredomUsageMinigame;
    sessionAttributes.STATS.boredom = Math.max(0, sessionAttributes.STATS.boredom);
    sessionAttributes.STATS.attachment += settings.MULTIPLIERS.attachmentPlusMinigame;
    sessionAttributes.STATS.attachment = Math.min(sessionAttributes.STATS.attachment, 100);
    sessionAttributes.STATS.fatigue += settings.MULTIPLIERS.fatigueMinigame;
    sessionAttributes.STATS.fatigue = Math.min(sessionAttributes.STATS.fatigue, 100);
    return sessionAttributes;
  },
  generateRandomNames: function (count) {
    var names = settings.NAMES.slice(0);
    var outputArr = [];
    for (var i = 0; i < count; i++) {
      var randIndex = Math.floor(Math.random() * names.length);
      outputArr.push(names[randIndex]);
      names.splice(randIndex, 1);
    }
    return outputArr;
  },
  generateRandomPetName: function (gender) {
    return settings.PET_NAMES[gender][Math.floor(Math.random() * settings.PET_NAMES[gender].length)];
  },
  shuffle: function (array) {
    var i = array.length, j = 0, temp;

    while (i--) {
      j = Math.floor(Math.random() * (i + 1));

      temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  },
  numberAlreadyGuessed: function (arr, number) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i].guess == number)
        return true;
    }
    return false;
  },
  closest: function (arr, x) {

    return arr.reduce(function (prev, curr) {
      return (Math.abs(curr - x) < Math.abs(prev - x) ? curr : prev);
    });
  },

  shuffleArray: function (array) {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  },
  arrayIsBlank: function (array) {

    for (var i = 0; i < array.length; i++) {
      if (array[i] != "" && array[i] != "Ratte" && array[i] != "Rat") {
        return false;
      }
    }
    return true;
  },

  /**
   * Function: askQuestion
   *
   * Function to gather the built responses, add them to the overall response and handle the
   * logic of retrieving and asking the next/same question depending on if the user got it right.
   */
  askQuestion: function (handlerInput, isFollowingAnswer) {
    let {
      requestEnvelope,
      attributesManager
    } = handlerInput;
    let sessionAttributes = attributesManager.getSessionAttributes();
    let ctx = attributesManager.getRequestAttributes();
    let questions = ctx.t('QUESTIONS');

    logger.debug('GAME: askQuestion (currentQuestion = ' + sessionAttributes.currentQuestion + ')');

    let currentQuestion;
    if ('currentQuestion' in sessionAttributes) {
      currentQuestion = parseInt(sessionAttributes.currentQuestion, 10);
    } else {
      currentQuestion = 1;
      sessionAttributes.currentQuestion = 1;
    }

    if (!sessionAttributes.orderedQuestions || (currentQuestion === 1 && !('repeat' in sessionAttributes))) {
      logger.debug('GamePlay: producing ordered question list for new game (using shuffling)!');
      // if this is the first question, then shuffle the questions
      let orderedQuestions = gameHelper.shuffleList(questions.map(q => q.index))
        .slice(0, settings.GAME.QUESTIONS_PER_GAME);
      // and store the ordered list of questions in the attributes
      sessionAttributes.orderedQuestions = orderedQuestions;
    }

    let shuffledQuestionIndex = sessionAttributes.orderedQuestions[currentQuestion - 1];
    let nextQuestion = questions.find(q => q.index == shuffledQuestionIndex);
    logger.debug('Ask question: ' + currentQuestion + ' of ' + settings.GAME.QUESTIONS_PER_GAME +
      ', next question: ' + JSON.stringify(nextQuestion, null, 2));
    if (!nextQuestion || currentQuestion > settings.GAME.QUESTIONS_PER_GAME) {
      /* call the 'endGame' helper to process the end of game logic */
      return Game.endGame(handlerInput);
    } else {

      // Display rendering temp disabled...
      /*if ('correct' in sessionAttributes) {
        // player answered the question - either correctly, or incorrectly
        let messageKey = 'SINGLE_PLAYER_ANSWER_QUESTION_INCORRECT_DISPLAY';
        let image = settings.pickRandom(settings.IMAGES.INCORRECT_ANSWER_IMAGES);
        if (sessionAttributes.correct) {
          messageKey = 'SINGLE_PLAYER_ANSWER_QUESTION_CORRECT_DISPLAY';
          image = settings.pickRandom(settings.IMAGES.CORRECT_ANSWER_IMAGES);
        }
        let responseMessage = ctx.t(messageKey, {
          player_number: sessionAttributes.answeringPlayer
        });
        responseMessage.image = image;
        ctx.render(handlerInput, responseMessage);
      } else {
        // if 'correct' is missing from attributes, this is the first question asked
        let messageKey = currentQuestion === 1 ? 'ASK_FIRST_QUESTION_NEW_GAME_DISPLAY' : 'ASK_FIRST_QUESTION_RESUME_DISPLAY';
        let responseMessage = ctx.t(messageKey);
        ctx.render(handlerInput, responseMessage);
      }*/

      // Use a shorter break for buttonless games
      let answers = ctx.t('QUIZ_ANSWERS').outputSpeech;
      if (nextQuestion.answers) {
        if (nextQuestion.answers.length > 1) {
          answers += nextQuestion.answers.slice(0, -1).join(', ') + ctx.t('QUIZ_ANSWERS_CONNECT').outputSpeech +
            nextQuestion.answers[nextQuestion.answers.length - 1];
        } else {
          answers = nextQuestion.answers[0];
        }
        answers += "?";
      }
      ctx.outputSpeech.push(nextQuestion.question);
      ctx.outputSpeech.push(answers);

      ctx.reprompt.push(answers);

      // Buttonless game - be ready for an answer immediately from the only player
      sessionAttributes.answeringPlayer = 1;

      // Display rendering temp disabled...
      // Buttonless game - render the ui for the question immediately as well
      /*let responseMessage = ctx.t('ASK_QUESTION_DISPLAY', {
        question_number: currentQuestion
      });
      responseMessage.displayText = nextQuestion.question;
      if (typeof sessionAttributes.correct !== 'undefined') {
        responseMessage.image = sessionAttributes.correct ?
        settings.pickRandom(settings.IMAGES.CORRECT_ANSWER_IMAGES) :
        settings.pickRandom(settings.IMAGES.INCORRECT_ANSWER_IMAGES);
      }
      ctx.render(handlerInput, responseMessage);*/
      delete sessionAttributes.correct;
    }
  },

  /**
   * Function for receiving voice input to answer a question
   */
  answerQuestion: function (handlerInput) {
    logger.debug('GAME: answerQuestion');
    let {
      requestEnvelope,
      attributesManager
    } = handlerInput;
    let ctx = attributesManager.getRequestAttributes();
    let sessionAttributes = attributesManager.getSessionAttributes();

    // get the answer out of the request event
    let answer = gameHelper.normalizeAnswer(requestEnvelope.request.intent.slots.answers.value, requestEnvelope.request.locale);

    if (answer == '') {
      delete sessionAttributes.correct;
      let responseMessage = ctx.t('QUIZ_MISUNDERSTOOD_ANSWER');
      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);
      return;
    }

    // get the current question from the question bank so we can compare answers
    let currentQuestionIndex = parseInt(sessionAttributes.currentQuestion || 1, 10);
    let shuffledQuestionIndex = sessionAttributes.orderedQuestions[currentQuestionIndex - 1];
    let questions = ctx.t('QUESTIONS');
    let currentQuestion = questions.find(q => q.index == shuffledQuestionIndex);
    // get the answers
    let answers = currentQuestion.answers.map(a => gameHelper.normalizeAnswer(a, requestEnvelope.request.locale));
    // get the correct answer
    let correct_answer = gameHelper.normalizeAnswer(currentQuestion.correct_answer, requestEnvelope.request.locale);
    // use a string similarity match to determine if the spoken answer is close to one of the supplied values
    var matches = stringSimilarity.findBestMatch(answer, answers);

    logger.debug("COMPARING '" + answer + "' to [" + answers + "]: (" + matches.ratings.length + " matches)");
    // flag to determine if we have a match
    var answered = false;
    // loop through all the matches, in case there is more than one that is good enough to consider
    for (var i = 0; i < matches.ratings.length; ++i) {
      var match = matches.ratings[i];
      if (match.rating > settings.GAME.ANSWER_SIMILARITY) {
        // Answer is the correct answer
        if (match.target == correct_answer) {
          // move onto the next question
          sessionAttributes.currentQuestion += 1;

          // check to see if we are keeping score yet
          if (!('scores' in sessionAttributes)) {
            sessionAttributes.scores = {};
            sessionAttributes.scores[sessionAttributes.answeringPlayer] = 1;
            // if this player already has a score, increment it
          } else if (sessionAttributes.answeringPlayer in sessionAttributes.scores) {
            sessionAttributes.scores[sessionAttributes.answeringPlayer] += 1;
            // otherwise just start a new score
          } else {
            sessionAttributes.scores[sessionAttributes.answeringPlayer] = 1;
          }

          let responseMessage = ctx.t('QUIZ_CORRECT_ANSWER_DURING_PLAY', { 'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'Meow/meowing-cat-4.mp3' : 'Bark/small-dog-bark-7.mp3' });

          // To change the correct answer sound, replace AUDIO.CORRECT_ANSWER_AUDIO
          // with your audio clip by updating config/settings.js
          ctx.outputSpeech.push(responseMessage.outputSpeech);
          // mark that the user answered this correct
          sessionAttributes.correct = true;

          logger.debug("Answer provided matched one of the expected answers!");
          answered = true;
          break;
        }
      }
    }
    // If we looped through the answer without a match
    if (answered === false) {
      // In a buttonless game we will not repeat the question, just mark is wrong and move to the next one
      sessionAttributes.currentQuestion += 1;
      let responseMessage = ctx.t('QUIZ_INCORRECT_ANSWER_DURING_PLAY', { 'answer': correct_answer, 'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'Meow/cat-meow-6.mp3' : 'Misc/dog-whine-0.mp3' });
      ctx.outputSpeech.push(responseMessage.outputSpeech);
      sessionAttributes.correct = false;
    }
    // Move on to asking the same/next question depending on the state
    Game.askQuestion(handlerInput, true);
  },

  /**
   *  Processes a request to end the current game and ends the session.
   *
   *  Depending the 'resetGame' parameter, the game state may be saved or reset.
   *  When the game is finished, 'resetGame' is always set to TRUE.
   */
  endGame: function (handlerInput) {
    logger.debug("GAME: end game");
    let ctx = handlerInput.attributesManager.getRequestAttributes();
    let sessionAttributes = handlerInput.attributesManager.getSessionAttributes();

    let responseMessage = ctx.t('QUIZ_FINISHED_ALL_WRONG');
    if ('scores' in sessionAttributes && sessionAttributes.scores[1] > 0) {
      responseMessage = ctx.t('QUIZ_FINISHED', { 'score': Game.switchEinsEine(sessionAttributes.scores[1], handlerInput.requestEnvelope), 'coin_reward': sessionAttributes.scores[1] * 10 });
      sessionAttributes.COINS += sessionAttributes.scores[1] * 10;
    }

    ctx.outputSpeech.push(responseMessage.outputSpeech);

    // Clean up some attributes to set up for the next game
    delete sessionAttributes.answeringPlayer;
    delete sessionAttributes.currentQuestion;
    delete sessionAttributes.correct;
    delete sessionAttributes.scores;
  }
};
module.exports = Game;