/*
* Copyright 2018 Amazon.com, Inc. and its affiliates. All Rights Reserved.
* Licensed under the Amazon Software License (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
* http://aws.amazon.com/asl/
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/
'use strict';

const Alexa = require('ask-sdk');
const settings = require('./config/settings.js');

/**
 * Import interceptors and handlers for the different skill states.
 */
const GlobalHandlers = require('./handlers/globalHandlers.js');
const StartHandlers = require('./handlers/startHandlers.js');
const GamePlayHandlers = require('./handlers/gamePlayHandlers.js');
const MiniGameHandlers = require('./handlers/miniGameHandlers.js');

/**
 * Lambda setup.
 */
exports.handler = function (event, context) {
  let factory = Alexa.SkillBuilders.standard()
    .addRequestHandlers(
      GamePlayHandlers.HomeHandler,
      GamePlayHandlers.CityHandler,
      GamePlayHandlers.ParkHandler,
      GamePlayHandlers.ParkWalkHandler,
      GamePlayHandlers.SupermarketHandler,
      GamePlayHandlers.VetHandler,
      GamePlayHandlers.VetExamineHandler,
      GamePlayHandlers.VetMedicateHandler,
      GamePlayHandlers.YesHandler,
      GamePlayHandlers.NoHandler,
      GamePlayHandlers.PetHandler,
      GamePlayHandlers.SleepHandler,
      GamePlayHandlers.BackpackHandler,
      GamePlayHandlers.StatHandler,
      GamePlayHandlers.SkipTutorialHandler,
      GamePlayHandlers.ComeComeHandler,
      GamePlayHandlers.UseItemHandler,
      GamePlayHandlers.BuyItemHandler,
      GamePlayHandlers.FeedHandler,
      GamePlayHandlers.LookAgeHandler,
      GamePlayHandlers.WaitHandler,
      GamePlayHandlers.ParkRepromptHandler,
      MiniGameHandlers.PetQuizHandler,
      MiniGameHandlers.PetQuizAnswerHandler,
      MiniGameHandlers.PetQuizDontKnowHandler,
      MiniGameHandlers.MemoryHandler,
      MiniGameHandlers.MemoryGuessHandler,
      MiniGameHandlers.WhoAmIHandler,
      MiniGameHandlers.WhoAmIGuessHandler,
      MiniGameHandlers.ShellGameHandler,
      MiniGameHandlers.ShellGameGuessHandler,
      MiniGameHandlers.StickWoolHandler,
      MiniGameHandlers.StickWoolGuessHandler,
      MiniGameHandlers.StickWoolYesHandler,
      MiniGameHandlers.StickWoolNoHandler,
      MiniGameHandlers.GuessSpeciesHandler,
      MiniGameHandlers.GuessSpeciesAnswerHandler,
      MiniGameHandlers.GuessSpeciesRepeatHandler,
      StartHandlers.YesHandler,
      StartHandlers.NoHandler,
      StartHandlers.LaunchPlayGameHandler,
      StartHandlers.SpeciesHandler,
      StartHandlers.NameHandler,
      StartHandlers.KeepHandler,
      StartHandlers.ReturnHandler,
      StartHandlers.SkipTutorialHandler,
      GlobalHandlers.CoinsHandler,
      GlobalHandlers.SkipDaysHandler,
      GlobalHandlers.SkipHoursHandler,
      GlobalHandlers.AddCoinsHandler,
      GlobalHandlers.HealPetHandler,
      GlobalHandlers.StatsHandler,
      GlobalHandlers.ResetSaveHandler,
      GlobalHandlers.HelpHandler,
      GlobalHandlers.StopCancelHandler,
	  GlobalHandlers.RepeatHandler,
      GlobalHandlers.SessionEndedRequestHandler,
      GlobalHandlers.DefaultHandler
    )
    .addRequestInterceptors(GlobalHandlers.RequestInterceptor)
    .addResponseInterceptors(GlobalHandlers.ResponseInterceptor)
    .addErrorHandlers(GlobalHandlers.ErrorHandler);

  if (settings.APP_ID) {
    factory.withSkillId(settings.APP_ID);
  }

  if (settings.STORAGE.SESSION_TABLE) {
    factory.withTableName(settings.STORAGE.SESSION_TABLE)
      .withAutoCreateTable(true);
  }

  let skill = factory.create();

  return skill.invoke(event, context);
}