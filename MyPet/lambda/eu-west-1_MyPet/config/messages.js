/*
 * Copyright 2018 Amazon.com, Inc. and its affiliates. All Rights Reserved.
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * http://aws.amazon.com/asl/
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
'use strict';

const questions = require('./questions');
const whoAmI = require('./whoAmI');
const guessSpecies = require('./guessSpecies');

const messages = {
  en: {
    translation: {
      'QUESTIONS': questions.questions_en_US,
      'WHOAMI': whoAmI.whoAmI_en_US,
      'GUESSSPECIES': guessSpecies.guessSpecies_en_US,
      'UNHANDLED_REQUEST': {
        outputSpeech: "Sorry, I didn't understand you. ",
        reprompt: "Please repeat what you wanted to say or ask for help if you're not sure. "
      },
      'GOOD_BYE': {
        outputSpeech: "Okay, goodbye. ",
        reprompt: ''
      },
      'RESET_REQUEST': {
        outputSpeech: "Are you sure you want to reset your save game? This will cause you to lose the entire game progress. ",
        reprompt: 'Reset your save game? Yes or No. '
      },
      'ERROR': {
        outputSpeech: 'An error occurred. Repeat what you wanted to say or say stop and try again later. '
      },
      'CURRENT_COINS': {
        outputSpeech: 'You currently have {{coins}} coins. '
      },

      'HELP_REPROMPT': {
        reprompt: 'Say help to find out what you can say. '
      },

      //
      //--------------------  Help Related Prompts -------------------------------------------------
      //
      'GENERAL_HELP': {
        outputSpeech: "With reset save game you can delete your current save game and start over. " +
          "You help always refers to the place you are visiting. " +
          "What do you want to do now? To repeat the help, say help again. ",
        reprompt: "What do you want to do now? To repeat the help, say help again. "
      },
      'HOME_HELP': {
        outputSpeech: "If you want to visit another place, say outside. You can also find out what {{name}} is missing by asking questions such as, " +
          "is {{name}} thirsty. It is also important that {{name}} sleeps daily as it affects {{gender_1}} health. To send {{gender_2}} to sleep, say go to sleep. " +
          "It's best to come back tomorrow so that {{gender_3}} can recover. " +
          "What do you want to do now? To repeat the help, say help again.",
        reprompt: "What do you want to do now? To repeat the help, say help again. "
      },
      'PARK_HELP': {
        outputSpeech: "Every day except Thursday, there are two mini-games alternating. Before each mini-game you will get a little guide on how the game works. " +
          "In addition, you can go for a walk with {{name}} here because movement is important for {{gender_1}}. To get back to town, say back to town. " +
          "What do you want to do now? To repeat the help, say help again.",
        reprompt: "What do you want to do now? To repeat the help, say help again. "
      },
      'CITY_HELP': {
        outputSpeech: "From here you will get to all places. You can have your pet examined or treated at the vet. The supermarket has all the products you need for pet care. " +
          "In the park you have the opportunity to walk with {{name}} or play mini-games to earn coins. At home, you can give {{name}} {{gender_1}} earned sleep. What do you want to do now? " +
          "What do you want to do now? To repeat the help, say help again.",
        reprompt: "What do you want to do now? To repeat the help, say help again. "
      },
      'VET_HELP': {
        outputSpeech: "The veterinarian is the fastest way to find out what is wrong with {{name}}. You can have {{gender_1}} examined preventively or treated here. " +
          "What do you want to do now? To repeat the help, say help again.",
        reprompt: "What do you want to do now? To repeat the help, say help again. "
      },
      'SUPERMARKET_HELP': {
        outputSpeech: "You will probably visit the supermarket almost every day. Here you can find various products for pet care. You can say assortment " +
          "to get a list of all offered items with their prices. To buy an item, simply state the quantity and the product name. " +
          "For example: five times food or a water. To leave the store, just say go outside. " +
          "What do you want to do now? To repeat the help, say help again.",
        reprompt: "What do you want to do now? To repeat the help, say help again. "
      },

      //
      //--------------------  Start Game Related Prompts -------------------------------------------
      //
      'START_GAME': {
        outputSpeech: '<audio src="https://nordicskills.de/audio/MeinHaustier/doorbell-short.mp3"/> Do you want to go to the door? ',
        reprompt: 'Would you like to go to the door? Say yes or no. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Eingangsbereich-Tür-geschlossen.jpg'
      },
      'DONT_OPEN': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/doorbell-long.mp3'/> Don't you want to go to the door? ",
        reprompt: "Don't you want to go to the door? Say yes or no.",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Eingangsbereich-Tür-geschlossen.jpg'
      },
      'STILL_DONT_OPEN': {
        outputSpeech: "Maybe you'll have more time tomorrow. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Eingangsbereich-Tür-geschlossen.jpg'
      },
      'OPEN_DOOR': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/door-open.mp3'/> Weird, there's no one at the door. " +
          "Do you hear that? <audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound_1}}'/> You turn on the light and see a basket in front of your feet. " +
          "Ooh. What's that? There is a small {{species_sweet}} in the basket. <audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound_2}}'/>" +
          "It's pretty cold outside, so you take the basket together the {{species_sweet}} and close the door. <audio src='https://nordicskills.de/audio/MeinHaustier/door-close.mp3'/> " +
          "Do you want to keep it or take it to a shelter? Say keep or shelter. ",
        reprompt: "Do you want to keep it or take it to a shelter? Say keep or shelter. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },

      'RETURN_PET': {
        outputSpeech: "You make your way with the basket to the next shelter. <audio src='https://nordicskills.de/audio/MeinHaustier/door-open-close.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/streets-short.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/bell-ring-01.mp3'/> " +
          "You hand off the {{species_sweet}} at the reception. Actually, you always wanted a pet. " +
          "Now that we're here, we could take a look around. Would you like to take a look around? ",
        reprompt: "Would you like to look around? Say yes or no. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Shelter.jpg'
      },
      'RETURN_AROUND': {
        outputSpeech: "Someone shows you a dog and a cat. What do you like more? Say dog or cat. ",
        reprompt: "What do you like more? Say dog or cat. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Shelter.jpg'
      },
      'RETURN_EXIT': {
        outputSpeech: "You leave the shelter with {{name}}. Do you want to give {{gender_1}} a new name? ",
        reprompt: "Do you want to give {{gender_1}} a new name? Say yes or no. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Eingangsbereich-Tür-geschlossen.jpg'
      },

      'KEEP_PET': {
        outputSpeech: "You will find a name tag in the basket. {{gender_1}} seems to be called {{name}}. Do you want to give {{gender_2}} a new name? ",
        reprompt: "Do you want to give {{gender_2}} a new name? Say yes or no. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },

      'NEW_NAME': {
        outputSpeech: "What do you want to call {{gender_1}}? Note: Simple names make it easier for {{gender_1}} to understand you. ",
        reprompt: "Give me {{gender_2}} name?",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'CONFIRM_NAME': {
        outputSpeech: "You want to name {{gender_1}} {{name}} right? ",
        reprompt: "You want to name {{gender_1}} {{name}} right? ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'NAME_REQUEST': {
        outputSpeech: "Say {{gender_3}} name. ",
        reprompt: "Say {{gender_3}} name. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'NAME_WRONG_TWICE': {
        outputSpeech: "{{name}} is a nice name. Don't you want to keep it? " +
          "Say yes if you want to keep the name or no if you want to give a own name. ",
        reprompt: "Say Yes if you want to keep the name or No if you want to give a own name. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },

      //
      //--------------------  Welcome Related Prompts -------------------------------------------
      //
      'START_HOME': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}'/> You are home and ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'START_CITY': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}'/> You are in town and ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'START_PARK': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}'/> You are in the park and ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },



      'WHATS_NEXT_1': {
        outputSpeech: "What do we want to do? ",
        reprompt: "Do you want to stay here or go to another place? If you don't know how. Say help. "
      },
      'WHATS_NEXT_2': {
        outputSpeech: "What do we want to do today? ",
        reprompt: "Do you want to stay here or go to another place? If you don't know how. Say help. "
      },
      'WHATS_NEXT_3': {
        outputSpeech: "Do you already have an idea what you want to do? ",
        reprompt: "Do you want to stay here or go to another place? If you don't know how. Say help. "
      },

      //
      //--------------------  Tutorial Related Prompts -------------------------------------------
      //
      'TUTORIAL_START': {
        outputSpeech: "I'm going to give you a short introduction to care for {{name}}. If you don't need the introduction, say: Alexa, skip. " +
          "Let's get started. <break time='1s'/> You need to make sure that {{gender_1}} sleeps enough so that {{gender_1}} doesn't get sick. A balanced daily life makes {{name}} feel comfortable with you. " +
          "You must also make sure that {{gender_1}} does not get too hungry or thirsty. Ask if {{name}} is hungry. ",
        reprompt: "Say: Is {{name}} hungry? ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'TUTORIAL_HUNGER': {
        outputSpeech: "Perfect, with such questions you can find out what {{name}} is missing. Plus, over time, using {{name}}'s sounds, you'll develop a sense of what {{gender_1}} needs. " +
          "Now it's time for some fresh air. Say go outside. ",
        reprompt: "Say go outside. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'TUTORIAL_CITY': {
        outputSpeech: "From here you can visit different places. Say help to find out what places there are and what you can do there. " +
          "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Oh. It looks like {{name}} is hungry by now. We should go to the supermarket first to get food. Say to the supermarket. ",
        reprompt: "Say to the supermarket. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'TUTORIAL_SUPERMARKET': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/folding-door-open-01.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/doorbell-ding-dong-01.mp3'/> You get 50 coins every day. In the course of the game, you can earn more. If you want to know how many coins you currently have, " +
          "say how many coins do I have. With them you can buy food, for example. Say buy food. ",
        reprompt: "Say buy food. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Supermarkt-gefüllt-ohne-Logo.jpg'
      },
      'TUTORIAL_FEED': {
        outputSpeech: "To feed {{name}}, say give food or use food. ",
        reprompt: "Say give food. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Supermarkt-gefüllt-ohne-Logo.jpg'
      },
      'TUTORIAL_END': {
        outputSpeech: "You can also look in your backpack to see what you have with you. Just say backpack for this. <break time='1s'/> All right, all right. " +
          "Okay, I will let you go now, you can still ask for help at any place and get individual help. " +
          "Let's go home for now. <break time='2s'/>"
      },

      //
      //--------------------  Game Play Related Prompts -------------------------------------------
      //     
      'ALREADY_HOME': {
        outputSpeech: "You're already at home. "
      },
      'ALREADY_CITY': {
        outputSpeech: "You're already in town. "
      },
      'ALREADY_PARK': {
        outputSpeech: "You're already in the park. "
      },
      'ALREADY_SUPERMARKET': {
        outputSpeech: "You're already in the Supermarket. "
      },
      'ALREADY_VET': {
        outputSpeech: "You are already at the vet. "
      },

      'EXIT_HOME': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/door-open-close.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/streets-short.mp3'/> "
      },
      'EXIT_PARK': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/streets-short.mp3'/> "
      },
      'EXIT_SUPERMARKET': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/folding-door-open-01.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/streets-short.mp3'/> "
      },
      'EXIT_VET': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/folding-door-open-01.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/streets-short.mp3'/> "
      },

      'CITY_WELCOME_FIRST': {
        outputSpeech: "You have a map in front of you. You can go to the park, to the nearest supermarket, to the vet or back home. ",
        reprompt: 'Where do you want to go, say to the park, to the supermarket, home or to the vet. '
      },
      'CITY_WELCOME_1': {
        outputSpeech: "You can go to the park, to the nearest supermarket, vet or back home. ",
        reprompt: 'Where would you like to go, say to the park, to the supermarket, home or to the vet. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'CITY_WELCOME_1_SHORT': {
        outputSpeech: "Where would you like to go? ",
        reprompt: 'Where would you like to go, say to the park, to the supermarket, home or to the vet. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'CITY_WELCOME_2': {
        outputSpeech: "Today is nice weather, do you want to go to the park? Or do you have something else to do? ",
        reprompt: 'Where would you like to go, say to the park, to the supermarket, home or to the vet. '
      },
      'CITY_WELCOME_3': {
        outputSpeech: "You haven't been shopping for a long time, how about going to the supermarket. ",
        reprompt: 'Where would you like to go, say to the park, to the supermarket, home or to the vet. '
      },
      'CITY_WELCOME_4': {
        outputSpeech: "{{name}} is all turned up. If you fancy some mini-games, say to the park. ",
        reprompt: 'Where would you like to go, say to the park, to the supermarket, home or to the vet. '
      },
      'CITY_WELCOME_ILL': {
        outputSpeech: "{{name}} hasn't been so well for a few days. Maybe we should go to the vet. ",
        reprompt: 'Where would you like to go, say to the park, to the supermarket, home or to the vet. '
      },
      'CITY_WELCOME_RAIN': {
        outputSpeech: "*Rain noise* Boah, it's pouring out of buckets today. Do you want to go somewhere today or go back home? ",
        reprompt: 'Where would you like to go, say to the park, to the supermarket, home or to the vet. '
      },

      'HOME_WELCOME': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/door-open-close.mp3'/> You're home again. Do you want to do something with {{name}} or get {{gender_1}} some sleep? " +
          "Say help to find out what you can do with {{name}} at home. ",
        reprompt: 'Say help to find out what you can do with {{name}} at home. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },

      'VET_WELCOME': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/folding-door-open-01.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/bell-ring-01.mp3'/> You are at the vet, you want to have {{name}} examined or treated. ",
        reprompt: 'Say Examine, Treat, or go outside. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'VET_EXAMINE': {
        outputSpeech: "You wait in the waiting room until {{name}} has been examined. <audio src='https://nordicskills.de/audio/MeinHaustier/waiting-short.mp3'/> Thanks for waiting. "
      },
      'VET_EXAMINE_FINE': {
        outputSpeech: "{{gender_1}} it's fine, you don't have to worry about {{gender_2}}. Say outside. ",
        reprompt: 'Say outside. '
      },
      'VET_EXAMINE_BAD': {
        outputSpeech: "{{gender_1}} is a little thin for {{gender_2}} age. Make sure that {{gender_1}} eats and drinks enough. Say outside. ",
        reprompt: 'Say outside. '
      },
      'VET_EXAMINE_VERY_BAD': {
        outputSpeech: "{{gender_1}} is not so well. You should have {{gender_2}} treated here. Say treat or go outside. ",
        reprompt: 'Say to treat or go outside. '
      },
      'VET_MEDICATE_NEEDED': {
        outputSpeech: "The treatment costs {{cost}} coins. Do you want to treat {{name}}? ",
        reprompt: 'Do you want to treat {{name}}? Say yes or no. '
      },
      'VET_MEDICATE_NOT_ENOUGH_COINS': {
        outputSpeech: "Unfortunately, you don't have enough coins for the treatment. It's best to come back quickly if you've saved enough coins. "
      },
      'VET_MEDICATE_NEEDED_YES': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/cash.mp3'/> Take a seat until {{name}} has been treated. <audio src='https://nordicskills.de/audio/MeinHaustier/waiting-long.mp3'/> Thanks for waiting. " +
          "{{name}} should be fine again in 1-2 nights. "
      },
      'VET_MEDICATE_NEEDED_NO': {
        outputSpeech: "Are you sure? The treatment is really necessary. Say yes or no. ",
        reprompt: 'Are you sure? The treatment is really necessary. Say yes or no. '
      },
      'VET_MEDICATE_NEEDED_NO_AGAIN': {
        outputSpeech: "Okay, then maybe come back later. "
      },
      'VET_MEDICATE_NOT_NEEDED': {
        outputSpeech: "No treatment is needed. "
      },

      'PARK_WELCOME': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Misc/park-short.mp3'/> There are different activities in the park for you and {{name}}. ",
      },
      'PARK_WELCOME_1': {
        outputSpeech: "Do you want to play pet quiz or memory? Alternatively, you can go for a walk with {{name}} or go back to town. ",
        reprompt: 'Say quiz, memory, go for a walk or go into town. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'PARK_WELCOME_2': {
        outputSpeech: "Do you want to play shell game or Who am I? Alternatively, you can go for a walk with {{name}} or go back to town. ",
        reprompt: 'Say Who am I, shell game, walk or go into town. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'PARK_WELCOME_3': {
        outputSpeech: "Do you want to play guess the species or throwing {{StickWool}}? Alternatively, you can go for a walk with {{name}} or go back to town. ",
        reprompt: 'Say throwing {{StickWool}}, guess the species, go for a walk or into the city. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'PARK_WELCOME_EMPTY': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Misc/park-short.mp3'/> The park is empty today. Maybe we should just go for a walk today and come back tomorrow. Say go for a walk or back to town. ",
        reprompt: 'Say go for a walk or back to town. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'PARK_WALK': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Misc/park-long.mp3'/> {{name}} looks tired. I think we can go home. Or do you have anything else to do? ",
        reprompt: 'Where would you like to go, say to the park, to the supermarket, home or to the vet. '
      },
      'PARK_WALK_GO_TO_PARK': {
        outputSpeech: "We should go in the park to go for a walk. If you want to go to the park, say to the park. ",
        reprompt: 'Where would you like to go, say to the park, to the supermarket, home or to the vet. '
      },
      'PARK_NO_LEASH': {
        outputSpeech: "You can only enter the park with {{name}} with a leash. You should get one at the supermarket. Where would you like to go, say to the park, to the supermarket, home or to the vet. ",
        reprompt: 'Where would you like to go, say to the park, to the supermarket, home or to the vet. '
      },

      'SUPERMARKET_WELCOME': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/folding-door-open-01.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/doorbell-ding-dong-01.mp3'/> You have {{coins}} coins. The store offers the following products: {{entries}}" +
          "Specify the quantity and the product you want to buy. For example, food one time. ",
        reprompt: "Name the quantity and the product you want to buy. For example, food one time. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Supermarkt-gefüllt-ohne-Logo.jpg'
      },
      'SUPERMARKET_OFFER': {
        outputSpeech: "The store offers the following products: {{entries}}" +
          "Specify the quantity and the product you want to buy. For example, food one time. ",
        reprompt: "Name the quantity and the product you want to buy. For example, food one time. "
      },
      'SUPERMARKET_ENTRY': {
        outputSpeech: "{{item_name}} for {{item_price}} coins"
      },
      'SUPERMARKET_BUY': {
        outputSpeech: "Do you want to buy {{item_amount_name}} for {{item_price}} coins? ",
        reprompt: "Would you like to buy {{item_amount_name}} for {{item_price}} coins? Say Yes or No. "
      },
      'SUPERMARKET_NOT_ENOUGH_COINS': {
        outputSpeech: "You don't have enough coins to buy {{item_amount_name}}. "
      },
      'SUPERMARKET_SUCCESS': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/cash.mp3'/> {{item_amount_name}} bought. "
      },
      'SUPERMARKET_PROMPT': {
        outputSpeech: "Do you want to continue shopping or leave the store. Say assortment, leave store or place another order. ",
        reprompt: "Do you want to continue shopping or leave the store? Say assortment, leave store or place another order. "
      },
      'SUPERMARKET_USE_OR_BUY': {
        outputSpeech: "You still have {{item_name}}. Do you want to use it or buy more? Say use or buy. ",
        reprompt: "Say use, buy or leave the store. "
      },

      'BACKPACK': {
        outputSpeech: "You have {{entries}} in your backpack. "
      },
      'BACKPACK_ENTRY': {
        outputSpeech: "{{item_amount_name}}"
      },
      'BACKPACK_EMPTY': {
        outputSpeech: "Your backpack is empty right now. "
      },
      'BACKPACK_USE_ITEM_FOOD': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Chewing/{{pet_sound}}'/> {{name}} looks happy. "
      },
      'BACKPACK_USE_ITEM_TREAT': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Chewing/{{pet_sound}}'/> {{name}} is happy, that was a good idea. "
      },
      'BACKPACK_USE_ITEM_WATER': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Drinking/{{pet_sound}}'/> {{name}} looks happy. "
      },
      'BACKPACK_USE_ITEM_TOY_WOOLBALL': {
        outputSpeech: "You give {{name}} the wool ball. <audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}'/> {{gender_1}} looks happy. "
      },
      'BACKPACK_USE_ITEM_TOY_BONE': {
        outputSpeech: "You give {{name}} the bone. <audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}'/> {{gender_1}} looks happy. "
      },
      'BACKPACK_USE_ITEM_NOT_USABLE': {
        outputSpeech: "{{name}} cannot be used. It will be used permanently. "
      },
      'BACKPACK_USE_ITEM_NOT_OWNED': {
        outputSpeech: "You don't have {{item_name}} in your backpack. "
      },

      'SLEEP': {
        outputSpeech: "{{name}} goes to sleep. <audio src='https://nordicskills.de/audio/MeinHaustier/Sleeping/{{pet_sound}}'/> Come back in a few hours or tomorrow. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'STILL_SLEEPING': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Sleeping/{{pet_sound}}'/> {{name}} is still asleep. Do you want to wake {{gender_1}} up? ",
        reprompt: "Do you want to wake up {{name}}? Say Yes or No. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },

      'SLEEP_PERFECT': {
        outputSpeech: "{{name}} slept {{sleep_time}} and is fully rested. "
      },
      'SLEEP_GOOD': {
        outputSpeech: "{{name}} slept {{sleep_time}} and is well rested. "
      },
      'SLEEP_OKAY': {
        outputSpeech: "{{name} slept {{sleep_time}} and hasn't yet fully rested, but would be ready for a few hours in the park. "
      },
      'SLEEP_BAD': {
        outputSpeech: "{{name}} slept {{sleep_time}} and is still dead tired. You might want to let {{gender_1}} sleep for a few more hours. "
      },
      'SLEEP_ALREADY_AWAKE': {
        outputSpeech: "{{name}} is already awake and fully rested. "
      },

      'CONTINUE_SLEEPING': {
        outputSpeech: "Okay then let's keep {{name}} sleeping. Come back in a few hours or tomorrow. "
      },

      'PET': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Misc/{{pet_sound}}'/> "
      },
      'COME_COME': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}'/> "
      },
      'FEED_PET': {
        outputSpeech: "You can feed {{name}} by saying give food or use food. If you don't have food, you can buy some at the supermarket. Say give food or go to the supermarket. ",
        reprompt: "Say give food or go to the supermarket. "
      },
      'WAIT': {
        outputSpeech: "Okay, lets take a break. <break time='3s'/> {{name}} is bored, don't you want to go for a walk or do something else? ",
        reprompt: "Say go for a walk or do something else. "
      },

      //
      //--------------------  Mini Game Related Prompts -------------------------------------------
      //            
      'QUIZ_INTRO': {
        outputSpeech: "Glad you decided to play pet quiz. " +
          "I ask you three questions in total and you have the chance to earn up to 30 coins. <break time='1s'/> Let's start with the first question. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Park-Spielbrett-Quiz.jpg'
      },
      'QUIZ_ANSWERS': {
        outputSpeech: "<break time='1s'/> Is it "
      },
      'QUIZ_ANSWERS_CONNECT': {
        outputSpeech: ", or, "
      },
      'QUIZ_MISUNDERSTOOD_ANSWER': {
        outputSpeech: "Sorry, I didn't understand you. Please try again! ",
        reprompt: "Please repeat your answer. "
      },
      'QUIZ_CORRECT_ANSWER_DURING_PLAY': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> That's right! Good work. "
      },
      'QUIZ_INCORRECT_ANSWER_DURING_PLAY': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> This was wrong. The correct answer was: {{answer}}. "
      },
      'QUIZ_FINISHED': {
        outputSpeech: "You answered {{score}} of 3 questions correctly. For this, you'll get {{coin_reward}} coins. <audio src='https://nordicskills.de/audio/MeinHaustier/muenzen.mp3'/>"
      },
      'QUIZ_FINISHED_ALL_WRONG': {
        outputSpeech: "You've unfortunately answered all the questions wrong, but well, next time you may have some questions. "
      },
      'QUIZ_SKIP_QUESTION': {
        outputSpeech: "Okay, let's try the next question. "
      },
      'QUIZ_SKIP_LAST_QUESTION': {
        outputSpeech: "Okay, that was the last question. "
      },

      'MEMORY_INTRO': {
        outputSpeech: "So you want to play a round of memory. " +
          "You have 9 face-down cards in front of you and have to name 2 numbers gradually. These cards then get revealed. " +
          "The goal is to find pairs. <break time='500ms'/> You can cancel the mini-game at any time by returning to the park.<break time='500ms'/> Let's get started: " +
          "Give me two numbers between 1 and 9. ",
        reprompt: "Give me two numbers between 1 and 9. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Park-Spielbrett-Memory.jpg'
      },
      'MEMORY_REVEAL': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MiniGame/card-flip-{{random_1}}.mp3' />  {{card_1}} is below the <say-as interpret-as='ordinal'>{{card_nr_1}}</say-as> card. <break time='500ms'/>" +
          "<audio src='https://nordicskills.de/audio/MiniGame/card-flip-{{random_2}}.mp3' />  {{card_2}} is below the <say-as interpret-as='ordinal'>{{card_nr_2}}</say-as> card. <break time='500ms'/>"
      },
      'MEMORY_CORRECT': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Correct, you earned 10 coins. <break time='500ms' />"
      },
      'MEMORY_INCORRECT': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Sorry, no pair. <break time='500ms'/>"
      },
      'MEMORY_INCORRECT_INPUT': {
        outputSpeech: "The numbers must be between 1 and 9. ",
        reprompt: "Give me two numbers between 1 and 9. ",
      },
      'MEMORY_ALREADY_REVEALED': {
        outputSpeech: "{{number}} has already been revealed. Give me two numbers between 1 and 9. ",
        reprompt: "Give me two numbers between 1 and 9. ",
      },
      'MEMORY_NEXT': {
        outputSpeech: "Give me two numbers between 1 and 9. ",
        reprompt: "Give me two numbers between 1 and 9. "
      },
      'MEMORY_END': {
        outputSpeech: "Okay, that was all the cards. You earned {{reward}} of 40 possible coins. "
      },


      'WHOAMI_INTRO': {
        outputSpeech: "This game is about guessing a person or an item. I'll give you tips little by little until you find the answer. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Park-Spielbrett-Wer-bin-ich.jpg'
      },
      'WHOAMI_CORRECT': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> The answer was correct! You needed {{hints}} clues. You earned {{reward}} coins. <audio src='https://nordicskills.de/audio/MeinHaustier/muenzen.mp3'/> "
      },
      'WHOAMI_INCORRECT': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> The answer is incorrect. "
      },
      'WHOAMI_FIRST_HINT': {
        outputSpeech: "First clue. "
      },
      'WHOAMI_LAST_HINT': {
        outputSpeech: "Last clue. "
      },
      'WHOAMI_SOLUTION': {
        outputSpeech: "The answer was {{solution}}. Unfortunately, no coins for you. "
      },
      'WHOAMI_REPROMPT': {
        reprompt: "If you don't know the answer. Say next. "
      },

      'SHELL_INTRO': {
        outputSpeech: "I'll put a marbel under one of three cups and swap them back and forth. After that, you give me a number between 1 and 3. <break time='1s'/>" +
          "Okay, let's go. <audio src='https://nordicskills.de/audio/MiniGame/shell.mp3' />" +
          "Hey, hey. Under what cup is the marbel? Give me a number between 1 and 3. ",
        reprompt: "Under what hat is the marbel? Give me a number between 1 and 3. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Park-Spielbrett-Hütchenspiel.jpg'
      },
      'SHELL_END': {
        outputSpeech: "Okay, <audio src='https://nordicskills.de/audio/MiniGame/shell_end.mp3' /> the marbel was under the <say-as interpret-as='ordinal'>{{winningShell}}</say-as> cup. "
      },
      'SHELL_WIN': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> You guessed right. You get 3 treats. <break time='1s'/>"
      },
      'SHELL_LOSE': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> You guessed wrong. Maybe you'll have better luck next time. <break time='1s'/>"
      },
      'SHELL_INCORRECT_INPUT': {
        outputSpeech: "The number must be between 1 and 3. ",
        reprompt: "Give me a number between 1 and 3. "
      },

      'STICK_WOOL_INTRO': {
        outputSpeech: "We play throwing {{stickWool_1}}. A {{stickWool_2}} will be thrown. " +
          "3 other park visitors and you, will then estimate one after the other, how long {{name}} will take to bring back {{stickWool_3}}. " +
          "In turns, you estimate a number between one and 60 seconds. The closest one gets the most coins. <break time='1s'/>" +
          "Here are the estimates: {{name_1}} estimates {{guess_1}},{{name_2}} {{guess_2}}, and {{name_3}} {{guess_3}} seconds. " +
          "Now it's your turn. Name a number between one and 60 seconds. ",
        reprompt: "Name a number between one and 60 seconds. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Park-Spielbrett-Stöckchen-werfen.jpg'
      },
      'STICK_WOOL_CONFIRM': {
        outputSpeech: "I understood {{number}} is that correct? ",
        reprompt: "I understood {{number}} is that right? "
      },
      'STICK_WOOL_ALREADY_USED': {
        outputSpeech: "I'm sorry. Unfortunately, the number has already been mentioned. Please name another one. ",
        reprompt: "Give me a number between one and 60. "
      },
      'STICK_WOOL_INCORRECT_INPUT': {
        outputSpeech: "Give me a number between one and 60. ",
        reprompt: "Give me a number between one and 60. "
      },
      'STICK_WOOL_FINISHED': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/throw.mp3' /><audio src='https://nordicskills.de/audio/MeinHaustier/Misc/running-dog.mp3' /> {{name}} took {{time}} seconds to get back to you. You placed <say-as interpret-as='ordinal'>{{rank}}</say-as> and earned {{reward}} coins. "
      },

      'GUESS_INTRO': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Animal/sheep.mp3'/> Did you listen carefully, we play guess the animal species. " +
          "You're about to hear an animal sound and you have to guess what kind of animal it came from. " +
          "We are playing a total of 3 rounds, for each hit you will receive coins. " +
          "Attention, here comes the first animal sound: <audio src='https://nordicskills.de/audio/MeinHaustier/Animal/{{animal_sound}}'/> " +
          "Do you have an idea. Just name the answer or say don't know. ",
        reprompt: "You can also say repeat to hear the sound again. <break time='1s'/>" +
          "Just name the answer or say next if you don't know. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Park-Spielbrett-Arten-raten.jpg'
      },
      'GUESS_WIN': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Right, the answer was {{animal}}. "
      },
      'GUESS_LOSE': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Sorry, the correct answer was {{animal}}. No coins for this round. "
      },
      'GUESS_SKIP': {
        outputSpeech: "Okay, the answer was {{animal}}. No coins for this round. "
      },
      'GUESS_NEXT': {
        outputSpeech: "Here comes the {{nextLast}} animal: <audio src='https://nordicskills.de/audio/MeinHaustier/Animal/{{animal_sound}}'/> Just name the answer or say don't know. ",
        reprompt: "Just name the answer or say don't know. "
      },
      'GUESS_REPEAT': {
        outputSpeech: "Here comes the sound: <audio src='https://nordicskills.de/audio/MeinHaustier/Animal/{{animal_sound}}'/> Just name the answer or say don't know. ",
        reprompt: "Just name the answer or say don't know. "
      },
      'GUESS_END': {
        outputSpeech: "You guessed {{correct}} of 3 sounds. For this, you will receive {{reward}} coins. "
      },


      'STAT_GENERAL': {
        outputSpeech: "Ask questions to find out what's wrong with {{name}}. For example, you can ask: Is {{name}} thirsty? You can also ask if {{gender_1}} is hungry, thirsty, tired, or sick. <break time='1s'/> "
      },

      'STAT_ALL_GOOD': {
        outputSpeech: "{{name}} looks happy. "
      },

      'STAT_HUNGER_LOW': {
        outputSpeech: "{{name}} is not hungry. "
      },
      'STAT_HUNGER_MID': {
        outputSpeech: "{{name}} is a little hungry. "
      },
      'STAT_HUNGER_HIGH_NO_FOOD': {
        outputSpeech: "{{name}} is very hungry. We should go to the supermarket and buy food as soon as possible. "
      },
      'STAT_HUNGER_HIGH_FOOD': {
        outputSpeech: "{{name}} is very hungry. You should have some food with you. "
      },

      'STAT_ILLNESS_LOW': {
        outputSpeech: "{{name}} is fine. "
      },
      'STAT_ILLNESS_MID': {
        outputSpeech: "{{name}} is not so well. You might want to visit the vet on the occasion. "
      },
      'STAT_ILLNESS_HIGH': {
        outputSpeech: "{{name}} is not good at all. You should urgently bring {{gender_1}} to the vet. "
      },

      'STAT_THIRST_LOW': {
        outputSpeech: "{{name}} is not thirsty. "
      },
      'STAT_THIRST_MID': {
        outputSpeech: "{{name}} is a bit thirsty. "
      },
      'STAT_THIRST_HIGH_NO_WATER': {
        outputSpeech: "{{name}} is very thirsty. We should go to the supermarket and buy water as soon as possible. "
      },
      'STAT_THIRST_HIGH_WATER': {
        outputSpeech: "{{name}} is very thirsty. You should have some water with you. "
      },

      'STAT_FATIGUE_LOW': {
        outputSpeech: "{{name}} is well slept. "
      },
      'STAT_FATIGUE_MID': {
        outputSpeech: "{{name}} is a little tired. "
      },
      'STAT_FATIGUE_HIGH': {
        outputSpeech: "{{name}} is dead tired. You should put {{gender_1}} to sleep at home. "
      },

      'STAT_BOREDOM_LOW': {
        outputSpeech: "{{name}} is not bored. "
      },
      'STAT_BOREDOM_MID': {
        outputSpeech: "{{name}} is a bit bored. "
      },
      'STAT_BOREDOM_HIGH': {
        outputSpeech: "{{name}} is very bored. You should urgently do something with {{gender_1}}. "
      },

      'TOO_FATIGUED_PARK': {
        outputSpeech: "{{name}} is too tired to go to the park. You should go home and put {{gender_1}} to sleep. "
      },
      'TOO_ILL_PARK': {
        outputSpeech: "{{name}} is not well. You should take {{gender_1}} to the vet. "
      },

      'LOOK_AGE_CAT': {
        outputSpeech: "{{name}} has a striped fur, especially noticeable on {{gender_1}} tail. {{gender_2}} has big eyes and is very playful. "
      },
      'LOOK_AGE_DOG': {
        outputSpeech: "{{name}} is dark soft fur. In some places it is light brown. {{gender_1}} has cute eyes and loves to be petted. "
      }
    }
  },

  //
  // To override by territory follow the below pattern
  //
  // For additional information on translations and formatting messages see https://www.i18next.com/
  //
  'de-DE': {
    translation: {
      'QUESTIONS': questions.questions_de_DE,
      'WHOAMI': whoAmI.whoAmI_de_DE,
      'GUESSSPECIES': guessSpecies.guessSpecies_de_DE,
      'UNHANDLED_REQUEST': {
        outputSpeech: "Entschuldigung, ich habe dich leider nicht verstanden. ",
        reprompt: "Bitte wiederhole was du sagen wolltest oder frage nach Hilfe, wenn du dir nicht sicher bist. "
      },
      'GOOD_BYE': {
        outputSpeech: "Okay, auf wiedersehen. ",
        reprompt: ''
      },
      'RESET_REQUEST': {
        outputSpeech: "Möchtest du den Spielstand wirklich zurücksetzen? Hierdurch verlierst du den gesamten Spielfortschritt. ",
        reprompt: 'Spielstand zurücksetzen? Ja oder Nein. '
      },
      'ERROR': {
        outputSpeech: 'Es ist ein Fehler aufgetreten. Wiederhole was du sagen wolltest oder sage stop und versuche es später nochmal. '
      },
      'CURRENT_COINS': {
        outputSpeech: 'Du besitzt derzeit {{coins}} Münzen. '
      },

      'HELP_REPROMPT': {
        reprompt: 'Sage Hilfe um zu erfahren, was du sagen kannst. '
      },

      //
      //--------------------  Help Related Prompts -------------------------------------------------
      //
      'GENERAL_HELP': {
        outputSpeech: "Du kannst jederzeit mit Spielstand zurücksetzen, deinen aktuellen Spielstand löschen und von vorn beginnen. " +
          "Die Hilfe bezieht sich immer auf den Ort, den du gerade besuchst. " +
          "Was möchtest du jetzt tun? Um die Hilfe zu wiederholen, sage nochmal Hilfe.",
        reprompt: "Was möchtest du jetzt tun? Um die Hilfe zu wiederholen, sage nochmal Hilfe. "
      },
      'HOME_HELP': {
        outputSpeech: "Wenn du einen anderen Ort besuchen willst, sage nach draußen. Du kannst außerdem herausfinden was {{name}} fehlt, indem du Fragen stellst, " +
          "wie z.B., Hat {{name}} durst. <break time='250ms'/> Wichtig ist auch, das {{name}} möglichst täglich schläft, denn dies hat Auswirkung auf {{gender_1}} Gesundheit. Um {{gender_2}} schlafen zu schicken, sage schlafen gehen. " +
          "Komm dann am besten später wieder, damit {{gender_3}} sich erholen kann. " +
          "Was möchtest du jetzt tun? Um die Hilfe zu wiederholen, sage nochmal Hilfe.",
        reprompt: "Was möchtest du jetzt tun? Um die Hilfe zu wiederholen, sage nochmal Hilfe. "
      },
      'PARK_HELP': {
        outputSpeech: "Jeden Tag außer Donnerstags, erwarten dich hier zwei <phoneme alphabet='ipa' ph='ˈmɪni'>Mini</phoneme>-Spiele im <phoneme alphabet='ipa' ph='ˈvɛksl̩'>Wechsel</phoneme>. <break time='250ms'/> Vor jedem <phoneme alphabet='ipa' ph='ˈmɪni'>Mini</phoneme>-Spiel bekommst du eine kleine Anleitung wie das Spiel funktioniert. " +
          "Zusätzlich kannst du hier jederzeit mit {{name}} spazieren gehen, denn Bewegung ist wichtig für {{gender_1}}. <break time='200ms'/> Um wieder zurück in die Stadt zu kommen, sage zurück in die Stadt. " +
          "Was möchtest du jetzt tun? Um die Hilfe zu wiederholen, sage nochmal Hilfe.",
        reprompt: "Was möchtest du jetzt tun? Um die Hilfe zu wiederholen, sage nochmal Hilfe. "
      },
      'CITY_HELP': {
        outputSpeech: "Von hier aus gelangst du zu allen Orten. <break time='200ms'/> Beim Tierarzt kannst du dein Haustier untersuchen oder behandeln lassen. Der Supermarkt hat alle Produkte, die du für die Tierpflege brauchst. " +
          "Im Park hast du die Möglichkeit mit {{name}} gassi zu gehen oder in Mini spielen, münzen dazu zu verdienen. <break time='200ms'/> Zuhause kannst du {{name}} {{gender_1}} verdienten Schlaf gönnen. " +
          "Was möchtest du jetzt tun? Um die Hilfe zu wiederholen, sage nochmal Hilfe.",
        reprompt: "Was möchtest du jetzt tun? Um die Hilfe zu wiederholen, sage nochmal Hilfe. "
      },
      'VET_HELP': {
        outputSpeech: "Beim Tierarzt kannst du am schnellsten Herausfinden, was mit {{name}} nicht stimmt. <break time='200ms'/> Du kannst {{gender_1}} hier vorsorglich untersuchen oder behandeln lassen. " +
          "Was möchtest du jetzt tun? Um die Hilfe zu wiederholen, sage nochmal Hilfe.",
        reprompt: "Was möchtest du jetzt tun? Um die Hilfe zu wiederholen, sage nochmal Hilfe. "
      },
      'SUPERMARKET_HELP': {
        outputSpeech: "Den Supermarkt wirst du wahrscheinlich so gut wie jeden Tag besuchen. Hier gibt es diverse Produkte zur Tierpflege. <break time='250ms'/> Du kannst Sortiment sagen, " +
          "um eine Liste alle angebotenen Artikel mit ihren Preisen zu bekommen. <break time='200ms'/> Um einen Artikel zu kaufen, nenne einfach die Anzahl und den Produkt-Namen. " +
          "Zum Beispiel: fünf mal Futter oder ein mal Wasser. <break time='250ms'/> Um den Laden wieder zu verlassen, sage einfach nach draußen. " +
          "Was möchtest du jetzt tun? Um die Hilfe zu wiederholen, sage nochmal Hilfe.",
        reprompt: "Was möchtest du jetzt tun? Um die Hilfe zu wiederholen, sage nochmal Hilfe. "
      },

      //
      //--------------------  Start Game Related Prompts -------------------------------------------
      //
      'START_GAME': {
        outputSpeech: '<audio src="https://nordicskills.de/audio/MeinHaustier/doorbell-short.mp3"/> Möchtest du zur <phoneme alphabet="ipa" ph="̯tyːɐ">Tür</phoneme> gehen? ',
        reprompt: 'Möchtest du zur <phoneme alphabet="ipa" ph="̯tyːɐ">Tür</phoneme> gehen. Sage Ja oder Nein.',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Eingangsbereich-Tür-geschlossen.jpg'
      },
      'DONT_OPEN': {
        outputSpeech: '<audio src="https://nordicskills.de/audio/MeinHaustier/doorbell-long.mp3"/> Möchtest du nicht doch zur <phoneme alphabet="ipa" ph="̯tyːɐ">Tür</phoneme> gehen? ',
        reprompt: 'Möchtest du nicht doch zur <phoneme alphabet="ipa" ph="̯tyːɐ">Tür</phoneme> gehen? Sage Ja oder Nein. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Eingangsbereich-Tür-geschlossen.jpg'
      },
      'STILL_DONT_OPEN': {
        outputSpeech: 'Vielleicht hast du morgen mehr Zeit. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Eingangsbereich-Tür-geschlossen.jpg'
      },
      'OPEN_DOOR': {
        outputSpeech: '<audio src="https://nordicskills.de/audio/MeinHaustier/door-open.mp3"/> Komisch, es ist niemand vor der Tür. ' +
          "<break time='250ms'/> Hörst du das auch? <audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound_1}}'/> Du schaltest das Licht an und siehst einen Korb vor deinen Füßen. <break time='250ms'/> " +
          "Huch. Was ist denn das? In dem Korb sitzt ein kleines {{species_sweet}}. <audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound_2}}'/>" +
          'Es ist ganz schön kalt draußen, deshalb nimmst du den Korb sammt {{species_sweet}} mit und schließt die Tür. <audio src="https://nordicskills.de/audio/MeinHaustier/door-close.mp3"/> ' +
          "Möchtest du es behalten oder zu einen Tierheim bringen. Sage behalten oder abgeben. ",
        reprompt: "Möchtest du es behalten oder zu einen Tierheim bringen. Sage behalten oder abgeben.",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },

      'RETURN_PET': {
        outputSpeech: "Du machst dich mit dem Korb auf dem Weg zum nächsten Tierheim. <audio src='https://nordicskills.de/audio/MeinHaustier/door-open-close.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/streets-short.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/bell-ring-01.mp3'/> " +
          "Du gibst das {{species_sweet}} am Empfang ab. <break time='500ms'/> Eigentlich hattest du dir doch schon immer ein Haustier gewünscht. " +
          "<break time='200ms'/> Und wenn wir schonmal hier sind, können wir uns ja auch mal umsehen. Und? möchtest du dich umsehen? ",
        reprompt: "Möchtest du dich Umsehen? Sage Ja oder Nein. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Shelter.jpg'
      },
      'RETURN_AROUND': {
        outputSpeech: "Jemand zeigt dir ein Hund und eine Katze. Was gefällt dir mehr? <break time='200ms'/> Sage Hund oder Katze. ",
        reprompt: "Was gefällt dir besser? Sage Hund oder Katze. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Shelter.jpg'
      },
      'RETURN_EXIT': {
        outputSpeech: "Du verlässt das Tierheim mit {{name}}. Möchtest du {{gender_1}} einen neuen Namen geben? ",
        reprompt: "Möchtest du {{gender_1}} einen neuen Namen geben? Sage Ja oder Nein. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Eingangsbereich-Tür-geschlossen.jpg'
      },

      'KEEP_PET': {
        outputSpeech: "Du findest ein Namensschild im Körbchen. {{gender_1}} scheint wohl {{name}} zu heißen. Möchtest du {{gender_2}} einen neuen Namen geben? ",
        reprompt: "Möchtest du {{gender_2}} einen neuen Namen geben? Sage Ja oder Nein. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },

      'NEW_NAME': {
        outputSpeech: "Wie möchtest du {{gender_1}} nennen? Tipp: Einfache Namen machen es für {{gender_1}} leichter dich zu verstehen. ",
        reprompt: "Nenne mir {{gender_2}} Namen?",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'CONFIRM_NAME': {
        outputSpeech: "Du möchtest {{gender_1}} {{name}} nennen, richtig? ",
        reprompt: "Du möchtest {{gender_1}} {{name}} nennen, richtig? ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'NAME_REQUEST': {
        outputSpeech: "Nenne {{gender_3}} Namen. ",
        reprompt: "Nenne {{gender_3}} Namen. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'NAME_WRONG_TWICE': {
        outputSpeech: "{{name}} ist doch ein schöner Name. Möchtest du ihn nicht vielleicht doch behalten. " +
          "Sage Ja, wenn du den Namen behalten willst oder Nein, wenn du einen eigenen Namen vergeben möchtest. ",
        reprompt: "Sage Ja, wenn du den Namen behalten willst oder Nein, wenn du einen eigenen Namen vergeben möchtest. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },

      //
      //--------------------  Tutorial Related Prompts -------------------------------------------
      //
      'TUTORIAL_START': {
        outputSpeech: "Ich werde dir eine kurze Einführung zum Umgang mit {{name}} geben. Wenn du die Einführung nicht brauchst, sage: Alexa, überspringen. " +
          "Legen wir los. <break time='1s'/> Du musst darauf achten, dass {{gender_1}} genug schläft, damit {{gender_1}} nicht krank wird. Ein ausgewogener Alltag sorgt dafür, dass {{name}} sich bei dir wohl fühlt. " +
          "Du musst auch dafür Sorgen, dass {{gender_1}} nicht zu hungrig oder durstig wird. <break time='300ms'/> Frage doch gleich einmal ob {{name}} hungrig ist. ",
        reprompt: "Sage: Hat {{name}} hunger? ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'TUTORIAL_HUNGER': {
        outputSpeech: "Super, mit solchen Fragen kannst du herausfinden was {{name}} fehlt. Außerdem wirst du mit der Zeit anhand {{name}}s Geräuschen, ein Gespühr entwickeln was {{gender_1}} braucht. <break time='300ms'/> " +
          "Nun ist es Zeit für etwas frische Luft. Sage nach draußen. ",
        reprompt: "Sage nach draußen. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'TUTORIAL_CITY': {
        outputSpeech: "Von hier aus kannst du unterschiedliche Orte besuchen. Sag Hilfe, um zu erfahren, welche Orte es gibt und was du dort unternehmen kannst. " +
          "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> <say-as interpret-as='interjection'>oje</say-as>. Es sieht so aus als hätte {{name}} inzwischen Hunger. Wir sollten zuerst zum Supermarkt gehen, um Futter zu besorgen. Sage zum Supermarkt. ",
        reprompt: "Sage zum Supermarkt. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'TUTORIAL_SUPERMARKET': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/folding-door-open-01.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/doorbell-ding-dong-01.mp3'/> Du erhälst täglich 50 Münzen. Im Spielverlauf kannst du dir weitere dazu verdienen. Wenn du wissen möchtest, wie viele du aktuell hast, " +
          "sage wie viele Münzen habe ich. Mit ihnen kannst du z.B. Futter kaufen. <break time='200ms'/> Versuch es einmal und sage Futter kaufen. ",
        reprompt: "Sage Futter kaufen. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Supermarkt-gefüllt-ohne-Logo.jpg'
      },
      'TUTORIAL_FEED': {
        outputSpeech: "Um {{name}} zu füttern, sage Futter geben oder Futter benutzen. ",
        reprompt: "Sage Futter geben. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Supermarkt-gefüllt-ohne-Logo.jpg'
      },
      'TUTORIAL_END': {
        outputSpeech: "Du kannst auch in deinen Rucksack schauen, um zu sehen, was du dabei hast. Sage dafür einfach Rucksack. <break time='1s'/> Na gut. " +
          "Ich denke du kommst ab jetzt alleine zu recht, du kannst trotzdem an jedem Ort nach Hilfe fragen um individuelle Hilfe zubekommen. " +
          "Gehen wir für jetzt erstmal wieder nach Hause. <break time='2s'/>"
      },

      //
      //--------------------  Welcome Related Prompts -------------------------------------------
      //
      'START_HOME': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}'/> Du bist Zuhause und ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'START_CITY': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}'/> Du bist in der Stadt und ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'START_PARK': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}'/> Du bist im Park und ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },



      'WHATS_NEXT_1': {
        outputSpeech: "Was wollen wir tun? ",
        reprompt: 'Möchtest du weiter hier warten oder zu einem anderen Ort. Wenn du nicht weißt wie. Sage Hilfe. '
      },
      'WHATS_NEXT_2': {
        outputSpeech: "Was wollen wir jetzt tun? ",
        reprompt: 'Möchtest du weiter hier warten oder zu einem anderen Ort. Wenn du nicht weißt wie. Sage Hilfe. '
      },
      'WHATS_NEXT_3': {
        outputSpeech: "Hast du schon eine Idee was du tun möchtest? ",
        reprompt: 'Möchtest du weiter hier warten oder zu einem anderen Ort. Wenn du nicht weißt wie. Sage Hilfe. '
      },

      //
      //--------------------  Game Play Related Prompts -------------------------------------------
      //     
      'ALREADY_HOME': {
        outputSpeech: "Du bist schon zu Hause. "
      },
      'ALREADY_CITY': {
        outputSpeech: "Du bist schon in der Stadt. "
      },
      'ALREADY_PARK': {
        outputSpeech: "Du bist schon im Park. "
      },
      'ALREADY_SUPERMARKET': {
        outputSpeech: "Du bist schon im Supermarkt. "
      },
      'ALREADY_VET': {
        outputSpeech: "Du bist schon beim Tierarzt. "
      },

      'EXIT_HOME': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/door-open-close.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/streets-short.mp3'/> "
      },
      'EXIT_PARK': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/streets-short.mp3'/> "
      },
      'EXIT_SUPERMARKET': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/folding-door-open-01.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/streets-short.mp3'/> "
      },
      'EXIT_VET': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/folding-door-open-01.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/streets-short.mp3'/> "
      },

      'CITY_WELCOME_FIRST': {
        outputSpeech: "Vor dir ist ein Stadtplan. Du kannst in den Park, zum nächsten Supermarkt, zum Tierarzt oder wieder nach Hause gehen. ",
        reprompt: 'Wo möchtest du hin gehen, sage in den Park, zum Supermarkt, nach Hause oder zum Tierarzt. '
      },
      'CITY_WELCOME_1': {
        outputSpeech: "Du kannst in den Park, zum nächsten Supermarkt, Tierarzt oder wieder nach Hause gehen. ",
        reprompt: 'Wo möchtest du hin gehen, sage in den Park, zum Supermarkt, nach Hause oder zum Tierarzt. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'CITY_WELCOME_1_SHORT': {
        outputSpeech: "Wo möchtest du hin gehen? ",
        reprompt: 'Wo möchtest du hin gehen, sage in den Park, zum Supermarkt, nach Hause oder zum Tierarzt. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'CITY_WELCOME_2': {
        outputSpeech: "Heute ist schönes Wetter hast du Lust in den Park? Oder hast du was anderes vor. ",
        reprompt: 'Wo möchtest du hin gehen, sage in den Park, zum Supermarkt, nach Hause oder zum Tierarzt. '
      },
      'CITY_WELCOME_3': {
        outputSpeech: "Du warst schon lange nicht mehr Einkaufen, wie wärs in den Supermarkt zu gehen. ",
        reprompt: 'Wo möchtest du hin gehen, sage in den Park, zum Supermarkt, nach Hause oder zum Tierarzt. '
      },
      'CITY_WELCOME_4': {
        outputSpeech: "{{name}} ist ganz aufgedreht. Hast du Lust auf ein paar Minispiele, dann sage zum Park. ",
        reprompt: 'Wo möchtest du hin gehen, sage in den Park, zum Supermarkt, nach Hause oder zum Tierarzt. '
      },
      'CITY_WELCOME_ILL': {
        outputSpeech: "{{name}} geht es schon seit einigen Tagen nicht so gut. Vielleicht sollten wir zum Tierarzt gehen. ",
        reprompt: 'Wo möchtest du hin gehen, sage in den Park, zum Supermarkt, nach Hause oder zum Tierarzt. '
      },
      'CITY_WELCOME_RAIN': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/rain-out.mp3'/> Boah, es schüttet heute wie aus Eimern. Möchtest du heute noch wo hin oder doch lieber zurück nach Hause. ",
        reprompt: 'Wo möchtest du hin gehen, sage in den Park, zum Supermarkt, nach Hause oder zum Tierarzt. '
      },

      'HOME_WELCOME': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/door-open-close.mp3'/> Du bist wieder zu Hause. Möchtest du noch etwas mit {{name}} unternehmen oder {{gender_1}} schlafen legen? " +
          "Sage Hilfe um zu erfahren, was du zuhause mit {{name}} machen kannst. ",
        reprompt: 'Sage Hilfe um zu erfahren, was du zuhause mit {{name}} machen kannst. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },

      'VET_WELCOME': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/folding-door-open-01.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/bell-ring-01.mp3'/> Du bist in der Tierpraxis, möchtest du {{name}} untersuchen oder behandeln lassen.",
        reprompt: 'Sage Untersuchen, Behandeln oder nach draußen. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'VET_EXAMINE': {
        outputSpeech: "Du wartest im Wartezimmer bis {{name}} untersucht wurde. <audio src='https://nordicskills.de/audio/MeinHaustier/waiting-short.mp3'/> Danke fürs warten. "
      },
      'VET_EXAMINE_FINE': {
        outputSpeech: "{{gender_1}} geht es prächtig, du musst dir keine sorgen machen. <break time='250ms'/> Sage nach draußen. ",
        reprompt: 'Sage nach draußen. '
      },
      'VET_EXAMINE_BAD': {
        outputSpeech: "{{gender_1}} ist etwas dünn für {{gender_2}} Alter. Achte darauf, das {{gender_1}} genug frisst und trinkt. <break time='250ms'/> Sage nach draußen. ",
        reprompt: 'Sage nach draußen. '
      },
      'VET_EXAMINE_VERY_BAD': {
        outputSpeech: "{{gender_1}} geht es leider nicht so gut. Du solltest {{gender_2}} hier behandeln lassen. Sage behandeln oder nach draußen. ",
        reprompt: 'Sage nach behandeln oder nach draußen. '
      },
      'VET_MEDICATE_NEEDED': {
        outputSpeech: "Die Behandlung kostet {{cost}} Münzen. Soll {{name}} behandelt werden? ",
        reprompt: 'Soll {{name}} behandelt werden? Sage Ja oder Nein. '
      },
      'VET_MEDICATE_NOT_ENOUGH_COINS': {
        outputSpeech: "Du hast leider zurzeit nicht genug Münzen für die Behandlung. Komm am besten schnell wieder vorbei, " +
          "wenn du genug Münzen gespart hast. "
      },
      'VET_MEDICATE_NEEDED_YES': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/cash.mp3'/> Nimm noch einen Augenblick platz bis {{name}} behandelt wurde. <audio src='https://nordicskills.de/audio/MeinHaustier/waiting-long.mp3'/> Danke fürs warten. " +
          "{{name}} sollte in ein, zwei Nächten wieder fit sein. "
      },
      'VET_MEDICATE_NEEDED_NO': {
        outputSpeech: "Bist du dir sicher? Die Behandlung ist wirklich nötig. Sage Ja oder Nein. ",
        reprompt: 'Bist du dir sicher? Die Behandlung ist wirklich nötig. Sage Ja oder Nein. '
      },
      'VET_MEDICATE_NEEDED_NO_AGAIN': {
        outputSpeech: "Okay, dann komm vielleicht später nochmal vorbei. "
      },
      'VET_MEDICATE_NOT_NEEDED': {
        outputSpeech: "Es ist keine Behandlung nötig. "
      },

      'PARK_WELCOME': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Misc/park-short.mp3'/> Im Park gibt es verschiedene Beschäftigungsmöglichkeiten für dich und {{name}}. ",
      },
      'PARK_WELCOME_1': {
        outputSpeech: "Möchtest du Haustierquiz oder Memory spielen? Alternativ kannst du auch mit {{name}} spazieren gehen oder zurück in die Stadt. ",
        reprompt: 'Sage Quiz, Memory, spazieren gehen oder in die Stadt. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'PARK_WELCOME_2': {
        outputSpeech: "Möchtest du Hütchenspiel oder Wer bin ich spielen? Alternativ kannst du auch mit {{name}} spazieren gehen oder zurück in die Stadt. ",
        reprompt: 'Sage Wer bin ich, Hütchenspiel, spazieren gehen oder in die Stadt. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'PARK_WELCOME_3': {
        outputSpeech: "Möchtest du Tierarten raten oder {{stickWool}} werfen spielen? Alternativ kannst du auch mit {{name}} spazieren gehen oder zurück in die Stadt. ",
        reprompt: 'Sage {{stickWool}} werfen, Tierarten raten, spazieren gehen oder in die Stadt. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'PARK_WELCOME_EMPTY': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Misc/park-short.mp3'/> Der Park ist heute aber leer. Vielleicht sollten wir heute einfach nur spazieren gehen und morgen nochmal vorbei kommen. Sage spazieren gehen oder zurück in die Stadt. ",
        reprompt: 'Sage spazieren gehen oder zurück in die Stadt. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'PARK_WALK': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Misc/park-long.mp3'/> {{name}} sieht müde aus. Ich denke wir können uns auf den Weg nach Hause machen. Oder hast du noch was anderes vor? ",
        reprompt: 'Wo möchtest du hin gehen, sage in den Park, zum Supermarkt, nach Hause oder zum Tierarzt. '
      },
      'PARK_WALK_GO_TO_PARK': {
        outputSpeech: "Zum Spazieren gehen sollten wir am besten in den Park. Wenn du zum Park möchtest sage zum Park. ",
        reprompt: 'Wo möchtest du hin gehen, sage in den Park, zum Supermarkt, nach Hause oder zum Tierarzt. '
      },
      'PARK_NO_LEASH': {
        outputSpeech: "Du kannst mit {{name}} nur mit einer Leine in den Park. Du solltest eine beim Supermarkt bekommen. Wo möchtest du hin gehen, sage in den Park, zum Supermarkt, nach Hause oder zum Tierarzt. ",
        reprompt: 'Wo möchtest du hin gehen, sage in den Park, zum Supermarkt, nach Hause oder zum Tierarzt. '
      },

      'SUPERMARKET_WELCOME': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/folding-door-open-01.mp3'/><audio src='https://nordicskills.de/audio/MeinHaustier/doorbell-ding-dong-01.mp3'/> Du hast {{coins}} Münzen. Der Laden bietet folgende Produkte an: {{entries}}" +
          "Nenne die Anzahl und das Produkt, welches du kaufen möchtest. Zum Beispiel, ein mal Futter. ",
        reprompt: "Nenne die Anzahl und das Produkt, welches du kaufen möchtest. Zum Beispiel, ein mal Futter. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Supermarkt-gefüllt-ohne-Logo.jpg'
      },
      'SUPERMARKET_OFFER': {
        outputSpeech: "Der Laden bietet folgende Produkte an: {{entries}}" +
          "Nenne die Anzahl und das Produkt, welches du kaufen möchtest. Zum Beispiel, ein mal Futter. ",
        reprompt: "Nenne die Anzahl und das Produkt, welches du kaufen möchtest. Zum Beispiel, ein mal Futter. "
      },
      'SUPERMARKET_ENTRY': {
        outputSpeech: "{{item_name}} für {{item_price}} Münzen"
      },
      'SUPERMARKET_BUY': {
        outputSpeech: "Möchtest du {{item_amount_name}} für {{item_price}} Münzen kaufen? ",
        reprompt: "Möchtest du {{item_amount_name}} für {{item_price}} Münzen kaufen? Sage Ja oder Nein. "
      },
      'SUPERMARKET_NOT_ENOUGH_COINS': {
        outputSpeech: "Du hast nicht genug Münzen, um {{item_amount_name}} zu kaufen. "
      },
      'SUPERMARKET_SUCCESS': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/cash.mp3'/> {{item_amount_name}} gekauft. "
      },
      'SUPERMARKET_PROMPT': {
        outputSpeech: "Möchtest du weiter einkaufen oder den Laden verlassen. Sage Sortiment, Laden verlassen oder gebe eine weitere Bestellung auf. ",
        reprompt: "Möchtest du weiter einkaufen oder den Laden verlassen. Sage Sortiment, Laden verlassen oder gebe eine weitere Bestellung auf. "
      },
      'SUPERMARKET_USE_OR_BUY': {
        outputSpeech: "Du besitzt noch {{item_name}}. Möchtest du es benutzen oder mehr kaufen? Sage benutzen oder kaufen. ",
        reprompt: "Sage kaufen, benutzen oder Laden verlassen. "
      },

      'BACKPACK': {
        outputSpeech: "Du hast {{entries}} in deinem Rucksack. "
      },
      'BACKPACK_ENTRY': {
        outputSpeech: "{{item_amount_name}}"
      },
      'BACKPACK_EMPTY': {
        outputSpeech: "Dein Rucksack ist im Moment leer. "
      },
      'BACKPACK_USE_ITEM_FOOD': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Chewing/{{pet_sound}}'/> {{name}} sieht glücklich aus, da hattest du den richtigen Riecher. "
      },
      'BACKPACK_USE_ITEM_TREAT': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Chewing/{{pet_sound}}'/> {{name}} hat sich gefreut, das war eine gute Idee. "
      },
      'BACKPACK_USE_ITEM_WATER': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Drinking/{{pet_sound}}'/> {{name}} sieht glücklich aus. "
      },
      'BACKPACK_USE_ITEM_TOY_WOOLBALL': {
        outputSpeech: "Du gibst {{name}} den Wollknäuel. <audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}'/> {{gender_1}} sieht glücklich aus. "
      },
      'BACKPACK_USE_ITEM_TOY_BONE': {
        outputSpeech: "Du gibst {{name}} den Knochen. <audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}'/> {{gender_1}} sieht glücklich aus. "
      },
      'BACKPACK_USE_ITEM_NOT_USABLE': {
        outputSpeech: "{{name}} kann nicht verwendet werden. Es wird dauerhaft verwendet. "
      },
      'BACKPACK_USE_ITEM_NOT_OWNED': {
        outputSpeech: "Du hast {{item_name}} nicht in deinem Rucksack. "
      },

      'SLEEP': {
        outputSpeech: "{{name}} legt sich schlafen. <audio src='https://nordicskills.de/audio/MeinHaustier/Sleeping/{{pet_sound}}'/> Komm in ein paar Stunden oder morgen wieder. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },
      'STILL_SLEEPING': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Sleeping/{{pet_sound}}'/> {{name}} schläft noch. Möchtest du {{gender_1}} wecken? ",
        reprompt: "Möchtest du {{name}} wecken? Sage Ja oder Nein. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/{{pic}}'
      },

      'SLEEP_PERFECT': {
        outputSpeech: "{{name}} hat {{sleep_time}} geschlafen und ist voll ausgeschlafen. "
      },
      'SLEEP_GOOD': {
        outputSpeech: "{{name}} hat {{sleep_time}} geschlafen und ist gut ausgeschlafen. "
      },
      'SLEEP_OKAY': {
        outputSpeech: "{{name} hat {{sleep_time}} geschlafen und ist noch nicht ganz ausgeschlafen, wäre aber bereit für ein paar Stunden im Park. "
      },
      'SLEEP_BAD': {
        outputSpeech: "{{name}} hat {{sleep_time}} geschlafen und ist noch tot müde. Du solltest {{gender_1}} vielleicht noch ein paar Stunden schlafen lassen. "
      },
      'SLEEP_ALREADY_AWAKE': {
        outputSpeech: "{{name}} ist bereits wach und voll ausgeschlafen. "
      },

      'CONTINUE_SLEEPING': {
        outputSpeech: "Okay dann lassen wir {{name}} weiter schlafen. Komm in ein paar Stunden oder morgen wieder. "
      },

      'PET': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Misc/{{pet_sound}}'/> "
      },
      'COME_COME': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}'/> "
      },
      'FEED_PET': {
        outputSpeech: "Du kannst {{name}} füttern indem du futter geben oder futter benutzen sagst. Wenn du kein Futter dabei hast, kannst du welches im Supermarkt kaufen. ",
        reprompt: "Sage Futter geben oder zum Supermarkt. "
      },
      'WAIT': {
        outputSpeech: "Gut, du machst eine Pause. <break time='3s'/> {{name}} ist langweilig, willst du nicht lieber spazieren gehen oder etwas anderes machen? ",
        reprompt: "Sage spazieren gehen oder was anderes machen. "
      },

      //
      //--------------------  Mini Game Related Prompts -------------------------------------------
      //            
      'QUIZ_INTRO': {
        outputSpeech: "Schön dass du dich entschieden hast ein Haustier-Quiz zu spielen. " +
          "Ich stelle dir insgesamt 3 Fragen und du hast die Chance bis zu 30 Münzen zu verdienen.<break time='1s'/> Los geht's mit der ersten Frage. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Park-Spielbrett-Quiz.jpg'
      },
      'QUIZ_ANSWERS': {
        outputSpeech: "<break time='1s'/> Ist es "
      },
      'QUIZ_ANSWERS_CONNECT': {
        outputSpeech: ", oder, "
      },
      'QUIZ_MISUNDERSTOOD_ANSWER': {
        outputSpeech: "Entschuldigung, ich habe dich leider nicht verstanden. Bitte versuchs nochmal! ",
        reprompt: "Bitte wiederhole deine Antwort. "
      },
      'QUIZ_CORRECT_ANSWER_DURING_PLAY': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Das ist richtig! Gute Arbeit. "
      },
      'QUIZ_INCORRECT_ANSWER_DURING_PLAY': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Das war leider nicht richtig. Die richtige Antwort war: {{answer}}. "
      },
      'QUIZ_FINISHED': {
        outputSpeech: "Du hast {{score}} von 3 Fragenrichtig beantwortet. Dafür bekommst du {{coin_reward}} Münzen. <audio src='https://nordicskills.de/audio/MeinHaustier/muenzen.mp3'/>"
      },
      'QUIZ_FINISHED_ALL_WRONG': {
        outputSpeech: "Heute war leider keine richtige Antwort dabei, aber naja, nächstes mal sind vielleicht auch Fragen für dich dabei. "
      },
      'QUIZ_SKIP_QUESTION': {
        outputSpeech: "Okay, versuchen wir die nächste Frage. "
      },
      'QUIZ_SKIP_LAST_QUESTION': {
        outputSpeech: "Das war´s, das war die letzte Frage."
      },

      'MEMORY_INTRO': {
        outputSpeech: "Du möchtest also eine Runde Memory spielen. <break time='300ms'/> Gut, die Karten sind gemischt dann lass uns anfangen. " +
          "Du hast 9 verdeckte Karten vor dir und musst gleich nach und nach 2 Zahlen nennen. Die genannten Karten werden dann aufgedeckt. " +
          "Ziel ist es Paare zu finden. <break time='500ms'/> Du kannst das Minispiel jeder Zeit abbrechen, indem du zum Park zurückkehrst. <break time='500ms'/> Legen wir los: " +
          "Nenne mir zwei Zahlen zwischen 1 und 9. ",
        reprompt: "Nenne mir zwei Zahlen zwischen 1 und 9. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Park-Spielbrett-Memory.jpg'
      },
      'MEMORY_REVEAL': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MiniGame/card-flip-{{random_1}}.mp3' />  {{card_1}} liegt unter der {{card_nr_1}}. Karte. <break time='500ms'/>" +
          "<audio src='https://nordicskills.de/audio/MiniGame/card-flip-{{random_2}}.mp3' />  {{card_2}} liegt unter der {{card_nr_2}}. Karte. <break time='500ms'/>"
      },
      'MEMORY_CORRECT': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Richtig, dafür gibt es 10 Münzen. <break time='500ms' />"
      },
      'MEMORY_INCORRECT': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Leider kein Paar. <break time='500ms'/>"
      },
      'MEMORY_INCORRECT_INPUT': {
        outputSpeech: "Die Zahlen müssen zwischen eins und 9 liegen. ",
        reprompt: "Nenne mir zwei Zahlen zwischen 1 und 9. ",
      },
      'MEMORY_ALREADY_REVEALED': {
        outputSpeech: "{{number}} wurde bereits aufgedeckt. Nenne mir zwei Zahlen zwischen 1 und 9.",
        reprompt: "Nenne mir zwei Zahlen zwischen 1 und 9. ",
      },
      'MEMORY_NEXT': {
        outputSpeech: "Nenne mir zwei Zahlen zwischen eins und 9. ",
        reprompt: "Nenne mir zwei Zahlen zwischen eins und 9. "
      },
      'MEMORY_END': {
        outputSpeech: "Okay, das waren alle Karten. Du hast {{reward}} von 40 möglichen Münzen verdient. "
      },


      'WHOAMI_INTRO': {
        outputSpeech: 'In diesem Spiel geht es darum eine Person oder einen Gegenstand zu erraten. Ich werde dir nach und nach <phoneme alphabet="ipa" ph="tɪps">Tipps</phoneme> geben, bis du auf die Lösung kommst. ',
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Park-Spielbrett-Wer-bin-ich.jpg'
      },
      'WHOAMI_CORRECT': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Die Antwort war richtig! Du hast {{hints}} Hinweise gebraucht. Du erhältst {{reward}} Münzen. <audio src='https://nordicskills.de/audio/MeinHaustier/muenzen.mp3'/> "
      },
      'WHOAMI_INCORRECT': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Die Antwort ist leider falsch. "
      },
      'WHOAMI_FIRST_HINT': {
        outputSpeech: "Erster Hinweis. "
      },
      'WHOAMI_LAST_HINT': {
        outputSpeech: "Letzter Hinweis. "
      },
      'WHOAMI_SOLUTION': {
        outputSpeech: "Die Lösung war {{solution}}. Leider keine Münzen für dich. "
      },
      'WHOAMI_REPROMPT': {
        reprompt: "Wenn du die Lösung nicht kennst. Sage weiter. "
      },

      'SHELL_INTRO': {
        outputSpeech: "Ich lege gleich eine Kugel unter eines von 3 Hütchen und tausche sie hin und her. Danach nennst du mir eine Zahl zwischen 1 und drei. <break time='1s'/>" +
          "Okay, legen wir los. <audio src='https://nordicskills.de/audio/MiniGame/shell.mp3' />" +
          "Na? Unter welchem Hütchen liegt die Kugel. Nenne mir eine Zahl zwischen eins und drei. ",
        reprompt: "Na? Unter welchem Hütchen liegt die Kugel. Nenne mir eine Zahl zwischen eins und drei. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Park-Spielbrett-Hütchenspiel.jpg'
      },
      'SHELL_END': {
        outputSpeech: "Okay, <audio src='https://nordicskills.de/audio/MiniGame/shell_end.mp3' /> Die Kugel war unter dem {{winningShell}}. Hütchen. "
      },
      'SHELL_WIN': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Du hast richtig geraten. Du erhältst 3 Leckerlies. <break time='1s'/>"
      },
      'SHELL_LOSE': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Leider nicht richtig geraten. Vielleicht hast du nächstes mal mehr Glück. <break time='1s'/>"
      },
      'SHELL_INCORRECT_INPUT': {
        outputSpeech: "Die Zahl muss zwischen eins und drei liegen. ",
        reprompt: "Nenne mir eine Zahl zwischen eins und drei. "
      },

      'STICK_WOOL_INTRO': {
        outputSpeech: "Wir spielen {{stickWool_1}} werfen. Es wird gleich ein {{stickWool_2}} geworfen. " +
          "3 andere Parkbesucher und du, schätzen nacheinander wie lange {{name}} braucht {{stickWool_3}} zurück zu bringen. " +
          "Der Reihe nach schätzt ihr eine Zahl zwischen einer und 60 Sekunden. Wer am nächsten dran ist bekommt die meisten Münzen. <break time='1s'/>" +
          "Hier kommen die Schätzungen: {{name_1}} schätzt {{guess_1}}, {{name_2}} {{guess_2}} und {{name_3}} {{guess_3}} Sekunden. " +
          "Nun bist du an der Reihe. Nenne eine Zahl zwischen einer und 60 Sekunden. ",
        reprompt: "Nenne eine Zahl zwischen einer und 60 Sekunden. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Park-Spielbrett-Stöckchen-werfen.jpg'
      },
      'STICK_WOOL_CONFIRM': {
        outputSpeech: "Ich habe {{number}} verstanden ist das richtig? ",
        reprompt: "Ich habe {{number}} verstanden ist das richtig? "
      },
      'STICK_WOOL_ALREADY_USED': {
        outputSpeech: "Tut mir Leid. Die Zahl wurde leider schon genannt. Bitte nenne eine andere. ",
        reprompt: "Nenne mir eine Zahl zwischen eins und 60. "
      },
      'STICK_WOOL_INCORRECT_INPUT': {
        outputSpeech: "Nenne mir eine Zahl zwischen eins und 60. ",
        reprompt: "Nenne mir eine Zahl zwischen eins und 60. "
      },
      'STICK_WOOL_FINISHED': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/throw.mp3' /><audio src='https://nordicskills.de/audio/MeinHaustier/Misc/running-dog.mp3' /> {{name}} hat {{time}} Sekunden gebraucht, damit erhältst du auf Platz {{rank}}, {{reward}} Münzen. "
      },

      'GUESS_INTRO': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/Animal/sheep.mp3'/> Hast du richtig hingehört, wir spielen Tierarten raten. " +
          "Du hörst gleich ein Tiergeräusch und du musst raten von welcher Tierart es kam. " +
          "Es werden insgesamt 3 Runden gespielt, für jeden Treffer erhälst du Münzen. " +
          "Achtung, hier kommt das erste Tiergeräusch: <audio src='https://nordicskills.de/audio/MeinHaustier/Animal/{{animal_sound}}'/> " +
          "Und hast du eine Idee. Nenne einfach die Lösung oder sage weiter, wenn du es nicht weißt. ",
        reprompt: "Du kannst auch wiederholen sagen, um das Geräusch nochmal zu hören. <break time='1s'/>" +
          "Nenne einfach die Lösung oder sage weiter, wenn du es nicht weißt. ",
        backgroundImage: 'https://nordicskills.de/audio/MeinHaustier/Display/Park-Spielbrett-Arten-raten.jpg'
      },
      'GUESS_WIN': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Richtig, die Lösung war {{animal}}. "
      },
      'GUESS_LOSE': {
        outputSpeech: "<audio src='https://nordicskills.de/audio/MeinHaustier/{{pet_sound}}' /> Leider falsch, die Lösung war {{animal}}. Keine Münzen für diese Runde. "
      },
      'GUESS_SKIP': {
        outputSpeech: "Okay, die Lösung war {{animal}}. Keine Münzen für diese Runde. "
      },
      'GUESS_NEXT': {
        outputSpeech: "Hier kommt das {{nextLast}} Tier: <audio src='https://nordicskills.de/audio/MeinHaustier/Animal/{{animal_sound}}'/> Nenne einfach die Lösung oder sage weiter, wenn du es nicht weißt. ",
        reprompt: "Nenne einfach die Lösung oder sage weiter, wenn du es nicht weißt. "
      },
      'GUESS_REPEAT': {
        outputSpeech: "Hier kommt das Geräusch: <audio src='https://nordicskills.de/audio/MeinHaustier/Animal/{{animal_sound}}'/> Nenne einfach die Lösung oder sage weiter, wenn du es nicht weißt. ",
        reprompt: "Nenne einfach die Lösung oder sage weiter, wenn du es nicht weißt. "
      },
      'GUESS_END': {
        outputSpeech: "Du hast {{correct}} von 3 Geräuschen erraten. Dafür erhälst du {{reward}} Münzen. "
      },

      'STAT_GENERAL': {
        outputSpeech: "Du kannst mit Fragen herausfinden, was mit {{name}} nicht stimmt. Du kannst z.B. fragen: Hat {{name}} durst? Du kannst auch fragen, ob {{gender_1}} hungrig, durstig, müde oder krank ist. <break time='1s'/> "
      },

      'STAT_ALL_GOOD': {
        outputSpeech: "{{name}} sieht glücklich aus. "
      },

      'STAT_HUNGER_LOW': {
        outputSpeech: "{{name}} ist nicht hungrig. "
      },
      'STAT_HUNGER_MID': {
        outputSpeech: "{{name}} ist etwas hungrig. "
      },
      'STAT_HUNGER_HIGH_NO_FOOD': {
        outputSpeech: "{{name}} ist sehr hungrig. Du solltest schnellstens zum Supermarkt und Futter kaufen. "
      },
      'STAT_HUNGER_HIGH_FOOD': {
        outputSpeech: "{{name}} ist sehr hungrig. Du solltest noch Futter dabei haben. "
      },

      'STAT_ILLNESS_LOW': {
        outputSpeech: "Es geht {{name}} gut. "
      },
      'STAT_ILLNESS_MID': {
        outputSpeech: "Es geht {{name}} nicht so gut. Du solltest vielleicht bei Gelegenheit beim Tierarzt vorbei schauen. "
      },
      'STAT_ILLNESS_HIGH': {
        outputSpeech: "Es geht {{name}} gar nicht gut. Du solltest {{gender_1}} dringend zum Tierarzt bringen. "
      },

      'STAT_THIRST_LOW': {
        outputSpeech: "{{name}} ist nicht durstig. "
      },
      'STAT_THIRST_MID': {
        outputSpeech: "{{name}} ist etwas durstig. "
      },
      'STAT_THIRST_HIGH_NO_WATER': {
        outputSpeech: "{{name}} ist sehr durstig. Du solltest schnellstens zum Supermarkt und Wasser kaufen. "
      },
      'STAT_THIRST_HIGH_WATER': {
        outputSpeech: "{{name}} ist sehr durstig. Du solltest noch Wasser dabei haben. "
      },

      'STAT_FATIGUE_LOW': {
        outputSpeech: "{{name}} ist gut ausgeschlafen. "
      },
      'STAT_FATIGUE_MID': {
        outputSpeech: "{{name}} ist etwas müde. "
      },
      'STAT_FATIGUE_HIGH': {
        outputSpeech: "{{name}} ist tot müde. Du solltest {{gender_1}} zuhause schlafen legen. "
      },

      'STAT_BOREDOM_LOW': {
        outputSpeech: "{{name}} ist nicht langweilig. "
      },
      'STAT_BOREDOM_MID': {
        outputSpeech: "{{name}} ist etwas langweilig. "
      },
      'STAT_BOREDOM_HIGH': {
        outputSpeech: "{{name}} ist sehr langweilig. Du solltest dringend was mit {{gender_1}} unternehmen. "
      },

      'TOO_FATIGUED_PARK': {
        outputSpeech: "{{name}} ist zu müde, um in den Park zu gehen. Du solltest nach Hause gehen und {{gender_1}} schlafen legen. "
      },
      'TOO_ILL_PARK': {
        outputSpeech: "{{name}} gehts nicht gut. Du solltest {{gender_1}} zum Tierarzt bringen. "
      },

      'LOOK_AGE_CAT': {
        outputSpeech: "{{name}} hat ein gestreiftes Fell, besonders auffällig an {{gender_1}} Schwanz. {{gender_2}} hat große Augen und ist sehr verspielt. "
      },
      'LOOK_AGE_DOG': {
        outputSpeech: "{{name}} hat dunkles weiches Fell. An einigen Stellen ist es hell braun. {{gender_1}} hat niedliche Augen und liebt Streicheleinheiten. "
      }
    }
  }
};

module.exports = messages;