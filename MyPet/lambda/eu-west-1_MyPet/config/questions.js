'use strict';
/*
 * Copyright 2018 Amazon.com, Inc. and its affiliates. All Rights Reserved.
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * http://aws.amazon.com/asl/
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */


/**
 * Questions library
 *
 * Use this file to create your own set of questions.
 *
 * Object properties:
 *      index:          The index of the question in this list
 *      question:       The question you want Alexa to ask
 *      answers:        The list of available answers
 *      correct_answer: The correct answer to the question
 *
 * When adding or updating questions and answers, you must take the list of all values
 * in each of the 'answers' arrays for all questions and add them to a custom slot
 * in your skill called 'answers'.
 *
 * The 'answers' custom slot is be mapped to a couple of intents in the interaction model.
 * One intent, named 'AnswerOnlyIntent', contains only the slot, by itself, in order
 * to maximize the accuracy of the model.
 *
 * For example:
 *      AnswerOnlyIntent {answers}
 *
 * The other intent, 'AnswerQuestionIntent', provides more complex speech patterns
 * to match other utternaces users may include with their answers.
 *
 * For example:
 *      AnswerQuestionIntent is it {answers}
 *      AnswerQuestionIntent it is {answers}
 *      AnswerQuestionIntent the answer is {answers}
 *      AnswerQuestionIntent I think the answer is {answers}
 *
 * See model file at models/en-US.json for a complete example.
 */
module.exports = Object.freeze({
  questions_en_US: [
    {
      index: 1,
      question: 'How many years ago did the dog become a pet?',
      answers: ['1,000 years', '10,000 years', '100,000 years'],
      correct_answer: '10,000 years'
    },
    {
      index: 2,
      question: 'Keeping a pet is not always cheap. Up to how much money should you plan as a dog owner per month?',
      answers: ['100 dollars', '50 dollars', '20 dollars'],
      correct_answer: '100 dollars'
    },
    {
      index: 3,
      question: "Who is man's best friend?",
      answers: ['Rabbit', 'Dog', 'Cat'],
      correct_answer: 'Dog'
    },
    {
      index: 4,
      question: 'Which animals should not be kept as pets?',
      answers: ['Wild animals', 'Insects', 'Reptiles'],
      correct_answer: 'Wild animals'
    },
    {
      index: 5,
      question: 'Which animals get along?',
      answers: ['Dog and Cat', 'Snake and Mouse', 'Fish and Raccoon'],
      correct_answer: 'Dog and Cat'
    },
    {
      index: 6,
      question: 'Which animal lives with us most often?',
      answers: ['Dog', 'Cat', 'Rodent'],
      correct_answer: 'Cat'
    },
    {
      index: 7,
      question: 'How should canaries be kept?',
      answers: ['Alone', 'In larger groups', 'Always in pairs'],
      correct_answer: 'In larger groups'
    },
    {
      index: 8,
      question: 'How many of the owners give their pet something for Christmas?',
      answers: ['14 percent', '64 percent', '34 percent'],
      correct_answer: '64 percent'
    },
    {
      index: 9,
      question: "What do guineapigs don't like?",
      answers: ['Live without consort', 'to be stroked', 'Pepper'],
      correct_answer: 'Live without consort'
    },
    {
      index: 10,
      question: 'What does a spider eat?',
      answers: ['Insects and small animals', 'Dry food', 'Fruits and Vegetables'],
      correct_answer: 'Insects and small animals'
    },
    {
      index: 11,
      question: 'In which country have cats been appreciated as pets since the 3rd millennium BC?',
      answers: ['Brazil', 'Germany', 'Egypt'],
      correct_answer: 'Egypt'
    },
    {
      index: 12,
      question: 'How often should a dog be bathed?',
      answers: ['Once a month', 'Every week', 'As needed'],
      correct_answer: 'As needed'
    },
    {
      index: 13,
      question: 'How do you know the mood of a cat?',
      answers: ['position of the ears', 'position of the tail', 'both'],
      correct_answer: 'both'
    },
    {
      index: 14,
      question: 'Which command should a dog learn first?',
      answers: ['Sit', 'Down', 'Wait'],
      correct_answer: 'Sit'
    },
    {
      index: 15,
      question: 'During the holiday season, their pet is annoying for many people. How many animals end up each year alone in the shelter in Germany? ',
      answers: ['700', '7,000', '70,000'],
      correct_answer: '70,000'
    },
    {
      index: 16,
      question: 'How to life a rabbit?',
      answers: ['With back fur', 'With hind legs', 'Under the belly and legs'],
      correct_answer: 'Under the belly and legs'
    }, {
      index: 17,
      question: 'What kind of animal is a chinois?',
      answers: ['Dog', 'Rodent', 'Parrots'],
      correct_answer: ' Dog '
    },
    {
      index: 18,
      question: 'What emotion does a cat show when it stretches its tail? ',
      answers: ['Joy', 'Nervousness', 'Anxiety'],
      correct_answer: 'Joy'
    }, {
      index: 19,
      question: 'How old can a parrot become?',
      answers: ['10 years', '50 years', '100 years'],
      correct_answer: '100 years'
    },
    {
      index: 20,
      question: 'When birds change their plumage you call this?',
      answers: ['Moult', 'Skin', 'Metamorphosis'],
      correct_answer: 'Moult'
    },
    {
      index: 21,
      question: 'How often does an aquarium need to be cleaned?',
      answers: ['once a month', 'once a week', 'once a day'],
      correct_answer: 'once a week'
    },
    {
      index: 22,
      question: 'What do turtles like especially?',
      answers: ['Society', 'to be stroked', 'their Rest'],
      correct_answer: 'their Rest'
    },
    {
      index: 23,
      question: 'At what temperature does a Guppy feel comfortable in the aquarium?',
      answers: ['25 degrees Celsius', '15 degrees Celsius', '5 degrees Celsius'],
      correct_answer: '25 degrees Celsius'
    }
  ],
  questions_de_DE: [
    {
      index: 1,
      question: 'Vor wie vielen Jahren wurde der Hund zum Haustier?',
      answers: ['eintausend Jahren', 'zehntausend Jahren', 'einhunderttausend Jahren'],
      correct_answer: 'zehntausend Jahren'
    },
    {
      index: 2,
      question: 'Ein Haustier zu halten ist nicht immer billig. Bis zu wie viel Geld sollte man als Hundebesitzer pro Monat einplanen?',
      answers: ['100 Euro', '50 Euro', '20 Euro'],
      correct_answer: '100 Euro'
    },
    {
      index: 3,
      question: 'Wer ist der beste Freund des Menschen?',
      answers: ['Kaninchen', 'Hund', 'Katze'],
      correct_answer: 'Hund'
    },
    {
      index: 4,
      question: 'Welche Tiere sollte man auf gar keinen Fall als Haustiere halten?',
      answers: ['Wildtiere', 'Insekten', 'Reptilien'],
      correct_answer: 'Wildtiere'
    },
    {
      index: 5,
      question: 'Welche Tiere vertragen sich?',
      answers: ['Hund und Katze', 'Schlange und Maus', 'Fisch und Waschbär'],
      correct_answer: 'Hund und Katze'
    },
    {
      index: 6,
      question: 'Welches Tier lebt am häufigsten mit uns unter einem Dach?',
      answers: ['Hund', 'Katze', 'Nagetier'],
      correct_answer: 'Katze'
    },
    {
      index: 7,
      question: 'Wie nennt man die Nase bei Pferden',
      answers: ['Nüstern', 'Füstern', 'Fostern'],
      correct_answer: 'Nüstern'
    },
    {
      index: 8,
      question: 'Wie viele der Besitzer schenken ihrem Haustier etwas zu Weihnachten?',
      answers: ['14 Prozent', '64 Prozent', '34 Prozent'],
      correct_answer: '64 Prozent'
    },
    {
      index: 9,
      question: 'Was mögen Meerschweinchen gar nicht?',
      answers: ['ohne Artgenossen leben', 'Streicheleinheiten', 'Paprika'],
      correct_answer: 'ohne Artgenossen leben'
    },
    {
      index: 10,
      question: 'Was frisst eine Vogelspinne?',
      answers: ['Obst und Gemüse', 'Trockenfutter', 'Insekten und kleine Tiere'],
      correct_answer: 'Insekten und kleine Tiere'
    },
    {
      index: 11,
      question: 'In welchem Land wurden Katzen schon ab dem 3. Jahrtausend v. Chr. als Haustiere geschätzt?',
      answers: ['Brasilien', 'Deutschland', 'Ägypten'],
      correct_answer: 'Ägypten'
    },
    {
      index: 12,
      question: 'Wie oft sollte ein Hund gebadet werden?',
      answers: ['Einmal im Monat', 'Jede Woche', 'Je nach Bedarf'],
      correct_answer: 'Je nach Bedarf.'
    },
    {
      index: 13,
      question: 'Woran kann man die Stimmung einer Katze erkennen?',
      answers: ['Stellung der Ohren', 'Haltung des Schwanzes', 'an beidem'],
      correct_answer: 'an beidem'
    },
    {
      index: 14,
      question: 'Welchen Befehl sollte ein Hund als erstes lernen?',
      answers: ['Sitz', 'Hier', 'Platz'],
      correct_answer: 'Hier'
    },
    {
      index: 15,
      question: 'In der Urlaubszeit ist vielen Menschen ihr Haustier lästig. Wie viele Tiere landen deshalb jedes Jahr allein in Deutschland im Tierheim?',
      answers: ['siebenhundert', 'siebentausend', 'siebzigtausend'],
      correct_answer: 'siebzigtausend'
    },
    {
      index: 16,
      question: 'Wie hebt man ein Kaninchen hoch?',
      answers: ['Am Rückenfell', 'An den Hinterläufen', 'Unter Bauch und Läufen'],
      correct_answer: 'Unter Bauch und Läufen'
    },
    {
      index: 17,
      question: 'Um was für ein Tier handelt es sich bei einem Chinois?',
      answers: ['Hund', 'Nagetier', 'Papageien'],
      correct_answer: 'Hund'
    },
    {
      index: 18,
      question: 'Welche Emotion zeigt eine Katze, wenn sie ihren Schwanz in die Höhe streckt?',
      answers: ['Freude', 'Nervosität', 'Angst'],
      correct_answer: 'Freude'
    },
    {
      index: 19,
      question: 'Wie alt kann ein Papagei werden, bis zu?',
      answers: ['10 Jahre', '50 Jahre', '100 Jahre'],
      correct_answer: '100 Jahre'
    },
    {
      index: 20,
      question: 'Wenn Vögel ihr Gefieder wechseln nennt man das?',
      answers: ['Mauser', 'Häutung', 'Metamorphose'],
      correct_answer: 'Mauser.'
    },
    {
      index: 21,
      question: 'Wie oft muss ein Aquarium gereinigt werden?',
      answers: ['einmal im Monat', 'einmal in der Woche', 'einmal am Tag'],
      correct_answer: 'einmal in der Woche'
    },
    {
      index: 22,
      question: 'Was mögen Schildkröten besonders gerne?',
      answers: ['Gesellschaft', 'Streicheleinheiten', 'Ihre Ruhe'],
      correct_answer: 'Ihre Ruhe'
    },
    {
      index: 23,
      question: 'Bei welcher Temperatur fühlt sich ein Guppy im Aquarium wohl?',
      answers: ['25 Grad Celsius', '15 Grad Celsius', '5 Grad Celsius'],
      correct_answer: '25 Grad Celsius'
    },
    {
      index: 24,
      question: 'Wie sollten Kanarienvögel gehalten werden?',
      answers: ['Allein', 'In größeren Gruppen', 'Immer zu zweit'],
      correct_answer: 'In größeren Gruppen'
    }
  ]
});
