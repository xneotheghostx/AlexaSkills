'use strict';
module.exports = Object.freeze({
  guessSpecies_en_US: [
    {
      answer: "Turkey",
      sound: "turkey.mp3"
    },
    {
      answer: "Crow",
      sound: "crow.mp3"
    },
    {
      answer: "Cow",
      sound: "cow.mp3"
    },
    {
      answer: "Cockroach",
      sound: "cockroach.mp3"
    },
    {
      answer: "Chimpanzee",
      sound: "chimpanzee.mp3"
    },
    {
      answer: "Chicken",
      sound: "chicken.mp3"
    },
    {
      answer: "Ant",
      sound: "ant.mp3"
    },
    {
      answer: "Alligator",
      sound: "alligator.mp3"
    },
    {
      answer: "Termite",
      sound: "termite.mp3"
    },
    {
      answer: "Spider",
      sound: "spider.mp3"
    },
    {
      answer: "Snake",
      sound: "snake.mp3"
    },
    {
      answer: "Sheep",
      sound: "sheep.mp3"
    },
    {
      answer: "Rat",
      sound: "rat.mp3"
    },
    {
      answer: "Pig",
      sound: "pig.mp3"
    },
    {
      answer: "Peacock",
      sound: "peacock.mp3"
    },
    {
      answer: "Parrot",
      sound: "parrot.mp3"
    },
    {
      answer: "Parakeet",
      sound: "parakeet.mp3"
    },
    {
      answer: "Mosquito",
      sound: "mosquito.mp3"
    },
    {
      answer: "Lion",
      sound: "lion.mp3"
    },
    {
      answer: "Lamb",
      sound: "lamb.mp3"
    },
    {
      answer: "Horse",
      sound: "horse.mp3"
    },
    {
      answer: "Hawk",
      sound: "hawk.mp3"
    },
    {
      answer: "Guineapig",
      sound: "guineapig.mp3"
    },
    {
      answer: "Goose",
      sound: "goose.mp3"
    },
    {
      answer: "Goat",
      sound: "goat.mp3"
    },
    {
      answer: "Donkey",
      sound: "donkey.mp3"
    }
  ],
  guessSpecies_de_DE: [
    {
      answer: "Truthahn",
      sound: "turkey.mp3"
    },
    {
      answer: "Krähe",
      sound: "crow.mp3"
    },
    {
      answer: "Kuh",
      sound: "cow.mp3"
    },
    {
      answer: "Kakarlake",
      sound: "cockroach.mp3"
    },
    {
      answer: "Schimpanse",
      sound: "chimpanzee.mp3"
    },
    {
      answer: "Huhn",
      sound: "chicken.mp3"
    },
    {
      answer: "Ameisen",
      sound: "ant.mp3"
    },
    {
      answer: "Alligator",
      sound: "alligator.mp3"
    },
    {
      answer: "Termiten",
      sound: "termite.mp3"
    },
    {
      answer: "Spinne",
      sound: "spider.mp3"
    },
    {
      answer: "Schlange",
      sound: "snake.mp3"
    },
    {
      answer: "Schaf",
      sound: "sheep.mp3"
    },
    {
      answer: "Ratte",
      sound: "rat.mp3"
    },
    {
      answer: "Schwein",
      sound: "pig.mp3"
    },
    {
      answer: "Pfau",
      sound: "peacock.mp3"
    },
    {
      answer: "Papagei",
      sound: "parrot.mp3"
    },
    {
      answer: "Wellensittich",
      sound: "parakeet.mp3"
    },
    {
      answer: "Moskito",
      sound: "mosquito.mp3"
    },
    {
      answer: "Löwe",
      sound: "lion.mp3"
    },
    {
      answer: "Lamm",
      sound: "lamb.mp3"
    },
    {
      answer: "Pferd",
      sound: "horse.mp3"
    },
    {
      answer: "Falke",
      sound: "hawk.mp3"
    },
    {
      answer: "Meerschweinchen",
      sound: "guineapig.mp3"
    },
    {
      answer: "Gans",
      sound: "goose.mp3"
    },
    {
      answer: "Ziege",
      sound: "goat.mp3"
    },
    {
      answer: "Esel",
      sound: "donkey.mp3"
    }
  ]
});
