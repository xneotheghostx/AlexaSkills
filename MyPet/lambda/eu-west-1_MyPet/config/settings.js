"use strict";
/*
 * Copyright 2018 Amazon.com, Inc. and its affiliates. All Rights Reserved.
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * http://aws.amazon.com/asl/
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

/**
 * Settings file
 *
 * Use this file to configure the behavior of your trivia game
 */
module.exports = (function () {
  /**
   * APP_ID:
   *  The skill ID to be matched against requests for confirmation.
   *  It helps protect against spamming your skill.
   *  Replace this with the value of your skill ID to enable this protection.
   */
  const APP_ID = 'amzn1.ask.skill.dc610e8c-303b-4a10-85a6-e6eb64acd881';

  /**
   * GAME - Game settings
   *      MAX_PLAYERS - A number between 2 and 4
   *      QUESTIONS - The total number of questions you will ask per game. Must be
   *          less than or equal to the total number of questions in config/questions.js
   *      QUESTIONS_PER_ROUND - Number of questions you want to ask before giving a game summary.
   *          Should divide evenly into the total number of questions.
   *      ANSWER_SIMILARITY - A percentage value marking how similar an answer need to be to the
   *          correct answer to be considered correct. Used with the string-similarity package
   *          See github readme for setup instructions
   *      MAX_ANSWERS_PER_QUESTION - Maximum number of answers allowed for each question.
   *      SHUFFLE_QUESTIONS - if enabled, questions are presented in randomized order, otherwise
   *          each question is presented in the same answer as they are listed in the questions file.
   */
  const GAME_OPTIONS = {
    QUESTIONS_PER_GAME: 3,
    QUESTIONS_PER_ROUND: 3,
    ANSWER_SIMILARITY: .80,
    MAX_ANSWERS_PER_QUESTION: 4
  };

  /**
   * 
   */
  const MULTIPLIERS = {
    coinsPerDay: 50,
    hungerMultiplier: 1,
    thirstMultiplier: 2,
    fatigueMultiplier: 2,
    boredomMultiplier: 1,
    hungerThreshold: 100,
    thirstThreshold: 100,
    fatigueThreshold: 100,
    hungerIllnessMultiplier: .5,
    thirstIllnessMultiplier: .5,
    fatigueIllnessMultiplier: 1,
    healingMultiplier: .25,
    toyAtHomeMultiplier: .25,
    boredomThreshold: 100,
    attachmentMultiplier: .25,

    waterUsageMinigame: 4,
    waterUsageWalking: 3,
    foodUsageMinigame: 2,
    foodUsageWalking: 1,
    boredomUsageMinigame: 10,
    boredomUsageWalking: 10,
    boredomUsageToy: 5,
    attachmentPlusMinigame: 5,
    attachmentPlusWalking: 5,
    attachmentPlusPet: 5,

    treatsHunger: 15,
    treatsAttachment: 5,

    fatigueMinigame: 10,
    fatigueWalking: 15,
    fatigueLocaChange: 3,

    sleepMultiplier: 10
  };

  /**
   * STORAGE.SESSION_TABLE:
   *  The name of the table in DynamoDB where you want to store session and game data.
   *  You can leave this empty if you do not wish to use DynamoDB to automatically
   *  store game data between sessions after each request.
   */
  const STORAGE = {
    // Session persistence
    SESSION_TABLE: 'MyPetDB'
  };

  /**
   * AUDIO - Links to sound effects used in the game
   *      ROLL_CALL_COMPLETE
   *          Once all players have buzzed in, play this sound
   *      WAITING_FOR_BUZZ_IN_AUDIO
   *          A ticking sound used to indicate that the skill is waiting for a button press
   *      BUZZ_IN_AUDIO
   *          The sound to play when a user 'buzzes in' and is ready to answer a question
   *      CORRECT_ANSWER_AUDIO
   *          A sound effect to play when the users answer correctly
   *      INCORRECT_ANSWER_AUDIO
   *          The sound effect to play when a user answers incorrectly
   */
  const AUDIO = Object.freeze({
    CORRECT_ANSWER_AUDIO: "<audio src='https://nordicskills.de/audio/MiniGame/correct.mp3'/>",
    INCORRECT_ANSWER_AUDIO: "<audio src='https://nordicskills.de/audio/MiniGame/incorrect.mp3'/>"
  });

  /**
   * A set of images to show on backgrounds and in display templates when the skill
   * is used with a device with a screen like the Echo Show or Echo Spot
   * https://developer.amazon.com/docs/custom-skills/display-interface-reference.html
   *
   * The skill template chooses images randomly from each array to provide some
   * variety for the user.
   */
  const IMAGES = Object.freeze({
    BACKGROUND_IMAGES: [
      'https://nordicskills.de/audio/MeinHaustier/hund.jpg'
    ],
    CORRECT_ANSWER_IMAGES: [
      'https://d2vbr0xakfjx9a.cloudfront.net/correct1.png',
      'https://d2vbr0xakfjx9a.cloudfront.net/correct2.png',
      'https://d2vbr0xakfjx9a.cloudfront.net/correct3.png',
      'https://d2vbr0xakfjx9a.cloudfront.net/correct4.png'
    ],
    INCORRECT_ANSWER_IMAGES: [
      'https://d2vbr0xakfjx9a.cloudfront.net/wrong1.png',
      'https://d2vbr0xakfjx9a.cloudfront.net/wrong2.png',
      'https://d2vbr0xakfjx9a.cloudfront.net/wrong3.png',
    ]
  });

  /**
   * 
   */
  const ITEMS = Object.freeze([
    {
      id: 0,
      name_de: 'Wasser',
      prompts_de: [
        'eine Flasche Wasser',
        '{0} Flaschen Wasser'
      ],
      name_en: 'Water',
      prompts_en: [
        'a bottle of water',
        '{0} bottles of water'
      ],
      type: 'Consumable',
      price: 10
    },
    {
      id: 1,
      name_de: 'Futter',
      prompts_de: [
        'ein mal Futter',
        '{0} mal Futter'
      ],
      name_en: 'Food',
      prompts_en: [
        'food',
        'food twice',
        'food {0} times'
      ],
      type: 'Consumable',
      price: 20
    },
    {
      id: 2,
      name_de: 'Leckerlies',
      prompts_de: [
        'ein mal Leckerlies',
        '{0} mal Leckerlies'
      ],
      name_en: 'Treats',
      prompts_en: [
        'treats',
        'treats twice',
        'treats {0} times'
      ],
      type: 'Consumable',
      price: 5
    },
    {
      id: 3,
      name_de: 'Leine',
      prompts_de: [
        'eine Leine',
        '{0} Leinen'
      ],
      name_en: 'Leash',
      prompts_en: [
        'a leash',
        '{0} leashes'
      ],
      type: 'Permanent',
      price: 50
    },
    {
      id: 4,
      name_de: 'Knochen',
      prompts_de: [
        'einen Knochen',
        '{0} Knochen'
      ],
      name_en: 'Bone',
      prompts_en: [
        'a bone',
        '{0} bones'
      ],
      type: 'Multi',
      price: 120
    },
    {
      id: 5,
      name_de: 'Spielball',
      prompts_de: [
        'ein Spielball',
        '{0} Spielbälle'
      ],
      name_en: 'Plaything',
      prompts_en: [
        'a plaything',
        '{0} playthings'
      ],
      type: 'Permanent',
      price: 200
    },
    {
      id: 6,
      name_de: 'Wollknäuel',
      prompts_de: [
        'ein Wollknäuel',
        '{0} Wollknäuel'
      ],
      name_en: 'Wool Ball',
      prompts_en: [
        'a wool ball',
        '{0} wool balls'
      ],
      type: 'Multi',
      price: 120
    },
    {
      id: 7,
      name_de: 'Kratz Baum',
      prompts_de: [
        'einen Kratz Baum',
        '{0} Kratz Bäume'
      ],
      name_en: 'Scratching Post',
      prompts_en: [
        'a scratching port',
        '{0} scratching posts'
      ],
      type: 'Permanent',
      price: 200
    }
  ]);

  const BARKS = Object.freeze([
    "small-dog-bark-1.mp3",
    "small-dog-bark-2.mp3",
    "small-dog-bark-3.mp3",
    "small-dog-bark-4.mp3",
    "small-dog-bark-5.mp3",
    "small-dog-bark-6.mp3",
    "small-dog-bark-7.mp3",
    "small-dog-bark-8.mp3",
    "small-dog-bark-9.mp3",
    "small-dog-bark-10.mp3"
  ]);

  const MEOWS = Object.freeze([
    "meowing-cat-1.mp3",
    "meowing-cat-2.mp3",
    "meowing-cat-3.mp3",
    "meowing-cat-4.mp3",
    "meowing-cat-5.mp3",
    "meowing-cat-6.mp3",
    "meowing-cat-7.mp3"
  ]);


  /**
   * 
   */
  const NAMES = Object.freeze(["Sarah", "Jan", "Anna", "Lukas", "Laura", "Finn", "Tim", "Tom", "Jannik", "Justin", "Marie", "Vanessa",
  "Lisa", "Annika", "Fabian", "Jakob", "Lennart", "Max", "Lina", "Daniel", "Nina", "Dennis", "Carolin", "Jennifer",
  "Lasse", "Celine", "Paul", "Moritz", "Simon", "Alexander", "Tobias"]);

  /**
   * 
   */
  const PET_NAMES = Object.freeze([
    ["Rocky", "Spike", "Sam", "Max", "Benny",
      "Ben", "Paul", "Balu", "Tyson", "Jacky",
      "Bobby", "Balou", "Buddy"],
    ["Luna", "Kira", "Emma", "Gina", "Charly", 
      "Paula", "Ronja", "Lucy", "Lilly", "Bella", 
      "Sunny", "Bonny","Lady"]]);

  /*
   * Define the different states that this skill can be in. For the Trivia skill,
   * we define ROLLCALL, GAME_LOOP, ROLLCALL_EXIT, and the initial state called
   * START_GAME_STATE (which maps to the initial state).
   */
  const SKILL_STATES = {
    // Start mode performs roll call and button registration.
    // https://developer.amazon.com/docs/gadget-skills/discover-echo-buttons.html
    START_GAME_STATE: '',
    MAIN_GAME_STATE: '_MAIN_GAME',
    MINI_GAME_STATE: '_MINI_GAME',
    SLEEPING_STATE: '_SLEEPING'
  };

  // return the externally exposed settings object
  return Object.freeze({
    APP_ID: APP_ID,
    STORAGE: STORAGE,
    AUDIO: AUDIO,
    IMAGES: IMAGES,
    ITEMS: ITEMS,
    NAMES: NAMES,
    BARKS: BARKS,
    MEOWS: MEOWS,
    PET_NAMES: PET_NAMES,
    GAME: GAME_OPTIONS,
    MULTIPLIERS: MULTIPLIERS,
    STATE: SKILL_STATES,
    LOG_LEVEL: 'WARN',
    pickRandom(arry) {
      if (Array.isArray(arry)) {
        return arry[Math.floor(Math.random() * Math.floor(arry.length))]
      }
      return arry;
    }
  });
})();