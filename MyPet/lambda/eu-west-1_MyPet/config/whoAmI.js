'use strict';
module.exports = Object.freeze({
  whoAmI_en_US: [
    {
      answers: [
        "Harry Potter"
      ],
      hints: [
        'I understand the language of the snakes. ',
        'My best friend once had terrible trouble because we were seen in the sky with his parents car in broad daylight. ',
        'When I get home, I live with my Muggel aunt and her husband. ',
        "I'm going to school in Hogwarts. ",
        "I'm a magician. "
      ]
    },
    {
      answers: [
        "Pippi Longstocking"
      ],
      hints: [
        'I have red hair that I always wear braided. ',
        'Tommy and Annika are my best friends. ',
        'I have a total of five first names. Windowshade and Mackrelmint are two of them. ',
        'I even have my own horse called Little Buddy and a monkey named Mr. Nilsson. ',
        'I live in the Villa Villekulla. '
      ]
    },
    {
      answers: [
        "Micky Mouse"
      ],
      hints: [
        'I have big ears. ',
        "I'm a well-known cartoon character. ",
        "My friend's name is Minnie. ",
        'Pluto is my dog. ',
        "I'm a mouse. Walt Disney invented me. "
      ]
    },
    {
      answers: [
        "Ole Kirk Christiansen"
      ],
      hints: [
        'I was born on 7 April 1891 in Filskov. ',
        "I'm a toy maker and my first toy was wooden blocks. ",
        'My home country is Denmark. ',
        'Today every child knows me and almost everyone has a toy from me at home. ',
        'My logo is mainly composed of red and white and consists of four letters. '
      ]
    },
    {
      answers: [
        "Albert Einstein"
      ],
      hints: [
        'I am a scientist and researcher. ',
        'I died at the age of 71. ',
        'I have gray hair and a striking mustache. ',
        'I am considered the most important physicist of all time. ',
        'The theory of relativity made me world famous. '
      ]
    },
    {
      answers: [
        "Thanos"
      ],
      hints: [
        "I'm not a human being and a fictional character. ",
        "I'm from the Marvel Universe. ",
        'I already had a short appearance in several films. ',
        'I have a purple or blue skin and often wear armor. ',
        'I am known for a golden glove with five gems. '
      ]
    },
    {
      answers: [
        "Angelina Jolie"
      ],
      hints: [
        "I'm an American actress. ",
        'I was Special Ambassador for the UN High Commissioner for Refugees. ',
        "I've played in a Disney movie before. ",
        'Brat Pitt is my husband. ',
        'I became internationally known with the role of the video game heroine Lara Croft. '
      ]
    },
    {
      answers: [
        "Arya Stark"
      ],
      hints: [
        "I'm female and relatively small. ",
        "I'm one of the most famous characters from a series. ",
        'I can fight very well and I am skillful dealing with sword and dagger. ',
        'I have four brothers, most of them are already dead. ',
        'I can take the face of any person I want. '
      ]
    },
    {
      answers: [
        "Mark Zuckerberg"
      ],
      hints: [
        "I grew up in New York and have three sisters. ",
        'I have a share of 28 percent in a company. ',
        "I'm married and I have two children. ",
        'I have a fortune of about $56 billion today. ',
        'I founded one of the most famous social networks in the world. '
      ]
    }
  ],
  whoAmI_de_DE: [
    {
      answers: [
        "Harry Potter"
      ],
      hints: [
        "Ich verstehe die Sprache der Schlangen.",
        "Mein bester Freund hatte einmal fürchterlich Ärger, weil wir mit dem Auto seiner Eltern am helllichten Tag am Himmel gesehen wurden.",
        "Wenn ich mal zu Hause bin, lebe ich bei meiner Muggel-Tante und ihrem Mann.",
        "Ich gehe in Hogwarts zur Schule.",
        "Ich bin ein Zauberer."
      ]
    },
    {
      answers: [
        "Pippi Langstrumpf"
      ],
      hints: [
        "Ich habe rote Haare, die ich immer zu Zöpfen geflochten trage.",
        "Tommy und Annika sind meine besten Freunde.",
        "Ich habe insgesamt fünf Vornamen. Rollgardina und Pfefferminz sind zwei davon.",
        "Ich habe sogar ein eigenes Pferd, das Kleiner Onkel heißt, und einen Affen mit dem Namen Herr Nilsson.",
        "Ich wohne in der Villa Kunterbunt."
      ]
    },
    {
      answers: [
        "Micky Maus"
      ],
      hints: [
        "Ich habe große Ohren.",
        "Ich bin eine bekannte Comicfigur.",
        "Meine Freundin heißt Minnie.",
        "Pluto ist mein Hund.",
        "Ich bin eine Maus. Walt Disney hat mich erfunden."
      ]
    },
    {
      answers: [
        "Ole Kirk Christiansen"
      ],
      hints: [
        "Ich wurde am 7. April 1891 in Filskov geboren.",
        "Ich bin Spielzeugmacher und mein erstes Spielzeug waren Holzbauklötze.",
        "Mein Heimatland ist Dänemark.",
        "Heute kennt mich jedes Kind und beinahe jeder hat ein Spielzeug von mir Zuhause.",
        "Mein Logo setzt sich überwiegend aus rot und weiß zusammen und besteht aus vier Buchstaben."
      ]
    },
    {
      answers: [
        "Albert Einstein"
      ],
      hints: [
        "Ich bin ein Wissenschafter und Forscher.",
        "Ich bin mit 71 Jahren verstorben.",
        "Ich habe graue Haare und einen markanten Schnauzbart.",
        "Ich gelte als bedeutendster Physiker aller Zeiten.",
        "Die Relativitätstheorie, machte mich weltberühmt."
      ]
    },
    {
      answers: [
        "Thanos"
      ],
      hints: [
        "Ich bin kein Mensch und ein fiktionaler Charakter.",
        "Ich stamme aus dem Marvel Universum.",
        "Ich hatte bereits in mehreren Filmen einen Kurzauftritt.",
        "Ich habe eine Lila bzw. Blaufarbene Haut und trage häufig eine Rüstung.",
        "Ich bin bekannt durch einen goldenen Handschuh mit Fünf Edelsteinen."
      ]
    },
    {
      answers: [
        "Angelina Jolie"
      ],
      hints: [
        "Ich habe mich schonmal mit meinem Mann angelegt.",
        "Ich bin US-amerikanische Schauspielerin.",
        "Ich war Sonderbotschafterin für das UNO-Hochkommissariat für Flüchtlinge.",
        "Ich habe schonmal in einem Disney-Film mitgespielt.",
        "Ich wurde mit der Darstellung der Videospielheldin Lara Croft international bekannt. "
      ]
    },
    {
      answers: [
        "Arya Stark"
      ],
      hints: [
        "Ich bin weiblich und relativ klein.",
        "Ich bin einer der bekanntesten Charaktere aus einer Serie.",
        "Ich kann sehr gut kämpfen und bin geschickt ihm Umgang mit Schwert und Dolch.",
        "Ich habe vier Brüder, die meisten sind bereits tot.",
        "Ich kann das Gesicht von jeder Person annehmen, die ich möchte."
      ]
    },
    {
      answers: [
        "Mark Zuckerberg"
      ],
      hints: [
        "Ich bin in New York aufgewachsen und habe drei Schwestern.",
        "Ich habe an einem Unternehmen einen Anteil von 28 Prozent.",
        "Ich bin verheiratet und habe selber zwei Kinder.",
        "Ich habe heute ein Vermögen von ca. 56 Milliarden US-Dollar.",
        "Ich gründete eines der bekanntesten sozialen Netzwerke der Welt."
      ]
    },
    {
      answers: [
        "Dieter Bohlen"
      ],
      hints: [
        "Ich bin ein deutscher Musiker.",
        "Ich habe eine eigene Tapeten Linie veröffentlicht.",
        "Ich bin in Ostfriesland geboren und aufgewachsen.",
        "Ich bin Jury-Mitglied in zwei der Bekanntesten Castings Shows Deutschlands.",
        "Ich wurde bekannt als Mitglied des Pop-Duos Modern Talking."
      ]
    }
  ]
});
