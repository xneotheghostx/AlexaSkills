/*
 * Copyright 2018 Amazon.com, Inc. and its affiliates. All Rights Reserved.
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * http://aws.amazon.com/asl/
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

const Game = require('../utils/game.js');
const logger = require('../utils/logger.js');
const settings = require('../config/settings.js');
const startHandlers = require('./startHandlers.js');

/**
 * 
 */
const gamePlayHandlers = {
  /**
   * 
   */
  HomeHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.HomeHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'HomeIntent' &&
        attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" &&
        (attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE ||
          attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE);
    },
    handle(handlerInput) {
      logger.debug('GAME.HomeHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      if (sessionAttributes.PLACE == "HOME") {
        responseMessage = ctx.t('ALREADY_HOME');
        ctx.outputSpeech.push(responseMessage.outputSpeech);
        if ('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
          ctx.outputSpeech.push(sessionAttributes.lastReprompt);
          ctx.reprompt.push(sessionAttributes.lastReprompt);
        } else {
          ctx.outputSpeech.push(ctx.t('HELP_REPROMPT').reprompt);
          ctx.reprompt.push(ctx.t('HELP_REPROMPT').reprompt);
        }
      } else {
        sessionAttributes.PLACE = "HOME";
        sessionAttributes.STATS.fatigue += settings.MULTIPLIERS.fatigueLocaChange;
        sessionAttributes.STATS.fatigue = Math.min(sessionAttributes.STATS.fatigue, 100);
        responseMessage = ctx.t('HOME_WELCOME', {
          'name': sessionAttributes.NAME,
          'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER === 0 ? 'ihn' : 'sie') : (sessionAttributes.GENDER == 0 ? "him" : "her"),
          'pic': Game.getHomePic(sessionAttributes)
        });
        ctx.outputSpeech.push(responseMessage.outputSpeech);
        ctx.reprompt.push(responseMessage.reprompt);
        ctx.render(handlerInput, responseMessage);
      }

      return responseBuilder.getResponse();
    }
  },
  CityHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.CityHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'CityIntent' &&
        (attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" || attributesManager.getSessionAttributes().TUTORIAL_STATE === "Hunger") &&
        (attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE ||
          attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE);
    },
    handle(handlerInput) {
      logger.debug('GAME.CityHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      switch (sessionAttributes.PLACE) {
        case "HOME":
          responseMessage = ctx.t('EXIT_HOME');
          ctx.outputSpeech.push(responseMessage.outputSpeech);
          break;
        case "PARK":
          responseMessage = ctx.t('EXIT_PARK');
          ctx.outputSpeech.push(responseMessage.outputSpeech);
          break;
        case "SUPERMARKET":
          responseMessage = ctx.t('EXIT_SUPERMARKET');
          ctx.outputSpeech.push(responseMessage.outputSpeech);
          break;
        case "VET":
          responseMessage = ctx.t('EXIT_VET');
          ctx.outputSpeech.push(responseMessage.outputSpeech);
          break;
        case "CITY":
          responseMessage = ctx.t('ALREADY_CITY');
          ctx.outputSpeech.push(responseMessage.outputSpeech);
          if ('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
            ctx.outputSpeech.push(sessionAttributes.lastReprompt);
            ctx.reprompt.push(sessionAttributes.lastReprompt);
          } else {
            ctx.outputSpeech.push(ctx.t('HELP_REPROMPT').reprompt);
            ctx.reprompt.push(ctx.t('HELP_REPROMPT').reprompt);
          }
          break;

        default:
          break;
      }


      if (sessionAttributes.PLACE != "CITY") {
        sessionAttributes.PLACE = "CITY";

        if (sessionAttributes.TUTORIAL_STATE === "Hunger") {
          sessionAttributes.TUTORIAL_STATE = "City";
          responseMessage = ctx.t('TUTORIAL_CITY', {
            'name': sessionAttributes.NAME,
            'pet_sound': Game.randomPetSound(sessionAttributes),
            'pic': sessionAttributes.SPECIES === "katze" ? (requestEnvelope.request.locale === 'de-DE' ? 'Stadtplan3D-Deutsch-Katze.jpg' : 'Stadtplan3D-Englisch-Katze.jpg') : (requestEnvelope.request.locale === 'de-DE' ? 'Stadtplan3D-Deutsch-Hund.jpg' : 'Stadtplan3D-Englisch-Hund.jpg')
          });
        } else {
          sessionAttributes.STATS.fatigue += settings.MULTIPLIERS.fatigueLocaChange;
          sessionAttributes.STATS.fatigue = Math.min(sessionAttributes.STATS.fatigue, 100);

          if (sessionAttributes.CITY_WELCOME_COUNT > 2) {
            responseMessage = ctx.t('CITY_WELCOME_1_SHORT', {
              'pic': Game.getCityPic(sessionAttributes, requestEnvelope)
            });
          } else {
            responseMessage = ctx.t('CITY_WELCOME_1', {
              'pic': Game.getCityPic(sessionAttributes, requestEnvelope)
            });
          }
          sessionAttributes.CITY_WELCOME_COUNT++;
        }
        ctx.outputSpeech.push(responseMessage.outputSpeech);
        ctx.reprompt.push(responseMessage.reprompt);
        ctx.render(handlerInput, responseMessage);
      }

      delete sessionAttributes.MINI_GAME;
      sessionAttributes.STATE = settings.STATE.MAIN_GAME_STATE;

      return responseBuilder.getResponse();
    }
  },
  SupermarketHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.SupermarketHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        (requestEnvelope.request.intent.name === 'SupermarketIntent' || requestEnvelope.request.intent.name === 'SupermarketOfferIntent') &&
        (attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" || attributesManager.getSessionAttributes().TUTORIAL_STATE === "City") &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('GAME.SupermarketHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      if (sessionAttributes.PLACE == "SUPERMARKET" && requestEnvelope.request.intent.name != 'SupermarketOfferIntent') {
        responseMessage = ctx.t('ALREADY_SUPERMARKET');
        ctx.outputSpeech.push(responseMessage.outputSpeech);
        if ('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
          ctx.outputSpeech.push(sessionAttributes.lastReprompt);
          ctx.reprompt.push(sessionAttributes.lastReprompt);
        } else {
          ctx.outputSpeech.push(ctx.t('HELP_REPROMPT').reprompt);
          ctx.reprompt.push(ctx.t('HELP_REPROMPT').reprompt);
        }
      } else {
        sessionAttributes.PLACE = "SUPERMARKET";
        sessionAttributes.STATS.fatigue += settings.MULTIPLIERS.fatigueLocaChange;
        sessionAttributes.STATS.fatigue = Math.min(sessionAttributes.STATS.fatigue, 100);

        if (sessionAttributes.TUTORIAL_STATE === "City") {
          sessionAttributes.TUTORIAL_STATE = "Supermarket";
          responseMessage = ctx.t('TUTORIAL_SUPERMARKET');
          ctx.outputSpeech.push(responseMessage.outputSpeech);
          ctx.reprompt.push(responseMessage.reprompt);
          ctx.render(handlerInput, responseMessage);

          return responseBuilder.getResponse();
        }

        let cleanedInventory = [];
        for (var i = 0; i < settings.ITEMS.length; i++) {
          if (sessionAttributes.SPECIES === 'katze' ? (settings.ITEMS[i].id != 4 && settings.ITEMS[i].id != 5) : (settings.ITEMS[i].id != 6 && settings.ITEMS[i].id != 7))
            if (!Game.hasItemInInventory(settings.ITEMS[i].id, sessionAttributes) || settings.ITEMS[i].type === 'Consumable')
              cleanedInventory.push(settings.ITEMS[i]);
        }
        let entries = "";
        for (var i = 0; i < cleanedInventory.length; i++) {
          entries += ctx.t('SUPERMARKET_ENTRY', {
            'item_name': requestEnvelope.request.locale == 'de-DE' ? cleanedInventory[i].name_de : cleanedInventory[i].name_en,
            'item_price': cleanedInventory[i].price
          }).outputSpeech;

          if (i == cleanedInventory.length - 2) {
            if (requestEnvelope.request.locale == 'de-DE')
              entries += " und ";
            else
              entries += " and ";
          } else if (i == cleanedInventory.length - 1) {
            entries += ". ";
          } else {
            entries += ", ";
          }
        }
        if (requestEnvelope.request.intent.name === 'SupermarketOfferIntent')
          responseMessage = ctx.t('SUPERMARKET_OFFER', { 'entries': entries });
        else
          responseMessage = ctx.t('SUPERMARKET_WELCOME', { 'coins': sessionAttributes.COINS, 'entries': entries });

        ctx.outputSpeech.push(responseMessage.outputSpeech);
        ctx.reprompt.push(responseMessage.reprompt);
        ctx.render(handlerInput, responseMessage);
      }

      delete sessionAttributes.MINI_GAME;
      sessionAttributes.STATE = settings.STATE.MAIN_GAME_STATE;

      return responseBuilder.getResponse();
    }
  },
  ParkHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.ParkHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'ParkIntent' &&
        attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" &&
        (attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE ||
          attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE);
    },
    handle(handlerInput) {
      logger.debug('GAME.ParkHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      if ((sessionAttributes.STATS.fatigue <= 100 && sessionAttributes.STATS.fatigue > 90) || (sessionAttributes.STATS.illness <= 100 && sessionAttributes.STATS.illness > 80)) {

        if (sessionAttributes.STATS.illness <= 100 && sessionAttributes.STATS.illness > 80)
          responseMessage = ctx.t('TOO_ILL_PARK', {
            'name': sessionAttributes.NAME,
            'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'ihn' : 'sie') : (sessionAttributes.GENDER == 0 ? "him" : "her")
          });
        else if (sessionAttributes.STATS.fatigue <= 100 && sessionAttributes.STATS.fatigue > 90)
          responseMessage = ctx.t('TOO_FATIGUED_PARK', {
            'name': sessionAttributes.NAME,
            'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'ihn' : 'sie') : (sessionAttributes.GENDER == 0 ? "him" : "her")
          });

        ctx.outputSpeech.push(responseMessage.outputSpeech);
        if ('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
          ctx.outputSpeech.push(sessionAttributes.lastReprompt);
          ctx.reprompt.push(sessionAttributes.lastReprompt);
        } else {
          ctx.outputSpeech.push(ctx.t('HELP_REPROMPT').reprompt);
          ctx.reprompt.push(ctx.t('HELP_REPROMPT').reprompt);
        }
      } else {
        if (!Game.hasItemInInventory(3, sessionAttributes)) {
          responseMessage = ctx.t('PARK_NO_LEASH', { 'name': sessionAttributes.NAME });
          ctx.outputSpeech.push(responseMessage.outputSpeech);
          ctx.reprompt.push(responseMessage.reprompt);
        } else {
          if (sessionAttributes.PLACE == "PARK") {
            responseMessage = ctx.t('ALREADY_PARK', { 'name': sessionAttributes.NAME });
            ctx.outputSpeech.push(responseMessage.outputSpeech);

            if (attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE) {
              return gamePlayHandlers.ParkRepromptHandler.handle(handlerInput);
            } else {
              if ('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
                ctx.outputSpeech.push(sessionAttributes.lastReprompt);
                ctx.reprompt.push(sessionAttributes.lastReprompt);
              } else {
                ctx.outputSpeech.push(ctx.t('HELP_REPROMPT').reprompt);
                ctx.reprompt.push(ctx.t('HELP_REPROMPT').reprompt);
              }
            }
          } else {
            sessionAttributes.PLACE = "PARK";
            sessionAttributes.STATS.fatigue += settings.MULTIPLIERS.fatigueLocaChange;
            sessionAttributes.STATS.fatigue = Math.min(sessionAttributes.STATS.fatigue, 100);
            sessionAttributes.WEEKDAY = Game.getCurrentWeekday();

            switch (sessionAttributes.WEEKDAY) {
              case "Mon":
                responseMessage = ctx.t('PARK_WELCOME', { 'name': sessionAttributes.NAME });
                ctx.outputSpeech.push(responseMessage.outputSpeech);
                responseMessage = ctx.t('PARK_WELCOME_1', {
                  'name': sessionAttributes.NAME,
                  'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
                });
                break;
              case "Tue":
                responseMessage = ctx.t('PARK_WELCOME', { 'name': sessionAttributes.NAME });
                ctx.outputSpeech.push(responseMessage.outputSpeech);
                responseMessage = ctx.t('PARK_WELCOME_2', {
                  'name': sessionAttributes.NAME,
                  'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
                });
                break;
              case "Wed":
                responseMessage = ctx.t('PARK_WELCOME', { 'name': sessionAttributes.NAME });
                ctx.outputSpeech.push(responseMessage.outputSpeech);
                responseMessage = ctx.t('PARK_WELCOME_3', {
                  'name': sessionAttributes.NAME,
                  'stickWool': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.SPECIES == "katze" ? "Wollknäuel" : "Stöckchen") : (sessionAttributes.SPECIES == "katze" ? "wool ball" : "stick"),
                  'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
                });
                break;
              case "Thu":
                responseMessage = ctx.t('PARK_WELCOME_EMPTY', {
                  'name': sessionAttributes.NAME,
                  'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
                });
                break;
              case "Fri":
                responseMessage = ctx.t('PARK_WELCOME', { 'name': sessionAttributes.NAME });
                ctx.outputSpeech.push(responseMessage.outputSpeech);
                responseMessage = ctx.t('PARK_WELCOME_1', {
                  'name': sessionAttributes.NAME,
                  'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
                });
                break;
              case "Sat":
                responseMessage = ctx.t('PARK_WELCOME', { 'name': sessionAttributes.NAME });
                ctx.outputSpeech.push(responseMessage.outputSpeech);
                responseMessage = ctx.t('PARK_WELCOME_2', {
                  'name': sessionAttributes.NAME,
                  'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
                });
                break;
              case "Sun":
                responseMessage = ctx.t('PARK_WELCOME', { 'name': sessionAttributes.NAME });
                ctx.outputSpeech.push(responseMessage.outputSpeech);
                responseMessage = ctx.t('PARK_WELCOME_3', {
                  'name': sessionAttributes.NAME,
                  'stickWool': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.SPECIES == "katze" ? "Wollknäuel" : "Stöckchen") : (sessionAttributes.SPECIES == "katze" ? "wool ball" : "stick"),
                  'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
                })
                break;

              default:
                break;
            }
            ctx.outputSpeech.push(responseMessage.outputSpeech);
            ctx.reprompt.push(responseMessage.reprompt);
            ctx.render(handlerInput, responseMessage);
          }
        }
      }

      delete sessionAttributes.MINI_GAME;
      sessionAttributes.STATE = settings.STATE.MAIN_GAME_STATE;

      return responseBuilder.getResponse();
    }
  },
  ParkWalkHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.ParkHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'WalkIntent' &&
        attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('GAME.ParkHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      if (sessionAttributes.PLACE == "PARK") {
        sessionAttributes.STATS.attachment += settings.MULTIPLIERS.attachmentPlusWalking;
        sessionAttributes.STATS.attachment = Math.min(sessionAttributes.STATS.attachment, 100);
        sessionAttributes.STATS.boredom -= settings.MULTIPLIERS.boredomUsageWalking;
        sessionAttributes.STATS.boredom = Math.max(0, sessionAttributes.STATS.boredom);
        sessionAttributes.STATS.fatigue += settings.MULTIPLIERS.fatigueWalking;
        sessionAttributes.STATS.fatigue = Math.min(sessionAttributes.STATS.fatigue, 100);
        responseMessage = ctx.t('PARK_WALK', { 'name': sessionAttributes.NAME });
      } else {
        responseMessage = ctx.t('PARK_WALK_GO_TO_PARK');
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },
  WaitHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.WaitHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'WaitIntent' &&
		attributesManager.getSessionAttributes().PLACE == "PARK" &&
        attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('GAME.WaitHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage = ctx.t('WAIT', { 'name': sessionAttributes.NAME });
      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },
  ParkRepromptHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.ParkRepromptHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'ParkRepromptIntent' &&
        attributesManager.getSessionAttributes().PLACE == "PARK" &&
        attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('GAME.ParkRepromptHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      sessionAttributes.WEEKDAY = Game.getCurrentWeekday();

      switch (sessionAttributes.WEEKDAY) {
        case "Mon":
          responseMessage = ctx.t('PARK_WELCOME_1', {
            'name': sessionAttributes.NAME,
            'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
          });
          break;
        case "Tue":
          responseMessage = ctx.t('PARK_WELCOME_2', {
            'name': sessionAttributes.NAME,
            'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
          });
          break;
        case "Wed":
          responseMessage = ctx.t('PARK_WELCOME_3', {
            'name': sessionAttributes.NAME,
            'stickWool': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.SPECIES == "katze" ? "Wollknäuel" : "Stöckchen") : (sessionAttributes.SPECIES == "katze" ? "wool ball" : "stick"),
            'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
          });
          break;
        case "Thu":
          responseMessage = ctx.t('PARK_WELCOME_EMPTY', {
            'name': sessionAttributes.NAME,
            'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
          });
          break;
        case "Fri":
          responseMessage = ctx.t('PARK_WELCOME_1', {
            'name': sessionAttributes.NAME,
            'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
          });
          break;
        case "Sat":
          responseMessage = ctx.t('PARK_WELCOME_2', {
            'name': sessionAttributes.NAME,
            'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
          });
          break;
        case "Sun":
          responseMessage = ctx.t('PARK_WELCOME_3', {
            'name': sessionAttributes.NAME,
            'stickWool': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.SPECIES == "katze" ? "Wollknäuel" : "Stöckchen") : (sessionAttributes.SPECIES == "katze" ? "wool ball" : "stick"),
            'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
          })
          break;

        default:
          break;
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },

  /**
   * 
   */
  VetHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.VetHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'VetIntent' &&
        attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('GAME.VetHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      if (sessionAttributes.PLACE == "VET") {
        responseMessage = ctx.t('ALREADY_VET');
        ctx.outputSpeech.push(responseMessage.outputSpeech);
        if ('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
          ctx.outputSpeech.push(sessionAttributes.lastReprompt);
          ctx.reprompt.push(sessionAttributes.lastReprompt);
        } else {
          ctx.outputSpeech.push(ctx.t('HELP_REPROMPT').reprompt);
          ctx.reprompt.push(ctx.t('HELP_REPROMPT').reprompt);
        }
      } else {
        sessionAttributes.PLACE = "VET";
        responseMessage = ctx.t('VET_WELCOME', {
          'name': sessionAttributes.NAME,
          'pic': sessionAttributes.SPECIES === 'katze' ? 'Tierarzt-mit-Katze.jpg' : 'Tierarzt-mit-Hund.jpg'
        });
        sessionAttributes.STATS.fatigue += settings.MULTIPLIERS.fatigueLocaChange;
        sessionAttributes.STATS.fatigue = Math.min(sessionAttributes.STATS.fatigue, 100);

        ctx.outputSpeech.push(responseMessage.outputSpeech);
        ctx.reprompt.push(responseMessage.reprompt);
        ctx.render(handlerInput, responseMessage);
      }
      return responseBuilder.getResponse();
    }
  },
  VetExamineHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.VetExamineHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'ExamineIntent' &&
        attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" &&
        attributesManager.getSessionAttributes().PLACE == "VET" &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('GAME.VetExamineHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      responseMessage = ctx.t('VET_EXAMINE', { 'name': sessionAttributes.NAME });
      ctx.outputSpeech.push(responseMessage.outputSpeech);

      if (sessionAttributes.STATS.illness == 100) {
        responseMessage = ctx.t('VET_EXAMINE_VERY_BAD', {
          'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "ihm" : "ihr") : (sessionAttributes.GENDER == 0 ? "he" : "she"),
          'gender_2': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "ihn" : "sie") : (sessionAttributes.GENDER == 0 ? "him" : "her")
        });
      } else if (sessionAttributes.STATS.hunger == 50 || sessionAttributes.STATS.thirst == 50) {
        responseMessage = ctx.t('VET_EXAMINE_BAD', {
          'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "er" : "sie") : (sessionAttributes.GENDER == 0 ? "he" : "she"),
          'gender_2': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "sein" : "ihr") : (sessionAttributes.GENDER == 0 ? "his" : "her")
        });
      } else {
        responseMessage = ctx.t('VET_EXAMINE_FINE', {
          'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "ihm" : "ihr") : (sessionAttributes.GENDER == 0 ? "He" : "She"),
          'gender_2': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "ihn" : "sie") : (sessionAttributes.GENDER == 0 ? "him" : "her")
        });
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },
  VetMedicateHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.VetMedicateHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'MedicateIntent' &&
        attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" &&
        attributesManager.getSessionAttributes().PLACE == "VET" &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('GAME.VetMedicateHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      if (sessionAttributes.STATS.illness > 80) {
        sessionAttributes.COST = 200;

        responseMessage = ctx.t('VET_MEDICATE_NEEDED', {
          'cost': sessionAttributes.COST,
          'name': sessionAttributes.NAME
        });
        sessionAttributes.GAME_STATE = "Medication";
      } else {
        responseMessage = ctx.t('VET_MEDICATE_NOT_NEEDED');
        ctx.outputSpeech.push(responseMessage.outputSpeech);
        return gamePlayHandlers.CityHandler.handle(handlerInput);
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },

  /**
   * The player has responded 'yes' to the option of resuming the previous game.
   */
  YesHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.YesHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'AMAZON.YesIntent' &&
        attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE &&
        (attributesManager.getSessionAttributes().GAME_STATE === "Medication" ||
          attributesManager.getSessionAttributes().GAME_STATE === "MedicationAgain" ||
          attributesManager.getSessionAttributes().GAME_STATE === "Buy");
    },
    handle(handlerInput) {
      logger.debug('GAME.YesHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;

      switch (sessionAttributes.GAME_STATE) {
        case "Medication":
        case "MedicationAgain":
          delete sessionAttributes.GAME_STATE;

          if (sessionAttributes.COINS >= sessionAttributes.COST) {
            sessionAttributes.COINS -= sessionAttributes.COST;
			sessionAttributes.STATS.illness = 0;
            delete sessionAttributes.COST;

            responseMessage = ctx.t('VET_MEDICATE_NEEDED_YES', { 'name': sessionAttributes.NAME });
            ctx.outputSpeech.push(responseMessage.outputSpeech);
            return gamePlayHandlers.CityHandler.handle(handlerInput);
          } else {
            responseMessage = ctx.t('VET_MEDICATE_NOT_ENOUGH_COINS');
            ctx.outputSpeech.push(responseMessage.outputSpeech);
            return gamePlayHandlers.CityHandler.handle(handlerInput);
          }
          break;
        case "Buy":
          delete sessionAttributes.GAME_STATE;

          if (sessionAttributes.COINS >= (sessionAttributes.SELECTED_ITEM.price * sessionAttributes.SELECTED_ITEM.amount)) {
            sessionAttributes.COINS -= (sessionAttributes.SELECTED_ITEM.price * sessionAttributes.SELECTED_ITEM.amount);

            sessionAttributes = Game.addItemToInventory(sessionAttributes.SELECTED_ITEM.id, sessionAttributes.SELECTED_ITEM.amount, sessionAttributes);

            responseMessage = ctx.t('SUPERMARKET_SUCCESS', {
              'item_amount_name': Game.buildBuyPrompt(sessionAttributes.SELECTED_ITEM.amount, sessionAttributes.SELECTED_ITEM.id, requestEnvelope)
            });
            ctx.outputSpeech.push(responseMessage.outputSpeech);
            responseMessage = ctx.t('SUPERMARKET_PROMPT');
          } else {
            responseMessage = ctx.t('SUPERMARKET_NOT_ENOUGH_COINS', {
              'item_amount_name': Game.buildBuyPrompt(sessionAttributes.SELECTED_ITEM.amount, sessionAttributes.SELECTED_ITEM.id, requestEnvelope)
            });
            ctx.outputSpeech.push(responseMessage.outputSpeech);
            responseMessage = ctx.t('SUPERMARKET_PROMPT');
          }
          delete sessionAttributes.SELECTED_ITEM;
          break;

        default:
          break;
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },
  /**
   * The player has responded 'no' to the option of resuming the previous game.
   */
  NoHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.NoHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'AMAZON.NoIntent' &&
        attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE &&
        (attributesManager.getSessionAttributes().GAME_STATE === "Medication" ||
          attributesManager.getSessionAttributes().GAME_STATE === "MedicationAgain" ||
          attributesManager.getSessionAttributes().GAME_STATE === "Buy");
    },
    handle(handlerInput) {
      logger.debug('GAME.NoHandler: handle');
      let {
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;

      switch (sessionAttributes.GAME_STATE) {
        case "Medication":
          responseMessage = ctx.t('VET_MEDICATE_NEEDED_NO');
          ctx.outputSpeech.push(responseMessage.outputSpeech);
          sessionAttributes.GAME_STATE = "MedicationAgain";
          return gamePlayHandlers.CityHandler.handle(handlerInput);
          break;
        case "MedicationAgain":
          responseMessage = ctx.t('VET_MEDICATE_NEEDED_NO_AGAIN');
          ctx.outputSpeech.push(responseMessage.outputSpeech);
          delete sessionAttributes.GAME_STATE;
          delete sessionAttributes.COST;
          return gamePlayHandlers.CityHandler.handle(handlerInput);
          break;
        case "Buy":
          responseMessage = ctx.t('SUPERMARKET_PROMPT');
          delete sessionAttributes.GAME_STATE;
          delete sessionAttributes.SELECTED_ITEM;
          break;

        default:
          break;
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },
  /**
   * 
   */
  PetHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.PetHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'PetIntent' &&
        attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('GAME.PetHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      sessionAttributes.STATS.attachment += settings.MULTIPLIERS.attachmentPlusPet;
      sessionAttributes.STATS.attachment = Math.min(sessionAttributes.STATS.attachment, 100);

      let responseMessage = ctx.t('PET', {
        'pet_sound': sessionAttributes.SPECIES === "katze" ? 'cat-purr-1.mp3' : 'dog-noises-7.mp3'
      });

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      if ('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
        ctx.outputSpeech.push(sessionAttributes.lastReprompt);
        ctx.reprompt.push(sessionAttributes.lastReprompt);
      } else {
        ctx.outputSpeech.push(ctx.t('HELP_REPROMPT').reprompt);
        ctx.reprompt.push(ctx.t('HELP_REPROMPT').reprompt);
      }

      return responseBuilder.getResponse();
    }
  },
  /**
   * 
   */
  SleepHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.SleepHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'SleepIntent' &&
        attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('GAME.SleepHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage = ctx.t('SLEEP', {
        'name': sessionAttributes.NAME,
        'pet_sound': sessionAttributes.SPECIES === "katze" ? 'cat-snoring.mp3' : 'dog-snoring.mp3',
        'pic': sessionAttributes.SPECIES === "katze" ? 'Wohnzimmer-Schalfkorb-Katze-einzeln.jpg' : 'Wohnzimmer-Schalfkorb-Hund-einzeln.jpg'
      });

      sessionAttributes.PLACE = "HOME";
      sessionAttributes.STATE = settings.STATE.SLEEPING_STATE;
      sessionAttributes = Game.saveSleepStart(sessionAttributes);

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.render(handlerInput, responseMessage);
      ctx.openMicrophone = false;
      ctx.endSession = true;

      return responseBuilder.getResponse();
    }
  },
  /**
   * 
   */
  BackpackHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.BackpackHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'BackpackIntent' &&
        attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('GAME.BackpackHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      if (Game.isInventoryEmpty(sessionAttributes)) {
        responseMessage = ctx.t('BACKPACK_EMPTY');
      } else {
        var cleanedInventory = [];
        for (var i = 0; i < sessionAttributes.INVENTORY.length; i++)
          if (sessionAttributes.INVENTORY[i].amount > 0 && (Game.getItemById(sessionAttributes.INVENTORY[i].id).type === 'Consumable' || Game.getItemById(sessionAttributes.INVENTORY[i].id).type === 'Multi'))
            cleanedInventory.push(sessionAttributes.INVENTORY[i]);

        var entries = "";
        for (var i = 0; i < cleanedInventory.length; i++) {
          entries += ctx.t('BACKPACK_ENTRY', {
            'item_amount_name': Game.buildBuyPrompt(cleanedInventory[i].amount, cleanedInventory[i].id, requestEnvelope)
          }).outputSpeech;

          if (i == cleanedInventory.length - 2) {
            if (requestEnvelope.request.locale == 'de-DE')
              entries += " und ";
            else
              entries += " and ";
          } else if (i == cleanedInventory.length - 1) {
            entries += "";
          } else {
            entries += ", ";
          }
        }
        responseMessage = ctx.t('BACKPACK', { 'entries': entries });
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      if ('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
        ctx.outputSpeech.push(sessionAttributes.lastReprompt);
        ctx.reprompt.push(sessionAttributes.lastReprompt);
      } else {
        ctx.outputSpeech.push(ctx.t('HELP_REPROMPT').reprompt);
        ctx.reprompt.push(ctx.t('HELP_REPROMPT').reprompt);
      }

      return responseBuilder.getResponse();
    }
  },
  /**
   * 
   */
  UseItemHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.UseItemHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        (requestEnvelope.request.intent.name === 'UseItemIntent' || requestEnvelope.request.intent.name === 'RewardIntent') &&
        (attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" || attributesManager.getSessionAttributes().TUTORIAL_STATE === "Feed") &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('GAME.UseItemHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;

      if (requestEnvelope.request.intent && requestEnvelope.request.intent.slots && requestEnvelope.request.intent.slots.items && requestEnvelope.request.intent.slots.items.value) {
        var item = Game.getItemByName(requestEnvelope.request.intent.slots.items.value, requestEnvelope.request.locale);
      } 
	  
	  if(!item) {
        // Workaround ask Bug "Cannot read property 'handle' of undefined"  
        ctx.outputSpeech.push(ctx.t('UNHANDLED_REQUEST').outputSpeech);

        if ('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
          ctx.outputSpeech.push(sessionAttributes.lastReprompt);
          ctx.reprompt.push(sessionAttributes.lastReprompt);
        } else {
          ctx.reprompt.push(ctx.t('UNHANDLED_REQUEST').reprompt);
        }

        return responseBuilder.getResponse();
        // Workaround End
      }

      sessionAttributes.SELECTED_ITEM = {
        id: item.id,
        name: requestEnvelope.request.locale == "de-DE" ? item.name_de : item.name_en,
        price: item.price,
        amount: requestEnvelope.request.intent.slots.amount !== undefined && requestEnvelope.request.intent.slots.amount.value !== undefined && requestEnvelope.request.intent.slots.amount.value != "?" ? requestEnvelope.request.intent.slots.amount.value : 1
      }

      if (requestEnvelope.request.intent.name === 'RewardIntent')
        item = Game.getItemById(2);

      if (Game.hasItemInInventory(item.id, sessionAttributes)) {
        switch (item.id) {
          case 0:
            responseMessage = ctx.t('BACKPACK_USE_ITEM_WATER', {
              'name': sessionAttributes.NAME,
              'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'cat-drinking.mp3' : 'dog-drinking.mp3'
            });
            sessionAttributes = Game.removeItemToInventory(item.id, sessionAttributes);
            sessionAttributes.STATS.thirst = 0;
            break;
          case 1:
            responseMessage = ctx.t('BACKPACK_USE_ITEM_FOOD', {
              'name': sessionAttributes.NAME,
              'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'cat-chew-food-1.mp3' : 'dog-chew-food-1.mp3'
            });
            sessionAttributes = Game.removeItemToInventory(item.id, sessionAttributes);
            sessionAttributes.STATS.hunger = 0;

            if (sessionAttributes.TUTORIAL_STATE === "Feed") {
              ctx.outputSpeech.push(responseMessage.outputSpeech);
              responseMessage = ctx.t('TUTORIAL_END');
              ctx.outputSpeech.push(responseMessage.outputSpeech);
              sessionAttributes.SETUP_STATE = "TutorialSkip"
              return require('./startHandlers.js').LaunchPlayGameHandler.handle(handlerInput);
            }
            break;
          case 2:
            responseMessage = ctx.t('BACKPACK_USE_ITEM_TREAT', {
              'name': sessionAttributes.NAME,
              'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'cat-chew-food-1-short.mp3' : 'dog-chew-food-1-short.mp3'
            });
            sessionAttributes = Game.removeItemToInventory(item.id, sessionAttributes);
            sessionAttributes.STATS.hunger -= settings.MULTIPLIERS.treatsHunger;
            sessionAttributes.STATS.hunger = Math.max(0, sessionAttributes.STATS.hunger);
            sessionAttributes.STATS.attachment += settings.MULTIPLIERS.treatsAttachment;
            sessionAttributes.STATS.attachment = Math.min(sessionAttributes.STATS.attachment, 100);
            break;
          case 3:
            responseMessage = ctx.t('BACKPACK_USE_ITEM_NOT_USABLE', { 'name': sessionAttributes.SELECTED_ITEM.name });
            break;
          case 4:
            responseMessage = ctx.t('BACKPACK_USE_ITEM_TOY_BONE', {
              'name': sessionAttributes.NAME,
              'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'Meow/cat-meow-5.mp3' : 'Misc/dog-noises-7.mp3',
              'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'Er' : 'Sie') : (sessionAttributes.GENDER == 0 ? "he" : "she")
            });
            sessionAttributes.STATS.boredom -= settings.MULTIPLIERS.boredomUsageToy;
            sessionAttributes.STATS.boredom = Math.max(0, sessionAttributes.STATS.boredom);
            break;
          case 5:
            responseMessage = ctx.t('BACKPACK_USE_ITEM_NOT_USABLE', { 'name': sessionAttributes.SELECTED_ITEM.name });
            break;
          case 6:
            responseMessage = ctx.t('BACKPACK_USE_ITEM_TOY_WOOLBALL', {
              'name': sessionAttributes.NAME,
              'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'Meow/cat-meow-5.mp3' : 'Misc/dog-noises-7.mp3',
              'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'Er' : 'Sie') : (sessionAttributes.GENDER == 0 ? "he" : "she")
            });
            sessionAttributes.STATS.boredom -= settings.MULTIPLIERS.boredomUsageToy;
            sessionAttributes.STATS.boredom = Math.max(0, sessionAttributes.STATS.boredom);
            break;
          case 7:
            responseMessage = ctx.t('BACKPACK_USE_ITEM_NOT_USABLE', { 'name': sessionAttributes.SELECTED_ITEM.name });
            break;
        }
      } else {
        responseMessage = ctx.t('BACKPACK_USE_ITEM_NOT_OWNED', { 'item_name': requestEnvelope.request.locale == 'de-DE' ? item.name_de : item.name_en });
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      if ('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
        ctx.outputSpeech.push(sessionAttributes.lastReprompt);
        ctx.reprompt.push(sessionAttributes.lastReprompt);
      } else {
        ctx.outputSpeech.push(ctx.t('HELP_REPROMPT').reprompt);
        ctx.reprompt.push(ctx.t('HELP_REPROMPT').reprompt);
      }

      return responseBuilder.getResponse();
    }
  },
  BuyItemHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.BuyItemHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'BuyItemIntent' &&
        attributesManager.getSessionAttributes().PLACE === "SUPERMARKET" &&
        (attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" || attributesManager.getSessionAttributes().TUTORIAL_STATE === "Supermarket") &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('GAME.BuyItemHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;

      if (requestEnvelope.request.intent && requestEnvelope.request.intent.slots && requestEnvelope.request.intent.slots.items && requestEnvelope.request.intent.slots.items.value) {
        var item = Game.getItemByName(requestEnvelope.request.intent.slots.items.value, requestEnvelope.request.locale);
      } else {
        // Workaround ask Bug "Cannot read property 'handle' of undefined"  
        ctx.outputSpeech.push(ctx.t('UNHANDLED_REQUEST').outputSpeech);

        if ('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
          ctx.outputSpeech.push(sessionAttributes.lastReprompt);
          ctx.reprompt.push(sessionAttributes.lastReprompt);
        } else {
          ctx.reprompt.push(ctx.t('UNHANDLED_REQUEST').reprompt);
        }

        return responseBuilder.getResponse();
        // Workaround End
      }

      sessionAttributes.SELECTED_ITEM = {
        id: item.id,
        name: requestEnvelope.request.locale == "de-DE" ? item.name_de : item.name_en,
        price: item.price,
        amount: requestEnvelope.request.intent.slots.amount !== undefined && requestEnvelope.request.intent.slots.amount.value !== undefined && requestEnvelope.request.intent.slots.amount.value != "?" ? requestEnvelope.request.intent.slots.amount.value : 1
      }

      sessionAttributes.GAME_STATE = "Buy";
      responseMessage = ctx.t('SUPERMARKET_BUY', {
        'item_amount_name': Game.buildBuyPrompt(sessionAttributes.SELECTED_ITEM.amount, sessionAttributes.SELECTED_ITEM.id, requestEnvelope),
        'item_price': sessionAttributes.SELECTED_ITEM.price * sessionAttributes.SELECTED_ITEM.amount
      });

      if (sessionAttributes.TUTORIAL_STATE === "Supermarket") {
        if (sessionAttributes.SELECTED_ITEM.id == 1) {
          sessionAttributes.SELECTED_ITEM.amount = 1;
          sessionAttributes = Game.addItemToInventory(sessionAttributes.SELECTED_ITEM.id, sessionAttributes.SELECTED_ITEM.amount, sessionAttributes);

          responseMessage = ctx.t('SUPERMARKET_SUCCESS', {
            'item_amount_name': Game.buildBuyPrompt(sessionAttributes.SELECTED_ITEM.amount, sessionAttributes.SELECTED_ITEM.id, requestEnvelope)
          });
          ctx.outputSpeech.push(responseMessage.outputSpeech);

          sessionAttributes.TUTORIAL_STATE = "Feed";
          responseMessage = ctx.t('TUTORIAL_FEED', {
            'name': sessionAttributes.NAME
          });
        } else { // Wrong Item id (Futter erwartet)

          responseMessage = ctx.t('TUTORIAL_SUPERMARKET');
          ctx.outputSpeech.push(responseMessage.reprompt);
          ctx.reprompt.push(responseMessage.reprompt);
          ctx.render(handlerInput, responseMessage);

          return responseBuilder.getResponse();
        }
      }
      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },
  /**
   * 
   */
  StatHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.StatHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'StatIntent' &&
        (attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" || attributesManager.getSessionAttributes().TUTORIAL_STATE === "Start") &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('GAME.StatHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      var stat = requestEnvelope.request.intent.slots && requestEnvelope.request.intent.slots.stats && requestEnvelope.request.intent.slots.stats.value ? requestEnvelope.request.intent.slots.stats.value : "";

      switch (stat) {
        case "langweilig":
        case "langweile":
        case "bored":
          if (sessionAttributes.STATS.boredom <= 100 && sessionAttributes.STATS.boredom > 90) {
            responseMessage = ctx.t('STAT_BOREDOM_HIGH', {
              'name': sessionAttributes.NAME,
              'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'ihm' : 'ihr') : (sessionAttributes.GENDER == 0 ? "him" : "her")
            });
          } else if (sessionAttributes.STATS.boredom <= 90 && sessionAttributes.STATS.boredom > 70) {
            responseMessage = ctx.t('STAT_BOREDOM_MID', { 'name': sessionAttributes.NAME });
          } else {
            responseMessage = ctx.t('STAT_BOREDOM_LOW', { 'name': sessionAttributes.NAME });
          }
          break;
        case "krank":
        case "ill":
        case "sick":
          if (sessionAttributes.STATS.illness <= 100 && sessionAttributes.STATS.illness > 80) {
            responseMessage = ctx.t('STAT_ILLNESS_HIGH', {
              'name': sessionAttributes.NAME,
              'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'ihn' : 'sie') : (sessionAttributes.GENDER == 0 ? "him" : "her")
            });
          } else if (sessionAttributes.STATS.illness <= 80 && sessionAttributes.STATS.illness > 40) {
            responseMessage = ctx.t('STAT_ILLNESS_MID', { 'name': sessionAttributes.NAME });
          } else {
            responseMessage = ctx.t('STAT_ILLNESS_LOW', { 'name': sessionAttributes.NAME });
          }
          break;
        case "hungrig":
        case "hunger":
        case "hungry":
          if (sessionAttributes.STATS.hunger <= 100 && sessionAttributes.STATS.hunger > 90) {
            if (Game.hasItemInInventory(1, sessionAttributes))
              speech = ctx.t('STAT_HUNGER_HIGH_FOOD', { 'name': sessionAttributes.NAME });
            else
              speech = ctx.t('STAT_HUNGER_HIGH_NO_FOOD', { 'name': sessionAttributes.NAME });
          } else if (sessionAttributes.STATS.hunger <= 90 && sessionAttributes.STATS.hunger > 60) {
            responseMessage = ctx.t('STAT_HUNGER_MID', { 'name': sessionAttributes.NAME });
          } else {
            responseMessage = ctx.t('STAT_HUNGER_LOW', { 'name': sessionAttributes.NAME });
          }
          break;
        case "durstig":
        case "durst":
        case "thirsty":
          if (sessionAttributes.STATS.thirst <= 100 && sessionAttributes.STATS.thirst > 90) {
            if (Game.hasItemInInventory(0, sessionAttributes))
              speech = ctx.t('STAT_THIRST_HIGH_WATER', { 'name': sessionAttributes.NAME });
            else
              speech = ctx.t('STAT_THIRST_HIGH_NO_WATER', { 'name': sessionAttributes.NAME });
          } else if (sessionAttributes.STATS.thirst <= 90 && sessionAttributes.STATS.thirst > 60) {
            responseMessage = ctx.t('STAT_THIRST_MID', { 'name': sessionAttributes.NAME });
          } else {
            responseMessage = ctx.t('STAT_THIRST_LOW', { 'name': sessionAttributes.NAME });
          }
          break;
        case "müde":
        case "tired":
          if (sessionAttributes.STATS.fatigue <= 100 && sessionAttributes.STATS.fatigue > 90) {
            responseMessage = ctx.t('STAT_FATIGUE_HIGH', {
              'name': sessionAttributes.NAME,
              'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'ihn' : 'sie') : (sessionAttributes.GENDER == 0 ? "him" : "her")
            });
          } else if (sessionAttributes.STATS.fatigue <= 90 && sessionAttributes.STATS.fatigue > 70) {
            responseMessage = ctx.t('STAT_FATIGUE_MID', { 'name': sessionAttributes.NAME });
          } else {
            responseMessage = ctx.t('STAT_FATIGUE_LOW', { 'name': sessionAttributes.NAME });
          }
          break;

        default:
          responseMessage = ctx.t('STAT_GENERAL', {
            'name': sessionAttributes.NAME,
            'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'er' : 'sie') : (sessionAttributes.GENDER == 0 ? "he" : "she")
          });
          break;
      }

      if (sessionAttributes.TUTORIAL_STATE === "Start") {
        ctx.outputSpeech.push(responseMessage.outputSpeech);
        sessionAttributes.TUTORIAL_STATE = "Hunger";
        responseMessage = ctx.t('TUTORIAL_HUNGER', {
          'name': sessionAttributes.NAME,
          'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'er' : 'sie') : (sessionAttributes.GENDER == 0 ? "he" : "she"),
          'pic': sessionAttributes.SPECIES === "katze" ? 'Wohnzimmer-Schalfkorb-Katze-einzeln.jpg' : 'Wohnzimmer-Schalfkorb-Hund-einzeln.jpg'
        });
        ctx.outputSpeech.push(responseMessage.outputSpeech);
        ctx.reprompt.push(responseMessage.outputSpeech);
        ctx.render(handlerInput, responseMessage);
      } else {
        ctx.outputSpeech.push(responseMessage.outputSpeech);
        if ('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
          ctx.outputSpeech.push(sessionAttributes.lastReprompt);
          ctx.reprompt.push(sessionAttributes.lastReprompt);
        } else {
          ctx.outputSpeech.push(ctx.t('HELP_REPROMPT').reprompt);
          ctx.reprompt.push(ctx.t('HELP_REPROMPT').reprompt);
        }
      }

      return responseBuilder.getResponse();
    }
  },
  /**
   * 
   */
  ComeComeHandler: {
    canHandle(handlerInput) {
      logger.debug('GAME.ComeComeHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'ComeComeIntent' &&
        attributesManager.getSessionAttributes().TUTORIAL_STATE === "Done" &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('GAME.ComeComeHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage = ctx.t('COME_COME', {
        'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'Meow/cat-small-kitty-08.mp3' : 'Misc/dog-whine-2.mp3'
      });

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      if ('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
        ctx.outputSpeech.push(sessionAttributes.lastReprompt);
        ctx.reprompt.push(sessionAttributes.lastReprompt);
      } else {
        ctx.outputSpeech.push(ctx.t('HELP_REPROMPT').reprompt);
        ctx.reprompt.push(ctx.t('HELP_REPROMPT').reprompt);
      }

      return responseBuilder.getResponse();
    }
  },
  SkipTutorialHandler: {
    canHandle(handlerInput) {
      logger.debug('START.SkipTutorialHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'AMAZON.NextIntent' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE &&
        attributesManager.getSessionAttributes().SETUP_STATE === 'Tutorial';
    },
    handle(handlerInput) {
      logger.debug('START.SkipTutorialHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      sessionAttributes.SETUP_STATE = 'TutorialSkip';
      return require('./startHandlers.js').LaunchPlayGameHandler.handle(handlerInput);
    }
  },
  FeedHandler: {
    canHandle(handlerInput) {
      logger.debug('START.FeedHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'FeedIntent' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE &&
        attributesManager.getSessionAttributes().SETUP_STATE === 'Done';
    },
    handle(handlerInput) {
      logger.debug('START.FeedHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage = ctx.t('FEED_PET', {
        'name': sessionAttributes.NAME
      });

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },
  LookAgeHandler: {
    canHandle(handlerInput) {
      logger.debug('START.LookAgeHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'LookAgeIntent' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE &&
        attributesManager.getSessionAttributes().SETUP_STATE === 'Done';
    },
    handle(handlerInput) {
      logger.debug('START.LookAgeHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      if (sessionAttributes.SPECIES == 'katze') {
        responseMessage = ctx.t('LOOK_AGE_CAT', {
          'name': sessionAttributes.NAME,
          'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'seinem' : 'ihrem') : (sessionAttributes.GENDER == 0 ? "his" : "her"),
          'gender_2': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'Er' : 'Sie') : (sessionAttributes.GENDER == 0 ? "he" : "she")
        });
      } else {
        responseMessage = ctx.t('LOOK_AGE_DOG', {
          'name': sessionAttributes.NAME,
          'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'Er' : 'Sie') : (sessionAttributes.GENDER == 0 ? "he" : "she")
        });
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      if ('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
        ctx.outputSpeech.push(sessionAttributes.lastReprompt);
        ctx.reprompt.push(sessionAttributes.lastReprompt);
      } else {
        ctx.outputSpeech.push(ctx.t('HELP_REPROMPT').reprompt);
        ctx.reprompt.push(ctx.t('HELP_REPROMPT').reprompt);
      }

      return responseBuilder.getResponse();
    }
  }
}

module.exports = gamePlayHandlers;