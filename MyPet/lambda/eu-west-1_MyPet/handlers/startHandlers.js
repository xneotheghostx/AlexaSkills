/*
 * Copyright 2018 Amazon.com, Inc. and its affiliates. All Rights Reserved.
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * http://aws.amazon.com/asl/
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

const logger = require('../utils/logger.js');
const settings = require('../config/settings.js');
const Game = require('../utils/game.js');
const gamePlayHandlers = require('./gamePlayHandlers.js');

const startHandlers = {
  /**
   * Invoked when a user says 'open' or 'play' or some other variant
   */
  LaunchPlayGameHandler: {
    canHandle(handlerInput) {
      logger.debug('START.LaunchPlayGameHandler: canHandle');
      let {
        requestEnvelope
      } = handlerInput;
      return (requestEnvelope.request.type === 'LaunchRequest' ||
        requestEnvelope.request.type === 'NewSession');
    },
    handle(handlerInput) {
      logger.debug('START.LaunchPlayGameHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let ctx = attributesManager.getRequestAttributes();
      let sessionAttributes = attributesManager.getSessionAttributes();

      /*var text = 'Schaue dir den Stadtplan an. Und Besuche verschiedene Orte. <break time="750ms"/>wie dein Zuhause, <break time="750ms"/>Supermarkt, <break time="750ms"/>Tierarzt <break time="750ms"/>oder den Park. Spiele dort bis zu 6 verschiedene Mini-Spiele. ' +
        'Zur Auswahl stehen: <break time="750ms"/> Tierarten raten, <break time="750ms"/>Haustier Quiz, <break time="750ms"/>Memory, <break time="750ms"/>Hütchenspiel, <break time="750ms"/>Wer bin ich, <break time="750ms"/>oder Stöckchen werfen. ' +
        "Nutze deine Münzen, um dir etwas für dein Haustier zu kaufen. " +
        "Hör und schau deinem Tier beim wachsen zu, denn das Aussehen und Verhalten verändert sich im Spielverlauf. " +
        "Auch Katzenliebhaber kommen nicht zu kurz. Der gesamte Skill ist auch mit Katzen spielbar. <audio src='https://nordicskills.de/audio/MeinHaustier/Meow/meowing-cat-1.mp3'/> " +
        "Mein Haustier funktioniert nicht nur auf dem Echo Show, sondern ist mit allen Alexa-Geräten nutzbar. ";
      ctx.outputSpeech.push(text);
      ctx.reprompt.push(text);

      return responseBuilder.getResponse();*/

      /*var text = 'Look at the map. And visit different places. <break time="750ms"/>like your home, <break time="750ms"/>supermarket, <break time="750ms"/>vet <break time="750ms"/>or park. Play up to 6 different mini-games there. ' +
        'You can choose from: <break time="750ms"/> guess the species, <break time="750ms"/>pet quiz, <break time="750ms"/>memory, <break time="750ms"/>shell game, <break time="750ms"/>who am i, <break time="750ms"/>or throwing stick. ' +
        "Use your coins to buy something for your pet. " +
        "Listen and watch your pet grow, because the appearance and behavior changes as the game progresses. " +
        "Cat lovers are also not neglected. The entire skill is also playable with cats. <audio src='https://nordicskills.de/audio/MeinHaustier/Meow/meowing-cat-1.mp3'/> " +
        "My pet not only works on the Echo Show, but can be used with all Alexa devices. ";
      ctx.outputSpeech.push(text);
      ctx.reprompt.push(text);

      return responseBuilder.getResponse();*/

      /**
       * If we have an active game ask to resume, otherwise start a new game and ask how many players.
       */
      let responseMessage;
      if (sessionAttributes.STATE === settings.STATE.SLEEPING_STATE) {

        var sleepFatigueDiff = Math.max(0, sessionAttributes.STATS.fatigue - (Game.getSleepDiff(sessionAttributes) * settings.MULTIPLIERS.sleepMultiplier));

        if (sleepFatigueDiff == 0 || sessionAttributes.SETUP_STATE === "ForceWake") {

          if (sleepFatigueDiff <= 100 && sleepFatigueDiff > 80) {
            responseMessage = ctx.t('SLEEP_BAD', {
              'name': sessionAttributes.NAME,
              'sleep_time': Game.switchEinsEineSleep(Game.getSleepDiff(sessionAttributes), requestEnvelope),
              'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'ihn' : 'sie') : (sessionAttributes.GENDER == 0 ? "him" : "her")
            });
          } else if (sleepFatigueDiff <= 80 && sleepFatigueDiff > 50) {
            responseMessage = ctx.t('SLEEP_OKAY', {
              'name': sessionAttributes.NAME,
              'sleep_time': Game.switchEinsEineSleep(Game.getSleepDiff(sessionAttributes), requestEnvelope)
            });
          } else if (sleepFatigueDiff <= 50 && sleepFatigueDiff > 20) {
            responseMessage = ctx.t('SLEEP_GOOD', {
              'name': sessionAttributes.NAME,
              'sleep_time': Game.switchEinsEineSleep(Game.getSleepDiff(sessionAttributes), requestEnvelope)
            });
          } else if (sleepFatigueDiff <= 20 && sleepFatigueDiff > 0) {
            responseMessage = ctx.t('SLEEP_PERFECT', {
              'name': sessionAttributes.NAME,
              'sleep_time': Game.switchEinsEineSleep(Game.getSleepDiff(sessionAttributes), requestEnvelope)
            });
          } else {
            responseMessage = ctx.t('SLEEP_ALREADY_AWAKE', {
              'name': sessionAttributes.NAME
            });
          }
          ctx.outputSpeech.push(responseMessage.outputSpeech);

          delete sessionAttributes.SLEEP_START;
          sessionAttributes.STATS.fatigue = sleepFatigueDiff;
          sessionAttributes.STATE = settings.STATE.START_GAME_STATE;
          sessionAttributes.SETUP_STATE = "Done";
        } else {
          responseMessage = ctx.t('STILL_SLEEPING', {
            'name': sessionAttributes.NAME,
            'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'ihn' : 'sie') : (sessionAttributes.GENDER == 0 ? "him" : "her"),
            'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'cat-snoring.mp3' : 'dog-snoring.mp3',
            'pic': sessionAttributes.SPECIES === "katze" ? 'Wohnzimmer-Schalfkorb-Katze-einzeln.jpg' : 'Wohnzimmer-Schalfkorb-Hund-einzeln.jpg'
          });
          sessionAttributes.SETUP_STATE = "WakeRequest";
        }
      }

      if (sessionAttributes.SETUP_STATE === "ResetRequest") { // Reset Request

        responseMessage = ctx.t('RESET_REQUEST');
      } else if (sessionAttributes.SETUP_STATE === "Tutorial" || sessionAttributes.SETUP_STATE === "TutorialSkip") { // Init after inital setup
        sessionAttributes.COINS = 100;
        sessionAttributes.STATS = {
          illness: 0,
          hunger: 0,
          thirst: 0,
          boredom: 0,
          fatigue: 0,
          attachment: 50,
          age: 1
        };
        sessionAttributes.INVENTORY = [];
        sessionAttributes.PLACE = "HOME";
        sessionAttributes = Game.updateAttributes(sessionAttributes);

        if (sessionAttributes.SETUP_STATE === "Tutorial") {
          sessionAttributes.STATE = settings.STATE.MAIN_GAME_STATE;
          sessionAttributes.TUTORIAL_STATE = "Start";
          responseMessage = ctx.t('TUTORIAL_START', {
            'name': sessionAttributes.NAME,
            'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'er' : 'sie') : (sessionAttributes.GENDER == 0 ? "he" : "she"),
            'pic': sessionAttributes.SPECIES === "katze" ? 'Wohnzimmer-Schalfkorb-Katze-einzeln.jpg' : 'Wohnzimmer-Schalfkorb-Hund-einzeln.jpg'
          });
        } else {
          sessionAttributes.SETUP_STATE = "Done";
          sessionAttributes.TUTORIAL_STATE = "Done";
          return startHandlers.LaunchPlayGameHandler.handle(handlerInput);
        }
        ctx.render(handlerInput, responseMessage);

      } else if (sessionAttributes.SETUP_STATE === "Done") { // Setup completed

        sessionAttributes.STATE = settings.STATE.MAIN_GAME_STATE;
        //return gamePlayHandlers.MenuHandler.handle(handlerInput);

        if (!sessionAttributes.PLACE)
          sessionAttributes.PLACE = "HOME";

        sessionAttributes = Game.updateAttributes(sessionAttributes);
        
        sessionAttributes.CITY_WELCOME_COUNT = 0;

        let speech;
        switch (sessionAttributes.PLACE) {
          case "HOME":
            speech = ctx.t('START_HOME', { 
              'pet_sound': Game.randomPetSound(sessionAttributes),
              'pic': Game.getHomePic(sessionAttributes)
            });
            break;
          case "PARK":
            speech = ctx.t('START_PARK', { 
              'pet_sound': Game.randomPetSound(sessionAttributes),
              'pic': sessionAttributes.SPECIES === "katze" ? 'Park-mit-Katze.jpg' : 'Park-mit-Hund.jpg'
            });
            break;

          default: // CITY, SUPERMARKET, VET
            sessionAttributes.PLACE = "CITY";
            speech = ctx.t('START_CITY', { 
              'pet_sound': Game.randomPetSound(sessionAttributes),
              'pic': Game.getCityPic(sessionAttributes, requestEnvelope)
            });
            break;
        }
        ctx.outputSpeech.push(speech.outputSpeech);
        ctx.render(handlerInput, speech);

        if (sessionAttributes.STATS.illness <= 100 && sessionAttributes.STATS.illness > 80) {
          speech = ctx.t('STAT_ILLNESS_HIGH', {
            'name': sessionAttributes.NAME,
            'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'ihn' : 'sie') : (sessionAttributes.GENDER == 0 ? "him" : "her")
          });
        } else if (sessionAttributes.STATS.fatigue <= 100 && sessionAttributes.STATS.fatigue > 90) {
          speech = ctx.t('STAT_FATIGUE_HIGH', {
            'name': sessionAttributes.NAME,
            'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'ihn' : 'sie') : (sessionAttributes.GENDER == 0 ? "him" : "her")
          });
        } else if (sessionAttributes.STATS.hunger <= 100 && sessionAttributes.STATS.hunger > 90) {
          if(Game.hasItemInInventory(1, sessionAttributes))
            speech = ctx.t('STAT_HUNGER_HIGH_FOOD', { 'name': sessionAttributes.NAME });
          else
            speech = ctx.t('STAT_HUNGER_HIGH_NO_FOOD', { 'name': sessionAttributes.NAME });
        } else if (sessionAttributes.STATS.thirst <= 100 && sessionAttributes.STATS.thirst > 90) {
          if(Game.hasItemInInventory(0, sessionAttributes))
            speech = ctx.t('STAT_THIRST_HIGH_WATER', { 'name': sessionAttributes.NAME });
          else
            speech = ctx.t('STAT_THIRST_HIGH_NO_WATER', { 'name': sessionAttributes.NAME });
        } else if (sessionAttributes.STATS.boredom <= 100 && sessionAttributes.STATS.boredom > 90) {
          speech = ctx.t('STAT_BOREDOM_HIGH', {
            'name': sessionAttributes.NAME,
            'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? 'ihm' : 'ihr') : (sessionAttributes.GENDER == 0 ? "him" : "her")
          });
        } else if (sessionAttributes.STATS.illness <= 80 && sessionAttributes.STATS.illness > 40) {
          speech = ctx.t('STAT_ILLNESS_MID', { 'name': sessionAttributes.NAME });
        } else if (sessionAttributes.STATS.fatigue <= 90 && sessionAttributes.STATS.fatigue > 70) {
          speech = ctx.t('STAT_FATIGUE_MID', { 'name': sessionAttributes.NAME });
        } else if (sessionAttributes.STATS.hunger <= 90 && sessionAttributes.STATS.hunger > 60) {
          speech = ctx.t('STAT_HUNGER_MID', { 'name': sessionAttributes.NAME });
        } else if (sessionAttributes.STATS.thirst <= 90 && sessionAttributes.STATS.thirst > 60) {
          speech = ctx.t('STAT_THIRST_MID', { 'name': sessionAttributes.NAME });
        } else if (sessionAttributes.STATS.boredom <= 90 && sessionAttributes.STATS.boredom > 70) {
          speech = ctx.t('STAT_BOREDOM_MID', { 'name': sessionAttributes.NAME });
        } else {
          speech = ctx.t('STAT_ALL_GOOD', { 'name': sessionAttributes.NAME });
        }
        ctx.outputSpeech.push(speech.outputSpeech);

        if(sessionAttributes.RequestParkReprompt) {
          delete sessionAttributes.RequestParkReprompt;
          return gamePlayHandlers.ParkRepromptHandler.handle(handlerInput);
        } else {
          responseMessage = ctx.t('WHATS_NEXT_' + (Math.floor(Math.random() * 3) + 1).toString());
        }

      } else if (sessionAttributes.SETUP_STATE !== "WakeRequest") { // Start Setup

        responseMessage = ctx.t('START_GAME');
        ctx.render(handlerInput, responseMessage);

        // it's a new game so delete all attributes
        let attributeNames = Object.keys(sessionAttributes);
        for (let k = 0; k < attributeNames.length; k++) {
          delete sessionAttributes[attributeNames[k]];
        }

        sessionAttributes.STATE = settings.STATE.START_GAME_STATE;
        sessionAttributes.NAME_TRIES = 0;
        sessionAttributes.SETUP_STATE = "OpenDoor";
      }
      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },

  /**
   * The player has responded 'yes' to the option of resuming the previous game.
   */
  YesHandler: {
    canHandle(handlerInput) {
      logger.debug('START.YesHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'AMAZON.YesIntent' &&
        (attributesManager.getSessionAttributes().STATE === settings.STATE.START_GAME_STATE ||
          attributesManager.getSessionAttributes().STATE === settings.STATE.SLEEPING_STATE) &&
        (attributesManager.getSessionAttributes().SETUP_STATE === "OpenDoor" ||
          attributesManager.getSessionAttributes().SETUP_STATE === "DontOpen" ||
          attributesManager.getSessionAttributes().SETUP_STATE === "LookAround" ||
          attributesManager.getSessionAttributes().SETUP_STATE === "NameRequest" ||
          attributesManager.getSessionAttributes().SETUP_STATE === "ConfirmName" ||
          attributesManager.getSessionAttributes().SETUP_STATE === "ResetRequest" ||
          attributesManager.getSessionAttributes().SETUP_STATE === "WakeRequest");
    },
    handle(handlerInput) {
      logger.debug('START.YesHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;

      switch (sessionAttributes.SETUP_STATE) {
        case "OpenDoor":
        case "DontOpen":
          sessionAttributes.GENDER = Math.round(Math.random()); // 0 = "Male" | 1 = "Female"
          sessionAttributes.SPECIES = Math.round(Math.random()) == 0 ? "hund" : "katze";
          sessionAttributes.NAME = Game.generateRandomPetName(sessionAttributes.GENDER);
          responseMessage = ctx.t('OPEN_DOOR', {
            'pet_sound_1': sessionAttributes.SPECIES === "katze" ? 'Meow/cat-meow-6.mp3' : 'dog-whining-01.mp3',
            'pet_sound_2': sessionAttributes.SPECIES === "katze" ? 'Meow/cat-meow-6.mp3' : 'dog-whining-02.mp3',
            'species_sweet': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.SPECIES === "katze" ? 'Kätzchen' : 'Hündchen') : (sessionAttributes.SPECIES === "katze" ? 'Kitten' : 'puppy'),
            'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "ihn" : "sie") : (sessionAttributes.GENDER == 0 ? "him" : "her"),
            'pic': sessionAttributes.SPECIES === "katze" ? 'Eingangsbereich-geöffnet-Katze.jpg' : 'Eingangsbereich-geöffnet-Hund.jpg'
          });
          sessionAttributes.SETUP_STATE = "KeepReturn";
          break;
        case "LookAround":
          responseMessage = ctx.t('RETURN_AROUND');
          sessionAttributes.SETUP_STATE = "ChooseSpecies";
          break;
        case "NameRequest":
          responseMessage = ctx.t('NEW_NAME', {
            'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "ihn" : "sie") : (sessionAttributes.GENDER == 0 ? "him" : "her"),
            'gender_2': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "seinen" : "ihren") : (sessionAttributes.GENDER == 0 ? "his" : "her"),
            'pic': sessionAttributes.SPECIES === "katze" ? 'Eingangsbereich-geöffnet-Katze.jpg' : 'Eingangsbereich-geöffnet-Hund.jpg'
          });
          sessionAttributes.SETUP_STATE = "ChooseName";
          break;
        case "ConfirmName":
          sessionAttributes.SETUP_STATE = "Tutorial";
          return startHandlers.LaunchPlayGameHandler.handle(handlerInput);
        case "ResetRequest":
          sessionAttributes.SETUP_STATE = "ChooseSpecies"; // ChooseSpecies just to force new game
          return startHandlers.LaunchPlayGameHandler.handle(handlerInput);
        case "WakeRequest":
          sessionAttributes.SETUP_STATE = "ForceWake";
          return startHandlers.LaunchPlayGameHandler.handle(handlerInput);

        default:
          break;
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);
      ctx.render(handlerInput, responseMessage);

      return responseBuilder.getResponse();
    }
  },
  /**
   * The player has responded 'no' to the option of resuming the previous game.
   */
  NoHandler: {
    canHandle(handlerInput) {
      logger.debug('START.NoHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'AMAZON.NoIntent' &&
        (attributesManager.getSessionAttributes().STATE === settings.STATE.START_GAME_STATE ||
          attributesManager.getSessionAttributes().STATE === settings.STATE.SLEEPING_STATE) &&
        (attributesManager.getSessionAttributes().SETUP_STATE === "OpenDoor" ||
          attributesManager.getSessionAttributes().SETUP_STATE === "DontOpen" ||
          attributesManager.getSessionAttributes().SETUP_STATE === "LookAround" ||
          attributesManager.getSessionAttributes().SETUP_STATE === "NameRequest" ||
          attributesManager.getSessionAttributes().SETUP_STATE === "ConfirmName" ||
          attributesManager.getSessionAttributes().SETUP_STATE === "ResetRequest" ||
          attributesManager.getSessionAttributes().SETUP_STATE === "WakeRequest");
    },
    handle(handlerInput) {
      logger.debug('START.NoHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;

      switch (sessionAttributes.SETUP_STATE) {
        case "OpenDoor":
          responseMessage = ctx.t('DONT_OPEN');
          sessionAttributes.SETUP_STATE = "DontOpen";
          ctx.outputSpeech.push(responseMessage.outputSpeech);
          ctx.reprompt.push(responseMessage.reprompt);
          ctx.render(handlerInput, responseMessage);
          break;
        case "DontOpen":
        case "LookAround":
          responseMessage = ctx.t('STILL_DONT_OPEN');
          ctx.outputSpeech.push(responseMessage.outputSpeech);
          ctx.render(handlerInput, responseMessage);
          ctx.openMicrophone = false;
          ctx.endSession = true;
          break;
        case "NameRequest":
          sessionAttributes.SETUP_STATE = "Tutorial";
          return startHandlers.LaunchPlayGameHandler.handle(handlerInput);
        case "ConfirmName":
          if (sessionAttributes.NAME_TRIES == 1) {
            responseMessage = ctx.t('NAME_WRONG_TWICE', {
              'name': sessionAttributes.NAME,
              'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "ihn" : "sie") : (sessionAttributes.GENDER == 0 ? "him" : "her"),
              'pic': sessionAttributes.SPECIES === "katze" ? 'Eingangsbereich-geöffnet-Katze.jpg' : 'Eingangsbereich-geöffnet-Hund.jpg'
            });
            sessionAttributes.SETUP_STATE = "ConfirmName";
            sessionAttributes.NAME_TRIES = 0;
          } else {
            responseMessage = ctx.t('NAME_REQUEST', { 
              'gender_3': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "seinen" : "ihren") : (sessionAttributes.GENDER == 0 ? "his" : "her"),
              'pic': sessionAttributes.SPECIES === "katze" ? 'Eingangsbereich-geöffnet-Katze.jpg' : 'Eingangsbereich-geöffnet-Hund.jpg'
            });
            sessionAttributes.SETUP_STATE = "ChooseName";
            sessionAttributes.NAME_TRIES += 1;
          }
          ctx.outputSpeech.push(responseMessage.outputSpeech);
          ctx.reprompt.push(responseMessage.reprompt);
          ctx.render(handlerInput, responseMessage);
          break;
        case "ResetRequest":
          sessionAttributes.SETUP_STATE = "Done";
          return startHandlers.LaunchPlayGameHandler.handle(handlerInput);
        case "WakeRequest":
          responseMessage = ctx.t('CONTINUE_SLEEPING', { 'name': sessionAttributes.NAME });
          ctx.outputSpeech.push(responseMessage.outputSpeech);
          ctx.render(handlerInput, responseMessage);
          ctx.openMicrophone = false;
          ctx.endSession = true;
          break;

        default:
          break;
      }

      return responseBuilder.getResponse();
    }
  },
  KeepHandler: {
    canHandle(handlerInput) {
      logger.debug('START.KeepHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'KeepIntent' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.START_GAME_STATE &&
        attributesManager.getSessionAttributes().SETUP_STATE === "KeepReturn";
    },
    handle(handlerInput) {
      logger.debug('START.KeepHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage = ctx.t('KEEP_PET', {
        'name': sessionAttributes.NAME,
        'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "Er" : "Sie") : (sessionAttributes.GENDER == 0 ? "he" : "she"),
        'gender_2': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "ihm" : "ihr") : (sessionAttributes.GENDER == 0 ? "him" : "her"),
        'pic': sessionAttributes.SPECIES === "katze" ? 'Wohnzimmer-Schalfkorb-Katze-einzeln.jpg' : 'Wohnzimmer-Schalfkorb-Hund-einzeln.jpg'
      });
      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);
      ctx.render(handlerInput, responseMessage);

      sessionAttributes.SETUP_STATE = "NameRequest";

      return responseBuilder.getResponse();
    }
  },
  ReturnHandler: {
    canHandle(handlerInput) {
      logger.debug('START.ReturnHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'ReturnIntent' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.START_GAME_STATE &&
        attributesManager.getSessionAttributes().SETUP_STATE === "KeepReturn";
    },
    handle(handlerInput) {
      logger.debug('START.ReturnHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage = ctx.t('RETURN_PET', { 
        'species_sweet': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.SPECIES === "katze" ? 'Kätzchen' : 'Hündchen') : (sessionAttributes.SPECIES === "katze" ? 'Kitten' : 'puppy')
      });
      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);
      ctx.render(handlerInput, responseMessage);

      sessionAttributes.SETUP_STATE = "LookAround";

      return responseBuilder.getResponse();
    }
  },
  SpeciesHandler: {
    canHandle(handlerInput) {
      logger.debug('START.SpeciesHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'AnswerOnlyIntent' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.START_GAME_STATE &&
        attributesManager.getSessionAttributes().SETUP_STATE === "ChooseSpecies";
    },
    handle(handlerInput) {
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      var answer = requestEnvelope.request.intent.slots.answers.value.toLowerCase();

      let responseMessage;
      if (answer == 'hund' || answer == 'katze' || answer == 'dog' || answer == 'cat') {
        if(sessionAttributes.GENDER == 0)
          sessionAttributes.GENDER = 1;
        else 
          sessionAttributes.GENDER = 0;
        
        sessionAttributes.NAME = Game.generateRandomPetName(sessionAttributes.GENDER);

        responseMessage = ctx.t('RETURN_EXIT', {
          'name': sessionAttributes.NAME,
          'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "ihm" : "ihr") : (sessionAttributes.GENDER == 0 ? "him" : "her")
        });
        ctx.outputSpeech.push(responseMessage.outputSpeech);

        sessionAttributes.SPECIES = (answer == 'hund' || answer == 'dog') ? "hund" : "katze";
        sessionAttributes.SETUP_STATE = "NameRequest";

      } else {
        responseMessage = ctx.t('RETURN_AROUND');

        ctx.outputSpeech.push(responseMessage.reprompt);
      }
      ctx.reprompt.push(responseMessage.reprompt);
      ctx.render(handlerInput, responseMessage);

      return responseBuilder.getResponse();
    }
  },
  NameHandler: {
    canHandle(handlerInput) {
      logger.debug('START.NameHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'NameIntent' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.START_GAME_STATE &&
        attributesManager.getSessionAttributes().SETUP_STATE === "ChooseName";
    },
    handle(handlerInput) {
      logger.debug('START.NameHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage = ctx.t('CONFIRM_NAME', {
        'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "ihn" : "sie") : (sessionAttributes.GENDER == 0 ? "him" : "her"),
        'name': requestEnvelope.request.intent.slots.name.value,
        'pic': sessionAttributes.SPECIES === "katze" ? 'Eingangsbereich-geöffnet-Katze.jpg' : 'Eingangsbereich-geöffnet-Hund.jpg'
      });
      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);
      ctx.render(handlerInput, responseMessage);

      sessionAttributes.NAME = requestEnvelope.request.intent.slots.name.value;
      sessionAttributes.SETUP_STATE = "ConfirmName";

      return responseBuilder.getResponse();
    }
  },
  SkipTutorialHandler: {
    canHandle(handlerInput) {
      logger.debug('START.SkipTutorialHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'AMAZON.NextIntent' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.START_GAME_STATE &&
        attributesManager.getSessionAttributes().SETUP_STATE === 'Tutorial';
    },
    handle(handlerInput) {
      logger.debug('START.SkipTutorialHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      sessionAttributes.SETUP_STATE = "TutorialSkip";
      return startHandlers.LaunchPlayGameHandler.handle(handlerInput);
    }
  }
}

module.exports = startHandlers;