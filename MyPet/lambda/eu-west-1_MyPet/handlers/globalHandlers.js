/*
 * Copyright 2018 Amazon.com, Inc. and its affiliates. All Rights Reserved.
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * http://aws.amazon.com/asl/
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
'use strict';

const display = require('../utils/display.js');
const i18n = require('i18next');
const logger = require('../utils/logger.js');
const messages = require('../config/messages.js');
const settings = require('../config/settings.js');
const Game = require('../utils/game.js');
const miniGameHandlers = require('./miniGameHandlers.js');

const globalHandlers = {
  RequestInterceptor: {
    async process(handlerInput) {
      logger.debug('Global.RequestInterceptor: pre-processing response');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      let ctx = attributesManager.getRequestAttributes();
      let persistentAtttributes = await attributesManager.getPersistentAttributes();
      let sessionAttributes = attributesManager.getSessionAttributes();

      /**
       * Ensure a state in case we're starting fresh
       */
      /*if (sessionAttributes.STATE == null) {
        logger.debug('SETTING STATE TO START_GAME_STATE');
        sessionAttributes.STATE = settings.STATE.START_GAME_STATE;
      } else if (sessionAttributes.STATE === '_GAME_LOOP'){
        logger.debug('Changing state from _GAME_LOOP for backward compatability.')
        sessionAttributes.STATE = settings.STATE.BUTTON_GAME_STATE;
      }*/

      // Apply the persistent attributes to the current session
      attributesManager.setSessionAttributes(Object.assign({}, persistentAtttributes, sessionAttributes));

      /**
       * Log the request for debug purposes.
       */
      logger.debug('----- REQUEST -----');
      logger.debug(JSON.stringify(requestEnvelope, null, 2));

      /**
       * Ensure we're starting at a clean state.
       */
      ctx.directives = [];
      ctx.outputSpeech = [];
      ctx.reprompt = [];

      /**
       * For ease of use we'll attach the utilities for rendering display
       * and handling localized tts to the request attributes.
       */
      logger.debug('Initializing messages for ' + handlerInput.requestEnvelope.request.locale);
      const localizationClient = i18n.init({
        lng: handlerInput.requestEnvelope.request.locale,
        resources: messages,
        returnObjects: true,
        fallbackLng: 'en'
      });
      ctx.t = function (...args) {
        return localizationClient.t(...args);
      };
      ctx.render = function (...args) {
        return display.render(...args);
      }
      logger.debug('Global.RequestInterceptor: pre-processing response complete');
    }
  },
  ResponseInterceptor: {
    async process(handlerInput) {
      logger.debug('Global.ResponseInterceptor: post-processing response');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let ctx = attributesManager.getRequestAttributes();
      let sessionAttributes = attributesManager.getSessionAttributes();
      let persistentAtttributes = await attributesManager.getPersistentAttributes();

      /**
       * Log the attributes and response for debug purposes.
       */
      logger.debug('----- REQUEST ATTRIBUTES -----');
      logger.debug(JSON.stringify(ctx, null, 2));

      logger.debug('----- SESSION ATTRIBUTES -----');
      logger.debug(JSON.stringify(sessionAttributes, null, 2));

      logger.debug('----- CURRENT PERSISTENT ATTRIBUTES -----');
      logger.debug(JSON.stringify(persistentAtttributes, null, 2));

      /**
       * Build the speech response.
       */
      if (ctx.outputSpeech.length > 0) {
        logger.debug('Global.ResponseInterceptor: adding ' +
          ctx.outputSpeech.length + ' speech parts');
		let outputSpeech = ctx.outputSpeech.join(' ');  
		
		sessionAttributes.lastSpeech = outputSpeech; 
        responseBuilder.speak(outputSpeech);
      }
      if (ctx.reprompt.length > 0) {
        logger.debug('Global.ResponseInterceptor: adding ' +
          ctx.outputSpeech.length + ' speech reprompt parts');
        let reprompt = ctx.reprompt.join(' ');
        
        sessionAttributes.lastReprompt = reprompt; 
        responseBuilder.reprompt(reprompt);
      }

      /**
       * Add the display response
       */
      if (ctx.renderTemplate) {
        if(ctx.renderTemplate.backgroundImage && ctx.renderTemplate.backgroundImage.sources && ctx.renderTemplate.backgroundImage.sources[0].url && 
          ctx.renderTemplate.backgroundImage.sources[0].url != 'https://nordicskills.de/audio/MeinHaustier/hund.jpg')
          sessionAttributes.lastBackgroundImage = ctx.renderTemplate.backgroundImage.sources[0].url;

        if(Game.supportsDisplay(requestEnvelope))
          responseBuilder.addRenderTemplateDirective(ctx.renderTemplate);
      } else if(sessionAttributes.lastBackgroundImage) {
        ctx.render(handlerInput, { backgroundImage: sessionAttributes.lastBackgroundImage });
        
        if(Game.supportsDisplay(requestEnvelope))
          responseBuilder.addRenderTemplateDirective(ctx.renderTemplate);
      }

      let response = responseBuilder.getResponse();

      /**
       * Apply the custom directives to the response.
       */
      if (Array.isArray(ctx.directives)) {
        logger.debug('Global.ResponseInterceptor: processing ' + ctx.directives.length + ' custom directives ');
        response.directives = response.directives || [];
        for (let i = 0; i < ctx.directives.length; i++) {
          response.directives.push(ctx.directives[i]);
        }
      }

      if ('openMicrophone' in ctx) {
        if (ctx.openMicrophone) {
          /**
           * setting shouldEndSession = false - lets Alexa know that we want an answer from the user
           * see: https://developer.amazon.com/docs/gadget-skills/receive-voice-input.html#open
           *      https://developer.amazon.com/docs/gadget-skills/keep-session-open.html
           */
          response.shouldEndSession = false;
          logger.debug('Global.ResponseInterceptor: request to open microphone -> shouldEndSession = false');
        } else {
          if (ctx.endSession){
            // We have explicitely asked for the session to end
            response.shouldEndSession = true;
          } else {
            /**
             * deleting shouldEndSession will keep the skill session going,
             * while the input handler is active, waiting for button presses
             * see: https://developer.amazon.com/docs/gadget-skills/keep-session-open.html
             */
            delete response.shouldEndSession;
          }

          logger.debug('Global.ResponseInterceptor: request to open microphone -> delete shouldEndSession');
        }
      }

      /**
       * Persist the current session attributes
       */
      attributesManager.setPersistentAttributes(sessionAttributes);
      await attributesManager.savePersistentAttributes();
      logger.debug('----- NEW PERSISTENT ATTRIBUTES -----');
      logger.debug(JSON.stringify(persistentAtttributes, null, 2));

      /**
       * Log the attributes and response for debug purposes.
       */
      logger.debug('----- RESPONSE -----');
      logger.debug(JSON.stringify(response, null, 2));

      return response;
    }
  },
  DefaultHandler: {
    canHandle(handlerInput) {
      logger.debug('Global.DefaultHandler: canHandle');

      /**
       * Catch all for requests.
       */
      return true;
    },
    handle(handlerInput) {
      logger.debug('Global.DefaultHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let ctx = attributesManager.getRequestAttributes();
      let sessionAttributes = attributesManager.getSessionAttributes();

      if(sessionAttributes.STATE === settings.STATE.MINI_GAME_STATE) {
        if(sessionAttributes.MINI_GAME === 'WhoAmI') {
          requestEnvelope.request.intent.slots = { "answers": { "value": "Placeholder" }};
          return miniGameHandlers.WhoAmIGuessHandler.handle(handlerInput);
        } else if(sessionAttributes.MINI_GAME === 'GuessSpecies') {
          requestEnvelope.request.intent.slots = { "answers": { "value": "Placeholder" }};
          return miniGameHandlers.GuessSpeciesAnswerHandler.handle(handlerInput);
        }
      }

      ctx.outputSpeech.push(ctx.t('UNHANDLED_REQUEST').outputSpeech);

      if('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
        ctx.outputSpeech.push(sessionAttributes.lastReprompt);
        ctx.reprompt.push(sessionAttributes.lastReprompt);
      } else {
        ctx.reprompt.push(ctx.t('UNHANDLED_REQUEST').reprompt);
      }

      return responseBuilder.getResponse();
    }
  },
  RepeatHandler: {
    canHandle(handlerInput) {
      logger.debug('Global.RepeatHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      let request = requestEnvelope.request;
      let sessionAttributes = attributesManager.getSessionAttributes();
      return request.type === 'IntentRequest' && request.intent.name === 'RepeatIntent'
	  attributesManager.getSessionAttributes().MINI_GAME !== 'GuessSpecies' &&
      attributesManager.getSessionAttributes().STATE !== settings.STATE.MINI_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('Global.RepeatHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let ctx = attributesManager.getRequestAttributes();
      let sessionAttributes = attributesManager.getSessionAttributes();
      
      let responseMessage;

	  if('lastSpeech' in sessionAttributes && sessionAttributes.lastSpeech != '') {
        ctx.outputSpeech.push(sessionAttributes.lastSpeech);
      } else {
		ctx.outputSpeech.push(ctx.t('UNHANDLED_REQUEST').outputSpeech);
      }
	  
	  if('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
        ctx.reprompt.push(sessionAttributes.lastReprompt);
      } else {
        ctx.reprompt.push(ctx.t('UNHANDLED_REQUEST').reprompt);
      }

      return responseBuilder.getResponse();
    }
  },
  HelpHandler: {
    canHandle(handlerInput) {
      logger.debug('Global.HelpHandler: canHandle');
      /**
       * Handle all help requests and treat don't know requests as
       * help requests except when in game loop state
       */
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      let request = requestEnvelope.request;
      let sessionAttributes = attributesManager.getSessionAttributes();
      return request.type === 'IntentRequest' &&
        request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
      logger.debug('Global.HelpHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let ctx = attributesManager.getRequestAttributes();
      let sessionAttributes = attributesManager.getSessionAttributes();
      
      let responseMessage;
      switch (sessionAttributes.PLACE) {
        case "HOME":
          responseMessage = ctx.t('HOME_HELP', {
            'name': sessionAttributes.NAME,
            'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "seine" : "ihre") : (sessionAttributes.GENDER == 0 ? "his" : "her"),
            'gender_2': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "ihn" : "sie") : (sessionAttributes.GENDER == 0 ? "him" : "her"),
            'gender_3': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "er" : "sie") : (sessionAttributes.GENDER == 0 ? "he" : "she")
          });
          break;
        case "PARK":
          responseMessage = ctx.t('PARK_HELP', {
            'name': sessionAttributes.NAME,
            'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "ihn" : "sie") : (sessionAttributes.GENDER == 0 ? "him" : "her")
          });
          break;
        case "CITY":
          responseMessage = ctx.t('CITY_HELP', {
            'name': sessionAttributes.NAME,
            'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "seinen" : "ihren") : (sessionAttributes.GENDER == 0 ? "his" : "her")
          });
          break;
        case "VET":
          responseMessage = ctx.t('VET_HELP', {
            'name': sessionAttributes.NAME,
            'gender_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.GENDER == 0 ? "ihn" : "sie") : (sessionAttributes.GENDER == 0 ? "him" : "her")
          });
          break;
        case "SUPERMARKET":
          responseMessage = ctx.t('SUPERMARKET_HELP');
          break;
      
        default:
          responseMessage = ctx.t('GENERAL_HELP');
          break;
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },
  /**
   * Requesting save game reset
   */
  ResetSaveHandler: {
    canHandle(handlerInput) {
      logger.debug('Global.ResetSaveHandler: canHandle');
      let request = handlerInput.requestEnvelope.request;
      return request.type === 'IntentRequest' &&
        request.intent.name === 'AMAZON.StartOverIntent';
    },
    handle(handlerInput) {
      logger.debug('Global.ResetSaveHandler: handle');
      let {
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();
      
      // Force launching of a new game
      sessionAttributes.STATE = settings.STATE.START_GAME_STATE;
      sessionAttributes.SETUP_STATE = "ResetRequest";
      
      let responseMessage = ctx.t('RESET_REQUEST');
      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },
  /**
   * 
   */
  CoinsHandler: {
    canHandle(handlerInput) {
      logger.debug('Global.CoinsHandler: canHandle');
      let request = handlerInput.requestEnvelope.request;
      return request.type === 'IntentRequest' &&
        request.intent.name === 'CoinsIntent';
    },
    handle(handlerInput) {
      logger.debug('Global.CoinsHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();
      
      let responseMessage = ctx.t('CURRENT_COINS', {'coins': sessionAttributes.COINS});
      
      ctx.outputSpeech.push(responseMessage.outputSpeech);
      if('lastReprompt' in sessionAttributes && sessionAttributes.lastReprompt != '') {
        ctx.outputSpeech.push(sessionAttributes.lastReprompt);
        ctx.reprompt.push(sessionAttributes.lastReprompt);
      } else {
        ctx.outputSpeech.push(ctx.t('HELP_REPROMPT').reprompt);
        ctx.reprompt.push(ctx.t('HELP_REPROMPT').reprompt);
      }

      return responseBuilder.getResponse();
    }
  },
  /**
   * DEBUG-Handler: 
   */
  HealPetHandler: {
    canHandle(handlerInput) {
      logger.debug('Global.HealPetHandler: canHandle');
      let request = handlerInput.requestEnvelope.request;
      return request.type === 'IntentRequest' &&
        request.intent.name === 'HealIntent' &&
        logger.LOG_VALUE === 3;
    },
    handle(handlerInput) {
      logger.debug('Global.HealPetHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();
      
      sessionAttributes.STATS.hunger = 0;
      sessionAttributes.STATS.thirst = 0;
      sessionAttributes.STATS.boredom = 0;
      sessionAttributes.STATS.fatigue = 0;
      sessionAttributes.STATS.illness = 0;
            
      ctx.outputSpeech.push(sessionAttributes.NAME + " geheilt. ");
      ctx.reprompt.push("Sage nach Hause. ");

      return responseBuilder.getResponse();
    }
  },
  /**
   * DEBUG-Handler: 
   */
  SkipHoursHandler: {
    canHandle(handlerInput) {
      logger.debug('Global.SkipHoursHandler: canHandle');
      let request = handlerInput.requestEnvelope.request;
      return request.type === 'IntentRequest' &&
        request.intent.name === 'SkipHoursIntent' &&
        logger.LOG_VALUE === 3;
    },
    handle(handlerInput) {
      logger.debug('Global.SkipHoursHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();
      
      sessionAttributes = Game.calculateStats(sessionAttributes, 0, requestEnvelope.request.intent.slots.days.value);
            
      ctx.outputSpeech.push(requestEnvelope.request.intent.slots.hours.value + " Stunden übersprungen. ");
      ctx.reprompt.push("Sage nach Hause. ");

      return responseBuilder.getResponse();
    }
  },
  /**
   * DEBUG-Handler: 
   */
  SkipDaysHandler: {
    canHandle(handlerInput) {
      logger.debug('Global.SkipDaysHandler: canHandle');
      let request = handlerInput.requestEnvelope.request;
      return request.type === 'IntentRequest' &&
        request.intent.name === 'SkipDaysIntent' &&
        logger.LOG_VALUE === 3;
    },
    handle(handlerInput) {
      logger.debug('Global.SkipDaysHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();
      
      sessionAttributes = Game.calculateStats(sessionAttributes, requestEnvelope.request.intent.slots.days.value, 0);
            
      ctx.outputSpeech.push(requestEnvelope.request.intent.slots.days.value + " Tage übersprungen. ");
      ctx.reprompt.push("Sage nach Hause. ");

      return responseBuilder.getResponse();
    }
  },
  /**
   * DEBUG-Handler: 
   */
  AddCoinsHandler: {
    canHandle(handlerInput) {
      logger.debug('Global.AddCoinsHandler: canHandle');
      let request = handlerInput.requestEnvelope.request;
      return request.type === 'IntentRequest' &&
        request.intent.name === 'AddCoinsIntent' &&
        logger.LOG_VALUE === 3;
    },
    handle(handlerInput) {
      logger.debug('Global.AddCoinsHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();
      
      sessionAttributes.COINS += parseInt(requestEnvelope.request.intent.slots.coins.value);

      ctx.outputSpeech.push(requestEnvelope.request.intent.slots.coins.value + " Münzen hinzugefügt. Du besitzt jetzt " + sessionAttributes.COINS + " Münzen. ");
      ctx.reprompt.push("Sage nach Hause. ");

      return responseBuilder.getResponse();
    }
  },
  /**
   * DEBUG-Handler: 
   */
  StatsHandler: {
    canHandle(handlerInput) {
      logger.debug('Global.StatsHandler: canHandle');
      let request = handlerInput.requestEnvelope.request;
      return request.type === 'IntentRequest' &&
        request.intent.name === 'StatsIntent' &&
        logger.LOG_VALUE === 3;
    },
    handle(handlerInput) {
      logger.debug('Global.StatsHandler: handle');
      let {
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      ctx.outputSpeech.push("Krankheit: <say-as interpret-as='cardinal'>" + sessionAttributes.STATS.illness + "</say-as>. ");
      ctx.outputSpeech.push("Hunger: <say-as interpret-as='cardinal'>" + sessionAttributes.STATS.hunger + "</say-as>. ");
      ctx.outputSpeech.push("Durst: <say-as interpret-as='cardinal'>" + sessionAttributes.STATS.thirst + "</say-as>. ");
      ctx.outputSpeech.push("Langeweile: <say-as interpret-as='cardinal'>" + sessionAttributes.STATS.boredom + "</say-as>. ");
      ctx.outputSpeech.push("Müdigkeit: <say-as interpret-as='cardinal'>" + sessionAttributes.STATS.fatigue + "</say-as>. ");
      ctx.outputSpeech.push("Zuneigung: <say-as interpret-as='cardinal'>" + sessionAttributes.STATS.attachment + "</say-as>. ");
      ctx.outputSpeech.push("Alter: <say-as interpret-as='cardinal'>" + sessionAttributes.STATS.age + " Tage</say-as>. ");

      ctx.reprompt.push("Sage nach Hause. ");

      return responseBuilder.getResponse();
    }
  },
  /**
   * Stop and Cancel both respond by saying goodbye and ending the session by not setting openMicrophone
   */
  StopCancelHandler: {
    canHandle(handlerInput) {
      logger.debug('Global.StopCancelHandler: canHandle');
      return handlerInput.requestEnvelope.request.type === 'IntentRequest' &&
        (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent' ||
        handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent')
    },
    handle(handlerInput) {
      logger.debug('Global.StopCancelHandler: handle');
      let {
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();
      let {
        outputSpeech
      } = ctx.t('GOOD_BYE');

      if(sessionAttributes.PLACE && sessionAttributes.PLACE === 'HOME' && sessionAttributes.SETUP_STATE && sessionAttributes.SETUP_STATE === 'Done') {
        sessionAttributes = Game.saveSleepStart(sessionAttributes);
        sessionAttributes.STATE = settings.STATE.SLEEPING_STATE;
      }

      ctx.outputSpeech.push(outputSpeech);
      ctx.openMicrophone = false;
      ctx.endSession = true;

      return responseBuilder.getResponse();
    }
  },
  SessionEndedRequestHandler: {
    canHandle(handlerInput) {
      logger.debug('Global.SessionEndedRequestHandler: canHandle');
      return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
      logger.debug('Global.SessionEndedRequestHandler: handle');
      logger.info(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);
      let {
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      
      /**
       * Clean out the session attributes that won't be persisted
       */
      delete sessionAttributes.STATE;

      /**
       *  setting shouldEndSession = true  -  lets Alexa know that the skill is done
       *  see: https://developer.amazon.com/docs/gadget-skills/receive-voice-input.html
       */
      let response = responseBuilder.getResponse();
      response.shouldEndSession = true;
      return response;
    }
  },
  ErrorHandler: {
    canHandle() {
      // Handle all errors. We'll just log them.
      logger.debug('Global.ErrorHandler: canHandle');
      return true;
    },
    handle(handlerInput, error) {
      logger.debug('Global.ErrorHandler: handle');
      logger.error('Global.ErrorHandler: Error = ' + error.message);
      logger.error(JSON.stringify(error, Object.getOwnPropertyNames(error), 2));

      return handlerInput.responseBuilder
        .speak(handlerInput.attributesManager.getRequestAttributes().t('ERROR').outputSpeech)
        .getResponse();
    }
  }
}

module.exports = globalHandlers;