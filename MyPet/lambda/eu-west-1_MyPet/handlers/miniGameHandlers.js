/*
 * Copyright 2018 Amazon.com, Inc. and its affiliates. All Rights Reserved.
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 * http://aws.amazon.com/asl/
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

const Game = require('../utils/game.js');
const logger = require('../utils/logger.js');
const settings = require('../config/settings.js');
const startHandlers = require('./startHandlers.js');

/**
 * Handling everything for the BUTTON_GAME_STATE state.
 */
const miniGameHandlers = {
  /**
   * 
   */
  PetQuizHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.PetQuizHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'PetQuizIntent' &&
        attributesManager.getSessionAttributes().PLACE === "PARK" &&
        (Game.getCurrentWeekday() === 'Mon' || Game.getCurrentWeekday() === 'Fri' || logger.LOG_VALUE === 3) &&
        (attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE ||
          attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE);
    },
    handle(handlerInput) {
      logger.debug('MiniGame.PetQuizHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      // Clean up some attributes to set up for the next game
      delete sessionAttributes.answeringPlayer;
      delete sessionAttributes.currentQuestion;
      delete sessionAttributes.correct;
      sessionAttributes.scores = {};
      sessionAttributes.scores[1] = 0;

      sessionAttributes.MINI_GAME = 'PetQuiz';

      ctx.outputSpeech.push(ctx.t('QUIZ_INTRO').outputSpeech);
      ctx.render(handlerInput, ctx.t('QUIZ_INTRO'))
      sessionAttributes.STATE = settings.STATE.MINI_GAME_STATE;
      Game.askQuestion(handlerInput, false);
      return responseBuilder.getResponse();
    }
  },
  PetQuizAnswerHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.AnswerHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        (requestEnvelope.request.intent.name === 'AnswerIntent' ||
          requestEnvelope.request.intent.name === 'AnswerOnlyIntent') &&
        attributesManager.getSessionAttributes().MINI_GAME === 'PetQuiz' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('MiniGame.AnswerHandler: handle');
      let { attributesManager } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      Game.answerQuestion(handlerInput);
      if (!('scores' in sessionAttributes)) {
        delete sessionAttributes.MINI_GAME;
        sessionAttributes.STATE = settings.STATE.MAIN_GAME_STATE;
        sessionAttributes = Game.addMinigameReward(sessionAttributes);
        sessionAttributes.RequestParkReprompt = true;
        return startHandlers.LaunchPlayGameHandler.handle(handlerInput);
      }
      return handlerInput.responseBuilder.getResponse();
    }
  },
  PetQuizDontKnowHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.DontKnowNextHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'AMAZON.NextIntent' &&
        attributesManager.getSessionAttributes().MINI_GAME === 'PetQuiz' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('MiniGame.DontKnowNextHandler: handle');
      let { attributesManager, responseBuilder } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      sessionAttributes.currentQuestion = parseInt(sessionAttributes.currentQuestion || 0, 10) + 1;

      let isLastQuestion = parseInt(sessionAttributes.currentQuestion || 1, 10) > settings.GAME.QUESTIONS_PER_GAME;
      let messageKey = isLastQuestion ? 'QUIZ_SKIP_LAST_QUESTION' : 'QUIZ_SKIP_QUESTION';
      let responseMessage = ctx.t(messageKey, sessionAttributes.currentQuestion);
      ctx.outputSpeech.push(responseMessage.outputSpeech + "<break time='1s'/>");

      Game.askQuestion(handlerInput, false);
      if (!('scores' in sessionAttributes)) {
        sessionAttributes.STATE = settings.STATE.MAIN_GAME_STATE;
        sessionAttributes = Game.addMinigameReward(sessionAttributes);
        sessionAttributes.RequestParkReprompt = true;
        return startHandlers.LaunchPlayGameHandler.handle(handlerInput);
      }
      return responseBuilder.getResponse();
    }
  },

  /**
   * 
   */
  MemoryHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.MemoryHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'MemoryIntent' &&
        attributesManager.getSessionAttributes().PLACE === "PARK" &&
        (Game.getCurrentWeekday() === 'Mon' || Game.getCurrentWeekday() === 'Fri' || logger.LOG_VALUE === 3) &&
        (attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE ||
          attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE);
    },
    handle(handlerInput) {
      logger.debug('MiniGame.MemoryHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      responseMessage = ctx.t('MEMORY_INTRO');

      sessionAttributes.MINI_GAME = "Memory";
      sessionAttributes.STATE = settings.STATE.MINI_GAME_STATE;
      sessionAttributes.memoryReward = 0;
      
      var cards;
      if(requestEnvelope.request.locale === "de-DE")
        cards = ["Hund", "Hund", "Katze", "Katze", "Maus", "Maus", "Hamster", "Hamster", "Ratte"];
      else
        cards = ["Dog", "Dog", "Cat", "Cat", "Mouse", "Mouse", "Hamster", "Hamster", "Rat"];

      sessionAttributes.memoryGuesses = [];
      sessionAttributes.memoryCards = Game.shuffleArray(cards);
      logger.debug('MiniGame.Memory: Card: ' + sessionAttributes.memoryCards);

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);
      ctx.render(handlerInput, responseMessage);

      return responseBuilder.getResponse();
    }
  },
  MemoryGuessHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.MemoryGuessHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'MemoryGuessIntent' &&
        attributesManager.getSessionAttributes().MINI_GAME === 'Memory' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('MiniGame.MemoryGuessHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;

      var number = requestEnvelope.request.intent.slots.numberOne.value;
      var numberTwo = requestEnvelope.request.intent.slots.numberTwo.value;

      if (!isNaN(number) && !isNaN(numberTwo) && number > 0 && number <= 9 && numberTwo > 0 && numberTwo <= 9) {
        var alreadyRevealed = false;
        if (sessionAttributes.memoryCards[number - 1] == "") {
          responseMessage = ctx.t('MEMORY_ALREADY_REVEALED', { 'number': number });
          alreadyRevealed = true;
        }
        if (sessionAttributes.memoryCards[numberTwo - 1] == "") {
          responseMessage = ctx.t('MEMORY_ALREADY_REVEALED', { 'number': numberTwo });
          alreadyRevealed = true;
        }

        if (!alreadyRevealed) {
          sessionAttributes.memoryGuesses[0] = number;
          sessionAttributes.memoryGuesses[1] = numberTwo;

          console.log(sessionAttributes.memoryCards);

          responseMessage = ctx.t('MEMORY_REVEAL', {
            'random_1': Math.floor(Math.random() * 4),
            'random_2': Math.floor(Math.random() * 4),
            'card_1': sessionAttributes.memoryCards[sessionAttributes.memoryGuesses[0] - 1],
            'card_2': sessionAttributes.memoryCards[sessionAttributes.memoryGuesses[1] - 1],
            'card_nr_1': sessionAttributes.memoryGuesses[0],
            'card_nr_2': sessionAttributes.memoryGuesses[1],
          });

          ctx.outputSpeech.push(responseMessage.outputSpeech);

          if (sessionAttributes.memoryCards[sessionAttributes.memoryGuesses[0] - 1] === sessionAttributes.memoryCards[sessionAttributes.memoryGuesses[1] - 1]) {

            sessionAttributes.memoryCards[sessionAttributes.memoryGuesses[0] - 1] = "";
            sessionAttributes.memoryCards[sessionAttributes.memoryGuesses[1] - 1] = "";

            sessionAttributes.memoryReward += 10;
            responseMessage = ctx.t('MEMORY_CORRECT', { 'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'Meow/meowing-cat-4.mp3' : 'Bark/small-dog-bark-7.mp3' });
            ctx.outputSpeech.push(responseMessage.outputSpeech);
          } else {
            responseMessage = ctx.t('MEMORY_INCORRECT', { 'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'Meow/cat-meow-6.mp3' : 'Misc/dog-whine-0.mp3' });
            ctx.outputSpeech.push(responseMessage.outputSpeech);
          }

          if (Game.arrayIsBlank(sessionAttributes.memoryCards)) {
            sessionAttributes.COINS += sessionAttributes.memoryReward;
            responseMessage = ctx.t('MEMORY_END', { 'reward': sessionAttributes.memoryReward });
            ctx.outputSpeech.push(responseMessage.outputSpeech);
            if(sessionAttributes.memoryReward > 0)
              ctx.outputSpeech.push("<audio src='https://nordicskills.de/audio/MeinHaustier/muenzen.mp3'/>");

            // End Memory
            delete sessionAttributes.MINI_GAME;
            sessionAttributes.STATE = settings.STATE.MAIN_GAME_STATE;
            sessionAttributes = Game.addMinigameReward(sessionAttributes);
            sessionAttributes.RequestParkReprompt = true;
            return startHandlers.LaunchPlayGameHandler.handle(handlerInput);
          } else {
            responseMessage = ctx.t('MEMORY_NEXT');
          }
        }
      } else {
        responseMessage = ctx.t('MEMORY_INCORRECT_INPUT');
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },

  /**
   * 
   */
  WhoAmIHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.WhoAmIHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'WhoAmIIntent' &&
        attributesManager.getSessionAttributes().PLACE === "PARK" &&
        (Game.getCurrentWeekday() === 'Tue' || Game.getCurrentWeekday() === 'Sat' || logger.LOG_VALUE === 3) &&
        (attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE ||
          attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE);
    },
    handle(handlerInput) {
      logger.debug('MiniGame.WhoAmIHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      let whoAmI = ctx.t('WHOAMI');

      sessionAttributes.MINI_GAME = "WhoAmI";
      sessionAttributes.STATE = settings.STATE.MINI_GAME_STATE;
      sessionAttributes.whoAmIIndex = Math.floor(Math.random() * whoAmI.length);
      sessionAttributes.whoAmIHint = 0;

      responseMessage = ctx.t('WHOAMI_INTRO');

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.render(handlerInput, responseMessage);

      return miniGameHandlers.WhoAmIGuessHandler.handle(handlerInput);
    }
  },
  WhoAmIGuessHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.WhoAmIGuessHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        (requestEnvelope.request.intent.name === 'AnswerIntent' || requestEnvelope.request.intent.name === 'AnswerOnlyIntent' || requestEnvelope.request.intent.name === 'NameIntent' || requestEnvelope.request.intent.name === "AMAZON.NextIntent") &&
        attributesManager.getSessionAttributes().MINI_GAME === 'WhoAmI' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('MiniGame.WhoAmIGuessHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;
      let whoAmI = ctx.t('WHOAMI');

      var answer = "";
      if (requestEnvelope.request.intent.slots && requestEnvelope.request.intent.slots.answers && requestEnvelope.request.intent.slots.answers.value) {
        answer = requestEnvelope.request.intent.slots.answers.value;
      } else if (requestEnvelope.request.intent.slots && requestEnvelope.request.intent.slots.name && requestEnvelope.request.intent.slots.name.value) {
        answer = requestEnvelope.request.intent.slots.name.value;
      }

      if (requestEnvelope.request.intent.name === "AMAZON.NextIntent") {
        sessionAttributes.whoAmIHint = sessionAttributes.whoAmIHint + 1;
      }

      if (answer.toLowerCase() == whoAmI[sessionAttributes.whoAmIIndex].answers[0].toLowerCase()) {
        sessionAttributes.COINS += 35 - ((sessionAttributes.whoAmIHint + 1) * 5);
        responseMessage = ctx.t('WHOAMI_CORRECT', {
          'hints': sessionAttributes.whoAmIHint,
          'reward': 35 - ((sessionAttributes.whoAmIHint + 1) * 5),
          'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'Meow/meowing-cat-4.mp3' : 'Bark/small-dog-bark-7.mp3'
        });
        ctx.outputSpeech.push(responseMessage.outputSpeech);

        // End WhoAmI
        delete sessionAttributes.MINI_GAME;
        sessionAttributes.STATE = settings.STATE.MAIN_GAME_STATE;
        sessionAttributes = Game.addMinigameReward(sessionAttributes);
        sessionAttributes.RequestParkReprompt = true;
        return startHandlers.LaunchPlayGameHandler.handle(handlerInput);
      } else if (answer != "") {
        responseMessage = ctx.t('WHOAMI_INCORRECT', { 'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'Meow/cat-meow-6.mp3' : 'Misc/dog-whine-0.mp3' });
        ctx.outputSpeech.push(responseMessage.outputSpeech);
      }

      if (sessionAttributes.whoAmIHint == 0) {
        responseMessage = ctx.t('WHOAMI_FIRST_HINT');
        ctx.outputSpeech.push(responseMessage.outputSpeech);
      } else if (sessionAttributes.whoAmIHint == whoAmI[sessionAttributes.whoAmIIndex].hints.length - 1) {
        responseMessage = ctx.t('WHOAMI_LAST_HINT');
        ctx.outputSpeech.push(responseMessage.outputSpeech);
      } else if (sessionAttributes.whoAmIHint == whoAmI[sessionAttributes.whoAmIIndex].hints.length) {
        responseMessage = ctx.t('WHOAMI_SOLUTION', { 'solution': whoAmI[sessionAttributes.whoAmIIndex].answers[0] });
        ctx.outputSpeech.push(responseMessage.outputSpeech);

        // End WhoAmI
        delete sessionAttributes.MINI_GAME;
        sessionAttributes.STATE = settings.STATE.MAIN_GAME_STATE;
        sessionAttributes = Game.addMinigameReward(sessionAttributes);
        sessionAttributes.RequestParkReprompt = true;
        return startHandlers.LaunchPlayGameHandler.handle(handlerInput);
      }

      ctx.outputSpeech.push(whoAmI[sessionAttributes.whoAmIIndex].hints[sessionAttributes.whoAmIHint]);

      responseMessage = ctx.t('WHOAMI_REPROMPT')
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },

  /**
   * 
   */
  ShellGameHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.ShellGameHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'ShellGameIntent' &&
        attributesManager.getSessionAttributes().PLACE === "PARK" &&
        (Game.getCurrentWeekday() === 'Tue' || Game.getCurrentWeekday() === 'Sat' || logger.LOG_VALUE === 3) &&
        (attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE ||
          attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE);
    },
    handle(handlerInput) {
      logger.debug('MiniGame.ShellGameHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      sessionAttributes.MINI_GAME = "ShellGame";
      sessionAttributes.STATE = settings.STATE.MINI_GAME_STATE;

      let responseMessage = ctx.t('SHELL_INTRO');

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);
      ctx.render(handlerInput, responseMessage);

      return responseBuilder.getResponse();
    }
  },
  ShellGameGuessHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.ShellGameHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'NumberIntent' &&
        attributesManager.getSessionAttributes().MINI_GAME === 'ShellGame' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('MiniGame.ShellGameHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;

      var number = requestEnvelope.request.intent.slots.number.value;

      if (!isNaN(number)) {
        if (number > 0 && number <= 3) {
          var winningShell = Math.floor(Math.random() * 3) + 1;
          responseMessage = ctx.t('SHELL_END', { 'winningShell': winningShell });
          ctx.outputSpeech.push(responseMessage.outputSpeech);

          if (number == winningShell) {
            responseMessage = ctx.t('SHELL_WIN', { 'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'Meow/meowing-cat-4.mp3' : 'Bark/small-dog-bark-7.mp3' });
            sessionAttributes = Game.addItemToInventory(2, 3, sessionAttributes);
          } else {
            responseMessage = ctx.t('SHELL_LOSE', { 'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'Meow/cat-meow-6.mp3' : 'Misc/dog-whine-0.mp3' });
          }
          ctx.outputSpeech.push(responseMessage.outputSpeech);

          delete sessionAttributes.MINI_GAME;
          sessionAttributes.STATE = settings.STATE.MAIN_GAME_STATE;
          sessionAttributes = Game.addMinigameReward(sessionAttributes);
          sessionAttributes.RequestParkReprompt = true;
          return startHandlers.LaunchPlayGameHandler.handle(handlerInput);
        } else {
          responseMessage = ctx.t('SHELL_INCORRECT_INPUT');
        }
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },

  /**
   * 
   */
  StickWoolHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.StickWoolHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'StickWoolIntent' &&
        attributesManager.getSessionAttributes().PLACE === "PARK" &&
        (Game.getCurrentWeekday() === 'Wed' || Game.getCurrentWeekday() === 'Sun' || logger.LOG_VALUE === 3) &&
        (attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE ||
          attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE);
    },
    handle(handlerInput) {
      logger.debug('MiniGame.StickWoolHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      sessionAttributes.MINI_GAME = 'StickWool';
      sessionAttributes.STATE = settings.STATE.MINI_GAME_STATE;
      sessionAttributes.guesses = [];
      sessionAttributes.names = Game.generateRandomNames(3);
      while (sessionAttributes.guesses.length < sessionAttributes.names.length) {
        var randomnumber = Math.floor(Math.random() * 60) + 1;
        if (sessionAttributes.guesses.indexOf(randomnumber) > -1) continue;
        sessionAttributes.guesses.push(randomnumber);
      }

      let responseMessage = ctx.t('STICK_WOOL_INTRO', {
        'stickWool_1': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.SPECIES == "katze" ? "Wollknäuel" : "Stöckchen") : (sessionAttributes.SPECIES == "katze" ? "wool ball" : "stick"),
        'stickWool_2': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.SPECIES == "katze" ? "Wollknäuel" : "Stock") : (sessionAttributes.SPECIES == "katze" ? "wool ball" : "stick"),
        'stickWool_3': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.SPECIES == 'katze' ? 'das Wollknäuel' : 'den Stock') : (sessionAttributes.SPECIES == 'katze' ? 'the wool ball' : 'the stick'),
        'name': sessionAttributes.NAME,
        'name_1': sessionAttributes.names[0],
        'name_2': sessionAttributes.names[1],
        'name_3': sessionAttributes.names[2],
        'guess_1': sessionAttributes.guesses[0],
        'guess_2': sessionAttributes.guesses[1],
        'guess_3': sessionAttributes.guesses[2]
      });

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);
      ctx.render(handlerInput, responseMessage);

      return responseBuilder.getResponse();
    }
  },
  StickWoolGuessHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.StickWoolHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'NumberIntent' &&
        attributesManager.getSessionAttributes().MINI_GAME === 'StickWool' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('MiniGame.StickWoolHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;

      var number = parseInt(requestEnvelope.request.intent.slots.number.value);

      if (!isNaN(number)) {
        if (number > 0 && number <= 60) {
          if (sessionAttributes.guesses.indexOf(number) === -1) {

            sessionAttributes.guesses[3] = number;
            sessionAttributes.names[3] = 'me';

            sessionAttributes.correct = true;
            responseMessage = ctx.t('STICK_WOOL_CONFIRM', { 'number': number });
          } else {
            responseMessage = ctx.t('STICK_WOOL_ALREADY_USED');
          }
        } else {
          responseMessage = ctx.t('STICK_WOOL_INCORRECT_INPUT');
        }
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },
  StickWoolYesHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.StickWoolHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'AMAZON.YesIntent' &&
        attributesManager.getSessionAttributes().correct &&
        attributesManager.getSessionAttributes().MINI_GAME === 'StickWool' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('MiniGame.StickWoolHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      delete sessionAttributes.correct;

      let responseMessage;

      var winningSeconds = Math.floor(Math.random() * 60) + 1;

      // Wer war am nächsten dran
      var rank = 0;
      for (var i = 0; i < 4; i++) {
        var winningIndex = sessionAttributes.guesses.indexOf(Game.closest(sessionAttributes.guesses, winningSeconds));
        console.log("wind" + winningIndex + " sec" + winningSeconds + " win" + sessionAttributes.names[winningIndex]);
        rank = i + 1;
        if (sessionAttributes.names[winningIndex] == 'me') {
          break;
        }
        sessionAttributes.guesses.splice(winningIndex, 1);
        sessionAttributes.names.splice(winningIndex, 1);
      }

      sessionAttributes.COINS += 40 - rank * 10;
      responseMessage = ctx.t('STICK_WOOL_FINISHED', {
        'name': sessionAttributes.NAME,
        'time': winningSeconds,
        'rank': rank,
        'reward': 40 - rank * 10
      });
      ctx.outputSpeech.push(responseMessage.outputSpeech);
      if(40 - rank * 10 > 0)
        ctx.outputSpeech.push("<audio src='https://nordicskills.de/audio/MeinHaustier/muenzen.mp3'/>");

      delete sessionAttributes.MINI_GAME;
      delete sessionAttributes.guesses;
      delete sessionAttributes.names;
      sessionAttributes.STATE = settings.STATE.MAIN_GAME_STATE;
      sessionAttributes = Game.addMinigameReward(sessionAttributes);
      sessionAttributes.RequestParkReprompt = true;
      return startHandlers.LaunchPlayGameHandler.handle(handlerInput);
    }
  },
  StickWoolNoHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.StickWoolHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'AMAZON.NoIntent' &&
        attributesManager.getSessionAttributes().correct &&
        attributesManager.getSessionAttributes().MINI_GAME === 'StickWool' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('MiniGame.StickWoolHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      delete sessionAttributes.correct;
      sessionAttributes.guesses.splice(-1, 1);

      let responseMessage;
      responseMessage = ctx.t('STICK_WOOL_INCORRECT_INPUT');

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },

  /**
   * 
   */
  GuessSpeciesHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.GuessSpeciesHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        requestEnvelope.request.intent.name === 'GuessSpeciesIntent' &&
        attributesManager.getSessionAttributes().PLACE === "PARK" &&
        (Game.getCurrentWeekday() === 'Wed' || Game.getCurrentWeekday() === 'Sun' || logger.LOG_VALUE === 3) &&
        (attributesManager.getSessionAttributes().STATE === settings.STATE.MAIN_GAME_STATE ||
          attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE);
    },
    handle(handlerInput) {
      logger.debug('MiniGame.GuessSpeciesHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      sessionAttributes.MINI_GAME = 'GuessSpecies';
      sessionAttributes.STATE = settings.STATE.MINI_GAME_STATE;

      let guessSpecies = ctx.t('GUESSSPECIES');
      sessionAttributes.randomSounds = Game.shuffle(guessSpecies);
      sessionAttributes.round = 1;
      sessionAttributes.reward = 0;

      let responseMessage;
      responseMessage = ctx.t('GUESS_INTRO', { 'animal_sound': sessionAttributes.randomSounds[sessionAttributes.round].sound });

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);
      ctx.render(handlerInput, responseMessage);

      return responseBuilder.getResponse();
    }
  },
  GuessSpeciesAnswerHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.GuessSpeciesHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        (requestEnvelope.request.intent.name === 'AnswerIntent' || requestEnvelope.request.intent.name === 'AnswerOnlyIntent' || requestEnvelope.request.intent.name === 'NameIntent' || requestEnvelope.request.intent.name === "AMAZON.NextIntent") &&
        attributesManager.getSessionAttributes().MINI_GAME === 'GuessSpecies' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('MiniGame.GuessSpeciesHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let responseMessage;

      var answer = "";
      if (requestEnvelope.request.intent.slots && requestEnvelope.request.intent.slots.answers && requestEnvelope.request.intent.slots.answers.value) {
        answer = requestEnvelope.request.intent.slots.answers.value;
      } else if (requestEnvelope.request.intent.slots && requestEnvelope.request.intent.slots.name && requestEnvelope.request.intent.slots.name.value) {
        answer = requestEnvelope.request.intent.slots.name.value;
      }

      if (requestEnvelope.request.intent.name === "AMAZON.NextIntent" || answer == "") {
        responseMessage = ctx.t('GUESS_SKIP', { 'animal': sessionAttributes.randomSounds[sessionAttributes.round].answer });
        ctx.outputSpeech.push(responseMessage.outputSpeech);
        sessionAttributes.round++;

        responseMessage = ctx.t('GUESS_NEXT', { 'animal_sound': sessionAttributes.randomSounds[sessionAttributes.round].sound, 
        'nextLast': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.round > 2 ? "letzte" : "nächste") : (sessionAttributes.round > 2 ? "last" : "next")
       });
      }

      if (answer.toLowerCase() == sessionAttributes.randomSounds[sessionAttributes.round].answer.toLowerCase()) {
        responseMessage = ctx.t('GUESS_WIN', {
          'animal': sessionAttributes.randomSounds[sessionAttributes.round].answer,
          'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'Meow/meowing-cat-4.mp3' : 'Bark/small-dog-bark-7.mp3'
        });
        ctx.outputSpeech.push(responseMessage.outputSpeech);

        sessionAttributes.reward += 10;
        sessionAttributes.round++;

        responseMessage = ctx.t('GUESS_NEXT', { 'animal_sound': sessionAttributes.randomSounds[sessionAttributes.round].sound, 
        'nextLast': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.round > 2 ? "letzte" : "nächste") : (sessionAttributes.round > 2 ? "last" : "next")
       });
      } else if (answer != "") {
        responseMessage = ctx.t('GUESS_LOSE', {
          'animal': sessionAttributes.randomSounds[sessionAttributes.round].answer,
          'pet_sound': sessionAttributes.SPECIES == 'katze' ? 'Meow/cat-meow-6.mp3' : 'Misc/dog-whine-0.mp3'
        });
        ctx.outputSpeech.push(responseMessage.outputSpeech);

        sessionAttributes.round++;

        responseMessage = ctx.t('GUESS_NEXT', { 'animal_sound': sessionAttributes.randomSounds[sessionAttributes.round].sound, 
        'nextLast': requestEnvelope.request.locale === 'de-DE' ? (sessionAttributes.round > 2 ? "letzte" : "nächste") : (sessionAttributes.round > 2 ? "last" : "next")
       });
      }

      if (sessionAttributes.round > 3) {
        sessionAttributes.COINS += sessionAttributes.reward;
        responseMessage = ctx.t('GUESS_END', { 'correct': Game.switchEinsEine(sessionAttributes.reward / 10, requestEnvelope), 'reward': sessionAttributes.reward });
        ctx.outputSpeech.push(responseMessage.outputSpeech);
        if(sessionAttributes.reward > 0)
          ctx.outputSpeech.push("<audio src='https://nordicskills.de/audio/MeinHaustier/muenzen.mp3'/>");

        delete sessionAttributes.MINI_GAME;
        delete sessionAttributes.randomSounds;
        sessionAttributes.STATE = settings.STATE.MAIN_GAME_STATE;
        sessionAttributes = Game.addMinigameReward(sessionAttributes);
        sessionAttributes.RequestParkReprompt = true;
        return startHandlers.LaunchPlayGameHandler.handle(handlerInput);
      }

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  },
  GuessSpeciesRepeatHandler: {
    canHandle(handlerInput) {
      logger.debug('MiniGame.GuessSpeciesHandler: canHandle');
      let {
        attributesManager,
        requestEnvelope
      } = handlerInput;
      return requestEnvelope.request.type === 'IntentRequest' &&
        (requestEnvelope.request.intent.name === 'RepeatIntent') &&
        attributesManager.getSessionAttributes().MINI_GAME === 'GuessSpecies' &&
        attributesManager.getSessionAttributes().STATE === settings.STATE.MINI_GAME_STATE;
    },
    handle(handlerInput) {
      logger.debug('MiniGame.GuessSpeciesHandler: handle');
      let {
        requestEnvelope,
        attributesManager,
        responseBuilder
      } = handlerInput;
      let sessionAttributes = attributesManager.getSessionAttributes();
      let ctx = attributesManager.getRequestAttributes();

      let guessSpecies = ctx.t('GUESSSPECIES');

      let responseMessage;
      ctx.t('GUESS_REPEAT', { 'animal_sound': sessionAttributes.randomSounds[sessionAttributes.round].sound });

      ctx.outputSpeech.push(responseMessage.outputSpeech);
      ctx.reprompt.push(responseMessage.reprompt);

      return responseBuilder.getResponse();
    }
  }
}

module.exports = miniGameHandlers;