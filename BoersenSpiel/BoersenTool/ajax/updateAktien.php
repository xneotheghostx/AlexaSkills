<?php
require 'aws.phar';
use Aws\DynamoDb\DynamoDbClient;

$client = DynamoDbClient::factory(array(
    'region' => 'eu-west-1',
	'version' => 'latest',
	'credentials' => [
        'key'    => 'AKIAJT2ERA44XLKHXTTA',
        'secret' => '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',
    ],
));

$result = $client->query(array(
	"TableName" => 'BoersenAktienDB',
	"IndexName" => "index-name-index",
	"KeyConditionExpression" => "#n = :n",
	//ConsistentRead: false,
	"ExpressionAttributeNames" => [
		"#n" => "index"
	],
	"ExpressionAttributeValues" => [
		":n" => array('S' => "DAX")
	],
	"ScanIndexForward" => true
));

$symbols = "";
$j = 0;
foreach ($result['Items'] as $i) {
	if($j > 0) {
		$symbols .= $i['symbol']['S'].",";
	}
	$j++;
}
$symbols = substr($symbols, 0, -1);

$url = "https://eodhistoricaldata.com/api/real-time/".$result['Items'][0]['symbol']['S']."?api_token=5a3c3ac54a5e9&fmt=json&s=".$symbols;

echo $url;
$content = file_get_contents($url, true);
$json = json_decode($content, true);
//print_r($result['Items']);

for($i = 0; $i < count($result['Items']); $i++) {
	$result2 = $client->putItem(array(
		'TableName' => 'BoersenAktienDB',
		'Item' => array(
			'id'  => array('S' => $result['Items'][$i]['id']['S']),
			'index'  => array('S' => $result['Items'][$i]['index']['S']),
			'name'  => array('S' => $result['Items'][$i]['name']['S']),
            'alias'  => array('S' => $result['Items'][$i]['alias']['S']),
			'symbol'    => array('S' => $result['Items'][$i]['symbol']['S']),
			'current'    => array('S' => (string)($json[$i]['previousClose'] + $json[$i]['change'])),
			'change'    => array('S' => (string)$json[$i]['change']),
            'volume'    => array('S' => (string)$json[$i]['volume']),
			'timestamp'    => array('S' => (string)$json[$i]['timestamp'])
		)
	));
}
?>