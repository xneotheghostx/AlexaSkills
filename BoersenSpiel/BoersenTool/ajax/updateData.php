<?php
require 'aws.phar';
use Aws\DynamoDb\DynamoDbClient;

$client = DynamoDbClient::factory(array(
    'region' => 'eu-west-1',
	'version' => 'latest',
	'credentials' => [
        'key'    => 'AKIAJT2ERA44XLKHXTTA',
        'secret' => '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',
    ],
));

$aktien = json_decode($_POST['aktien'], true);
$result = $client->query(array(
	"TableName" => 'BoersenAktienDB',
	"IndexName" => "index-name-index",
	"KeyConditionExpression" => "#n = :n",
	//ConsistentRead: false,
	"ExpressionAttributeNames" => [
		"#n" => "index"
	],
	"ExpressionAttributeValues" => [
		":n" => array('S' => "DAX")
	],
	"ScanIndexForward" => true
));

for($i = 0; $i < count($result['Items']); $i++) {
    $result2 = $client->putItem(array(
		'TableName' => 'BoersenAktienDB',
		'Item' => array(
			'id'  => array('S' => $result['Items'][$i]['id']['S']),
			'index'  => array('S' => $result['Items'][$i]['index']['S']),
			'name'  => array('S' => $result['Items'][$i]['name']['S']),
            'alias'  => array('S' => (string)$aktien[substr($result['Items'][$i]['id']['S'], 0, 13)."_Alias"]),
			'symbol'    => array('S' => $result['Items'][$i]['symbol']['S']),
			'current'    => array('S' => $result['Items'][$i]['current']['S']),
			'change'    => array('S' => $result['Items'][$i]['change']['S']),
            'volume'    => array('S' => $result['Items'][$i]['volume']['S']),
			'timestamp'    => array('S' => $result['Items'][$i]['timestamp']['S'])
		)
	));
}

$events = json_decode($_POST['events'], true);
$keys = array_keys($events);

for($i = 0; $i < count($events); $i++)  {
	$result3 = $client->putItem(array(
		'TableName' => 'BoersenEventDB',
		'Item' => array(
			'id'  => array('S' => (string)$keys[$i]),
            'aktienId'  => array('S' => (string)$events[$keys[$i]][$keys[$i]."_ID"]),
            'type'  => array('S' => "Standard"),
            'speech'  => array('S' => (string)$events[$keys[$i]][$keys[$i]."_Speech"]),
            'start'  => array('S' => (string)$events[$keys[$i]][$keys[$i]."_Start"]),
            'offset'  => array('S' => (string)$events[$keys[$i]][$keys[$i]."_Offset"]),
		)
	));
}
echo "Done";


?>