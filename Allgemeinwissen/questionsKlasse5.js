﻿'use strict';

module.exports = {
    /**
     * Auf ? und Zeichen achten.
     * Erste Frage ist immer die Richtige.
     */
    "QUESTIONS_DE_DE" : [
		{
			"<break time='50ms'/>Was erfand Johannes Gutenberg vor mehr als 500 Jahren?": [
				"Er erfand den Buchdruck.",
				"Er erfand den Schreibfüller.",
				"Er erfand das Papier."
			]
        },
        {
            "<break time='50ms'/>Zwischen welchen beiden Ländern liegt der Ärmelkanal?": [
                "Frankreich und Großbritannien.",
                "Russland und Finnland.",
                "Spanien und Portugal."
            ]
        },
        {
            "<break time='50ms'/>Wie heißt das englische Wort für Schließfach?": [
                "locker.",
                "lose.",
                "fest."
            ]
        },
		{
            "<break time='50ms'/>Welches Wahrzeichen ist in Paris zu bewundern?": [
				"Eiffelturm.",
				"Die kleine Meerjungfrau.",
				"Der Big Ben."
			]
		},
		{
            "<break time='50ms'/>Aus wie vielen Linien bestehen Notenzeilen?": [
				"5",
				"4",
				"7"
			]
		},
		{
			"<break time='50ms'/>Wie viele Bundesländer hat Deutschland?": [
				"16",
				"21",
				"14"
			]
		},
		{
			"<break time='50ms'/>Welcher geometrische Körper hat fünf Flächen?": [
				"Pyramide.",
				"Zylinder.",
				"Würfel."
			]
		},
        {
            "<break time='50ms'/>Aus wie vielen Evangelien besteht das Neue Testament?": [
                "4",
                "3",
                "5"
            ]
        },
        {
            "<break time='50ms'/>Welche sind die größten Fleisch fressenden Tiere der Erde?": [
                "Bären",
                "Giraffen",
                "Elefanten"
            ]
        },
        {
            "<break time='50ms'/>Von wem können Informationen auf Internet-Seiten stammen?": [
                "von irgendwelchen Leuten",
                "von Fernsehsendern, Zeitschriften und Verlagen ",
                "von Regierungen"
            ]
        },
        {
            "<break time='50ms'/>Was feiern Christen am Ostersonntag?": [
                "Jesu Auferstehung.",
                "die Segnung der Speisen.",
                "die Empfängnis des Heiligen Geistes."
            ]
        },
        {
            "<break time='50ms'/>Wie heißt die Hauptstadt der Türkei?": [
                "Ankara.",
                "Istanbul",
                "Mercin"
            ]
        },
        {
            "<break time='50ms'/>Kann man 9€ gerecht auf vier Kinder verteilen?": [
                "Ja, jeder erhält 225 Cent",
                "Nein, das geht nicht!",
                "Ja, jeder bekommt 3 Euro und 33 Cent"
            ]
        },
        {
            "<break time='50ms'/>Was ist ein Ozelot?": [
                "eine Wildkatze.",
                "ein Gerät zur Tiefenmessung im Wasser.",
                "ein Süßwasserkrebs."
            ]
        },
        {
            "<break time='50ms'/>Wie stellt man fest, ob eine Zahl durch 5 teilbar ist?": [
                "Die letzte Ziffer muss eine 5 oder 0 sein.",
                "Die erste Ziffer muss eine 5 sein.",
                "Die Quersumme der Zahl muss durch 5 teilbar sein."
            ]
        },
        {
            "<break time='50ms'/>Aus welchem Material besteht Bernstein?": [
                "Harz",
                "Sandkristalle",
                "Kunststoff"
            ]
        },
        {
            "<break time='50ms'/>Welcher Ozean ist der größte und tiefste?": [
                "Pazifik",
                "Indischer Ozean",
                "Atlantik"
            ]
        },
        {
            "<break time='50ms'/>Was ist ein Marathon?": [
                "ein Wettlauf über 42,195 Kilometer.",
                "ein Wettbewerb aus Schwimmen, Radfahren und Laufen.",
                "ein berühmtes Bauwerk in Athen."
            ]
        },
        {
            "<break time='50ms'/>Was braucht Feuer zum Brennen?": [
                "Sauerstoff.",
                "Kohlendioxid.",
                "Helium."
            ]
        },
        {
            "<break time='50ms'/>Warum leuchtet der Mond?": [
                "Weil er von der Sonne angestrahlt wird.",
                "Weil er aus heißem Gas besteht.",
                "durch ständige kleine Explosionen auf seiner Oberfläche."
            ]
        },
        {
            "<break time='50ms'/>Wie heißt der höchste Berg der Alpen?": [
                "Mont Blanc.",
                "Großglockner.",
                "Monte Rosa."
            ]
        },
        {
            "<break time='50ms'/>Wie fängt die deutsche Nationalhymne an?": [
                "Einigkeit und Recht und Freiheit",
                "Danach lasst uns alle streben",
                "Blühe deutsches Vaterland"
            ]
        },
        {
            "<break time='50ms'/>Wie heißt das heilige Buch der Muslime?": [
                "Koran.",
                "Bibel.",
                "Thora." 
            ]
        },
        {
            "<break time='50ms'/>Welcher Baum wirft im Winter seine Nadeln ab?": [
                "Die Lärche.",
                "Die Fichte.",
                "Die Kiefer."
            ]
        },
        {
            "<break time='50ms'/><s>Aus welchem Märchen stammt der Satz</s> Die guten ins Töpfchen, die schlechten ins Kröpfchen?": [
                "Aschenputtel.",
                "Schneewittchen.",
                "Dornröschen."
            ]
        }
    ]
};