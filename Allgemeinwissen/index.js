"use strict";
var APP_ID = 'amzn1.ask.skill.6e097d07-1fbf-4e24-a3a0-89406dc74bd5';

var SKILL_CLASS = '5'; // Hier muss die Klasse vor dem hochladen geändert werden. 5 oder 10.

var ANSWER_COUNT = 3; // Die Anzahl der möglichen Antworten je Frage.
var GAME_LENGTH = 25;  // Die Anzahl der Fragen pro Spiel.
var GAME_STATES = {
    TRIVIA: "_TRIVIAMODE", // Fragen stellen.
    START: "_STARTMODE", // Spiel Startet
    HELP: "_HELPMODE" // Der Spieler fragt nach Hilfe.
};
var questions = require("./questionsKlasse" + SKILL_CLASS);

/**
 * Bei der Bearbeitung der Fragen achte auf Ihre Zeichensetzung. Stelle sicher, dass Du Fragezeichen oder Punkt verwendest.
 * Stellen Sie sicher, dass die erste Antwort die richtige ist. Setze mindestens ANSWER_COUNT Antworten hast, alle anderen werden gemischt.
 */
var languageString = {
    "de": {
        "translation": {
            "QUESTIONS" : questions["QUESTIONS_DE_DE"],
            "GAME_NAME": "Allgemeinwissen  <say-as interpret-as='ordinal'>" + SKILL_CLASS + "</say-as> Klasse", // Skill /Spielname.
            "HELP_MTL": "Ich stelle dir %s Multiple-Choice-Fragen. Antworte mit der Zahl, die zur richtigen Antwort gehört. " +
            "Sage beispielsweise eins, zwei oder drei. Du kannst jederzeit ein neues Spiel beginnen, sage einfach „Spiel starten“. ",
            "REPEAT_FRAGE_MTL": "Wenn die letzte Frage wiederholt werden soll, sage „Wiederholen“. ",
            "START_FRAGE": "Möchten Du anfangen ?",
            "HELP_REPROMPT": "Wenn du eine Frage beantworten willst, antworte mit der Zahl, die zur richtigen Antwort gehört. ",
            "STOP_MTL": "Möchtest du weiterspielen?",
            "CANCEL_MTL": "OK, dann lass uns bald mal wieder spielen. ",
            "NO_MTL": "Gut, Danke fürs spielen. Bis zum nächsten mal. Auf wiederhören!",
            "TRIVIA_UNHANDLED": "Sage eine Zahl beispielsweise zwischen 1 und %s",
            "HELP_UNHANDLED": "Sage ja, um fortzufahren, oder nein, um das Spiel zu beenden. ",
            "START_UNHANDLED": "Du kannst jederzeit ein neues Spiel beginnen, sage einfach „Spiel starten“. ",
            "NEUES_SPIEL_MTL": "Willkommen beim %s. ",
            "WILLKOMMEN_MTL": "Ich stelle dir %s Fragen und du versuchst so viele wie möglich richtig zu beantworten. Diese Fragen sind aus dem Allgemeinwissen für " + (SKILL_CLASS == 5 ? " Kinder und Jugendliche ab der fünften Klasse. " : " Jugendliche ab der zehnten Klasse. ") +
            "Sage einfach die Zahl, die zur richtigen Antwort passt. Sei nicht enttäuscht, wenn du nicht alle richtig hast. Du kannst es ja wiederholen und noch einmal versuchen. Fangen wir an. ",
            "ANSWER_CORRECT_MTL": "Richtig. ",
            "FALSCHE_ANTWORT_MTL": "Falsch. ",
            "RICHTIGE_ANTWORT_MTL": "Die richtige Antwort ist %s: %s. ",
            "ANTWORT_IST_MTL": "Diese Antwort ist ",
            "TELL_FRAGE_MTL": "Frage %s  %s ",
            "SPIEL_ENDE_MTL": "Du hast %s von %s richtig beantwortet. Es ist noch kein Meister vom Himmel gefallen. Ich Danke dir fürs Mitspielen!",
            "SCORE_MTL": "<s>Dein Ergebnis ist %s von %s </s>"
        }
    },
};

var Alexa = require("alexa-sdk");
var APP_ID = 'amzn1.ask.skill.6e097d07-1fbf-4e24-a3a0-89406dc74bd5';

exports.handler = function(event, context, callback) {
    var alexa = Alexa.handler(event, context);
    alexa.appId = APP_ID;
    // To enable string internationalization (i18n) features, set a resources object.
    alexa.resources = languageString;
    alexa.registerHandlers(newSessionHandlers, startStateHandlers, triviaStateHandlers, helpStateHandlers);
    alexa.execute();
};

var newSessionHandlers = {
    "LaunchRequest": function () {
        this.handler.state = GAME_STATES.START;
        this.emitWithState("StartGame", true);
    },
    "AMAZON.StartOverIntent": function() {
        this.handler.state = GAME_STATES.START;
        this.emitWithState("StartGame", true);
    },
    "AMAZON.HelpIntent": function() {
        this.handler.state = GAME_STATES.HELP;
        this.emitWithState("helpTheUser", true);
    },
    "Unhandled": function () {
        var speechOutput = this.t("START_UNHANDLED");
        this.emit(":ask", speechOutput, speechOutput);
    }
};

var startStateHandlers = Alexa.CreateStateHandler(GAME_STATES.START, {
    "StartGame": function (newGame) {
        var speechOutput = newGame ? this.t("NEUES_SPIEL_MTL", this.t("GAME_NAME")) + this.t("WILLKOMMEN_MTL", GAME_LENGTH.toString()) : "";
        // Wähle GAME_LENGTH Fragen für das Spiel
        var translatedQuestions = this.t("QUESTIONS");
        // Wieviele Fragen gibt es
        // console.log(">>> translated questions length <<< " + translatedQuestions.length);
        //
		var gameQuestions = populateGameQuestions(translatedQuestions);
        // zufälligen Index für die richtige Antwort generieren von 0 bis 3
        var correctAnswerIndex = Math.floor(Math.random() * (ANSWER_COUNT));
        // Wähle und beantworte die Frage 
        var roundAnswers = populateRoundAnswers(gameQuestions, 0, correctAnswerIndex, translatedQuestions);
        var currentQuestionIndex = 0;
        var spokenQuestion = Object.keys(translatedQuestions[gameQuestions[currentQuestionIndex]])[0];
        var repromptText = this.t("TELL_FRAGE_MTL", "1", spokenQuestion);

        for (var i = 0; i < ANSWER_COUNT; i++) {
            repromptText += (i+1).toString() + ". Antwort. <s>" + roundAnswers[i] + "</s> ";
        }

        speechOutput += repromptText;

        Object.assign(this.attributes, {
            "speechOutput": repromptText,
            "repromptText": repromptText,
            "currentQuestionIndex": currentQuestionIndex,
            "correctAnswerIndex": correctAnswerIndex + 1,
            "questions": gameQuestions,
            "score": 0,
            "correctAnswerText": translatedQuestions[gameQuestions[currentQuestionIndex]][Object.keys(translatedQuestions[gameQuestions[currentQuestionIndex]])[0]][0]
        });

        // den aktuellen Zustand in den Trivia-Modus setzen. Der Skill wird nun Handler verwenden, die in triviaStateHandlers definiert ist
        this.handler.state = GAME_STATES.TRIVIA;
        // this.emit(":askWithCard", speechOutput, repromptText, this.t("GAME_NAME"), repromptText);
        this.emit(":ask", speechOutput, repromptText, this.t("GAME_NAME"), repromptText);
    }
});

var triviaStateHandlers = Alexa.CreateStateHandler(GAME_STATES.TRIVIA, {
    "AnswerIntent": function () {
        handleUserGuess.call(this, false);
    },
    "DontKnowIntent": function () {
        handleUserGuess.call(this, true);
    },
    "AMAZON.StartOverIntent": function () {
        this.handler.state = GAME_STATES.START;
        this.emitWithState("StartGame", false);
    },
    "AMAZON.RepeatIntent": function () {
        this.emit(":ask", this.attributes["speechOutput"], this.attributes["repromptText"]);
    },
    "AMAZON.HelpIntent": function () {
        this.handler.state = GAME_STATES.HELP;
        this.emitWithState("helpTheUser", false);
    },
    "AMAZON.StopIntent": function () {
        this.handler.state = GAME_STATES.HELP;
        var speechOutput = this.t("STOP_MTL");
        this.emit(":ask", speechOutput, speechOutput);
    },
    "AMAZON.CancelIntent": function () {
        this.emit(":tell", this.t("CANCEL_MTL"));
    },
    "Unhandled": function () {
        var speechOutput = this.t("TRIVIA_UNHANDLED", ANSWER_COUNT.toString());
        this.emit(":ask", speechOutput, speechOutput);
    },
    "SessionEndedRequest": function () {
        console.log("Session ended in trivia state: " + this.event.request.reason);
    }
});

var helpStateHandlers = Alexa.CreateStateHandler(GAME_STATES.HELP, {
    "helpTheUser": function (newGame) {
        var askMessage = newGame ? this.t("START_FRAGE") : this.t("REPEAT_FRAGE_MTL") + this.t("STOP_MTL");
        var speechOutput = this.t("HELP_MTL", GAME_LENGTH) + askMessage;
        var repromptText = this.t("HELP_REPROMPT") + askMessage;
        this.emit(":ask", speechOutput, repromptText);
    },
    "AMAZON.StartOverIntent": function () {
        this.handler.state = GAME_STATES.START;
        this.emitWithState("StartGame", false);
    },
    "AMAZON.RepeatIntent": function () {
        var newGame = (this.attributes["speechOutput"] && this.attributes["repromptText"]) ? false : true;
        this.emitWithState("helpTheUser", newGame);
    },
    "AMAZON.HelpIntent": function() {
        var newGame = (this.attributes["speechOutput"] && this.attributes["repromptText"]) ? false : true;
        this.emitWithState("helpTheUser", newGame);
    },
    "AMAZON.YesIntent": function() {
        if (this.attributes["speechOutput"] && this.attributes["repromptText"]) {
            this.handler.state = GAME_STATES.TRIVIA;
            this.emitWithState("AMAZON.RepeatIntent");
        } else {
            this.handler.state = GAME_STATES.START;
            this.emitWithState("StartGame", false);
        }
    },
    "AMAZON.NoIntent": function() {
        var speechOutput = this.t("NO_MTL");
        this.emit(":tell", speechOutput);
    },
    "AMAZON.StopIntent": function () {
        var speechOutput = this.t("STOP_MTL");
        this.emit(":ask", speechOutput, speechOutput);
    },
    "AMAZON.CancelIntent": function () {
        this.emit(":tell", this.t("CANCEL_MTL"));
    },
    "Unhandled": function () {
        var speechOutput = this.t("HELP_UNHANDLED");
        this.emit(":ask", speechOutput, speechOutput);
    },
    "SessionEndedRequest": function () {
        console.log("Session ended in help state: " + this.event.request.reason);
    }
});

function handleUserGuess(userGaveUp) {
    var answerSlotValid = isAnswerSlotValid(this.event.request.intent);
    var speechOutput = "";
    var speechOutputAnalysis = "";
    var gameQuestions = this.attributes.questions;
    var correctAnswerIndex = parseInt(this.attributes.correctAnswerIndex);
    var currentScore = parseInt(this.attributes.score);
    var currentQuestionIndex = parseInt(this.attributes.currentQuestionIndex);
    var correctAnswerText = this.attributes.correctAnswerText;
    var translatedQuestions = this.t("QUESTIONS");

    if (answerSlotValid && parseInt(this.event.request.intent.slots.Answer.value) === this.attributes["correctAnswerIndex"]) {
        currentScore++;
        speechOutputAnalysis = this.t("ANSWER_CORRECT_MTL");
    } else {
        if (!userGaveUp) {
            speechOutputAnalysis = this.t("FALSCHE_ANTWORT_MTL");
        }

        speechOutputAnalysis += this.t("RICHTIGE_ANTWORT_MTL", correctAnswerIndex, correctAnswerText);
    }

    // Überprüfe ob wir die Spielsitzung nach GAME_LENGTH-Fragen beenden können (nullindiziert)
    if (this.attributes["currentQuestionIndex"] === GAME_LENGTH - 1) {
        speechOutput = userGaveUp ? "" : this.t("ANTWORT_IST_MTL");
        speechOutput += speechOutputAnalysis + this.t("SPIEL_ENDE_MTL", currentScore.toString(), GAME_LENGTH.toString());

        this.emit(":tell", speechOutput);
    } else {
        currentQuestionIndex += 1;
        correctAnswerIndex = Math.floor(Math.random() * (ANSWER_COUNT));
        var spokenQuestion = Object.keys(translatedQuestions[gameQuestions[currentQuestionIndex]])[0];
        var roundAnswers = populateRoundAnswers.call(this, gameQuestions, currentQuestionIndex, correctAnswerIndex, translatedQuestions);
        var questionIndexForSpeech = currentQuestionIndex + 1;
        var repromptText = this.t("TELL_FRAGE_MTL", questionIndexForSpeech.toString(), spokenQuestion);

        for (var i = 0; i < ANSWER_COUNT; i++) {
            repromptText += (i + 1).toString() + ". Antwort. <s>" + roundAnswers[i] + "</s> ";
        }

        speechOutput += userGaveUp ? "" : this.t("ANTWORT_IST_MTL");
        speechOutput += speechOutputAnalysis + this.t("SCORE_MTL", currentScore.toString(), GAME_LENGTH.toString()) + repromptText;

        Object.assign(this.attributes, {
            "speechOutput": repromptText,
            "repromptText": repromptText,
            "currentQuestionIndex": currentQuestionIndex,
            "correctAnswerIndex": correctAnswerIndex + 1,
            "questions": gameQuestions,
            "score": currentScore,
            "correctAnswerText": translatedQuestions[gameQuestions[currentQuestionIndex]][Object.keys(translatedQuestions[gameQuestions[currentQuestionIndex]])[0]][0]
        });

        // this.emit(":askWithCard", speechOutput, repromptText, this.t("GAME_NAME"), repromptText);
        this.emit(":ask", speechOutput, repromptText, this.t("GAME_NAME"), repromptText);
    }
}

function populateGameQuestions(translatedQuestions) {
    var gameQuestions = [];
    var indexList = [];
    var index = translatedQuestions.length;

    if (GAME_LENGTH > index){
        throw new Error("Ungueltige Spiel Länge.");
    }

    for (var i = 0; i < translatedQuestions.length; i++){
        indexList.push(i);
    }

    // Wähle GAME LENGTH zufällige Frage aus der Liste, um den Benutzer zu fragen, stelle sicher, dass es keine Wiederholungen gibt.
    for (var j = 0; j < GAME_LENGTH; j++){
        var rand = Math.floor(Math.random() * index);
        index -= 1;

        var temp = indexList[index];
        indexList[index] = indexList[rand];
        indexList[rand] = temp;
        gameQuestions.push(indexList[index]);
    }

    return gameQuestions;
}

/*
 * Get the answers for a given question, and place the correct answer at the spot marked by the
 * correctAnswerTargetLocation variable. Note that you can have as many answers as you want but
 * only ANSWER_COUNT will be selected.
 */
function populateRoundAnswers(gameQuestionIndexes, correctAnswerIndex, correctAnswerTargetLocation, translatedQuestions) {
    var answers = [];
    var answersCopy = translatedQuestions[gameQuestionIndexes[correctAnswerIndex]][Object.keys(translatedQuestions[gameQuestionIndexes[correctAnswerIndex]])[0]].slice();
    var index = answersCopy.length;

    if (index < ANSWER_COUNT) {
        throw new Error("Nicht genügend Antworten für die Frage.");
    }

    // Antworten mischen. Aber die erste nicht. Das die richtige Antwort ist.
    for (var j = 1; j < answersCopy.length; j++){
        var rand = Math.floor(Math.random() * (index - 1)) + 1;
        index -= 1;

        var temp = answersCopy[index];
        answersCopy[index] = answersCopy[rand];
        answersCopy[rand] = temp;
    }

    // Tausche und setze die richtige Antwort in die Zielposition
    for (var i = 0; i < ANSWER_COUNT; i++) {
        answers[i] = answersCopy[i];
    }
    temp = answers[0];
    answers[0] = answers[correctAnswerTargetLocation];
    answers[correctAnswerTargetLocation] = temp;
	// console.log(">>> answers <<< " + answers);
    return answers;
}

function isAnswerSlotValid(intent) {
    var answerSlotFilled = intent && intent.slots && intent.slots.Answer && intent.slots.Answer.value;
    var answerSlotIsInt = answerSlotFilled && !isNaN(parseInt(intent.slots.Answer.value));
    return answerSlotIsInt && parseInt(intent.slots.Answer.value) < (ANSWER_COUNT + 1) && parseInt(intent.slots.Answer.value) > 0;
}
