﻿'use strict';

module.exports = {
    /**
     * Auf ? und Zeichen achten.
     * Erste Frage ist immer die Richtige.
     */
    "QUESTIONS_DE_DE" : [
		{
			"<break time='50ms'/>Welcher deutsche Autor schrieb die Blechtrommel?": [
				"Günter Grass.",
				"Thomas Mann.",
				"Heinrich Böll."
			]
        },
        {
            "<break time='50ms'/>Was ist ein Blizzard?": [
                "Ein sehr starker Schneesturm Kälteeinbruch.",
                "Es heißt Blitzer auf Englisch.",
                "Ein Vogel."
            ]
        },
        {
            "<break time='50ms'/>Wie viele Feldspieler braucht ein Basketball-Team?": [
                "5",
                "6",
                "7"
            ]
        },
		{
            "<break time='50ms'/>Seit wann bezahlen wir mit Euro?": [
				"Zweitausendzwei",
				"Zweitausendeins",
				"Neunzehnhundertachtundneunzieg"
			]
		},
		{
            "<break time='50ms'/>Wo sitzt das Bundesverfassungsgericht?": [
				"Karlsruhe",
				"Berlin",
				"Koblenz"
			]
		},
		{
			"<break time='50ms'/>Worum ging es beim 30-jährigen Krieg?": [
				"um evangelischen oder katholischen Glauben.",
				"um das Gebiet des heutigen Polen.",
				"um einen Machtkampf zwischen Fürsten und König."
			]
		},
		{
			"<break time='50ms'/>Wofür braucht der Körper Galle?": [
				"zur Fettverdauung.",
				"zur Blutreinigung.",
				"zur Produktion von Enzymen."
			]
		},
        {
            "<break time='50ms'/>Was ist H2SO4?": [
                "Schwefelsäure.",
                "Ethanal.",
                "Salzsäure."
            ]
        },
        {
            "<break time='50ms'/>Welcher amerikanische Künstler malte Portraits von Prominenten und Suppendosen?": [
                "Andy Warhol.",
                "Roy Lichtenstein.",
                "Keith Haring."
            ]
        },
        {
            "<break time='50ms'/>Wann wurden Wagner und Verdi geboren?": [
                "Achzehnhundertdreizehn",
                "Achzehnhunderfünfzieg",
                "Neunzehnhunderteins"
            ]
        },
        {
            "<break time='50ms'/>Was bedeutet Kohärenz?": [
                "Ein Zusammenhang.",
                "Das Wort gibt es nicht.",
                "Die Kooperation verschiedener Länder."
            ]
        },
        {
            "<break time='50ms'/><s>Auf 1500 Euro gewährt eine Bank jedes Jahr 3 Prozent Zinsen</s> Wie viel Geld ist das nach drei Jahren?": [
                "1639 Euro",
                "1635 Euro",
                "1545 Euro"
            ]
        },
        {
            "<break time='50ms'/>Wer gehört nicht zu den vier Evangelisten?": [
                "Michael.",
                "Lukas.",
                "Markus."
            ]
        },
        {
            "<break time='50ms'/>Welcher Maler hat sich selbst ein Ohr abgeschnitten?": [
                "Vincent van Gogh.",
                "Rembrandt.",
                "Albrecht Dürer."
            ]
        },
        {
            "<break time='50ms'/>Welches Gebirge liegt nicht in Europa?": [
                "die Anden.",
                "die Karpaten.",
                "die Pyrenäen."
            ]
        },
        {
            "<break time='50ms'/>Was bringt uns Dolby Digital im Kino?": [
                "Töne aus verschiedenen Richtungen.",
                "ein schärferes Bild.",
                "Farben aus dem Naturspektrum."
            ]
        },
        {
            "<break time='50ms'/>Was gab es in der DDR nicht?": [
                "Reisefreiheit.",
                "eine Armee.",
                "das Abitur."
            ]
        },
        {
            "<break time='50ms'/>Was wird in Ampere gemessen?": [
                "die Stromstärke.",
                "die Spannung.",
                "das Magnetfeld."
            ]
        },
        {
            "<break time='50ms'/><s>Wie hieß der venezianische Kaufmann, der im 14</s> Jahrhundert bis nach China reiste?": [
                "Marco Polo.",
                "Vasco da Gama.",
                "Magellan."
            ]
        },
        {
            "<break time='50ms'/>In welcher Stadt steht das wichtigste Heiligtum des Islam?": [
                "Mekka.",
                "Medina.",
                "Jerusalem."
            ]
        },
        {
            "<break time='50ms'/>Welche Lehre begründete Charles Darwin?": [
                "Die Evolutionslehre.",
                "Die Verhaltensregel.",
                "Die Vererbungslehre."
            ]
        },
        {
            "<break time='50ms'/>Was ist am kleinsten?": [
                "Proton",
                "Atom",
                "Molekül"
            ]
        },
        {
            "<break time='50ms'/>Welcher Malstil setzt die Bilder aus unzähligen kleinen Farbtupfern zusammen?": [
                "Impressionismus.",
                "Kubismus.",
                "Expressionismus." 
            ]
        },
        {
            "<break time='50ms'/>Die Leiden des jungen Werther, heißt ein Roman von?": [
                "Goethe.",
                "Schiller.",
                "Lessing."
            ]
        },
        {
            "<break time='50ms'/>Was gehört nicht in die Oper?": [
                "die Sinfonie.",
                "die Arie.",
                "die Ouvertüre."
            ]
        }
    ]
};