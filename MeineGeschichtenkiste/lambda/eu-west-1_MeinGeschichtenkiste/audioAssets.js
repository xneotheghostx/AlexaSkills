'use strict';

// Audio Source - https://nordicskills.de/audio/MeineGeschichtenkiste/
var audioData = [
    {
        'title': 'Die Abfahrt',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Ruegen/Ruegen1.mp3',
        'category': 'Abenteuer auf Rügen'
    },
    {
        'title': 'Der nächste Morgen',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Ruegen/Ruegen2.mp3',
        'category': 'Abenteuer auf Rügen'
    },
	{
        'title': 'Tag zwei',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Ruegen/Ruegen3.mp3',
        'category': 'Abenteuer auf Rügen'
    },
	{
        'title': 'Tag drei',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Ruegen/Ruegen4.mp3',
        'category': 'Abenteuer auf Rügen'
    },
	{
        'title': 'Tag vier',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Ruegen/Ruegen5.mp3',
        'category': 'Abenteuer auf Rügen'
    },
	{
        'title': 'Die Bootsfahrt',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Ruegen/Ruegen6.mp3',
        'category': 'Abenteuer auf Rügen'
    },
	{
        'title': 'Der Heimweg',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Ruegen/Ruegen7.mp3',
        'category': 'Abenteuer auf Rügen'
    },
	{
        'title': 'Eine Insel ohne Namen',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track01.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
	{
        'title': 'Stärker als Pipi Langstrumpf',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track02.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
	{
        'title': 'Die versunkene Stadt',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track03.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
	{
        'title': 'Die Bernstein Hexe',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track04.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
	{
        'title': 'Ein Abendessen bei den Rittern',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track05.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
	{
        'title': 'Auf dem Meeresgrund',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track06.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
	{
        'title': 'Piratenschätze und Gesang',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track07.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
	{
        'title': 'Piranhas und Edelsteine',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track08.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
	{
        'title': 'Wo Kaiser baden',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track09.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
	{
        'title': 'Besuch in den Tropen',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track10.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
	{
        'title': 'Besuch von Oma und Opa',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track11.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
	{
        'title': 'Zweitausend Schmetterlinge',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track12.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
	{
        'title': 'Von Bären und Mäusen',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track13.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
	{
        'title': 'Der Osterhase kommt',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track14.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
	{
        'title': 'Die warheit über Veneta',
        'url': 'https://nordicskills.de/audio/MeineGeschichtenkiste/Usedom/Track15.mp3',
        'category': 'Sagenhafte Ferien auf Usedom'
    },
];

module.exports = audioData;
