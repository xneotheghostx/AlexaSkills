'use strict';

var Alexa = require('alexa-sdk');
var audioData = require('./audioAssets');
var constants = require('./constants');

var stateHandlers = {
  startModeIntentHandlers: Alexa.CreateStateHandler(constants.states.START_MODE, {
    /*
         *  All Intent Handlers for state : START_MODE
         */
    'LaunchRequest': function() {
      //  Change state to START_MODE
      this.handler.state = constants.states.START_MODE;

      var message = '<say-as interpret-as="interjection">Sesam öffne dich.</say-as><audio src="https://nordicskills.de/audio/MeineGeschichtenkiste/kiste_auf.mp3" /> '
	  + '<say-as interpret-as="interjection">Willkommen</say-as><break time="200ms"/> und schön das du Zeit hast. ' 
	  + 'Der Verlag Biber und Butzemann hat seine Geschichtenkiste für dich geöffnet. In der Kiste findest du 2 Feriengeschichten<break time="200ms"/> '
	  + 'Sag mir bitte welche Geschichte du hören möchtest. <break time="200ms"/> Entweder "Abenteuer auf Rügen" oder "Sagenhafte Ferien auf Usedom". ';
      var reprompt = 'Folgende Geschichten stehen für dich zur Wahl: '
          + buildSpeechFromArr(getCategories(audioData)) + " Du findest alle verfügbaren Geschichten auch in deiner Alexa-App. Welche Geschichte möchtest du hören? ";
      this.response.speak(message).listen(reprompt);
      var cardContent = buildCardFromArr(getCategories(audioData));
      this.response.cardRenderer("Meine Geschichtenkiste: Auswahl", cardContent);

      this.emit(':responseReady');
    },
    'PlayAudio': function() {
      if (!this.attributes['playOrder']) {
        // Initialize Attributes if undefined.
        this.attributes['playOrder'] = Array.apply(null, {length: audioData.length}).map(Number.call, Number);
        this.attributes['index'] = 0;
        this.attributes['offsetInMilliseconds'] = 0;
        this.attributes['loop'] = false;
        this.attributes['shuffle'] = false;
        this.attributes['playbackIndexChanged'] = true;
        //  Change state to START_MODE
        this.handler.state = constants.states.START_MODE;
        }
      console.log(this.event.request.intent.slots.Sound.value);
      controller.play.call(this);
    },
    'ListCategory': function () {

        var slots = this.event.request.intent.slots;
        if (slots && slots.Category && slots.Category.value) {

            var arr = itemsOfCategory(audioData, slots.Category.value);
			
			if (arr.length <= 0)
                this.emitWithState('Unhandled');
						
            var message = "Folgende Kapitel gehören zur Geschichte " + slots.Category.value + '.<break time="1s" />' + buildSpeechFromArr(arr) + ' <break time="700ms" /> Und welches Kapitel möchtest du jetzt hören?';
            this.response.speak(message).listen(message);

            var cardContent = buildCardFromArr(arr);
            this.response.cardRenderer("Meine Geschichtenkiste: " + slots.Category.value.replace(/regenn/gi, "Regen"), cardContent);
        }

        this.emit(':responseReady');
    },
    'AMAZON.HelpIntent': function() {
        var message = 'Hallo und Willkommen bei "Deiner Geschichtenkiste". Hier kommt etwas Hilfe. <break time="250ms" /> In deiner Kiste sind derzeit 2 Geschichten mit 22 Kapiteln. '
		+ 'Um zu starten sage, "Abenteuer auf Rügen" oder "Sagenhafte Ferien auf Usedom". <break time="200ms" /> Du kannst mit weiter oder zurück in den Kapitel wechseln. '
		+ 'Mit Neustarten startest du das gerade gehörte Kapitel neu. <break time="200ms" /> Mit Stop oder Pause beendest du deine Geschichte und kannst Sie später weiter hören. '
		+ '<break time="200ms" /> und jetzt sage mir bitte welche Geschichte du hören möchtest. ';
      this.response.speak(message).listen(message);
      this.emit(':responseReady');
    },
    'AMAZON.StopIntent': function() {
      var message = 'Tschüs und danke fürs zuhören, wünscht dir der Kinderbuch Verlag Biber und Butzemann. ';
	  delete this.attributes['STATE'];
      this.response.speak(message);
      this.emit(':responseReady');
    },
    'AMAZON.CancelIntent': function() {
      var message = 'Tschüs und danke fürs zuhören, wünscht dir der Kinderbuch Verlag Biber und Butzemann. ';
	  delete this.attributes['STATE'];
      this.response.speak(message);
      this.emit(':responseReady');
    },
    'SessionEndedRequest': function() {
      // No session ended logic
    },
    'Unhandled': function() {
      var message = 'Es tut mir leid, ich habe dich nicht verstanden. Sage entweder "Abenteuer auf Rügen" oder "Sagenhafte Ferien auf Usedom" .';
      this.response.speak(message).listen(message);
      this.emit(':responseReady');
    }
  }),
  playModeIntentHandlers: Alexa.CreateStateHandler(constants.states.PLAY_MODE, {
    /*
         *  All Intent Handlers for state : PLAY_MODE
         */
    'LaunchRequest': function() {
      /*
             *  Session resumed in PLAY_MODE STATE.
             *  If playback had finished during last session :
             *      Give welcome message.
             *      Change state to START_STATE to restrict user inputs.
             *  Else :
             *      Ask user if he/she wants to resume from last position.
             *      Change state to RESUME_DECISION_MODE
             */
      var message;
      var reprompt;
      if (this.attributes['playbackFinished']) {
        this.handler.state = constants.states.START_MODE;
        message = 'Es tut mir leid, ich habe dich nicht verstanden.';
        reprompt = 'Du kannst sagen erzähl mir eine Geschichte, um zu starten.';
      } else {
        this.handler.state = constants.states.RESUME_DECISION_MODE;
        message = 'Du hörst gerade ' + audioData[this.attributes['playOrder'][this.attributes['index']]].title + ' möchtest du es weiter hören?';
        reprompt = 'Du kannst ja sagen um es weiter zuhören oder nein um eine andere Geschichte zu starten. ';
      }

      this.response.speak(message).listen(reprompt);
      this.emit(':responseReady');
    },
    'PlayAudio': function() {
      controller.play.call(this)
    },
    'ListCategory': function () {

        var slots = this.event.request.intent.slots;
        if (slots && slots.Category && slots.Category.value) {

            var arr = itemsOfCategory(audioData, slots.Category.value);

            var message = "Folgende Kapitel gehören zu der Geschichte " + slots.Category.value + '<break time="1s" />' + buildSpeechFromArr(arr);
            this.response.speak(message).listen(message);

            var cardContent = buildCardFromArr(arr);
            this.response.cardRenderer("Meine Geschichtenkiste: " + slots.Category.value, cardContent);
        }

        this.emit(':responseReady');
    },
    'AMAZON.NextIntent': function() {
      controller.playNext.call(this)
    },
    'AMAZON.PreviousIntent': function() {
      controller.playPrevious.call(this)
    },
    'AMAZON.PauseIntent': function() {
      controller.stop.call(this)
    },
    'AMAZON.StopIntent': function() {
      controller.stop.call(this)
    },
    'AMAZON.CancelIntent': function() {
      controller.stop.call(this)
    },
    'AMAZON.ResumeIntent': function() {
      controller.play.call(this)
    },
    'AMAZON.LoopOnIntent': function() {
      controller.loopOn.call(this)
    },
    'AMAZON.LoopOffIntent': function() {
      controller.loopOff.call(this)
    },
    'AMAZON.ShuffleOnIntent': function() {
      controller.shuffleOn.call(this)
    },
    'AMAZON.ShuffleOffIntent': function() {
      controller.shuffleOff.call(this)
    },
    'AMAZON.StartOverIntent': function() {
      controller.startOver.call(this)
    },
    'AMAZON.HelpIntent': function() {
      // This will called while audio is playing and a user says "ask <invocation_name> for help"
      var message = 'Du hörst "deine Geschichtenkiste". Du kannst weiter oder zurück sagen um ein Kapitel der Geschichte zu wechseln oder zu überspringen. ' +
      'Du kannst jederzeit Pause sagen um zu Pausieren oder weiter, um weiter zuhören. Und mit "Alexa Stopp" beendest du deine Geschichtenkiste. ';
      this.response.speak(message).listen(message);
      this.emit(':responseReady');
    },
    'SessionEndedRequest': function() {
      // No session ended logic
    },
    'Unhandled': function() {
      var message = 'Es tut mir leid, ich habe dich nicht verstanden. Du kannst weiter oder zurück sagen, um ein Kapitel in der Geschichte vor oder zurück zu gehen. ';
      this.response.speak(message).listen(message);
      this.emit(':responseReady');
    }
  }),
  remoteControllerHandlers: Alexa.CreateStateHandler(constants.states.PLAY_MODE, {
    /*
         *  All Requests are received using a Remote Control. Calling corresponding handlers for each of them.
         */
    'PlayCommandIssued': function() {
      controller.play.call(this)
    },
    'PauseCommandIssued': function() {
      controller.stop.call(this)
    },
    'NextCommandIssued': function() {
      controller.playNext.call(this)
    },
    'PreviousCommandIssued': function() {
      controller.playPrevious.call(this)
    }
  }),
  resumeDecisionModeIntentHandlers: Alexa.CreateStateHandler(constants.states.RESUME_DECISION_MODE, {
    /*
         *  All Intent Handlers for state : RESUME_DECISION_MODE
         */
    'LaunchRequest': function() {
      var message = 'Du hörst gerade ' + audioData[this.attributes['playOrder'][this.attributes['index']]].title + ' möchtest du es weiter hören?';
      var reprompt = 'Du kannst ja sagen um die Geschichte weiter zuhören oder nein um eine neue Geschichte zu starten. ';
      this.response.speak(message).listen(reprompt);
      this.emit(':responseReady');
    },
    'AMAZON.YesIntent': function() {
      controller.play.call(this)
    },
    'AMAZON.NoIntent': function() {
      //controller.reset.call(this)
      this.handler.state = constants.states.START_MODE;
      this.attributes['STATE'] = '';
	  this.attributes['offsetInMilliseconds'] = 0;
      this.emit('LaunchRequest');
    },
    'AMAZON.HelpIntent': function() {
      var message = 'Du hörst gerade ' + audioData[this.attributes['index']].title + ' möchtest du das Kapitel weiter hören?';
      var reprompt = 'Du kannst ja sagen um es weiter zuhören oder nein um eine neue Geschichte zu starten.';
      this.response.speak(message).listen(reprompt);
      this.emit(':responseReady');
    },
    'AMAZON.StopIntent': function() {
      var message = 'Tschüs und ich danke die fürs zuhören, wünscht dir der Kinderbuch Verlag Biber und Butzemann. ';
      this.response.speak(message);
      this.emit(':responseReady');
    },
    'AMAZON.CancelIntent': function() {
      var message = 'Tschüs und ich danke dir fürs zuhören, wünscht dir der Kinderbuch Verlag Biber und Butzemann. ';
      this.response.speak(message);
      this.emit(':responseReady');
    },
    'SessionEndedRequest': function() {
      // No session ended logic
    },
    'Unhandled': function() {
      var message = 'Es tut mir leid, dass kannst du an dieser Stelle nicht sagen. Bitte sage Hilfe, um zu hören was du sagen kannst. ';
      this.response.speak(message).listen(message);
      this.emit(':responseReady');
    }
  })
};

module.exports = stateHandlers;

var controller = function() {
  return {
    play: function() {
      /*
             *  Using the function to begin playing audio when:
             *      Play Audio intent invoked.
             *      Resuming audio when stopped/paused.
             *      Next/Previous commands issued.
             */
      this.handler.state = constants.states.PLAY_MODE;

      if (this.attributes['playbackFinished']) {
        // Reset to top of the playlist when reached end.
        this.attributes['index'] = 0;
        this.attributes['offsetInMilliseconds'] = 0;
        this.attributes['playbackIndexChanged'] = true;
        this.attributes['playbackFinished'] = false;
      }
      
      var token;
      var playBehavior = 'REPLACE_ALL';
      var podcast = null;
      var slots = this.event.request.intent.slots;
      if (slots && slots.Sound && slots.Sound.value) {
        var soundName = slots.Sound.value
        podcast = audioData.find(function(data, idx){
          token = idx
          return data.title.toLowerCase() === soundName.toLowerCase()
        });
      }
      if (!podcast) {
        podcast = audioData[this.attributes['playOrder'][this.attributes['index']]];
        token = String(this.attributes['playOrder'][this.attributes['index']]);
      }
      var offsetInMilliseconds = this.attributes['offsetInMilliseconds'];
      // Since play behavior is REPLACE_ALL, enqueuedToken attribute need to be set to null.
      this.attributes['enqueuedToken'] = null;

      if (canThrowCard.call(this)) {
        var cardTitle = 'Du hörst ' + podcast.title.replace("Regenn", "Regen");
        var cardContent = 'Du hörst ' + podcast.title.replace("Regenn", "Regen");
        this.response.cardRenderer(cardTitle, cardContent, null);
      }

      if (slots && slots.Sound && slots.Sound.value) {
          this.response.speak(slots.Sound.value);
          console.log(slots.Sound.value);
      }
      this.response.audioPlayerPlay(playBehavior, podcast.url, token, null, offsetInMilliseconds);
      this.emit(':responseReady');
    },
    stop: function() {
      /*
             *  Issuing AudioPlayer.Stop directive to stop the audio.
             *  Attributes already stored when AudioPlayer.Stopped request received.
             */
      this.response.audioPlayerStop();
      this.emit(':responseReady');
    },
    playNext: function() {
      /*
             *  Called when AMAZON.NextIntent or PlaybackController.NextCommandIssued is invoked.
             *  Index is computed using token stored when AudioPlayer.PlaybackStopped command is received.
             *  If reached at the end of the playlist, choose behavior based on "loop" flag.
             */
      var index = this.attributes['index'];
      
      if (!this.attributes['loop']) {
        index += 1;
      }
      // Check for last audio file.
      if (index === 7 || index === audioData.length) {
        if (this.attributes['loop']) {
          index = 0;
        } else {
          // Reached at the end. Thus reset state to start mode and stop playing.
          this.handler.state = constants.states.START_MODE;

          var message = 'Du hast das letzte Kapitel der Geschichte erreicht. <break time="200ms" /> Schau doch einmal im Internet nach Biber und Butzemann dem Kinderbuch Verlag. '
		  + 'Da findest du noch weitere tolle Geschichten. ';
          this.response.speak(message).audioPlayerStop();
          return this.emit(':responseReady');
        }
      }
      // Set values to attributes.
      this.attributes['index'] = index;
      this.attributes['offsetInMilliseconds'] = 0;
      this.attributes['playbackIndexChanged'] = true;

      controller.play.call(this);
    },
    playPrevious: function() {
      /*
             *  Called when AMAZON.PreviousIntent or PlaybackController.PreviousCommandIssued is invoked.
             *  Index is computed using token stored when AudioPlayer.PlaybackStopped command is received.
             *  If reached at the end of the playlist, choose behavior based on "loop" flag.
             */
      var index = this.attributes['index'];
      
      if (!this.attributes['loop']) {
        index -= 1;
      }
      // Check for last audio file.
      if (index === -1) {
        if (this.attributes['loop']) {
          index = audioData.length - 1;
        } else {
          // Reached at the end. Thus reset state to start mode and stop playing.
          this.handler.state = constants.states.START_MODE;

          var message = 'Du hast den Anfang der Geschichte erreicht. ';
          this.response.speak(message).audioPlayerStop();
          return this.emit(':responseReady');
        }
      }
      // Set values to attributes.
      this.attributes['index'] = index;
      this.attributes['offsetInMilliseconds'] = 0;
      this.attributes['playbackIndexChanged'] = true;

      controller.play.call(this);
    },
    loopOn: function() {
      // Turn on loop play.
      this.attributes['loop'] = true;
      var message = 'Die Endlosschleife ist eingeschaltet.';
      this.response.speak(message);
      this.emit(':responseReady');
    },
    loopOff: function() {
      // Turn off looping
      this.attributes['loop'] = false;
      var message = 'die Endlosschleife ist ausgeschaltet.';
      this.response.speak(message);
      this.emit(':responseReady');
    },
    shuffleOn: function() {
      // Turn on shuffle play.
      this.attributes['shuffle'] = true;
      shuffleOrder((newOrder) => {
        // Play order have been shuffled. Re-initializing indices and playing first song in shuffled order.
        this.attributes['playOrder'] = newOrder;
        this.attributes['index'] = 0;
        this.attributes['offsetInMilliseconds'] = 0;
        this.attributes['playbackIndexChanged'] = true;
        controller.play.call(this);
      });
    },
    shuffleOff: function() {
      // Turn off shuffle play.
      if (this.attributes['shuffle']) {
        this.attributes['shuffle'] = false;
        // Although changing index, no change in audio file being played as the change is to account for reordering playOrder
        this.attributes['index'] = this.attributes['playOrder'][this.attributes['index']];
        this.attributes['playOrder'] = Array.apply(null, {length: audioData.length}).map(Number.call, Number);
      }
      controller.play.call(this);
    },
    startOver: function() {
      // Start over the current audio file.
      this.attributes['offsetInMilliseconds'] = 0;
      controller.play.call(this);
    },
    reset: function() {
      // Reset to top of the playlist.
      this.attributes['index'] = 0;
      this.attributes['offsetInMilliseconds'] = 0;
      this.attributes['playbackIndexChanged'] = true;
      controller.play.call(this);
    }
  }
}();

function canThrowCard() {
  /*
     * To determine when can a card should be inserted in the response.
     * In response to a PlaybackController Request (remote control events) we cannot issue a card,
     * Thus adding restriction of request type being "IntentRequest".
     */
  if (this.event.request.type === 'IntentRequest' && this.attributes['playbackIndexChanged']) {
    this.attributes['playbackIndexChanged'] = false;
    return true;
  } else {
    return false;
  }
}

function shuffleOrder(callback) {
  // Algorithm : Fisher-Yates shuffle
  var array = Array.apply(null, {length: audioData.length}).map(Number.call, Number);
  var currentIndex = array.length;
  var temp,
    randomIndex;

  while (currentIndex >= 1) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temp = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temp;
  }
  callback(array);
}



function buildRandomIndex(data, count) {

    var arr = []
    while (arr.length < count) {
        var randomnumber = Math.ceil(Math.random() * data.length - 1)
        if (arr.indexOf(randomnumber) > -1) continue;
        arr[arr.length] = randomnumber;
    }

    var speech = "";
    for (var i = 0; i < arr.length; i++) {
        speech += data[arr[i]].title + ", <break time='1s' />";
    }

    return speech;
}

function getCategories(data) {

    var arr = [];
    for (var i = 0; i < data.length; i++) {
        if (arr.indexOf(data[i].category) === -1) {
            arr.push(data[i].category);
        }
    }
    return arr;
}

function buildSpeechFromArr(categories) {

    var speech = "";
    for (var i = 0; i < categories.length; i++) {
        speech += categories[i] + ", <break time='1s' />";
    }

    return speech;
}
function buildCardFromArr(categories) {

    var content = "";
    for (var i = 0; i < categories.length; i++) {
        content += categories[i].replace("Regenn", "Regen") + "\n";
    }

    return content;
}

function itemsOfCategory(data, category) {

    var arr = [];
    for (var i = 0; i < data.length; i++) {
        if (data[i].category.toLowerCase() === category.toLowerCase()) {
            arr.push(data[i].title);
        }
    }
    return arr;
} 

