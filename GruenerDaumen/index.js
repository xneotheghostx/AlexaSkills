/**
 * Gruenerdaumen ist ein Alexa Echo Skill. : Copyright 2016-17 bei Jürgen Carstensen
 *
 * Beispiel:
 *  Benutzer: "Alexa, frage Gruenerdaumen wie giesse ich Orchideen."
 *  Alexa: "(liest Hilfe fuer Orchideen)"
 */

'use strict';

var AlexaSkill = require('./AlexaSkill'),
    pflegetext = require('./pflegetext');

var APP_ID = 'amzn1.ask.skill.2ba56676-44aa-4ad9-acfa-f75e087529de';

var HowTo = function () {
    AlexaSkill.call(this, APP_ID);
};

// Extend AlexaSkill
HowTo.prototype = Object.create(AlexaSkill.prototype);
HowTo.prototype.constructor = HowTo;

HowTo.prototype.eventHandlers.onLaunch = function (launchRequest, session, response) {
    var speechText = "Hallo hier ist dein Grüner Daumen. Womit kann ich dir Helfen ? ";
    // Wenn der Benutzer entweder nicht auf die Willkommensnachricht antwortet oder etwas sagt, was nicht zur Pflanzenhilfe past
    // Wird er mit diesem Text erneut aufgefordert.
    var repromptText = "Sage zum beispiel Grünpflanzen oder Blühpflanzen. Wenn du Hilfe brauchst, wie du eine Frage stellen kannst, dann sage Hilfe. ";
    response.ask(speechText, repromptText);
};

HowTo.prototype.intentHandlers = {
    "PflegeIntent": function (intent, session, response) {
        var itemSlot = intent.slots.Name,
            itemName = "";
        if (itemSlot && itemSlot.value){
            itemName = itemSlot.value.toLowerCase();
        }

        var cardTitle = "Hier kommen die Tipps und Hinweise für " + itemName.substring(0, 1).toUpperCase() + itemName.substring(1),
            pflege = "<speak>" + pflegetext[itemName] + " Und? Womit kann ich dir noch helfen? </speak>",
            pflegeCards = pflegetext[itemName],
            speechOutput,
            repromptOutput;
        if (pflegetext[itemName]) {
            pflegeCards = pflegetext[itemName].replace(/<[^>]*>/g, '');

            speechOutput = {
                speech: pflege,
                type: AlexaSkill.speechOutputType.SSML
            };
            response.askWithCard(speechOutput, " Wenn du keine Hilfe mehr brauchst, sage stop oder beenden. ", cardTitle, pflegeCards);
        } else {
            var speech;
            if (itemName) {
                speech = "<speak>Es tut mir leid, ich kann dir derzeit keine Hilfe für " + itemName + " anbieten. <break time='150ms'/> Versuche es bitte mit etwas anderem. </speak>";
            } else {
                speech = "<speak>Es tut mir leid, ich kann dir derzeit damit nicht helfen. <break time='150ms'/> Kann ich sonnst etwas für dich tun? </speak>";
            }
            speechOutput = {
                speech: speech,
                type: AlexaSkill.speechOutputType.SSML
            };
            repromptOutput = {
                speech: "<speak>Und? Womit kann ich dir noch helfen? <break time='100ms'/> oder sage Stop zum beenden. </speak>",
                type: AlexaSkill.speechOutputType.SSML
            };
            response.ask(speechOutput, repromptOutput);
        }
    },

    "AMAZON.StopIntent": function (intent, session, response) {
        var speechOutput = "Auf wiederhören und viel Erfolg mit deinen Pflanzen. ";
        var cardTitle = "Auf wiederhören und viel Erfolg mit deinen Pflanzen. ";
        var cardText = "Danke, dass ich dir helfen durfte.\n\nWenn dir dieser Skill gefallen hat, freut sich das Team von \"Nordic Skills\" über eine positive Bewertung von dir!\nDies hilft ihnen zukünftig weitere Skills zu entwickeln. \nUnd so bewertest du einen Skill:\n\n1. Öffne die Alexa App auf deinem Smartphone, Laptop oder PC\n\n2. Gehe in den Bereich „Skills“\n\n3. Tippe auf „Ihre Skills“ in der rechten oberen Ecke\n\n4. Suche nach „Grüner Daumen“\n\n5. Scrolle bis nach unten und tippe auf „Schreiben Sie eine Rezension“\n\n6. Unterstütze das Team von \"Nordic Skills\" mit deiner 5-Sterne-Bewertung\n\nDanke!";
        response.tellWithPictureCard(speechOutput, cardTitle, cardText, "https://nordicskills.de/bilder/DerZauberwaldKendell/Danke.jpg", "https://nordicskills.de/bilder/DerZauberwaldKendell/Danke.jpg");
    },

    "AMAZON.CancelIntent": function (intent, session, response) {
        var speechOutput = "Auf wiederhören und viel Erfolg mit deinen Pflanzen. ";
        var cardTitle = "Auf wiederhören und viel Erfolg mit deinen Pflanzen. ";
        var cardText = "Danke, dass ich dir helfen durfte.\n\nWenn dir dieser Skill gefallen hat, freut sich das Team von \"Nordic Skills\" über eine positive Bewertung von dir!\nDies hilft ihnen zukünftig weitere Skills zu entwickeln. \nUnd so bewertest du einen Skill:\n\n1. Öffne die Alexa App auf deinem Smartphone, Laptop oder PC\n\n2. Gehe in den Bereich „Skills“\n\n3. Tippe auf „Ihre Skills“ in der rechten oberen Ecke\n\n4. Suche nach „Grüner Daumen“\n\n5. Scrolle bis nach unten und tippe auf „Schreiben Sie eine Rezension“\n\n6. Unterstütze das Team von \"Nordic Skills\" mit deiner 5-Sterne-Bewertung\n\nDanke!";
        response.tellWithPictureCard(speechOutput, cardTitle, cardText, "https://nordicskills.de/bilder/DerZauberwaldKendell/Danke.jpg", "https://nordicskills.de/bilder/DerZauberwaldKendell/Danke.jpg");
    },

    "AMAZON.HelpIntent": function (intent, session, response) {
        var speechText = "<speak>Du kannst mich fragen, wie Dünge ich eine Amaryllis? <break time='100ms'/> Wieviel Wasser brauchen Orchideen? oder Wo steht eine Yocca Palme am besten? " 
            + "Wenn du dir nicht sicher bist wie deine Blume oder Pflanze mit Namen heist, schau einmal in den Blumentopf, oft steckt dort ein Schild in der Erde "
            + "oder der Name steht am Blumentopf. Derzeit stehen über 130 Pflanzen zur Auswahl. <break time='100ms'/> Nun, wie kann ich dir helfen? </speak>";
        var repromptText = "<speak>Du kannst zum beispiel auch sagen Grünpflanzen, dann erzähle ich dir etwas über die Pflege von Grünpflanzen. Oder du sagst Blühpflanzen. <break time='100ms'/> Nun, womit kann ich dir helfen? </speak>";
        var speechOutput = {
            speech: speechText,
            type: AlexaSkill.speechOutputType.SSML
        };
        var repromptOutput = {
            speech: repromptText,
            type: AlexaSkill.speechOutputType.SSML
        };
        response.ask(speechOutput, repromptOutput);
    }
};

exports.handler = function (event, context) {
    var howTo = new HowTo();
    howTo.execute(event, context);
};