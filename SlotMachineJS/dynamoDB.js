'use strict'

var fs = require('fs');
var AWS = require("aws-sdk")
var skill = require('./index').skill
var config = require('./config.json')

var LOCAL_CONTEXT = true

try {
  fs.accessSync( process.env[(process.platform === 'win32') ? 'USERPROFILE' : 'HOME'] + '/.aws')
} catch (e) {
  LOCAL_CONTEXT = false
}

// if this is being tested locally, load appropriate creds
if ( LOCAL_CONTEXT && config.awsProfileName ) {
  AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: config.awsProfileName })
}

var docClient = new AWS.DynamoDB.DocumentClient({ region: config.dynamoRegion || 'us-east-1' })

function getHighscore( session, cb ) {
    if (!config.dynamoTableName) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

    var params = {
        TableName: config.dynamoTableName,
        IndexName: "userType-score-index",
        KeyConditionExpression: "#n = :n",
        ConsistentRead: false,
        ExpressionAttributeNames:  {
            "#n": "userType"
        },
        ExpressionAttributeValues: {
            ":n": "MainUser"
        },
        ScanIndexForward: false,
        Limit: 10,
        Select: "ALL_ATTRIBUTES"
    }

    docClient.query(params, handler(cb))
}


function getJackpot( session, cb ) {
    if (!config.dynamoTableName) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

    var params = {
        "TableName": config.dynamoTableName,
        "Key": {
            "userId": "Jackpot",
            "userType": "Jackpot"
        },
        "AttributesToGet": [
            "score"
        ],
    }

    docClient.get(params, handler(cb))
}
function setJackpot(session, cb) {
    if (!config.dynamoTableName) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

    var params = {
        "TableName": config.dynamoTableName,
        "Item": {
            "userId": "Jackpot",
            "userType": "Jackpot",
            "bet": 1,
            "score": session.attributes.jackpot,
            "userName": "Jackpot",
            "timestamp": "Jackpot"
        }
    }

    docClient.put(params, handler(cb))
}


function getUserState ( session, cb ) {
  if ( ! config.dynamoTableName ) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

  var params = {
    "TableName": config.dynamoTableName,
    "Key": {
        "userId": session.user.userId,
        "userType": "MainUser"
    },
    "AttributesToGet": [
      "userType",
      "bet",
      "score",
      "userName",
      "timestamp",
      "firstTimestamp",
      "lastTimestamp",
      "sessionCount",
      "echoButtons"
    ],
  }

  docClient.get( params, handler( cb ) )
}
function putUserState(session, cb) {
    if (!config.dynamoTableName) return cb({ message: 'ERROR: Dynamo DB table name was not provided', item: null }) // exit

    var params = {
        "TableName": config.dynamoTableName,
        "Item": {
            "userId": session.user.userId,
            "userType": "MainUser",
            "bet": session.attributes.bet,
            "score": session.attributes.score,
            "userName": session.attributes.userName,
            "timestamp": session.attributes.timestamp,
            "firstTimestamp": session.attributes.firstTimestamp,
            "lastTimestamp": session.attributes.lastTimestamp,
            "sessionCount": session.attributes.sessionCount,
            "echoButtons": session.attributes.echoButtons
        }
    }

    docClient.put(params, handler(cb))
}


function handler ( cb ) {
  return function ( err, data ) {
    if (err) {
      cb({
        message: "ERROR: Dynamo DB - " + JSON.stringify( err, null, 2 ),
        item: null
      })
    } else {
        if (typeof data.Items === "undefined") {
            cb({
                message: "SUCCESS: Dynamo DB",
                item: data.Item
            })
        } else {
            cb({
                message: "SUCCESS: Dynamo DB",
                item: data.Items
            })
        }
    }
  }
}

module.exports = {
  getHighscore: getHighscore,
  setJackpot: setJackpot,
  getJackpot: getJackpot,
  putUserState: putUserState,
  getUserState: getUserState
}
