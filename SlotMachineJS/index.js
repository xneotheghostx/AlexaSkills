﻿var Alexa = require('alexa-sdk');
var dynamo = require('./dynamoDB');
var constants = require('./constants');
var moment = require('./moment');

var locale = "";
var shouldRecieveReward = false;

exports.handler = function (event, context, callback) {

    locale = event.request.locale;

    if (locale == "de-DE") {
        slotItems = [
            {
                "name": "Kirsche",
                "multiplier": 8,
                "probability": 6
            },
            {
                "name": "Pflaume",
                "multiplier": 16,
                "probability": 5
            },
            {
                "name": "Wassermelone",
                "multiplier": 25,
                "probability": 4
            },
            {
                "name": "Glocke",
                "multiplier": 50,
                "probability": 3
            },
            {
                "name": "Bar",
                "multiplier": 100,
                "probability": 2
            },
            {
                "name": "Sieben",
                "multiplier": 250,
                "probability": 1
            }
        ];
    } else { //if(locale == "en-US" || locale == "en-GB")
        slotItems = [
            {
                "name": "Cherry",
                "multiplier": 8,
                "probability": 6
            },
            {
                "name": "Plum",
                "multiplier": 16,
                "probability": 5
            },
            {
                "name": "Watermelon",
                "multiplier": 25,
                "probability": 4
            },
            {
                "name": "Bell",
                "multiplier": 50,
                "probability": 3
            },
            {
                "name": "Bar",
                "multiplier": 100,
                "probability": 2
            },
            {
                "name": "Seven",
                "multiplier": 250,
                "probability": 1
            }
        ];
    }

    var alexa = Alexa.handler(event, context);
    alexa.appId = constants.appId;
    alexa.registerHandlers(handlers.setupModeIntentHandlers, handlers.gameModeIntentHandlers);
    alexa.execute();
};

var handlers = {

    setupModeIntentHandlers: Alexa.CreateStateHandler(constants.states.SETUP_MODE, {
        'NewSession': function () {

            console.log("Session init");
            this.handler.state = constants.states.SETUP_MODE;

            var self = this;
            dynamo.getJackpot(self.event.session, function (data) {

                console.log("Jackpot: " + data.message);

                if (data.item) {
                    self.event.session.attributes['jackpot'] = data.item.score;
                    self.event.session.attributes['jackpotDelta'] = 0;
                }

                dynamo.getUserState(self.event.session, function (data) {

                    console.log("User: " + data.message);

                    if (data.item) {
                        Object.assign(self.event.session.attributes, data.item);

                        var currentDateTime = moment().add(1, 'hours');
                        if (self.event.session.attributes['timestamp'] != "") {

                            var parsedDateTime = moment(self.event.session.attributes['timestamp'], "DD/MM/YYYY HH:mm");

                            console.log("test: " + currentDateTime.date() + " .  " +  parsedDateTime.date())
                            if (currentDateTime.date() != parsedDateTime.date()) {
                                shouldRecieveReward = true;

                                // Rückkehrer-Bonus hinzufügen
                                self.event.session.attributes['score'] += 25;
                            } else {
                                shouldRecieveReward = false;
                            }
                            self.event.session.attributes['timestamp'] = currentDateTime.format("DD/MM/YYYY HH:mm");
                        }

                        
                        if (self.attributes["userName"] === "InitalName") {
                            self.emit('NoFirstName');
                        } else {
                            self.emit('WelcomeBackIntent');
                        }
                    } else {
                        self.emit('ResetIntent');
                    }
                });
            });            
        },
        'LaunchRequest': function () {

            this.emit('NewSession');
        },
        'ResetIntent': function () {

            this.attributes["userName"] = "InitalName";
            this.attributes["userType"] = "MainUser";
            this.attributes["bet"] = 1;
            this.attributes["score"] = 100;
            this.attributes["timestamp"] = moment().format("DD/MM/YYYY HH:mm");

            this.handler.state = constants.states.SETUP_MODE;

            this.emit('WelcomeIntent');
        },

        'WelcomeIntent': function () {

            this.attributes['requestName'] = true;
            if (locale == "de-DE") {
                var speech = "Willkommen beim Einarmigen Bandit. "
                    + "Jeder Spieler erhält zum anfang 100 Münzen. "
                    + "Für die Hilfe im Spiel sage Hilfe. Für die Regeln sage Regeln. "
                    + "Wenn du in meiner Highscore-Liste auftauchen möchtest, nenne mir einen Namen.";

                var reprompt = "Wenn du nicht namentlich in der Liste auftauchen möchtest, "
                    + "sage Anonym oder nenne jetzt einen Namen.";
            } else { //if(locale == "en-GB" || locale == "en-US")
                var speech = "Welcome to the slot machine. "
                    + "Each player receives 100 coins at the beginning. "
                    + "For help in the game say help. For the rules say rules. "
                    + "If you want to appear in the highscore list, give me a name.";

                var reprompt = "If you do not want to appear in the list by name, say Anonymous.";
            }

            this.emit(':ask', speech, reprompt);
        },

        'WelcomeBackIntent': function () {

            if (locale == "de-DE") {
                var speech = "Hallo " + (this.attributes.userName == "Anonym" ? "Unbekannter" : this.attributes.userName)
                    + (this.attributes.score != 100 ? ". " : ". Du hast noch 100 Münzen. ")
                    + (shouldRecieveReward ? "Willkommen zurück. Du hast einen Bonus von 25 Münzen bekommen. <break time='30ms'/> " : "")
                    + "Dein Einsatz " + (this.attributes.bet > 1 ? "sind " + this.attributes.bet + " Münzen. " : "ist eine Münze. ")
                    + "Wenn du spielen möchtest sage ziehen.";

                var reprompt = "Wenn du spielen möchtest sage ziehen.";
            } else { //if(locale == "en-GB" || locale == "en-US")
                var speech = "Hello " + (this.attributes.userName == "Anonym" ? "" : this.attributes.userName)
                    + (this.attributes.score != 100 ? ". " : ". You have 100 coins left.")
                    + (shouldRecieveReward ? "Welcome back. You got an bonus of 25 coins. <break time='30ms'/> " : "")
                    + "Your bet is " + this.attributes.bet + (this.attributes.bet > 1 ? " coins. " : " coin. ")
                    + "If you want to start say pull.";

                var reprompt = "If you want to start say pull.";
            }

            this.emit(':ask', speech, reprompt);
        },


        'NoFirstName': function () {
            if (locale == "de-DE") {
                var speech = "Willkommen beim Einarmigen Bandit. "
                    + "Du hast dir noch keinen Namen gegeben. Wenn du in meiner Highscore-Liste auftauchen möchtest, nenne mir einen Namen.";
                var reprompt = "Wenn du nicht namentlich in der Liste auftauchen möchtest, "
                    + "sage Anonym oder nenne jetzt einen Namen.";
            }
            else { //if (locale == "en-GB" || locale == "en-US")
                var speech = "Welcome to the slot machine. "
                    + "You have not given yourself a name yet. If you want to appear in the highscore list, give me a name.";
                var reprompt = "If you do not want to appear in the list by name, say Anonymous.";
            }
            this.emit(':ask', speech, reprompt);
        },

        'VerifyFirstName': function () {
            
            if (this.attributes['requestName']) {
                if (blacklist.indexOf(this.event.request.intent.slots.firstName.value.toLowerCase()) == -1) {
                    this.attributes.userName = this.event.request.intent.slots.firstName.value;

                    if (locale == "de-DE") {
                        var speech = "Dein Name lautet nun " + this.attributes.userName + " . Ist das richtig oder falsch?";
                        var reprompt = "Dein Name lautet nun " + this.attributes.userName + " . Ist das richtig oder falsch?";
                    }
                    else { //if (locale == "en-GB" || locale == "en-US")
                        var speech = "Your name is now " + this.attributes.userName + " . Is that right or wrong?";
                        var reprompt = "Your name is now " + this.attributes.userName + " . Is that right or wrong?";
                    }
                }
                else {
                    if (locale == "de-DE") {
                        var speech = "Der genannte Name ist nicht erlaubt, bitte nenne einen anderen.";
                        var reprompt = "Nenne mir einen Namen.";
                    }
                    else { //if (locale == "en-GB" || locale == "en-US")
                        var speech = "The name mentioned is not allowed, please give me another.";
                        var reprompt = "Give me a name.";
                    }
                }
                this.emit(':ask', speech, reprompt);
            } else {
                this.emit('Unhandled');
            }
        },
        'SetFirstName': function () {
            var self = this;
            saveToDynamoDB(self, false, function () {
                self.attributes['requestName'] = false;
                if (locale == "de-DE") {
                    var speech = "Okay " + self.attributes.userName + ", möchtest du jetzt spielen ? Sage Ja oder Nein.";
                    var reprompt = "Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "Okay " + self.attributes.userName + ", do you want to play? Say yes or no.";
                    var reprompt = "Do you want to play? Say yes or no.";
                }
                self.emit(':ask', speech, reprompt);
            });
        },
        'AbortFirstName': function () {
            this.attributes['requestName'] = true;
            if (locale == "de-DE") {
                var speech = "OK, nenne mir einen Namen.";
                var reprompt = "Nenne mir einen Namen.";
            }
            else { //if (locale == "en-GB" || locale == "en-US")
                var speech = "Okay, give me a name.";
                var reprompt = "Give me a name.";
            }
            this.emit(':ask', speech, reprompt);
        },
        'AnonymIntent': function () {
            this.attributes.userName = "Anonym";

            var self = this;
            saveToDynamoDB(self, false, function () {
                self.attributes['requestName'] = false;
                if (locale == "de-DE") {
                    var speech = "OK, ich respektiere deinen Wunsch. Möchtest du jetzt spielen?";
                    var reprompt = "Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "Okay, I respect your wish. Do you want to play?";
                    var reprompt = "Do you want to play? Say yes or no.";
                }
                self.emit(':ask', speech, reprompt);
            });
        },


        'HighScoreIntent': function () {
            var self = this;
            saveToDynamoDB(self, false, function () {
                dynamo.getHighscore(self.event.session, function (data) {
                    console.log("Highscore");

                    if (locale == "de-DE") {
                        var outp = "";
                        for (var i = 0; i < data.item.length; i++) {
                            outp += " Platz " + (i + 1) + " " + data.item[i].userName + "<break time='50ms'/> mit " + data.item[i].score + " Münzen. ";
                        }

                        var speech = outp + " Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                        var reprompt = "Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                    } else { //if (locale == "en-GB" || locale == "en-US")     
                        var outp = "";
                        for (var i = 0; i < data.item.length; i++) {
                            outp += " Number " + (i + 1) + " " + data.item[i].userName + "<break time='50ms'/> with " + data.item[i].score + " coins. ";
                        }

                        var speech = outp + " Do you want to play? Say yes or no.";
                        var reprompt = "Do you want to play? Say yes or no.";
                    }
                    self.emit(':ask', speech, reprompt);
                });
            });
        },

        'EnableEchoButtons': function () {
            this.attributes["echoButtons"] = true;
            this.attributes["echoButtonsSession"] = "initialize";
            this.emit('StartSpinIntent');
        },
        'RequestEchoButtons': function () {
            this.attributes["requestButtons"] = true;

            if (locale == "de-DE") {
                var speech = "Bevor es los geht. Dieser Skill unterstützt jetzt Echo Buttons. Möchtest du mit Echo Buttons spielen? ";
            } else { //if (locale == "en-GB" || locale == "en-US")
                var speech = "Before we start. This skill supports Echo Buttons. Do you want to play with Echo Buttons? ";
            }
            this.emit(':ask', speech, speech);
        },

        'AMAZON.YesIntent': function () {

            if (this.attributes["requestButtons"]) {
                this.attributes["requestButtons"] = false;
                this.attributes["echoButtons"] = true;
                this.attributes["echoButtonsSession"] = "initialize";
            }
            this.emit('StartSpinIntent');
        },
        'AMAZON.NoIntent': function () {

            if (this.attributes["requestButtons"]) {
                this.attributes["requestButtons"] = false;
                this.attributes["echoButtons"] = false;

                if (locale == "de-DE") {
                    var speech = "Echo Buttons wurden deaktiviert. Wenn du sie aktivieren möchtest sage, Echo Buttons aktivieren. Oder ziehen, um ohne sie zu spielen. ";
                } else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "Echo Buttons disabled. If you want to enable them, say enable echo buttons. Or pull to play without buttons. ";
                }
                this.emit(':ask', speech, speech);
            }
            this.emit('SessionEndedRequest');
        },
        
        'StartSpinIntent': function () {

            // Erster Start oder letztes Mal mit Buttons gespielt.
            if (!this.attributes["echoButtonsSession"] && (typeof this.attributes["echoButtons"] == 'undefined' || this.attributes["echoButtons"] == true)) {
                this.emit('RequestEchoButtons');
            }

            if (this.attributes["echoButtonsSession"] == 'initialize') {

                this.response._addDirective({
                    "type": "GameEngine.StartInputHandler",
                    "timeout": 20000, // In milliseconds.
                    "recognizers": {  // Defines which Button Presses your Skill would like to receive as events.
                        "button_down_recognizer": {
                            type: "match",
                            fuzzy: false,
                            anchor: "end",
                            "pattern": [{
                                "action": "down"
                            }]
                        },
                        "button_up_recognizer": {
                            type: "match",
                            fuzzy: false,
                            anchor: "end",
                            "pattern": [{
                                "action": "up"
                            }]
                        }
                    },
                    "events": {  // These events will be sent to your Skill in a request.
                        "button_down_event": {
                            "meets": ["button_down_recognizer"],
                            "reports": "matches",
                            "shouldEndInputHandler": false
                        },
                        "button_up_event": {
                            "meets": ["button_up_recognizer"],
                            "reports": "matches",
                            "shouldEndInputHandler": false
                        },
                        "timeout": {
                            "meets": ["timed out"],
                            "reports": "history",
                            "shouldEndInputHandler": true
                        }
                    }
                });

                this.attributes.inputHandler_originatingRequestId = this.event.request.requestId;
                this.attributes.buttonCount = 0;

                // Build the breathing animation that will play immediately.
                this.response._addDirective(buildButtonIdleAnimationDirective([], breathAnimationRed));

                // Build the 'button down' animation for when the button is pressed.
                this.response._addDirective(buildButtonDownAnimationDirective([]));

                // Build the 'button up' animation for when the button is released.
                this.response._addDirective(buildButtonUpAnimationDirective([]));

                if (locale == "de-DE") {
                    this.response.speak("Drücke deinen Echo Button, um zu ziehen. ").listen("Drücke deinen Echo Button, um zu ziehen. ");
                } else { //if (locale == "en-GB" || locale == "en-US")
                    this.response.speak("Press your echo button, to pull. ").listen("Press your echo button, to pull. ");
                }
                

                /*
                    Deleting `shouldEndSession` will keep the session open, but NOT open
                    the microphone.
                 */
                //delete this.handler.response.response.shouldEndSession;
                this.handler.response.response.shouldEndSession = false;

                this.attributes["echoButtonsSession"] = 'initialized';

                this.emit(':responseReady');
            } else {
                
                var genNumbers = GenerateSlots(3);
                console.log(genNumbers);
                if (this.attributes.score <= 0) {
                    this.attributes.score = 100;

                    if (locale == "de-DE") {
                        if (this.attributes["echoButtonsSession"] == 'initialized') {
                            var speech = "Du bist Pleite. Denn du hast keine Münzen mehr. Hier <break time='100ms'/> die Bank gibt dir noch einmal 100 Münzen. Drücke deinen Button, um weiterzuspielen.";
                            var reprompt = "Drücke deinen Button, um weiterzuspielen.";
                        } else {
                            var speech = "Du bist Pleite. Denn du hast keine Münzen mehr. Hier <break time='100ms'/> die Bank gibt dir noch einmal 100 Münzen. Möchtest du weiterspielen?";
                            var reprompt = "Möchtest du noch einmal Spielen ?";
                        }
                    }
                    else { //if (locale == "en-GB" || locale == "en-US")
                        if (this.attributes["echoButtonsSession"] == 'initialized') {
                            var speech = "You're broke. Because you have no more coins. Here <break time='100ms'/> the bank gives you another 100 coins. Press your button, to continue.";
                            var reprompt = "Press your button, to continue.";
                        } else {
                            var speech = "You're broke. Because you have no more coins. Here <break time='100ms'/> the bank gives you another 100 coins. Do you want to play again?";
                            var reprompt = "Do you want to play again?";
                        }
                    }
                }
                else {
                    if (this.attributes.score < this.attributes.bet)
                        this.attributes.bet = this.attributes.score;

                    this.attributes.score = this.attributes.score - this.attributes.bet;
                    this.attributes.score = this.attributes.score + GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers);

                    this.attributes.jackpotDelta = this.attributes.jackpotDelta + 1; // Jackpot um einen erhöhen

                    if (locale == "de-DE") {
                        var speech = "<audio src='https://alexanews.de/audio/SlotMachine/einwurf.mp3' /><audio src='https://alexanews.de/audio/SlotMachine/starten.mp3' />"
                            + slotItems[genNumbers[0]].name
                            + "<audio src='https://alexanews.de/audio/SlotMachine/walze2.mp3' />"
                            + slotItems[genNumbers[1]].name
                            + "<audio src='https://alexanews.de/audio/SlotMachine/walze2.mp3' />"
                            + slotItems[genNumbers[2]].name
                            + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "de-DE");

                        if (this.attributes["echoButtonsSession"] == 'initialized') {
                            speech += " Drücke deinen Button, um nochmal zu spielen.";
                            var reprompt = "Drücke deinen Button, um nochmal zu spielen.";
                        } else {
                            speech += " Möchtest du noch einmal Spielen ?";
                            var reprompt = "Möchtest du noch einmal Spielen ?";
                        }
                    }
                    else //if (locale == "en-GB" || locale == "en-US")
                    {
                        var speech = "<audio src='https://alexanews.de/audio/SlotMachine/einwurf.mp3' /><audio src='https://alexanews.de/audio/SlotMachine/starten.mp3' />"
                            + slotItems[genNumbers[0]].name
                            + "<audio src='https://alexanews.de/audio/SlotMachine/walze2.mp3' />"
                            + slotItems[genNumbers[1]].name
                            + "<audio src='https://alexanews.de/audio/SlotMachine/walze2.mp3' />"
                            + slotItems[genNumbers[2]].name
                            + BuildResultString(GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers), this.attributes.bet, this.attributes.jackpot, "en-US");
                        
                        if (this.attributes["echoButtonsSession"] == 'initialized') {
                            speech += " Press your button, to play again.";
                            var reprompt = "Press your button, to play again.";
                        } else {
                            speech += " Do you want to play again?";
                            var reprompt = "Do you want to play again?";
                        }
                    }

                    // Reset jackpot
                    if (GetPrize(this.attributes.bet, this.attributes.jackpot, genNumbers) == this.attributes.bet + Math.floor(this.attributes.jackpot / 10)) {

                        console.log(this.attributes.jackpot + " Münzen Jackpot geknackt von " + this.attributes["userName"]);

                        var self = this;
                        saveToDynamoDB(self, true, function () {

                            if (self.attributes["echoButtonsSession"] == 'initialized') {

                                self.response.speak(speech).listen(reprompt);

                                //delete self.handler.response.response.shouldEndSession;
                                self.handler.response.response.shouldEndSession = false;
                                self.emit(':responseReady');
                            } else {
                                self.emit(':ask', speech, reprompt);
                            }
                        });
                    }
                }

                if (this.attributes["echoButtonsSession"] == 'initialized') {

                    this.response.speak(speech).listen(reprompt);

                    //delete this.handler.response.response.shouldEndSession;
                    this.handler.response.response.shouldEndSession = false;
                    this.emit(':responseReady');
                } else {
                    this.emit(':ask', speech, reprompt);
                }
            }
        },
        'GameEngine.InputHandlerEvent': function () {

            let gameEngineEvents = this.event.request.events || [];
            for (let i = 0; i < gameEngineEvents.length; i++) {

                let buttonId;

                /*
                   In this request type, we'll see one or more incoming events that
                   correspond to the StartInputHandler directive we sent above.
                */
                switch (gameEngineEvents[i].name) {
                    case 'button_down_event':

                        // ID of the button that triggered the event.
                        buttonId = gameEngineEvents[i].inputEvents[0].gadgetId;

                        // Recognize a new button.
                        let isNewButton = false;
                        if (this.attributes[buttonId + '_initialized'] === undefined) {
                            isNewButton = true;
                            this.attributes.buttonCount += 1;
                            /*
                               This is a new button, as in new to our understanding.
                               Because this button may have just woken up, it may not have
                               received the initial animations during the launch intent.
                               We'll resend the animations here, but instead of the empty array
                               broadcast above, we'll send the animations ONLY to this buttonId.
                            */
                            this.response._addDirective(buildButtonIdleAnimationDirective([buttonId], breathAnimationRed));
                            this.response._addDirective(buildButtonDownAnimationDirective([buttonId]));
                            this.response._addDirective(buildButtonUpAnimationDirective([buttonId]));

                            this.attributes[buttonId + '_initialized'] = true;
                        }                                               

                        // Again, this means don't end the session, and don't open the microphone.
                        //delete this.handler.response.response.shouldEndSession;
                        this.handler.response.response.shouldEndSession = false;

                        //if (isNewButton) {
                            // Say something when we first encounter a button.
                            //this.response.speak('Hello button ' + this.attributes.buttonCount + ". Good to see you. <audio src='https://s3.amazonaws.com/ask-soundlibrary/foley/amzn_sfx_rhythmic_ticking_30s_01.mp3'/>");
                        //}
                        //this.response.speak('Hello button ' + this.attributes.buttonCount + ". Good to see you. <audio src='https://s3.amazonaws.com/ask-soundlibrary/foley/amzn_sfx_rhythmic_ticking_30s_01.mp3'/>");

                        console.log(JSON.stringify(this.handler.response));
                        // Once more, we finish with this because we directly manipulated the response.
                        //this.emit(':responseReady');
                        this.emit('StartSpinIntent');

                        break;

                    case 'button_up_event':

                        buttonId = gameEngineEvents[i].inputEvents[0].gadgetId;

                        /*
                           On releasing the button, we'll replace the 'none' animation
                           with a new color from a set of animations.
                        */
                        let newAnimationIndex = ((attributes, buttonId, maxLength) => {
                            let index = 1;

                            if (attributes[buttonId] !== undefined) {
                                let newValue = attributes[buttonId] + 1;
                                if (newValue >= maxLength) {
                                    newValue = 0;
                                }
                                index = newValue;
                            }

                            attributes[buttonId] = index;

                            return index;
                        })(this.attributes, buttonId, animations.length);

                        let newAnimation = animations[newAnimationIndex];

                        this.response._addDirective(buildButtonIdleAnimationDirective([buttonId], newAnimation));

                        //delete this.handler.response.response.shouldEndSession;
                        this.handler.response.response.shouldEndSession = false;

                        console.log(JSON.stringify(this.handler.response));

                        if (this.attributes["echoButtonsSession"] == 'initialized') {

                            this.response._addDirective({
                                "type": "GameEngine.StartInputHandler",
                                "timeout": 20000, // In milliseconds.
                                "recognizers": {  // Defines which Button Presses your Skill would like to receive as events.
                                    "button_down_recognizer": {
                                        type: "match",
                                        fuzzy: false,
                                        anchor: "end",
                                        "pattern": [{
                                            "action": "down"
                                        }]
                                    },
                                    "button_up_recognizer": {
                                        type: "match",
                                        fuzzy: false,
                                        anchor: "end",
                                        "pattern": [{
                                            "action": "up"
                                        }]
                                    }
                                },
                                "events": {  // These events will be sent to your Skill in a request.
                                    "button_down_event": {
                                        "meets": ["button_down_recognizer"],
                                        "reports": "matches",
                                        "shouldEndInputHandler": false
                                    },
                                    "button_up_event": {
                                        "meets": ["button_up_recognizer"],
                                        "reports": "matches",
                                        "shouldEndInputHandler": false
                                    },
                                    "timeout": {
                                        "meets": ["timed out"],
                                        "reports": "history",
                                        "shouldEndInputHandler": true
                                    }
                                }
                            });

                            this.attributes.inputHandler_originatingRequestId = this.event.request.requestId;

                            // Build the breathing animation that will play immediately.
                            this.response._addDirective(buildButtonIdleAnimationDirective([], breathAnimationRed));

                            // Build the 'button down' animation for when the button is pressed.
                            this.response._addDirective(buildButtonDownAnimationDirective([]));

                            // Build the 'button up' animation for when the button is released.
                            this.response._addDirective(buildButtonUpAnimationDirective([]));
                        }

                        this.emit(':responseReady');

                        break;

                    case 'timeout': // User hat innerhalb von 20 Sek keinen Button gedrückt.
                        if (this.attributes.buttonCount == 0) {
                            if (locale == "de-DE") {
                                this.response.speak("Es wurden keine Echo Buttons erkannt. Du kannst es beim nächsten Start des Skills nochmal versuchen.");
                            }
                            else { //if (locale == "en-GB" || locale == "en-US")
                                this.response.speak("No echo buttons were detected. You can start the skill to try again.");
                            }
                        } else {
                            this.emit('SessionEndedRequest');
                        }

                        this.handler.response.response.shouldEndSession = true;

                        console.log(JSON.stringify(this.handler.response));

                        this.emit(':responseReady');

                        break;
                }
            }
        },

        'RaiseBetIntent': function () {

            if (this.attributes.bet < 5) {
                if ((this.attributes.score - this.attributes.bet) <= 0) {

                    if (locale == "de-DE") {
                        var speech = "Du kannst deinen Einsatz nicht mehr erhöhen. Möchtest du weiterspielen?";
                    }
                    else { //if (locale == "en-GB" || locale == "en-US")
                        var speech = "You can not increase your bet any further. Do you want to continue playing?";
                    }
                }
                else {
                    this.attributes.bet = this.attributes.bet + 1;

                    if (locale == "de-DE") {
                        var speech = "Dein Einsatz ist jetzt <say-as interpret-as='number'>" + this.attributes.bet + "</say-as>. Möchtest du erhöhen oder weiterspielen?";
                    }
                    else { //if (locale == "en-GB" || locale == "en-US")
                        var speech = "Your bet is now <say-as interpret-as='number'>" + this.attributes.bet + "</say-as>. Do you want to raise or continue playing?";
                    }
                }
            }
            else {
                if (locale == "de-DE") {
                    var speech = "Maximaleinsatz sind 5 Münzen. Möchtest du weiterspielen?";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "Maximum bet is 5 coins. Do you want to continue playing?";
                }
            }

            if (locale == "de-DE") {
                var reprompt = "Möchtest du weiterspielen?";
            }
            else { //if (locale == "en-GB" || locale == "en-US")
                var reprompt = "Do you want to continue playing?";
            }

            if (this.attributes["echoButtonsSession"] == 'initialized') {
                this.attributes["echoButtonsSession"] = 'initialize';
            }

            this.emit(':ask', speech, reprompt);
        },
        'LowerBetIntent': function () {

            if (this.attributes.bet > 1) {
                this.attributes.bet = this.attributes.bet - 1;

                if (locale == "de-DE") {
                    var speech = "Dein Einsatz ist jetzt <say-as interpret-as='number'>" + this.attributes.bet + "</say-as>. Möchtest du verringern oder weiterspielen?";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "Your bet is now <say-as interpret-as='number'>" + this.attributes.bet + "</say-as>. Do you want to lower or continue playing?";
                }
            }
            else {
                if (locale == "de-DE") {
                    var speech = "Mindesteinsatz ist eine Münze. Möchtest du weiterspielen?";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "Minimum bet is one coin. Do you want to continue playing?";
                }
            }

            if (locale == "de-DE") {
                var reprompt = "Möchtest du weiterspielen?";
            }
            else { //if (locale == "en-GB" || locale == "en-US")
                var reprompt = "Do you want to continue playing?";
            }

            if (this.attributes["echoButtonsSession"] == 'initialized') {
                this.attributes["echoButtonsSession"] = 'initialize';
            }

            this.emit(':ask', speech, reprompt);
        },


        'CurrentScoreIntent': function () {
            var self = this;
            saveToDynamoDB(self, false, function () {
                if (locale == "de-DE") {
                    var speech = "Du hast noch " + self.attributes.score + " Münzen.<break time='50ms'/> Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                    var reprompt = "Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "You have " + self.attributes.score + " coins left. <break time='50ms'/> Do you want to play? Say yes or no.";
                    var reprompt = "Do you want to play? Say yes or no.";
                }

                if (self.attributes["echoButtonsSession"] == 'initialized') {
                    self.attributes["echoButtonsSession"] = 'initialize';
                }

                self.emit(':ask', speech, reprompt);
            });
        },
        'CurrentJackpotIntent': function () {
            var self = this;
            saveToDynamoDB(self, false, function () {
                if (locale == "de-DE") {
                    var speech = "Derzeit sind im Jackpot " + Math.floor((self.attributes.jackpot + self.attributes.jackpotDelta) / 10) + " Münzen.<break time='50ms'/> Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                    var reprompt = "Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                }
                else { //if (locale == "en-GB" || locale == "en-US")
                    var speech = "Currently, the jackpot is " + Math.floor((self.attributes.jackpot + self.attributes.jackpotDelta) / 10) + " coins.<break time='50ms'/> Do you want to play? Say yes or no.";
                    var reprompt = "Do you want to play? Say yes or no.";
                }

                if (self.attributes["echoButtonsSession"] == 'initialized') {
                    self.attributes["echoButtonsSession"] = 'initialize';
                }

                self.emit(':ask', speech, reprompt);
            });
        },



        'RoleIntent': function () {

            if (locale == "de-DE") {

                var speech = "Hier sind die Regeln für den Einarmigen Banditen.<break time='50ms'/> Bei Übereinstimmung von zwei Symbolen, bekommst du deinen Einsatz zurück. <break time='50ms'/>Bei drei gleichen Symbolen,"
                    + " wie zum beispiel Kirschen <break time='50ms'/>bekommst du 8 Münzen. Bei drei Pflaumen 16 Münzen <break time='50ms'/> für drei mal Wassermelone 25, drei mal Glocke 50, "
                    + "und bei 3 mal Bar 100 Münzen. <break time='50ms'/> Der größte Gewinn sind drei mal sieben <break time='30ms'/> dann Zahlt dir die Bank den Jackpot aus. <break time='50ms'/> Bei den gewinnen mit drei Übereinstimmungen, "
                    + "erhöht sich dein Gewinn, je Faktor, eins bis maximal fünf. Abhängig von deinem Einsatz. Hier ein Beispiel <break time='50ms'/>. Du hast deinen Einsatz auf vier Münzen erhöht und gewinnst. "
                    + "Bei drei mal Glocke, also 50 Münzen mal vier, das sind dann 200 Münzen. Der Jackpot erhöht sich alle zehn Spiele um eine Münze. Er füllt sich durch das spielen aller Spieler. <break time='50ms'/>"
                    + "Ich wünsche dir viel Spaß und viel Glück, bei meinem Einarmigen Banditen.<break time='50ms'/> Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                var reprompt = "Möchtest du jetzt spielen ? Sage Ja oder Nein.";
            } else { //if (locale == "en-GB" || locale == "en-US")

                var speech = "Here are the rules for the slot machine.<break time='50ms'/> If two symbols match, you will get your bet back. <break time='50ms'/>With three equal symbols,"
                    + " for example three cherries <break time='50ms'/>you get 8 coins. With three plums 16 coins, <break time='50ms'/> for three times watermelon 25, three times bell 50, "
                    + "and at three times bar 100 coins. <break time='50ms'/> The biggest win is three times seven <break time='30ms'/> then the bank pays out the jackpot. <break time='50ms'/> If you win with three matches, "
                    + "your prize will increases by a factor of one to a maximum of five, depending on your bet. Here's an example <break time='50ms'/>. You have increased your bet to four coins and win. "
                    + "With three times bell, you will get 50 coins multiplied by four, so you will earn 200 coins. The jackpot increases all ten games by a coin, it fills itself with the playing of all players. <break time='50ms'/>"
                    + "Now, I wish you good luck and lots of fun.<break time='50ms'/> Do you want to play? Say yes or no.";
                var reprompt = "Do you want to play? Say yes or no.";
            }

            if (this.attributes["echoButtonsSession"] == 'initialized') {
                this.attributes["echoButtonsSession"] = 'initialize';
            }

            this.emit(':ask', speech, reprompt);
        },
        'AMAZON.HelpIntent': function () {

            if (locale == "de-DE") {

                var speech = "Beim Spiel Einarmiger Bandit, hast du drei Walzen. Jede Walze hat sechs verschiedene Symbole. Beim Start bekommst du von der Bank 100 Münzen. Mit dem Befehl ziehen, startest du dein erstes Spiel ."
                    + "Bei Übereinstimmung von zwei Walzen, bekommst du deinen Einsatz zurück. Bei Übereinstimmung von drei Walzen, bekommst du je nach Symbol, deinen Gewinn gut geschrieben. <break time='50ms'/>"
                    + "Du kannst deinen Einsatz erhöhen oder verringern. Dafür sagst du Einsatz erhöhen oder Einsatz verringern. Der Maximal Einsatz pro Spiel beträgt fünf Münzen<break time='30ms'/> und der kleinste Einsatz eine Münze ."
                    + "Beim beenden des Spiels wird dein Punktestand in die Highscoreliste eingetragen. Bei einem Neustart startest du mit deinem alten Guthaben und spielst weiter.<break time='50ms'/>"
                    + "Wenn du mit Echo Buttons spielen möchtest. Sage, Echo Buttons aktivieren. "
                    + "Du kannst diese Hilfe jederzeit mit dem befehl Hilfe<break time='30ms'/> wieder aufrufen. Und jetzt, wünsche ich dir viel Spaß und viel Glück, bei meinem Einarmigen Banditen."
                    + "<break time='50ms'/> Möchtest du jetzt spielen ? Sage Ja oder Nein.";
                var reprompt = "Möchtest du jetzt spielen ? Sage Ja oder Nein.";
            } else { //if (locale == "en-GB" || locale == "en-US")

                var speech = "In slot machine, you have three reels. Each reel has six different symbols. At the start you get 100 coins from the bank. With the command pull to start your first game. "
                    + "If two reels match, you will get bet back. If three reels match, you'll get a prize, depending on the symbol. <break time='50ms'/>"
                    + "You can increase or decrease your bet. You can say raise bet or reduce bet. The maximum bet per game is five coins<break time='30ms'/> and the minimum bet one coin. "
                    + "When you complete a round, your score is entered in the highscore list. When you restart, you continue with your old credit.<break time='50ms'/> "
                    + "If you want to play with echo buttons. Say, enable echo buttons. "
                    + "You can replay the help at any time using the command help. And now, I wish you good luck and lots of fun. "
                    + "<break time='50ms'/>Do you want to play? Say yes or no.";
                var reprompt = "Do you want to play? Say yes or no.";
            }

            if (this.attributes["echoButtonsSession"] == 'initialized') {
                this.attributes["echoButtonsSession"] = 'initialize';
            }

            this.emit(':ask', speech, reprompt);
        },
        'AMAZON.CancelIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'AMAZON.StopIntent': function () {
            this.emit('SessionEndedRequest');
        },
        'SessionEndedRequest': function () {

            this.attributes = updateRatingAttributes(this.attributes, locale);
            var speech = this.attributes["ratingSpeech"];

            var self = this;
            saveToDynamoDB(self, false, function () {

                dynamo.getHighscore(self.event.session, function (data) {
                    console.log("Highscore-End: " + data.message);

                    if (data.item) {
                        if (locale == "de-DE") {

                            if (speech == "") { 
                                speech = "Vielen Dank, das du mit mir gespielt hast. Bis zum nächsten mal. "
                                    + "Du bist nicht in den Top Ten.";
                            }
                            var cardTitle = "Auf wiederhören und vielen Dank für´s Spielen.";
                            var cardText = "Danke, dass du mit mir gespielt hast.\n\nWenn dir dieser Skill gefallen hat, freut sich das Team von \"Nordic Skills\" über eine positive Bewertung von dir!\nDies hilft ihnen zukünftig weitere Skills zu entwickeln. \nUnd so bewertest du einen Skill:\n\n1. Öffne die Alexa App auf deinem Smartphone, Laptop oder PC\n\n2. Gehe in den Bereich „Skills“\n\n3. Tippe auf „Ihre Skills“ in der rechten oberen Ecke\n\n4. Suche nach „Einarmiger Bandit“\n\n5. Scrolle bis nach unten und tippe auf „Schreiben Sie eine Rezension“\n\n6. Unterstütze das Team von \"Nordic Skills\" mit deiner 5-Sterne-Bewertung\n\nDanke!";
                            var imageObj = { smallImageUrl: "https://nordicskills.de/bilder/DerZauberwaldKendell/Danke.jpg", largeImageUrl: "https://nordicskills.de/bilder/DerZauberwaldKendell/Danke.jpg" };
                        } else { //if (locale == "en-GB" || locale == "en-US")

                            if (speech == "") {
                                speech = "Thank you for playing with me. Until next time. "
                                    + "You're not in the top ten.";
                            }
                            var cardTitle = "Thank you for playing with me. Until next time.";
                            var cardText = "Thank you for playing with me. Until next time.";
                            var imageObj = null;
                        }

                        for (var i = 0; i < data.item.length; i++) {
                            if (data.item[i].userId == self.event.session.user.userId) {

                                if (locale == "de-DE") {

                                    if (speech == "") {
                                        speech = "Du bist zurzeit auf Platz <say-as interpret-as='number'>" + (i + 1) + "</say-as>. "
                                            + "Vielen Dank, dass du mit mir gespielt hast. Bis zum nächsten mal. ";
                                    }
                                } else { //if (locale == "en-GB" || locale == "en-US")

                                    if (speech == "") {
                                        speech = "You are currently at " + (i + 1) + " place. "
                                            + "Thank you for playing with me. Until next time.";
                                    }
                                }
                            }
                        }

                        

                        self.emit(':tellWithCard', speech, cardTitle, cardText, imageObj);
                    } else {
                        self.emit('Unhandled');
                    }
                });

            });
        },
        'System.ExceptionEncountered': function () {
            console.log('ExceptionEncountered');
            console.log(this.event.request.error);
            console.log(this.event.request.cause);
        },
        'Unhandled': function () {

            if (locale == "de-DE") {
                var speech = 'Entschuldigung, Ich habe dich leider nicht verstanden.';
            } else { //if (locale == "en-GB" || locale == "en-US")     
                var speech = 'Sorry, I did not understand you.';
            }
            this.emit(':ask', speech, speech);
        }
    }),
    gameModeIntentHandlers: Alexa.CreateStateHandler(constants.states.GAME_MODE, {

    })
};

function updateRatingAttributes(attributes, locale) {

    var returnRating = false;

    if (!attributes["firstTimestamp"])
        attributes["firstTimestamp"] = moment().valueOf();

    attributes["lastTimestamp"] = moment().valueOf();

    if (!attributes["sessionCount"]) {
        attributes["sessionCount"] = 0;
        returnRating = true;
    }

    if (attributes["sessionCount"] >= 3 && moment(attributes["lastTimestamp"]).diff(moment(attributes["firstTimestamp"]), 'days') > 7) {
        attributes["sessionCount"] = 0;
    }

    if (!returnRating && attributes["sessionCount"] < 3 && moment(attributes["lastTimestamp"]).diff(moment(attributes["firstTimestamp"]), 'days') <= 14)
        if (Math.floor(Math.random() * 5) == 0) // 20% Wahrscheinlichkeit innerhalb von 2 Wochen
            returnRating = true;

    if (returnRating) {
        attributes["sessionCount"] = attributes["sessionCount"] + 1;

        if (locale == "de-DE")
            attributes["ratingSpeech"] = "Hat Dir der Einarmiger Bandit gefallen? Dann bewerte ihn doch auf der Amazon Webseite oder über Deine Alexa-App! ";
        else //if (locale == "en-GB" || locale == "en-US")
            attributes["ratingSpeech"] = "Did you like the Slote Machine? Then rate it on the Amazon website or via your Alexa app! ";
    } else {
        attributes["ratingSpeech"] = "";
    }
    return attributes;
}

function saveToDynamoDB(self, newJackpot, cb) {

    dynamo.getJackpot(self.event.session, function (data) {
        console.log("GetJackpot: " + data.message);

        if (newJackpot) {
            self.event.session.attributes['jackpot'] = 1000;
            self.event.session.attributes['jackpotDelta'] = 0;
        } else {
            self.event.session.attributes['jackpot'] = data.item.score + self.event.session.attributes['jackpotDelta'];
            self.event.session.attributes['jackpotDelta'] = 0;
        }
 
        dynamo.setJackpot(self.event.session, function (data) {
            console.log("SetJackpot: " + data.message);

            dynamo.putUserState(self.event.session, function (data) {
                console.log("PutUserState: " + data.message);

                console.log("All done!");
                cb();
            });
        });
    });
}

function BuildResultString(prize, bet, jackpot, locale)
{
    if (locale == "de-DE") {
        if (prize == bet + Math.floor(jackpot / 10)) // Seven-Jackpot
        {
            return "<audio src='https://alexanews.de/audio/SlotMachine/jackpot.mp3' /> <say-as interpret-as='interjection'>Glückwunsch</say-as>, du hast den Jackpot geknackt, du gewinnst " + Math.floor(jackpot / 10) + " Münzen.";
        }
        else if (prize > bet) // Gewonnen (kleiner Gewinn)
        {
            return "<audio src='https://alexanews.de/audio/SlotMachine/muenzen_pot.mp3' /> <say-as interpret-as='interjection'>Glückwunsch</say-as>, du hast " + prize + " Münzen gewonnen.";
        }
        else if (prize == bet) // Einsatz wiederbekommen
        {

            return "<audio src='https://alexanews.de/audio/SlotMachine/muenzen.mp3' /> Du hast deinen Einsatz wiederbekommen.";
        }
        else // Verloren
        {
            return " <break time='200ms'/> <audio src='https://alexanews.de/audio/SlotMachine/verloren.mp3' /> Schade, du hast verloren.";
        }
    }
    else // if (locale == "en-GB" || locale = "en-US") 
    {
        if (prize / bet == slotItems[slotItems.length - 1].multiplier) // Seven-Jackpot
        {
            return "<audio src='https://alexanews.de/audio/SlotMachine/jackpot.mp3' /> <say-as interpret-as='interjection'>Congratulations</say-as>, you hit the Jackpot. You won " + Math.floor(jackpot / 10) + " coins.";
        }
        else if (prize > bet) // Gewonnen (kleiner Gewinn)
        {
            return "<audio src='https://alexanews.de/audio/SlotMachine/muenzen_pot.mp3' /> <say-as interpret-as='interjection'>Congratulations</say-as> you won " + prize + " coins.";
        }
        else if (prize == bet) // Einsatz wiederbekommen
        {

            return "<audio src='https://alexanews.de/audio/SlotMachine/muenzen.mp3' /> You got your bet back.";
        }
        else // Verloren
        {
            return " <break time='200ms'/> <audio src='https://alexanews.de/audio/SlotMachine/verloren.mp3' /> Too bad, you lost.";
        }
    }
}

function GenerateSlots(count) // Random randNumGen,
{
    var randNums = new Array(count).fill(0);

    for (var i = 0; i < randNums.length; i++)
    {
        var randNum = Math.floor(Math.random() * 22);

        var counter = 0;

        for (var j = 0; j < slotItems.length; j++)
        {
            if (randNum >= counter && randNum < counter + slotItems[j].probability) {
                randNums[i] = j;
                break;
            }
            counter += slotItems[j].probability;
        }
    }

    return randNums;
}

function GetPrize(bet, jackpot, genNums)
{
    if (genNums[0] == genNums[1] && genNums[1] == genNums[2]) // Drei Übereinstimmungen
    {
        if (genNums[0] == 5)
            return bet + Math.floor(jackpot / 10);

        for (var i = 0; i < slotItems.length; i++)
        {
            if (genNums[0] == i)
                return bet * slotItems[i].multiplier;
        }
    }
    else if (genNums[0] == genNums[1] || genNums[1] == genNums[2] || genNums[0] == genNums[2]) // Zwei Übereinstimmungen
    {
        return bet;
    }

    return 0; // Keine Übereinstimmung
}


function supportsDisplay() {
    var hasDisplay =
        this.event.context &&
        this.event.context.System &&
        this.event.context.System.device &&
        this.event.context.System.device.supportedInterfaces &&
        this.event.context.System.device.supportedInterfaces.Display

    return hasDisplay;
}

function isSimulator() {
    var isSimulator = !this.event.context; //simulator doesn't send context
    return isSimulator;
}


var slotItems;

var blacklist =
[
    "hitler",
    "nazi",
    "neonazi",
    "hure",
    "porno",
    "pornostar",
    "kinderporno",
    "kindersex",
    "kinderfick",
    "kinderficker",
    "kinderschänder",
    "kindergewalt",
    "vergewaltigung",
    "vergewaltiger",
    "start",
    "jackpot",
    "bitch",
    "whore",
    "fuck",
    "porn",
    "rapist"
    ]

/*
  The following are animation generation functions that work with the
  hexadecimal format that SetLight expects.
*/

const buildBreathAnimation = function (fromRgbHex, toRgbHex, steps, totalDuration) {
    const halfSteps = steps / 2;
    const halfTotalDuration = totalDuration / 2;
    return buildSeqentialAnimation(fromRgbHex, toRgbHex, halfSteps, halfTotalDuration)
        .concat(buildSeqentialAnimation(toRgbHex, fromRgbHex, halfSteps, halfTotalDuration));
}

const buildSeqentialAnimation = function (fromRgbHex, toRgbHex, steps, totalDuration) {
    const fromRgb = parseInt(fromRgbHex, 16);
    let fromRed = fromRgb >> 16;
    let fromGreen = (fromRgb & 0xff00) >> 8;
    let fromBlue = fromRgb & 0xff;

    const toRgb = parseInt(toRgbHex, 16);
    const toRed = toRgb >> 16;
    const toGreen = (toRgb & 0xff00) >> 8;
    const toBlue = toRgb & 0xff;

    const deltaRed = (toRed - fromRed) / steps;
    const deltaGreen = (toGreen - fromGreen) / steps;
    const deltaBlue = (toBlue - fromBlue) / steps;

    const oneStepDuration = Math.floor(totalDuration / steps);

    const result = [];

    for (let i = 0; i < steps; i++) {
        result.push({
            "durationMs": oneStepDuration,
            "color": rgb2h(fromRed, fromGreen, fromBlue),
            "blend": true
        });
        fromRed += deltaRed;
        fromGreen += deltaGreen;
        fromBlue += deltaBlue;
    }

    return result;
}

const rgb2h = function (r, g, b) {
    return '' + n2h(r) + n2h(g) + n2h(b);
}
// Number to hex with leading zeros.
const n2h = function (n) {
    return ('00' + (Math.floor(n)).toString(16)).substr(-2);
}

const breathAnimationRed = buildBreathAnimation('552200', 'ff0000', 30, 1200);
const breathAnimationGreen = buildBreathAnimation('004411', '00ff00', 30, 1200);
const breathAnimationBlue = buildBreathAnimation('003366', '0000ff', 30, 1200);
const animations = [breathAnimationRed, breathAnimationGreen, breathAnimationBlue];

/*
  Build a 'button down' animation directive.
  The animation will overwrite the default 'button down' animation.
*/
const buildButtonDownAnimationDirective = function (targetGadgets) {
    return {
        "type": "GadgetController.SetLight",
        "version": 1,
        "targetGadgets": targetGadgets,
        "parameters": {
            "animations": [{
                "repeat": 1,
                "targetLights": ["1"],
                "sequence": [{
                    "durationMs": 300,
                    "color": "FFFF00",
                    "blend": false
                }]
            }],
            "triggerEvent": "buttonDown",
            "triggerEventTimeMs": 0
        }
    }
};

// Build a 'button up' animation directive.
const buildButtonUpAnimationDirective = function (targetGadgets) {
    return {
        "type": "GadgetController.SetLight",
        "version": 1,
        "targetGadgets": targetGadgets,
        "parameters": {
            "animations": [{
                "repeat": 1,
                "targetLights": ["1"],
                "sequence": [{
                    "durationMs": 300,
                    "color": "00FFFF",
                    "blend": false
                }]
            }],
            "triggerEvent": "buttonUp",
            "triggerEventTimeMs": 0
        }
    }
};

// Build an idle animation directive.
const buildButtonIdleAnimationDirective = function (targetGadgets, animation) {
    return {
        "type": "GadgetController.SetLight",
        "version": 1,
        "targetGadgets": targetGadgets,
        "parameters": {
            "animations": [{
                "repeat": 100,
                "targetLights": ["1"],
                "sequence": animation
            }],
            "triggerEvent": "none",
            "triggerEventTimeMs": 0
        }
    }
};

/*
   Called when closing the Skill if an InputHandler is active.  We can do this
   because we kept the requestId when we started this InputHandler.  It's always
   a good idea to clean up when your Skill's session ends.
*/
const buttonStopInputHandlerDirective = function (inputHandlerOriginatingRequestId) {
    return {
        "type": "GameEngine.StopInputHandler",
        "originatingRequestId": inputHandlerOriginatingRequestId
    }
};