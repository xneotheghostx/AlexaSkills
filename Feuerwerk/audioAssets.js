'use strict';

// Audio Source - https://nordicskills.de/audio/Feuerwerk//
var audioData = [
    {
        'title': 'Brummer',
        'url': 'https://nordicskills.de/audio/Feuerwerk/Brummer.mp3',
        'category': 'Leichte'
    },
    {
        'title': 'Knaller Klein',
        'url': 'https://nordicskills.de/audio/Feuerwerk/Knallerteppich_A.mp3',
        'category': 'Teppiche'
    },
    {
        'title': 'Knaller Mittel',
        'url': 'https://nordicskills.de/audio/Feuerwerk/Knallerteppich_B.mp3',
        'category': 'Teppiche'
    },
    {
        'title': 'Heuler',
        'url': 'https://nordicskills.de/audio/Feuerwerk/Heuler.mp3',
        'category': 'Leichte'
    },
    {
        'title': 'Böller',
        'url': 'https://nordicskills.de/audio/Feuerwerk/Boeller.mp3',
        'category': 'Mittlere'
    },
    {
        'title': 'Rakete mit Knall',
        'url': 'https://nordicskills.de/audio/Feuerwerk/Rakete_mit_Knall.mp3',
        'category': 'Mittlere'
    },
    {
        'title': 'Batterie Klein',
        'url': 'https://nordicskills.de/audio/Feuerwerk/Batterie_A.mp3',
        'category': 'Mittlere'
    },
    {
        'title': 'Batterie Mittel',
        'url': 'https://nordicskills.de/audio/Feuerwerk/Batterie_B.mp3',
        'category': 'Mittlere'
    },
    {
        'title': 'Feuerwerk mit Heulern',
        'url': 'https://nordicskills.de/audio/Feuerwerk/Feuerwerk_mit_Heulern.mp3',
        'category': 'Laute'
    },
    {
        'title': 'Feuerwerk mit Böllern',
        'url': 'https://nordicskills.de/audio/Feuerwerk/Feuerwerk_mit_Boellern.mp3',
        'category': 'Laute'
    },
    {
        'title': 'Kleines Feuerwerk',
        'url': 'https://nordicskills.de/audio/Feuerwerk/Kleinesfeuerwerk.mp3',
        'category': 'Laute'
    },
    {
        'title': 'Grosses Feuerwerk',
        'url': 'https://nordicskills.de/audio/Feuerwerk/Grossesfeuerwerk.mp3',
        'category': 'Laute'
    }
];

module.exports = audioData;
